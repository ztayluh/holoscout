﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_HLGUI
struct MGR_HLGUI_t3333927834;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_HLGUI::.ctor()
extern "C"  void MGR_HLGUI__ctor_m1143505859 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_HLGUI::Awake()
extern "C"  void MGR_HLGUI_Awake_m2690111322 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_HLGUI::readStates()
extern "C"  void MGR_HLGUI_readStates_m3798139293 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_HLGUI::addDBEntry()
extern "C"  void MGR_HLGUI_addDBEntry_m3452731226 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_HLGUI::buttonPressed()
extern "C"  void MGR_HLGUI_buttonPressed_m1867206739 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_HLGUI::OnGUI()
extern "C"  void MGR_HLGUI_OnGUI_m4057632545 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
