﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_HoloText
struct  MGR_HoloText_t1166120216  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TextMesh MGR_HoloText::playlistText
	TextMesh_t1641806576 * ___playlistText_2;
	// MGR_Cloud MGR_HoloText::myCloud
	MGR_Cloud_t726529104 * ___myCloud_3;
	// System.String MGR_HoloText::updateText
	String_t* ___updateText_4;

public:
	inline static int32_t get_offset_of_playlistText_2() { return static_cast<int32_t>(offsetof(MGR_HoloText_t1166120216, ___playlistText_2)); }
	inline TextMesh_t1641806576 * get_playlistText_2() const { return ___playlistText_2; }
	inline TextMesh_t1641806576 ** get_address_of_playlistText_2() { return &___playlistText_2; }
	inline void set_playlistText_2(TextMesh_t1641806576 * value)
	{
		___playlistText_2 = value;
		Il2CppCodeGenWriteBarrier(&___playlistText_2, value);
	}

	inline static int32_t get_offset_of_myCloud_3() { return static_cast<int32_t>(offsetof(MGR_HoloText_t1166120216, ___myCloud_3)); }
	inline MGR_Cloud_t726529104 * get_myCloud_3() const { return ___myCloud_3; }
	inline MGR_Cloud_t726529104 ** get_address_of_myCloud_3() { return &___myCloud_3; }
	inline void set_myCloud_3(MGR_Cloud_t726529104 * value)
	{
		___myCloud_3 = value;
		Il2CppCodeGenWriteBarrier(&___myCloud_3, value);
	}

	inline static int32_t get_offset_of_updateText_4() { return static_cast<int32_t>(offsetof(MGR_HoloText_t1166120216, ___updateText_4)); }
	inline String_t* get_updateText_4() const { return ___updateText_4; }
	inline String_t** get_address_of_updateText_4() { return &___updateText_4; }
	inline void set_updateText_4(String_t* value)
	{
		___updateText_4 = value;
		Il2CppCodeGenWriteBarrier(&___updateText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
