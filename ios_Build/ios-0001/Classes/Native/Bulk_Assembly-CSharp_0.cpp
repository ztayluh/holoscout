﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AssemblyCSharp.GUIUtils
struct GUIUtils_t2318875170;
// MGR_Anim
struct MGR_Anim_t3221826994;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// MGR_Anim/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t2856749856;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// MGR_Cloud/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t1215233438;
// MGR_Cloud/<readCoachDB>c__Iterator1
struct U3CreadCoachDBU3Ec__Iterator1_t3826372699;
// MGR_Cloud/<readHashDB>c__Iterator4
struct U3CreadHashDBU3Ec__Iterator4_t2884699250;
// MGR_Cloud/<readPLDB>c__Iterator3
struct U3CreadPLDBU3Ec__Iterator3_t728369779;
// MGR_Cloud/<readTraineeDB>c__Iterator2
struct U3CreadTraineeDBU3Ec__Iterator2_t81797702;
// MGR_GUI
struct MGR_GUI_t2229806790;
// UnityEngine.Canvas
struct Canvas_t209405766;
// MGR_HLGUI
struct MGR_HLGUI_t3333927834;
// MGR_HoloText
struct MGR_HoloText_t1166120216;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// SetAnimationInAnimator
struct SetAnimationInAnimator_t2420164504;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_GUIUtils2318875170.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_GUIUtils2318875170MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_Entry779635924.h"
#include "AssemblyU2DCSharp_Entry779635924MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_MGR_Anim3221826994.h"
#include "AssemblyU2DCSharp_MGR_Anim3221826994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator02856749856MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator02856749856.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_MGR_Cloud726529104.h"
#include "AssemblyU2DCSharp_MGR_Cloud726529104MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CPlayU3Ec__Iterator01215233438MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CPlayU3Ec__Iterator01215233438.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadCoachDBU3Ec__It3826372699MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadCoachDBU3Ec__It3826372699.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadTraineeDBU3Ec__It81797702MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadTraineeDBU3Ec__It81797702.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadPLDBU3Ec__Iterat728369779MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadPLDBU3Ec__Iterat728369779.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadHashDBU3Ec__Ite2884699250MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadHashDBU3Ec__Ite2884699250.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_GUI2229806790.h"
#include "AssemblyU2DCSharp_MGR_GUI2229806790MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_HLGUI3333927834.h"
#include "AssemblyU2DCSharp_MGR_HLGUI3333927834MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_HoloText1166120216.h"
#include "AssemblyU2DCSharp_MGR_HoloText1166120216MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039MethodDeclarations.h"
#include "AssemblyU2DCSharp_SetAnimationInAnimator2420164504.h"
#include "AssemblyU2DCSharp_SetAnimationInAnimator2420164504MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MGR_Anim>()
#define Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191(__this, method) ((  MGR_Anim_t3221826994 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MGR_Cloud>()
#define GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(__this, method) ((  MGR_Cloud_t726529104 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, method) ((  Canvas_t209405766 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MGR_Anim>()
#define GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355(__this, method) ((  MGR_Anim_t3221826994 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
#define Component_GetComponent_TisTextMesh_t1641806576_m4177416886(__this, method) ((  TextMesh_t1641806576 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MGR_Cloud>()
#define Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677(__this, method) ((  MGR_Cloud_t726529104 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AssemblyCSharp.GUIUtils::.ctor()
extern "C"  void GUIUtils__ctor_m880850310 (GUIUtils_t2318875170 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: Entry
extern "C" void Entry_t779635924_marshal_pinvoke(const Entry_t779635924& unmarshaled, Entry_t779635924_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
extern "C" void Entry_t779635924_marshal_pinvoke_back(const Entry_t779635924_marshaled_pinvoke& marshaled, Entry_t779635924& unmarshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
// Conversion method for clean up from marshalling of: Entry
extern "C" void Entry_t779635924_marshal_pinvoke_cleanup(Entry_t779635924_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Entry
extern "C" void Entry_t779635924_marshal_com(const Entry_t779635924& unmarshaled, Entry_t779635924_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
extern "C" void Entry_t779635924_marshal_com_back(const Entry_t779635924_marshaled_com& marshaled, Entry_t779635924& unmarshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
// Conversion method for clean up from marshalling of: Entry
extern "C" void Entry_t779635924_marshal_com_cleanup(Entry_t779635924_marshaled_com& marshaled)
{
}
// System.Void MGR_Anim::.ctor()
extern "C"  void MGR_Anim__ctor_m915439051 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Anim::Start()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const uint32_t MGR_Anim_Start_m1643617151_MetadataUsageId;
extern "C"  void MGR_Anim_Start_m1643617151 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_Start_m1643617151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Entry_t779635924 * L_0 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_1 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_1, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_0->set_playlists_4(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_3 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_3, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_2->set_currentPlaylist_3(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_4->set_trainees_6(L_5);
		Entry_t779635924 * L_6 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_7 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_7, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_6->set_coaches_5(L_7);
		Entry_t779635924 * L_8 = __this->get_address_of_entry_4();
		List_1_t1440998580 * L_9 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_9, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		L_8->set_hashString_7(L_9);
		GameObject_t1756533147 * L_10 = __this->get_myGO_2();
		NullCheck(L_10);
		Animator_t69676727 * L_11 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_10, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_3(L_11);
		return;
	}
}
// System.Void MGR_Anim::setEntry(Entry,UnityEngine.Animator,System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m555649161_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1722794520;
extern const uint32_t MGR_Anim_setEntry_m3451898502_MetadataUsageId;
extern "C"  void MGR_Anim_setEntry_m3451898502 (MGR_Anim_t3221826994 * __this, Entry_t779635924  ___e0, Animator_t69676727 * ___a1, int32_t ___numOfClips2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_setEntry_m3451898502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Animator_t69676727 * L_0 = ___a1;
		__this->set_myAnimator_3(L_0);
		int32_t L_1 = ___numOfClips2;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1722794520, L_3, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Entry_t779635924 * L_5 = __this->get_address_of_entry_4();
		String_t* L_6 = (&___e0)->get_coach_0();
		L_5->set_coach_0(L_6);
		Entry_t779635924 * L_7 = __this->get_address_of_entry_4();
		String_t* L_8 = (&___e0)->get_trainee_1();
		L_7->set_trainee_1(L_8);
		Entry_t779635924 * L_9 = __this->get_address_of_entry_4();
		int32_t L_10 = ___numOfClips2;
		L_9->set_hSize_11(L_10);
		int32_t L_11 = ___numOfClips2;
		List_1_t1440998580 * L_12 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m555649161(L_12, L_11, /*hidden argument*/List_1__ctor_m555649161_MethodInfo_var);
		__this->set_myhashPL_7(L_12);
		V_0 = 0;
		goto IL_0098;
	}

IL_005f:
	{
		Entry_t779635924 * L_13 = __this->get_address_of_entry_4();
		List_1_t1440998580 * L_14 = L_13->get_hashString_7();
		List_1_t1440998580 * L_15 = (&___e0)->get_hashString_7();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		int32_t L_17 = List_1_get_Item_m1921196075(L_15, L_16, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		NullCheck(L_14);
		List_1_Add_m2828939739(L_14, L_17, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		List_1_t1440998580 * L_18 = __this->get_myhashPL_7();
		List_1_t1440998580 * L_19 = (&___e0)->get_hashString_7();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		int32_t L_21 = List_1_get_Item_m1921196075(L_19, L_20, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		NullCheck(L_18);
		List_1_Add_m2828939739(L_18, L_21, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		int32_t L_22 = V_0;
		V_0 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_23 = V_0;
		int32_t L_24 = ___numOfClips2;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_005f;
		}
	}
	{
		return;
	}
}
// System.Void MGR_Anim::playAnims()
extern "C"  void MGR_Anim_playAnims_m3338700699 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = MGR_Anim_Play_m3675863081(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MGR_Anim::Play()
extern Il2CppClass* U3CPlayU3Ec__Iterator0_t2856749856_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Anim_Play_m3675863081_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Anim_Play_m3675863081 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_Play_m3675863081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPlayU3Ec__Iterator0_t2856749856 * V_0 = NULL;
	{
		U3CPlayU3Ec__Iterator0_t2856749856 * L_0 = (U3CPlayU3Ec__Iterator0_t2856749856 *)il2cpp_codegen_object_new(U3CPlayU3Ec__Iterator0_t2856749856_il2cpp_TypeInfo_var);
		U3CPlayU3Ec__Iterator0__ctor_m1257570099(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayU3Ec__Iterator0_t2856749856 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CPlayU3Ec__Iterator0_t2856749856 * L_2 = V_0;
		return L_2;
	}
}
// System.Void MGR_Anim::UpdateAnim(System.String,System.String)
extern "C"  void MGR_Anim_UpdateAnim_m248568853 (MGR_Anim_t3221826994 * __this, String_t* ___current0, String_t* ___next1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m1257570099 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Anim/<Play>c__Iterator0::MoveNext()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_MoveNext_m4201796769_MetadataUsageId;
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m4201796769 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_MoveNext_m4201796769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0138;
		}
	}
	{
		goto IL_017b;
	}

IL_0021:
	{
		MGR_Anim_t3221826994 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = L_2->get_myAnimator_3();
		NullCheck(L_3);
		AnimatorStateInfo_t2577870592  L_4 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_3, 0, /*hidden argument*/NULL);
		__this->set_U3CcurrInfoU3E__0_0(L_4);
		MGR_Anim_t3221826994 * L_5 = __this->get_U24this_3();
		NullCheck(L_5);
		Entry_t779635924 * L_6 = L_5->get_address_of_entry_4();
		int32_t L_7 = L_6->get_hSize_11();
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->set_U3CiU3E__1_1(0);
		goto IL_0159;
	}

IL_005e:
	{
		MGR_Anim_t3221826994 * L_10 = __this->get_U24this_3();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_currPLItem_5();
		MGR_Anim_t3221826994 * L_12 = __this->get_U24this_3();
		NullCheck(L_12);
		Entry_t779635924 * L_13 = L_12->get_address_of_entry_4();
		int32_t L_14 = L_13->get_hSize_11();
		if ((((int32_t)L_11) >= ((int32_t)L_14)))
		{
			goto IL_008f;
		}
	}
	{
		MGR_Anim_t3221826994 * L_15 = __this->get_U24this_3();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_currPLItem_5();
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_009b;
		}
	}

IL_008f:
	{
		MGR_Anim_t3221826994 * L_17 = __this->get_U24this_3();
		NullCheck(L_17);
		L_17->set_currPLItem_5(0);
	}

IL_009b:
	{
		MGR_Anim_t3221826994 * L_18 = __this->get_U24this_3();
		MGR_Anim_t3221826994 * L_19 = __this->get_U24this_3();
		NullCheck(L_19);
		Entry_t779635924 * L_20 = L_19->get_address_of_entry_4();
		List_1_t1440998580 * L_21 = L_20->get_hashString_7();
		MGR_Anim_t3221826994 * L_22 = __this->get_U24this_3();
		NullCheck(L_22);
		int32_t L_23 = L_22->get_currPLItem_5();
		NullCheck(L_21);
		int32_t L_24 = List_1_get_Item_m1921196075(L_21, L_23, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		NullCheck(L_18);
		L_18->set_currHashID_6(L_24);
		AnimatorStateInfo_t2577870592 * L_25 = __this->get_address_of_U3CcurrInfoU3E__0_0();
		float L_26 = AnimatorStateInfo_get_length_m3151009408(L_25, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__2_2(L_26);
		MGR_Anim_t3221826994 * L_27 = __this->get_U24this_3();
		NullCheck(L_27);
		int32_t L_28 = L_27->get_currHashID_6();
		int32_t L_29 = L_28;
		Il2CppObject * L_30 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_29);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_31 = __this->get_U24this_3();
		NullCheck(L_31);
		Animator_t69676727 * L_32 = L_31->get_myAnimator_3();
		MGR_Anim_t3221826994 * L_33 = __this->get_U24this_3();
		NullCheck(L_33);
		int32_t L_34 = L_33->get_currHashID_6();
		NullCheck(L_32);
		Animator_CrossFade_m1425728227(L_32, L_34, (1.0f), 0, /*hidden argument*/NULL);
		float L_35 = __this->get_U3CtimeU3E__2_2();
		WaitForSeconds_t3839502067 * L_36 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_36, ((float)((float)L_35-(float)(1.0f))), /*hidden argument*/NULL);
		__this->set_U24current_4(L_36);
		bool L_37 = __this->get_U24disposing_5();
		if (L_37)
		{
			goto IL_0133;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0133:
	{
		goto IL_017d;
	}

IL_0138:
	{
		MGR_Anim_t3221826994 * L_38 = __this->get_U24this_3();
		MGR_Anim_t3221826994 * L_39 = L_38;
		NullCheck(L_39);
		int32_t L_40 = L_39->get_currPLItem_5();
		NullCheck(L_39);
		L_39->set_currPLItem_5(((int32_t)((int32_t)L_40+(int32_t)1)));
		int32_t L_41 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)((int32_t)L_41+(int32_t)1)));
	}

IL_0159:
	{
		int32_t L_42 = __this->get_U3CiU3E__1_1();
		MGR_Anim_t3221826994 * L_43 = __this->get_U24this_3();
		NullCheck(L_43);
		Entry_t779635924 * L_44 = L_43->get_address_of_entry_4();
		int32_t L_45 = L_44->get_hSize_11();
		if ((((int32_t)L_42) < ((int32_t)L_45)))
		{
			goto IL_005e;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_017b:
	{
		return (bool)0;
	}

IL_017d:
	{
		return (bool)1;
	}
}
// System.Object MGR_Anim/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m504323601 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object MGR_Anim/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2596265641 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m2829949778 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_Reset_m1939598112_MetadataUsageId;
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m1939598112 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_Reset_m1939598112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud__ctor_m1683574021_MetadataUsageId;
extern "C"  void MGR_Cloud__ctor_m1683574021 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud__ctor_m1683574021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_currClipName_5(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Cloud::Awake()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m28427054_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const uint32_t MGR_Cloud_Awake_m4069120028_MetadataUsageId;
extern "C"  void MGR_Cloud_Awake_m4069120028 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Awake_m4069120028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Anim_t3221826994 * L_0 = Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191(__this, /*hidden argument*/Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191_MethodInfo_var);
		__this->set_anims_7(L_0);
		Dictionary_2_t3943999495 * L_1 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m28427054(L_1, /*hidden argument*/Dictionary_2__ctor_m28427054_MethodInfo_var);
		__this->set_traineeRecords_6(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_3 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_3, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_2->set_playlists_4(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_4->set_currentPlaylist_3(L_5);
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_7 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_7, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_6->set_trainees_6(L_7);
		Entry_t779635924 * L_8 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_9 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_9, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_8->set_coaches_5(L_9);
		Entry_t779635924 * L_10 = __this->get_address_of_myEntry_2();
		List_1_t1440998580 * L_11 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_11, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		L_10->set_hashString_7(L_11);
		return;
	}
}
// System.Void MGR_Cloud::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_Start_m899893525_MetadataUsageId;
extern "C"  void MGR_Cloud_Start_m899893525 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Start_m899893525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		L_0->set_coach_0(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		L_2->set_trainee_1(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		L_4->set_playlistName_2(L_5);
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		L_6->set_cSize_9(0);
		Entry_t779635924 * L_7 = __this->get_address_of_myEntry_2();
		L_7->set_pSize_8(0);
		Entry_t779635924 * L_8 = __this->get_address_of_myEntry_2();
		L_8->set_tSize_10(0);
		Entry_t779635924 * L_9 = __this->get_address_of_myEntry_2();
		L_9->set_hSize_11(0);
		Entry_t779635924 * L_10 = __this->get_address_of_myEntry_2();
		L_10->set_numOfPlaylists_12(0);
		MGR_Cloud_startReadDB_m986115961(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Cloud::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1144830560;
extern const uint32_t MGR_Cloud_Update_m1977723834_MetadataUsageId;
extern "C"  void MGR_Cloud_Update_m1977723834 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Update_m1977723834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1749539436(NULL /*static, unused*/, _stringLiteral1144830560, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		MGR_Anim_t3221826994 * L_1 = __this->get_anims_7();
		NullCheck(L_1);
		MGR_Anim_playAnims_m3338700699(L_1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void MGR_Cloud::setEntry(System.String,System.String,System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m1170681985_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3686231158_MethodInfo_var;
extern const uint32_t MGR_Cloud_setEntry_m1738080337_MetadataUsageId;
extern "C"  void MGR_Cloud_setEntry_m1738080337 (MGR_Cloud_t726529104 * __this, String_t* ___cName0, String_t* ___tName1, String_t* ___pName2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_setEntry_m1738080337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		String_t* L_1 = ___pName2;
		L_0->set_playlistName_2(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = ___cName0;
		L_2->set_coach_0(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		String_t* L_5 = ___tName1;
		L_4->set_trainee_1(L_5);
		Dictionary_2_t3943999495 * L_6 = __this->get_traineeRecords_6();
		String_t* L_7 = ___tName1;
		NullCheck(L_6);
		bool L_8 = Dictionary_2_ContainsKey_m1170681985(L_6, L_7, /*hidden argument*/Dictionary_2_ContainsKey_m1170681985_MethodInfo_var);
		if (L_8)
		{
			goto IL_0055;
		}
	}
	{
		Dictionary_2_t3943999495 * L_9 = __this->get_traineeRecords_6();
		String_t* L_10 = ___tName1;
		String_t* L_11 = ___cName0;
		NullCheck(L_9);
		Dictionary_2_Add_m3686231158(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		Entry_t779635924 * L_12 = __this->get_address_of_myEntry_2();
		Entry_t779635924 * L_13 = L_12;
		int32_t L_14 = L_13->get_cSize_9();
		L_13->set_cSize_9(((int32_t)((int32_t)L_14+(int32_t)1)));
	}

IL_0055:
	{
		return;
	}
}
// System.Void MGR_Cloud::initFB(System.String,System.String,System.String)
extern "C"  void MGR_Cloud_initFB_m761421013 (MGR_Cloud_t726529104 * __this, String_t* ___cName0, String_t* ___tName1, String_t* ___pName2, const MethodInfo* method)
{
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		String_t* L_1 = ___pName2;
		L_0->set_playlistName_2(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = ___cName0;
		L_2->set_coach_0(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		String_t* L_5 = ___tName1;
		L_4->set_trainee_1(L_5);
		return;
	}
}
// System.Collections.IEnumerator MGR_Cloud::Play()
extern Il2CppClass* U3CPlayU3Ec__Iterator0_t1215233438_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_Play_m771278319_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_Play_m771278319 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Play_m771278319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPlayU3Ec__Iterator0_t1215233438 * V_0 = NULL;
	{
		U3CPlayU3Ec__Iterator0_t1215233438 * L_0 = (U3CPlayU3Ec__Iterator0_t1215233438 *)il2cpp_codegen_object_new(U3CPlayU3Ec__Iterator0_t1215233438_il2cpp_TypeInfo_var);
		U3CPlayU3Ec__Iterator0__ctor_m4064257613(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayU3Ec__Iterator0_t1215233438 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CPlayU3Ec__Iterator0_t1215233438 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readCoachDB()
extern Il2CppClass* U3CreadCoachDBU3Ec__Iterator1_t3826372699_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readCoachDB_m3748347533_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readCoachDB_m3748347533 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readCoachDB_m3748347533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadCoachDBU3Ec__Iterator1_t3826372699 * V_0 = NULL;
	{
		U3CreadCoachDBU3Ec__Iterator1_t3826372699 * L_0 = (U3CreadCoachDBU3Ec__Iterator1_t3826372699 *)il2cpp_codegen_object_new(U3CreadCoachDBU3Ec__Iterator1_t3826372699_il2cpp_TypeInfo_var);
		U3CreadCoachDBU3Ec__Iterator1__ctor_m2869939524(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadCoachDBU3Ec__Iterator1_t3826372699 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CreadCoachDBU3Ec__Iterator1_t3826372699 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readTraineeDB(System.String)
extern Il2CppClass* U3CreadTraineeDBU3Ec__Iterator2_t81797702_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readTraineeDB_m1908492693_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readTraineeDB_m1908492693 (MGR_Cloud_t726529104 * __this, String_t* ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readTraineeDB_m1908492693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadTraineeDBU3Ec__Iterator2_t81797702 * V_0 = NULL;
	{
		U3CreadTraineeDBU3Ec__Iterator2_t81797702 * L_0 = (U3CreadTraineeDBU3Ec__Iterator2_t81797702 *)il2cpp_codegen_object_new(U3CreadTraineeDBU3Ec__Iterator2_t81797702_il2cpp_TypeInfo_var);
		U3CreadTraineeDBU3Ec__Iterator2__ctor_m1417225815(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadTraineeDBU3Ec__Iterator2_t81797702 * L_1 = V_0;
		String_t* L_2 = ___c0;
		NullCheck(L_1);
		L_1->set_c_0(L_2);
		U3CreadTraineeDBU3Ec__Iterator2_t81797702 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadTraineeDBU3Ec__Iterator2_t81797702 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readPLDB(System.String)
extern Il2CppClass* U3CreadPLDBU3Ec__Iterator3_t728369779_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readPLDB_m994522313_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readPLDB_m994522313 (MGR_Cloud_t726529104 * __this, String_t* ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readPLDB_m994522313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadPLDBU3Ec__Iterator3_t728369779 * V_0 = NULL;
	{
		U3CreadPLDBU3Ec__Iterator3_t728369779 * L_0 = (U3CreadPLDBU3Ec__Iterator3_t728369779 *)il2cpp_codegen_object_new(U3CreadPLDBU3Ec__Iterator3_t728369779_il2cpp_TypeInfo_var);
		U3CreadPLDBU3Ec__Iterator3__ctor_m3324892502(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadPLDBU3Ec__Iterator3_t728369779 * L_1 = V_0;
		String_t* L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CreadPLDBU3Ec__Iterator3_t728369779 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadPLDBU3Ec__Iterator3_t728369779 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readHashDB(System.String)
extern Il2CppClass* U3CreadHashDBU3Ec__Iterator4_t2884699250_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readHashDB_m3218359951_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readHashDB_m3218359951 (MGR_Cloud_t726529104 * __this, String_t* ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readHashDB_m3218359951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadHashDBU3Ec__Iterator4_t2884699250 * V_0 = NULL;
	{
		U3CreadHashDBU3Ec__Iterator4_t2884699250 * L_0 = (U3CreadHashDBU3Ec__Iterator4_t2884699250 *)il2cpp_codegen_object_new(U3CreadHashDBU3Ec__Iterator4_t2884699250_il2cpp_TypeInfo_var);
		U3CreadHashDBU3Ec__Iterator4__ctor_m3255918799(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadHashDBU3Ec__Iterator4_t2884699250 * L_1 = V_0;
		String_t* L_2 = ___p0;
		NullCheck(L_1);
		L_1->set_p_0(L_2);
		U3CreadHashDBU3Ec__Iterator4_t2884699250 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadHashDBU3Ec__Iterator4_t2884699250 * L_4 = V_0;
		return L_4;
	}
}
// System.Void MGR_Cloud::startReadDB()
extern "C"  void MGR_Cloud_startReadDB_m986115961 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_Cloud::addPL(System.String,System.String,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3314292812;
extern Il2CppCodeGenString* _stringLiteral2032843076;
extern Il2CppCodeGenString* _stringLiteral439627463;
extern Il2CppCodeGenString* _stringLiteral947094105;
extern Il2CppCodeGenString* _stringLiteral545555276;
extern const uint32_t MGR_Cloud_addPL_m3533519414_MetadataUsageId;
extern "C"  void MGR_Cloud_addPL_m3533519414 (MGR_Cloud_t726529104 * __this, String_t* ___c0, String_t* ___t1, String_t* ___pl2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_addPL_m3533519414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	WWW_t2919945039 * V_2 = NULL;
	{
		V_0 = _stringLiteral3314292812;
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2032843076);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2032843076);
		StringU5BU5D_t1642385972* L_1 = L_0;
		String_t* L_2 = ___c0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral439627463);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral439627463);
		StringU5BU5D_t1642385972* L_4 = L_3;
		String_t* L_5 = ___t1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral947094105);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral947094105);
		StringU5BU5D_t1642385972* L_7 = L_6;
		String_t* L_8 = ___pl2;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_8);
		StringU5BU5D_t1642385972* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral545555276);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral545555276);
		StringU5BU5D_t1642385972* L_10 = L_9;
		String_t* L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m626692867(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13 = V_1;
		WWW_t2919945039 * L_14 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_14, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		return;
	}
}
// System.Void MGR_Cloud::addPL()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3287239316;
extern Il2CppCodeGenString* _stringLiteral2032843076;
extern Il2CppCodeGenString* _stringLiteral439627463;
extern Il2CppCodeGenString* _stringLiteral947094105;
extern Il2CppCodeGenString* _stringLiteral545555276;
extern const uint32_t MGR_Cloud_addPL_m817357320_MetadataUsageId;
extern "C"  void MGR_Cloud_addPL_m817357320 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_addPL_m817357320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	WWW_t2919945039 * V_2 = NULL;
	{
		V_0 = _stringLiteral3287239316;
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2032843076);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2032843076);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = L_2->get_coach_0();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral439627463);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral439627463);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		String_t* L_7 = L_6->get_trainee_1();
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral947094105);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral947094105);
		StringU5BU5D_t1642385972* L_9 = L_8;
		Entry_t779635924 * L_10 = __this->get_address_of_myEntry_2();
		String_t* L_11 = L_10->get_playlistName_2();
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_9;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral545555276);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral545555276);
		StringU5BU5D_t1642385972* L_13 = L_12;
		String_t* L_14 = V_0;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m626692867(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_15;
		String_t* L_16 = V_1;
		WWW_t2919945039 * L_17 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_17, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		return;
	}
}
// System.Void MGR_Cloud/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m4064257613 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<Play>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_MoveNext_m260193359_MetadataUsageId;
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m260193359 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_MoveNext_m260193359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0113;
		}
	}
	{
		goto IL_0156;
	}

IL_0021:
	{
		MGR_Cloud_t726529104 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		MGR_Anim_t3221826994 * L_3 = L_2->get_anims_7();
		NullCheck(L_3);
		Animator_t69676727 * L_4 = L_3->get_myAnimator_3();
		NullCheck(L_4);
		AnimatorStateInfo_t2577870592  L_5 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_4, 0, /*hidden argument*/NULL);
		__this->set_U3CcurrInfoU3E__0_0(L_5);
		__this->set_U3CiU3E__1_1(0);
		goto IL_0134;
	}

IL_0049:
	{
		MGR_Cloud_t726529104 * L_6 = __this->get_U24this_3();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_currClip_4();
		MGR_Cloud_t726529104 * L_8 = __this->get_U24this_3();
		NullCheck(L_8);
		Entry_t779635924 * L_9 = L_8->get_address_of_myEntry_2();
		int32_t L_10 = L_9->get_pSize_8();
		if ((((int32_t)L_7) >= ((int32_t)L_10)))
		{
			goto IL_007a;
		}
	}
	{
		MGR_Cloud_t726529104 * L_11 = __this->get_U24this_3();
		NullCheck(L_11);
		int32_t L_12 = L_11->get_currClip_4();
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_0086;
		}
	}

IL_007a:
	{
		MGR_Cloud_t726529104 * L_13 = __this->get_U24this_3();
		NullCheck(L_13);
		L_13->set_currClip_4(0);
	}

IL_0086:
	{
		MGR_Cloud_t726529104 * L_14 = __this->get_U24this_3();
		MGR_Cloud_t726529104 * L_15 = __this->get_U24this_3();
		NullCheck(L_15);
		Entry_t779635924 * L_16 = L_15->get_address_of_myEntry_2();
		List_1_t1398341365 * L_17 = L_16->get_currentPlaylist_3();
		MGR_Cloud_t726529104 * L_18 = __this->get_U24this_3();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_currClip_4();
		NullCheck(L_17);
		String_t* L_20 = List_1_get_Item_m1112119647(L_17, L_19, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_14);
		L_14->set_currClipName_5(L_20);
		AnimatorStateInfo_t2577870592 * L_21 = __this->get_address_of_U3CcurrInfoU3E__0_0();
		float L_22 = AnimatorStateInfo_get_length_m3151009408(L_21, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__2_2(L_22);
		MGR_Cloud_t726529104 * L_23 = __this->get_U24this_3();
		NullCheck(L_23);
		MGR_Anim_t3221826994 * L_24 = L_23->get_anims_7();
		NullCheck(L_24);
		Animator_t69676727 * L_25 = L_24->get_myAnimator_3();
		MGR_Cloud_t726529104 * L_26 = __this->get_U24this_3();
		NullCheck(L_26);
		String_t* L_27 = L_26->get_currClipName_5();
		NullCheck(L_25);
		Animator_CrossFade_m1558283744(L_25, L_27, (1.0f), 0, /*hidden argument*/NULL);
		float L_28 = __this->get_U3CtimeU3E__2_2();
		WaitForSeconds_t3839502067 * L_29 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_29, ((float)((float)L_28-(float)(0.5f))), /*hidden argument*/NULL);
		__this->set_U24current_4(L_29);
		bool L_30 = __this->get_U24disposing_5();
		if (L_30)
		{
			goto IL_010e;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_010e:
	{
		goto IL_0158;
	}

IL_0113:
	{
		MGR_Cloud_t726529104 * L_31 = __this->get_U24this_3();
		MGR_Cloud_t726529104 * L_32 = L_31;
		NullCheck(L_32);
		int32_t L_33 = L_32->get_currClip_4();
		NullCheck(L_32);
		L_32->set_currClip_4(((int32_t)((int32_t)L_33+(int32_t)1)));
		int32_t L_34 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)((int32_t)L_34+(int32_t)1)));
	}

IL_0134:
	{
		int32_t L_35 = __this->get_U3CiU3E__1_1();
		MGR_Cloud_t726529104 * L_36 = __this->get_U24this_3();
		NullCheck(L_36);
		Entry_t779635924 * L_37 = L_36->get_address_of_myEntry_2();
		int32_t L_38 = L_37->get_pSize_8();
		if ((((int32_t)L_35) < ((int32_t)L_38)))
		{
			goto IL_0049;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_0156:
	{
		return (bool)0;
	}

IL_0158:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3924763223 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object MGR_Cloud/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3219668319 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void MGR_Cloud/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m2172260480 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_Cloud/<Play>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_Reset_m2344447862_MetadataUsageId;
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m2344447862 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_Reset_m2344447862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::.ctor()
extern "C"  void U3CreadCoachDBU3Ec__Iterator1__ctor_m2869939524 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readCoachDB>c__Iterator1::MoveNext()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1695502239;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern const uint32_t U3CreadCoachDBU3Ec__Iterator1_MoveNext_m540735720_MetadataUsageId;
extern "C"  bool U3CreadCoachDBU3Ec__Iterator1_MoveNext_m540735720 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadCoachDBU3Ec__Iterator1_MoveNext_m540735720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_0126;
	}

IL_0021:
	{
		int32_t L_2 = 1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1695502239, L_3, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_0(L_4);
		String_t* L_5 = __this->get_U3CurlU3E__0_0();
		WWW_t2919945039 * L_6 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_1(L_6);
		WWW_t2919945039 * L_7 = __this->get_U3Ccu_getU3E__1_1();
		__this->set_U24current_4(L_7);
		bool L_8 = __this->get_U24disposing_5();
		if (L_8)
		{
			goto IL_0063;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0063:
	{
		goto IL_0128;
	}

IL_0068:
	{
		WWW_t2919945039 * L_9 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m3092701216(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0097;
		}
	}
	{
		WWW_t2919945039 * L_11 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_11);
		String_t* L_12 = WWW_get_error_m3092701216(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_12, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		goto IL_011f;
	}

IL_0097:
	{
		WWW_t2919945039 * L_14 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_text_m1558985139(L_14, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_2(L_15);
		String_t* L_16 = __this->get_U3CdataU3E__2_2();
		CharU5BU5D_t1328083999* L_17 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_16);
		StringU5BU5D_t1642385972* L_18 = String_Split_m3326265864(L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		MGR_Cloud_t726529104 * L_19 = __this->get_U24this_3();
		NullCheck(L_19);
		Entry_t779635924 * L_20 = L_19->get_address_of_myEntry_2();
		List_1_t1398341365 * L_21 = L_20->get_coaches_5();
		NullCheck(L_21);
		List_1_Clear_m2928955906(L_21, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_22 = __this->get_U24this_3();
		NullCheck(L_22);
		Entry_t779635924 * L_23 = L_22->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_24 = V_1;
		NullCheck(L_24);
		L_23->set_cSize_9(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_0114;
	}

IL_00f0:
	{
		MGR_Cloud_t726529104 * L_25 = __this->get_U24this_3();
		NullCheck(L_25);
		Entry_t779635924 * L_26 = L_25->get_address_of_myEntry_2();
		List_1_t1398341365 * L_27 = L_26->get_coaches_5();
		StringU5BU5D_t1642385972* L_28 = V_1;
		int32_t L_29 = V_2;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		String_t* L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_27);
		List_1_Add_m4061286785(L_27, L_31, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		StringU5BU5D_t1642385972* L_32 = V_1;
		int32_t L_33 = V_2;
		NullCheck(L_32);
		int32_t L_34 = L_33;
		String_t* L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		int32_t L_36 = V_2;
		V_2 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_0114:
	{
		int32_t L_37 = V_2;
		StringU5BU5D_t1642385972* L_38 = V_1;
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length))))-(int32_t)1)))))
		{
			goto IL_00f0;
		}
	}

IL_011f:
	{
		__this->set_U24PC_6((-1));
	}

IL_0126:
	{
		return (bool)0;
	}

IL_0128:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readCoachDB>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2325030518 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object MGR_Cloud/<readCoachDB>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m998574222 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::Dispose()
extern "C"  void U3CreadCoachDBU3Ec__Iterator1_Dispose_m3427768733 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadCoachDBU3Ec__Iterator1_Reset_m1753351231_MetadataUsageId;
extern "C"  void U3CreadCoachDBU3Ec__Iterator1_Reset_m1753351231 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadCoachDBU3Ec__Iterator1_Reset_m1753351231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator4::.ctor()
extern "C"  void U3CreadHashDBU3Ec__Iterator4__ctor_m3255918799 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readHashDB>c__Iterator4::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m3644677550_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3854425081;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t U3CreadHashDBU3Ec__Iterator4_MoveNext_m2665498253_MetadataUsageId;
extern "C"  bool U3CreadHashDBU3Ec__Iterator4_MoveNext_m2665498253 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHashDBU3Ec__Iterator4_MoveNext_m2665498253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0085;
		}
	}
	{
		goto IL_0147;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral3854425081);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3854425081);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_p_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 4;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_11);
		WWW_t2919945039 * L_12 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_12);
		bool L_13 = __this->get_U24disposing_6();
		if (L_13)
		{
			goto IL_0080;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0080:
	{
		goto IL_0149;
	}

IL_0085:
	{
		WWW_t2919945039 * L_14 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_error_m3092701216(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00bf;
		}
	}
	{
		WWW_t2919945039 * L_16 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m3092701216(L_16, /*hidden argument*/NULL);
		String_t* L_18 = __this->get_U3CurlU3E__0_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral2285603187, L_17, _stringLiteral372029352, L_18, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_00bf:
	{
		WWW_t2919945039 * L_20 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_20);
		String_t* L_21 = WWW_get_text_m1558985139(L_20, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_21);
		String_t* L_22 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_23 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_22);
		StringU5BU5D_t1642385972* L_24 = String_Split_m3326265864(L_22, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		MGR_Cloud_t726529104 * L_25 = __this->get_U24this_4();
		NullCheck(L_25);
		Entry_t779635924 * L_26 = L_25->get_address_of_myEntry_2();
		List_1_t1440998580 * L_27 = L_26->get_hashString_7();
		NullCheck(L_27);
		List_1_Clear_m3644677550(L_27, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		MGR_Cloud_t726529104 * L_28 = __this->get_U24this_4();
		NullCheck(L_28);
		Entry_t779635924 * L_29 = L_28->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_30 = V_1;
		NullCheck(L_30);
		L_29->set_hSize_11((((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))));
		V_2 = 0;
		goto IL_0137;
	}

IL_0116:
	{
		MGR_Cloud_t726529104 * L_31 = __this->get_U24this_4();
		NullCheck(L_31);
		Entry_t779635924 * L_32 = L_31->get_address_of_myEntry_2();
		List_1_t1440998580 * L_33 = L_32->get_hashString_7();
		StringU5BU5D_t1642385972* L_34 = V_1;
		int32_t L_35 = V_2;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_38 = Convert_ToInt32_m2593311249(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_33);
		List_1_Add_m2828939739(L_33, L_38, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		int32_t L_39 = V_2;
		V_2 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0137:
	{
		int32_t L_40 = V_2;
		StringU5BU5D_t1642385972* L_41 = V_1;
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_0116;
		}
	}

IL_0140:
	{
		__this->set_U24PC_7((-1));
	}

IL_0147:
	{
		return (bool)0;
	}

IL_0149:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readHashDB>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m761012313 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object MGR_Cloud/<readHashDB>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3044320145 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator4::Dispose()
extern "C"  void U3CreadHashDBU3Ec__Iterator4_Dispose_m2996611800 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator4::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadHashDBU3Ec__Iterator4_Reset_m3436070926_MetadataUsageId;
extern "C"  void U3CreadHashDBU3Ec__Iterator4_Reset_m3436070926 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHashDBU3Ec__Iterator4_Reset_m3436070926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator3::.ctor()
extern "C"  void U3CreadPLDBU3Ec__Iterator3__ctor_m3324892502 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readPLDB>c__Iterator3::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral859641999;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern Il2CppCodeGenString* _stringLiteral2543474434;
extern const uint32_t U3CreadPLDBU3Ec__Iterator3_MoveNext_m741270346_MetadataUsageId;
extern "C"  bool U3CreadPLDBU3Ec__Iterator3_MoveNext_m741270346 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadPLDBU3Ec__Iterator3_MoveNext_m741270346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0085;
		}
	}
	{
		goto IL_014d;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral859641999);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral859641999);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_t_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 3;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_11);
		WWW_t2919945039 * L_12 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_12);
		bool L_13 = __this->get_U24disposing_6();
		if (L_13)
		{
			goto IL_0080;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0080:
	{
		goto IL_014f;
	}

IL_0085:
	{
		WWW_t2919945039 * L_14 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_error_m3092701216(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00b4;
		}
	}
	{
		WWW_t2919945039 * L_16 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m3092701216(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_17, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		goto IL_0146;
	}

IL_00b4:
	{
		WWW_t2919945039 * L_19 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_19);
		String_t* L_20 = WWW_get_text_m1558985139(L_19, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_20);
		String_t* L_21 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_22 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_21);
		StringU5BU5D_t1642385972* L_23 = String_Split_m3326265864(L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral2543474434, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_24 = __this->get_U24this_4();
		NullCheck(L_24);
		Entry_t779635924 * L_25 = L_24->get_address_of_myEntry_2();
		List_1_t1398341365 * L_26 = L_25->get_currentPlaylist_3();
		NullCheck(L_26);
		List_1_Clear_m2928955906(L_26, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_27 = __this->get_U24this_4();
		NullCheck(L_27);
		Entry_t779635924 * L_28 = L_27->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_29 = V_1;
		NullCheck(L_29);
		L_28->set_pSize_8(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_013b;
	}

IL_0117:
	{
		MGR_Cloud_t726529104 * L_30 = __this->get_U24this_4();
		NullCheck(L_30);
		Entry_t779635924 * L_31 = L_30->get_address_of_myEntry_2();
		List_1_t1398341365 * L_32 = L_31->get_currentPlaylist_3();
		StringU5BU5D_t1642385972* L_33 = V_1;
		int32_t L_34 = V_2;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		String_t* L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_32);
		List_1_Add_m4061286785(L_32, L_36, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		StringU5BU5D_t1642385972* L_37 = V_1;
		int32_t L_38 = V_2;
		NullCheck(L_37);
		int32_t L_39 = L_38;
		String_t* L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_013b:
	{
		int32_t L_42 = V_2;
		StringU5BU5D_t1642385972* L_43 = V_1;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length))))-(int32_t)1)))))
		{
			goto IL_0117;
		}
	}

IL_0146:
	{
		__this->set_U24PC_7((-1));
	}

IL_014d:
	{
		return (bool)0;
	}

IL_014f:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readPLDB>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadPLDBU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m342871002 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object MGR_Cloud/<readPLDB>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadPLDBU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1769159186 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator3::Dispose()
extern "C"  void U3CreadPLDBU3Ec__Iterator3_Dispose_m1736126113 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadPLDBU3Ec__Iterator3_Reset_m2254435207_MetadataUsageId;
extern "C"  void U3CreadPLDBU3Ec__Iterator3_Reset_m2254435207 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadPLDBU3Ec__Iterator3_Reset_m2254435207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::.ctor()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2__ctor_m1417225815 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readTraineeDB>c__Iterator2::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2054597853;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern const uint32_t U3CreadTraineeDBU3Ec__Iterator2_MoveNext_m3049364361_MetadataUsageId;
extern "C"  bool U3CreadTraineeDBU3Ec__Iterator2_MoveNext_m3049364361 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadTraineeDBU3Ec__Iterator2_MoveNext_m3049364361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0085;
		}
	}
	{
		goto IL_0143;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral2054597853);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2054597853);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_c_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 2;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_11);
		WWW_t2919945039 * L_12 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_12);
		bool L_13 = __this->get_U24disposing_6();
		if (L_13)
		{
			goto IL_0080;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0080:
	{
		goto IL_0145;
	}

IL_0085:
	{
		WWW_t2919945039 * L_14 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_error_m3092701216(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00b4;
		}
	}
	{
		WWW_t2919945039 * L_16 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m3092701216(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_17, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		goto IL_013c;
	}

IL_00b4:
	{
		WWW_t2919945039 * L_19 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_19);
		String_t* L_20 = WWW_get_text_m1558985139(L_19, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_20);
		String_t* L_21 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_22 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_21);
		StringU5BU5D_t1642385972* L_23 = String_Split_m3326265864(L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		MGR_Cloud_t726529104 * L_24 = __this->get_U24this_4();
		NullCheck(L_24);
		Entry_t779635924 * L_25 = L_24->get_address_of_myEntry_2();
		List_1_t1398341365 * L_26 = L_25->get_trainees_6();
		NullCheck(L_26);
		List_1_Clear_m2928955906(L_26, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_27 = __this->get_U24this_4();
		NullCheck(L_27);
		Entry_t779635924 * L_28 = L_27->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_29 = V_1;
		NullCheck(L_29);
		L_28->set_tSize_10(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_0131;
	}

IL_010d:
	{
		MGR_Cloud_t726529104 * L_30 = __this->get_U24this_4();
		NullCheck(L_30);
		Entry_t779635924 * L_31 = L_30->get_address_of_myEntry_2();
		List_1_t1398341365 * L_32 = L_31->get_trainees_6();
		StringU5BU5D_t1642385972* L_33 = V_1;
		int32_t L_34 = V_2;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		String_t* L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_32);
		List_1_Add_m4061286785(L_32, L_36, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		StringU5BU5D_t1642385972* L_37 = V_1;
		int32_t L_38 = V_2;
		NullCheck(L_37);
		int32_t L_39 = L_38;
		String_t* L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_0131:
	{
		int32_t L_42 = V_2;
		StringU5BU5D_t1642385972* L_43 = V_1;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length))))-(int32_t)1)))))
		{
			goto IL_010d;
		}
	}

IL_013c:
	{
		__this->set_U24PC_7((-1));
	}

IL_0143:
	{
		return (bool)0;
	}

IL_0145:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2933058785 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1937447801 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::Dispose()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2_Dispose_m1515506888 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadTraineeDBU3Ec__Iterator2_Reset_m3846835722_MetadataUsageId;
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2_Reset_m3846835722 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadTraineeDBU3Ec__Iterator2_Reset_m3846835722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_GUI::.ctor()
extern "C"  void MGR_GUI__ctor_m1251067095 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	{
		__this->set_showmenu_2((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_GUI::Awake()
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2063026683_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const uint32_t MGR_GUI_Awake_m2956493910_MetadataUsageId;
extern "C"  void MGR_GUI_Awake_m2956493910 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_Awake_m2956493910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_myGO_18();
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_20(L_1);
		Dictionary_2_t3986656710 * L_2 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2063026683(L_2, /*hidden argument*/Dictionary_2__ctor_m2063026683_MethodInfo_var);
		__this->set_DictOfAnims_16(L_2);
		MGR_GUI_readStates_m194826505(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_myGO_18();
		NullCheck(L_3);
		MGR_Cloud_t726529104 * L_4 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_3, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_17(L_4);
		Canvas_t209405766 * L_5 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		__this->set_myCanvas_19(L_5);
		return;
	}
}
// System.Void MGR_GUI::readStates()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3588976330_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t MGR_GUI_readStates_m194826505_MetadataUsageId;
extern "C"  void MGR_GUI_readStates_m194826505 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_readStates_m194826505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AnimationClip_t3510324950 * V_1 = NULL;
	AnimationClipU5BU5D_t3936083219* V_2 = NULL;
	int32_t V_3 = 0;
	AnimationClip_t3510324950 * V_4 = NULL;
	AnimationClipU5BU5D_t3936083219* V_5 = NULL;
	int32_t V_6 = 0;
	{
		V_0 = 0;
		Animator_t69676727 * L_0 = __this->get_myAnimator_20();
		NullCheck(L_0);
		RuntimeAnimatorController_t670468573 * L_1 = Animator_get_runtimeAnimatorController_m652575931(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationClipU5BU5D_t3936083219* L_2 = RuntimeAnimatorController_get_animationClips_m3074969690(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0026;
	}

IL_001a:
	{
		AnimationClipU5BU5D_t3936083219* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AnimationClip_t3510324950 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_9 = V_3;
		AnimationClipU5BU5D_t3936083219* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->set_clipNames_15(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_0 = 0;
		Animator_t69676727 * L_12 = __this->get_myAnimator_20();
		NullCheck(L_12);
		RuntimeAnimatorController_t670468573 * L_13 = Animator_get_runtimeAnimatorController_m652575931(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationClipU5BU5D_t3936083219* L_14 = RuntimeAnimatorController_get_animationClips_m3074969690(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		V_6 = 0;
		goto IL_00d6;
	}

IL_0057:
	{
		AnimationClipU5BU5D_t3936083219* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		AnimationClip_t3510324950 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		StringU5BU5D_t1642385972* L_19 = __this->get_clipNames_15();
		int32_t L_20 = V_0;
		AnimationClip_t3510324950 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (String_t*)L_23);
		Dictionary_2_t3986656710 * L_24 = __this->get_DictOfAnims_16();
		StringU5BU5D_t1642385972* L_25 = __this->get_clipNames_15();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		bool L_29 = Dictionary_2_ContainsKey_m3588976330(L_24, L_28, /*hidden argument*/Dictionary_2_ContainsKey_m3588976330_MethodInfo_var);
		if (L_29)
		{
			goto IL_00cc;
		}
	}
	{
		AnimationClip_t3510324950 * L_30 = V_4;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_30);
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		StringU5BU5D_t1642385972* L_34 = __this->get_clipNames_15();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2000667605(NULL /*static, unused*/, L_33, _stringLiteral372029310, L_38, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		Dictionary_2_t3986656710 * L_40 = __this->get_DictOfAnims_16();
		StringU5BU5D_t1642385972* L_41 = __this->get_clipNames_15();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		String_t* L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		AnimationClip_t3510324950 * L_45 = V_4;
		NullCheck(L_45);
		int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_45);
		NullCheck(L_40);
		Dictionary_2_Add_m1209957957(L_40, L_44, L_46, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
	}

IL_00cc:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)((int32_t)L_47+(int32_t)1));
		int32_t L_48 = V_6;
		V_6 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_00d6:
	{
		int32_t L_49 = V_6;
		AnimationClipU5BU5D_t3936083219* L_50 = V_5;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_0057;
		}
	}
	{
		return;
	}
}
// System.Void MGR_GUI::addDBEntry()
extern "C"  void MGR_GUI_addDBEntry_m376179630 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	{
		InputField_t1631627530 * L_0 = __this->get_Input_Coach_6();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		__this->set_i_coach_12(L_1);
		InputField_t1631627530 * L_2 = __this->get_Input_Trainee_7();
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m409351770(L_2, /*hidden argument*/NULL);
		__this->set_i_trainee_13(L_3);
		InputField_t1631627530 * L_4 = __this->get_Input_Playlist_8();
		NullCheck(L_4);
		String_t* L_5 = InputField_get_text_m409351770(L_4, /*hidden argument*/NULL);
		__this->set_i_playlist_14(L_5);
		MGR_Cloud_t726529104 * L_6 = __this->get_myCloud_17();
		String_t* L_7 = __this->get_i_coach_12();
		String_t* L_8 = __this->get_i_trainee_13();
		String_t* L_9 = __this->get_i_playlist_14();
		NullCheck(L_6);
		MGR_Cloud_addPL_m3533519414(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_GUI::OnGUI()
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029319;
extern Il2CppCodeGenString* _stringLiteral2077193552;
extern Il2CppCodeGenString* _stringLiteral20327090;
extern const uint32_t MGR_GUI_OnGUI_m858956101_MetadataUsageId;
extern "C"  void MGR_GUI_OnGUI_m858956101 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_OnGUI_m858956101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		bool L_0 = __this->get_showmenu_2();
		if (!L_0)
		{
			goto IL_061f;
		}
	}
	{
		int32_t L_1 = __this->get_count_25();
		if ((((int32_t)L_1) >= ((int32_t)1)))
		{
			goto IL_0037;
		}
	}
	{
		MGR_Cloud_t726529104 * L_2 = __this->get_myCloud_17();
		NullCheck(L_2);
		Il2CppObject * L_3 = MGR_Cloud_readCoachDB_m3748347533(L_2, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_count_25();
		__this->set_count_25(((int32_t)((int32_t)L_4+(int32_t)1)));
	}

IL_0037:
	{
		int32_t L_5 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Rect__ctor_m1220545469(&L_7, (0.0f), (0.0f), (((float)((float)L_5))), (((float)((float)L_6))), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3297699023(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = __this->get_coachScrollPosition_21();
		GUILayoutOptionU5BU5D_t2108882777* L_9 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_10 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_11 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_10/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_11);
		GUILayoutOptionU5BU5D_t2108882777* L_12 = L_9;
		int32_t L_13 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_14 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_13)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_14);
		Vector2_t2243707579  L_15 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_8, L_12, /*hidden argument*/NULL);
		__this->set_coachScrollPosition_21(L_15);
		GUILayoutOptionU5BU5D_t2108882777* L_16 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_17 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_18 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_17/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_18);
		GUILayoutOptionU5BU5D_t2108882777* L_19 = L_16;
		int32_t L_20 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_21 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_20)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_21);
		bool L_22 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_19, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00e2;
		}
	}

IL_00e2:
	{
		V_0 = 0;
		goto IL_0191;
	}

IL_00e9:
	{
		MGR_Cloud_t726529104 * L_23 = __this->get_myCloud_17();
		NullCheck(L_23);
		Entry_t779635924 * L_24 = L_23->get_address_of_myEntry_2();
		List_1_t1398341365 * L_25 = L_24->get_coaches_5();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		String_t* L_27 = List_1_get_Item_m1112119647(L_25, L_26, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_28 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_29 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_30 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_29/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_30);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_30);
		GUILayoutOptionU5BU5D_t2108882777* L_31 = L_28;
		int32_t L_32 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_33 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_32)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_33);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_33);
		bool L_34 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_27, L_31, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_018d;
		}
	}
	{
		__this->set_coachSelected_3((bool)1);
		__this->set_traineeSelected_4((bool)0);
		__this->set_playlistSelected_5((bool)0);
		MGR_Cloud_t726529104 * L_35 = __this->get_myCloud_17();
		NullCheck(L_35);
		Entry_t779635924 * L_36 = L_35->get_address_of_myEntry_2();
		List_1_t1398341365 * L_37 = L_36->get_coaches_5();
		int32_t L_38 = V_0;
		NullCheck(L_37);
		String_t* L_39 = List_1_get_Item_m1112119647(L_37, L_38, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_coach_9(L_39);
		MGR_Cloud_t726529104 * L_40 = __this->get_myCloud_17();
		MGR_Cloud_t726529104 * L_41 = __this->get_myCloud_17();
		NullCheck(L_41);
		Entry_t779635924 * L_42 = L_41->get_address_of_myEntry_2();
		List_1_t1398341365 * L_43 = L_42->get_coaches_5();
		int32_t L_44 = V_0;
		NullCheck(L_43);
		String_t* L_45 = List_1_get_Item_m1112119647(L_43, L_44, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_40);
		Il2CppObject * L_46 = MGR_Cloud_readTraineeDB_m1908492693(L_40, L_45, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_46, /*hidden argument*/NULL);
	}

IL_018d:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0191:
	{
		int32_t L_48 = V_0;
		MGR_Cloud_t726529104 * L_49 = __this->get_myCloud_17();
		NullCheck(L_49);
		Entry_t779635924 * L_50 = L_49->get_address_of_myEntry_2();
		int32_t L_51 = L_50->get_cSize_9();
		if ((((int32_t)L_48) < ((int32_t)L_51)))
		{
			goto IL_00e9;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_52 = __this->get_coachSelected_3();
		if (!L_52)
		{
			goto IL_0324;
		}
	}
	{
		Vector2_t2243707579  L_53 = __this->get_traineeScrollPosition_22();
		GUILayoutOptionU5BU5D_t2108882777* L_54 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_55 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_56 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_55/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, L_56);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_56);
		GUILayoutOptionU5BU5D_t2108882777* L_57 = L_54;
		int32_t L_58 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_59 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_58)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, L_59);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_59);
		Vector2_t2243707579  L_60 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_53, L_57, /*hidden argument*/NULL);
		__this->set_traineeScrollPosition_22(L_60);
		GUILayoutOptionU5BU5D_t2108882777* L_61 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_62 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_63 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_62/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_63);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_63);
		GUILayoutOptionU5BU5D_t2108882777* L_64 = L_61;
		int32_t L_65 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_66 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_65)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_64);
		ArrayElementTypeCheck (L_64, L_66);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_66);
		bool L_67 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_64, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_023c;
		}
	}

IL_023c:
	{
		V_1 = 0;
		goto IL_0309;
	}

IL_0243:
	{
		MGR_Cloud_t726529104 * L_68 = __this->get_myCloud_17();
		NullCheck(L_68);
		Entry_t779635924 * L_69 = L_68->get_address_of_myEntry_2();
		List_1_t1398341365 * L_70 = L_69->get_trainees_6();
		int32_t L_71 = V_1;
		NullCheck(L_70);
		String_t* L_72 = List_1_get_Item_m1112119647(L_70, L_71, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_73 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_74 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_75 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_74/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_75);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_75);
		GUILayoutOptionU5BU5D_t2108882777* L_76 = L_73;
		int32_t L_77 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_78 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_77)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_76);
		ArrayElementTypeCheck (L_76, L_78);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_78);
		bool L_79 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_72, L_76, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_0305;
		}
	}
	{
		__this->set_traineeSelected_4((bool)1);
		__this->set_playlistSelected_5((bool)0);
		MGR_Cloud_t726529104 * L_80 = __this->get_myCloud_17();
		NullCheck(L_80);
		Entry_t779635924 * L_81 = L_80->get_address_of_myEntry_2();
		List_1_t1398341365 * L_82 = L_81->get_trainees_6();
		int32_t L_83 = V_1;
		NullCheck(L_82);
		String_t* L_84 = List_1_get_Item_m1112119647(L_82, L_83, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_trainee_10(L_84);
		MGR_Cloud_t726529104 * L_85 = __this->get_myCloud_17();
		NullCheck(L_85);
		Entry_t779635924 * L_86 = L_85->get_address_of_myEntry_2();
		List_1_t1398341365 * L_87 = L_86->get_trainees_6();
		int32_t L_88 = V_1;
		NullCheck(L_87);
		String_t* L_89 = List_1_get_Item_m1112119647(L_87, L_88, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_90 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2077193552, L_89, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_91 = __this->get_myCloud_17();
		MGR_Cloud_t726529104 * L_92 = __this->get_myCloud_17();
		NullCheck(L_92);
		Entry_t779635924 * L_93 = L_92->get_address_of_myEntry_2();
		List_1_t1398341365 * L_94 = L_93->get_trainees_6();
		int32_t L_95 = V_1;
		NullCheck(L_94);
		String_t* L_96 = List_1_get_Item_m1112119647(L_94, L_95, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_91);
		Il2CppObject * L_97 = MGR_Cloud_readPLDB_m994522313(L_91, L_96, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_97, /*hidden argument*/NULL);
	}

IL_0305:
	{
		int32_t L_98 = V_1;
		V_1 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_0309:
	{
		int32_t L_99 = V_1;
		MGR_Cloud_t726529104 * L_100 = __this->get_myCloud_17();
		NullCheck(L_100);
		Entry_t779635924 * L_101 = L_100->get_address_of_myEntry_2();
		int32_t L_102 = L_101->get_tSize_10();
		if ((((int32_t)L_99) < ((int32_t)L_102)))
		{
			goto IL_0243;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0324:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_103 = __this->get_traineeSelected_4();
		if (!L_103)
		{
			goto IL_0495;
		}
	}
	{
		Vector2_t2243707579  L_104 = __this->get_plScrollPosition_23();
		GUILayoutOptionU5BU5D_t2108882777* L_105 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_106 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_107 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_106/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_105);
		ArrayElementTypeCheck (L_105, L_107);
		(L_105)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_107);
		GUILayoutOptionU5BU5D_t2108882777* L_108 = L_105;
		int32_t L_109 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_110 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_109)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_108);
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_110);
		Vector2_t2243707579  L_111 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_104, L_108, /*hidden argument*/NULL);
		__this->set_plScrollPosition_23(L_111);
		GUILayoutOptionU5BU5D_t2108882777* L_112 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_113 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_114 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_113/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_112);
		ArrayElementTypeCheck (L_112, L_114);
		(L_112)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_114);
		GUILayoutOptionU5BU5D_t2108882777* L_115 = L_112;
		int32_t L_116 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_117 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_116)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_115);
		ArrayElementTypeCheck (L_115, L_117);
		(L_115)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_117);
		bool L_118 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_115, /*hidden argument*/NULL);
		if (!L_118)
		{
			goto IL_03b4;
		}
	}

IL_03b4:
	{
		V_2 = 0;
		goto IL_047a;
	}

IL_03bb:
	{
		MGR_Cloud_t726529104 * L_119 = __this->get_myCloud_17();
		NullCheck(L_119);
		Entry_t779635924 * L_120 = L_119->get_address_of_myEntry_2();
		List_1_t1398341365 * L_121 = L_120->get_currentPlaylist_3();
		int32_t L_122 = V_2;
		NullCheck(L_121);
		String_t* L_123 = List_1_get_Item_m1112119647(L_121, L_122, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_124 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_125 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_126 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_125/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_124);
		ArrayElementTypeCheck (L_124, L_126);
		(L_124)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_126);
		GUILayoutOptionU5BU5D_t2108882777* L_127 = L_124;
		int32_t L_128 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_129 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_128)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_127);
		ArrayElementTypeCheck (L_127, L_129);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_129);
		bool L_130 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_123, L_127, /*hidden argument*/NULL);
		if (!L_130)
		{
			goto IL_0476;
		}
	}
	{
		__this->set_playlistSelected_5((bool)1);
		MGR_Cloud_t726529104 * L_131 = __this->get_myCloud_17();
		NullCheck(L_131);
		Entry_t779635924 * L_132 = L_131->get_address_of_myEntry_2();
		List_1_t1398341365 * L_133 = L_132->get_currentPlaylist_3();
		int32_t L_134 = V_2;
		NullCheck(L_133);
		String_t* L_135 = List_1_get_Item_m1112119647(L_133, L_134, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_playlist_11(L_135);
		MGR_Cloud_t726529104 * L_136 = __this->get_myCloud_17();
		NullCheck(L_136);
		Entry_t779635924 * L_137 = L_136->get_address_of_myEntry_2();
		List_1_t1398341365 * L_138 = L_137->get_currentPlaylist_3();
		int32_t L_139 = V_2;
		NullCheck(L_138);
		String_t* L_140 = List_1_get_Item_m1112119647(L_138, L_139, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_141 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral20327090, L_140, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_141, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_142 = __this->get_myCloud_17();
		MGR_Cloud_t726529104 * L_143 = __this->get_myCloud_17();
		NullCheck(L_143);
		Entry_t779635924 * L_144 = L_143->get_address_of_myEntry_2();
		List_1_t1398341365 * L_145 = L_144->get_currentPlaylist_3();
		int32_t L_146 = V_2;
		NullCheck(L_145);
		String_t* L_147 = List_1_get_Item_m1112119647(L_145, L_146, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_142);
		Il2CppObject * L_148 = MGR_Cloud_readHashDB_m3218359951(L_142, L_147, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_148, /*hidden argument*/NULL);
	}

IL_0476:
	{
		int32_t L_149 = V_2;
		V_2 = ((int32_t)((int32_t)L_149+(int32_t)1));
	}

IL_047a:
	{
		int32_t L_150 = V_2;
		MGR_Cloud_t726529104 * L_151 = __this->get_myCloud_17();
		NullCheck(L_151);
		Entry_t779635924 * L_152 = L_151->get_address_of_myEntry_2();
		int32_t L_153 = L_152->get_pSize_8();
		if ((((int32_t)L_150) < ((int32_t)L_153)))
		{
			goto IL_03bb;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0495:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_154 = __this->get_playlistSelected_5();
		if (!L_154)
		{
			goto IL_060b;
		}
	}
	{
		Vector2_t2243707579  L_155 = __this->get_hashScrollPosition_24();
		GUILayoutOptionU5BU5D_t2108882777* L_156 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_157 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_158 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_157/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_156);
		ArrayElementTypeCheck (L_156, L_158);
		(L_156)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_158);
		GUILayoutOptionU5BU5D_t2108882777* L_159 = L_156;
		int32_t L_160 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_161 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_160)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_159);
		ArrayElementTypeCheck (L_159, L_161);
		(L_159)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_161);
		Vector2_t2243707579  L_162 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_155, L_159, /*hidden argument*/NULL);
		__this->set_hashScrollPosition_24(L_162);
		GUILayoutOptionU5BU5D_t2108882777* L_163 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_164 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_165 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_164/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_163);
		ArrayElementTypeCheck (L_163, L_165);
		(L_163)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_165);
		GUILayoutOptionU5BU5D_t2108882777* L_166 = L_163;
		int32_t L_167 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_168 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_167)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_166);
		ArrayElementTypeCheck (L_166, L_168);
		(L_166)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_168);
		bool L_169 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_166, /*hidden argument*/NULL);
		if (!L_169)
		{
			goto IL_0525;
		}
	}

IL_0525:
	{
		V_3 = 0;
		goto IL_05f0;
	}

IL_052c:
	{
		MGR_Cloud_t726529104 * L_170 = __this->get_myCloud_17();
		NullCheck(L_170);
		Entry_t779635924 * L_171 = L_170->get_address_of_myEntry_2();
		List_1_t1440998580 * L_172 = L_171->get_hashString_7();
		int32_t L_173 = V_3;
		NullCheck(L_172);
		int32_t L_174 = List_1_get_Item_m1921196075(L_172, L_173, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_4 = L_174;
		String_t* L_175 = Int32_ToString_m2960866144((&V_4), /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_176 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_177 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_178 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_177/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_176);
		ArrayElementTypeCheck (L_176, L_178);
		(L_176)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_178);
		GUILayoutOptionU5BU5D_t2108882777* L_179 = L_176;
		int32_t L_180 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_181 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_180)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_179);
		ArrayElementTypeCheck (L_179, L_181);
		(L_179)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_181);
		bool L_182 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_175, L_179, /*hidden argument*/NULL);
		if (!L_182)
		{
			goto IL_05ec;
		}
	}
	{
		__this->set_playlistSelected_5((bool)1);
		MGR_Cloud_t726529104 * L_183 = __this->get_myCloud_17();
		NullCheck(L_183);
		Entry_t779635924 * L_184 = L_183->get_address_of_myEntry_2();
		List_1_t1440998580 * L_185 = L_184->get_hashString_7();
		int32_t L_186 = V_3;
		NullCheck(L_185);
		int32_t L_187 = List_1_get_Item_m1921196075(L_185, L_186, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_5 = L_187;
		String_t* L_188 = Int32_ToString_m2960866144((&V_5), /*hidden argument*/NULL);
		__this->set_playlist_11(L_188);
		MGR_Cloud_t726529104 * L_189 = __this->get_myCloud_17();
		NullCheck(L_189);
		Entry_t779635924 * L_190 = L_189->get_address_of_myEntry_2();
		List_1_t1440998580 * L_191 = L_190->get_hashString_7();
		int32_t L_192 = V_3;
		NullCheck(L_191);
		int32_t L_193 = List_1_get_Item_m1921196075(L_191, L_192, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_6 = L_193;
		String_t* L_194 = Int32_ToString_m2960866144((&V_6), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_195 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral20327090, L_194, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_195, /*hidden argument*/NULL);
	}

IL_05ec:
	{
		int32_t L_196 = V_3;
		V_3 = ((int32_t)((int32_t)L_196+(int32_t)1));
	}

IL_05f0:
	{
		int32_t L_197 = V_3;
		MGR_Cloud_t726529104 * L_198 = __this->get_myCloud_17();
		NullCheck(L_198);
		Entry_t779635924 * L_199 = L_198->get_address_of_myEntry_2();
		int32_t L_200 = L_199->get_hSize_11();
		if ((((int32_t)L_197) < ((int32_t)L_200)))
		{
			goto IL_052c;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_060b:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2019304577(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1904221074(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_061f:
	{
		return;
	}
}
// System.Void MGR_HLGUI::.ctor()
extern "C"  void MGR_HLGUI__ctor_m1143505859 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::Awake()
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2063026683_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const uint32_t MGR_HLGUI_Awake_m2690111322_MetadataUsageId;
extern "C"  void MGR_HLGUI_Awake_m2690111322 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_Awake_m2690111322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_myGO_15();
		NullCheck(L_0);
		MGR_Anim_t3221826994 * L_1 = GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355(L_0, /*hidden argument*/GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355_MethodInfo_var);
		__this->set_myAnim_14(L_1);
		GameObject_t1756533147 * L_2 = __this->get_myGO_15();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_17(L_3);
		Dictionary_2_t3986656710 * L_4 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2063026683(L_4, /*hidden argument*/Dictionary_2__ctor_m2063026683_MethodInfo_var);
		__this->set_DictOfAnims_12(L_4);
		GameObject_t1756533147 * L_5 = __this->get_myGO_15();
		NullCheck(L_5);
		MGR_Cloud_t726529104 * L_6 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_5, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_13(L_6);
		Canvas_t209405766 * L_7 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		__this->set_myCanvas_16(L_7);
		return;
	}
}
// System.Void MGR_HLGUI::readStates()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3588976330_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t MGR_HLGUI_readStates_m3798139293_MetadataUsageId;
extern "C"  void MGR_HLGUI_readStates_m3798139293 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_readStates_m3798139293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AnimationClip_t3510324950 * V_1 = NULL;
	AnimationClipU5BU5D_t3936083219* V_2 = NULL;
	int32_t V_3 = 0;
	AnimationClip_t3510324950 * V_4 = NULL;
	AnimationClipU5BU5D_t3936083219* V_5 = NULL;
	int32_t V_6 = 0;
	{
		V_0 = 0;
		Animator_t69676727 * L_0 = __this->get_myAnimator_17();
		NullCheck(L_0);
		RuntimeAnimatorController_t670468573 * L_1 = Animator_get_runtimeAnimatorController_m652575931(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationClipU5BU5D_t3936083219* L_2 = RuntimeAnimatorController_get_animationClips_m3074969690(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0026;
	}

IL_001a:
	{
		AnimationClipU5BU5D_t3936083219* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AnimationClip_t3510324950 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_9 = V_3;
		AnimationClipU5BU5D_t3936083219* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->set_clipNames_11(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_0 = 0;
		Animator_t69676727 * L_12 = __this->get_myAnimator_17();
		NullCheck(L_12);
		RuntimeAnimatorController_t670468573 * L_13 = Animator_get_runtimeAnimatorController_m652575931(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationClipU5BU5D_t3936083219* L_14 = RuntimeAnimatorController_get_animationClips_m3074969690(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		V_6 = 0;
		goto IL_00d6;
	}

IL_0057:
	{
		AnimationClipU5BU5D_t3936083219* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		AnimationClip_t3510324950 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		StringU5BU5D_t1642385972* L_19 = __this->get_clipNames_11();
		int32_t L_20 = V_0;
		AnimationClip_t3510324950 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (String_t*)L_23);
		Dictionary_2_t3986656710 * L_24 = __this->get_DictOfAnims_12();
		StringU5BU5D_t1642385972* L_25 = __this->get_clipNames_11();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		bool L_29 = Dictionary_2_ContainsKey_m3588976330(L_24, L_28, /*hidden argument*/Dictionary_2_ContainsKey_m3588976330_MethodInfo_var);
		if (L_29)
		{
			goto IL_00cc;
		}
	}
	{
		AnimationClip_t3510324950 * L_30 = V_4;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_30);
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		StringU5BU5D_t1642385972* L_34 = __this->get_clipNames_11();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2000667605(NULL /*static, unused*/, L_33, _stringLiteral372029310, L_38, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		Dictionary_2_t3986656710 * L_40 = __this->get_DictOfAnims_12();
		StringU5BU5D_t1642385972* L_41 = __this->get_clipNames_11();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		String_t* L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		AnimationClip_t3510324950 * L_45 = V_4;
		NullCheck(L_45);
		int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_45);
		NullCheck(L_40);
		Dictionary_2_Add_m1209957957(L_40, L_44, L_46, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
	}

IL_00cc:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)((int32_t)L_47+(int32_t)1));
		int32_t L_48 = V_6;
		V_6 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_00d6:
	{
		int32_t L_49 = V_6;
		AnimationClipU5BU5D_t3936083219* L_50 = V_5;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_0057;
		}
	}
	{
		return;
	}
}
// System.Void MGR_HLGUI::addDBEntry()
extern "C"  void MGR_HLGUI_addDBEntry_m3452731226 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	{
		InputField_t1631627530 * L_0 = __this->get_Input_Coach_2();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		__this->set_i_coach_8(L_1);
		InputField_t1631627530 * L_2 = __this->get_Input_Trainee_3();
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m409351770(L_2, /*hidden argument*/NULL);
		__this->set_i_trainee_9(L_3);
		InputField_t1631627530 * L_4 = __this->get_Input_Playlist_4();
		NullCheck(L_4);
		String_t* L_5 = InputField_get_text_m409351770(L_4, /*hidden argument*/NULL);
		__this->set_i_playlist_10(L_5);
		MGR_Cloud_t726529104 * L_6 = __this->get_myCloud_13();
		String_t* L_7 = __this->get_i_coach_8();
		String_t* L_8 = __this->get_i_trainee_9();
		String_t* L_9 = __this->get_i_playlist_10();
		NullCheck(L_6);
		MGR_Cloud_addPL_m3533519414(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::buttonPressed()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral232122944;
extern const uint32_t MGR_HLGUI_buttonPressed_m1867206739_MetadataUsageId;
extern "C"  void MGR_HLGUI_buttonPressed_m1867206739 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_buttonPressed_m1867206739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_13();
		NullCheck(L_0);
		Entry_t779635924 * L_1 = L_0->get_address_of_myEntry_2();
		int32_t L_2 = L_1->get_hSize_11();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral232122944, L_4, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_6 = __this->get_myAnim_14();
		NullCheck(L_6);
		MGR_Anim_playAnims_m3338700699(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::OnGUI()
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral192568482;
extern const uint32_t MGR_HLGUI_OnGUI_m4057632545_MetadataUsageId;
extern "C"  void MGR_HLGUI_OnGUI_m4057632545 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_OnGUI_m4057632545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_count_22();
		if ((((int32_t)L_0) >= ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		MGR_Cloud_t726529104 * L_1 = __this->get_myCloud_13();
		NullCheck(L_1);
		Il2CppObject * L_2 = MGR_Cloud_readPLDB_m994522313(L_1, _stringLiteral192568482, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_count_22();
		__this->set_count_22(((int32_t)((int32_t)L_3+(int32_t)1)));
	}

IL_0031:
	{
		V_0 = 0;
		goto IL_0065;
	}

IL_0038:
	{
		MGR_Cloud_t726529104 * L_4 = __this->get_myCloud_13();
		NullCheck(L_4);
		Entry_t779635924 * L_5 = L_4->get_address_of_myEntry_2();
		List_1_t1440998580 * L_6 = L_5->get_hashString_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = List_1_get_Item_m1921196075(L_6, L_7, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_1 = L_8;
		String_t* L_9 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0065:
	{
		int32_t L_11 = V_0;
		MGR_Cloud_t726529104 * L_12 = __this->get_myCloud_13();
		NullCheck(L_12);
		Entry_t779635924 * L_13 = L_12->get_address_of_myEntry_2();
		int32_t L_14 = L_13->get_hSize_11();
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_0038;
		}
	}
	{
		return;
	}
}
// System.Void MGR_HoloText::.ctor()
extern "C"  void MGR_HoloText__ctor_m419231409 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::Start()
extern const MethodInfo* Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1275371535;
extern Il2CppCodeGenString* _stringLiteral2183870267;
extern Il2CppCodeGenString* _stringLiteral1760029775;
extern Il2CppCodeGenString* _stringLiteral919633924;
extern const uint32_t MGR_HoloText_Start_m3634721657_MetadataUsageId;
extern "C"  void MGR_HoloText_Start_m3634721657 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HoloText_Start_m3634721657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t1641806576 * L_0 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		__this->set_playlistText_2(L_0);
		MGR_Cloud_t726529104 * L_1 = Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677(__this, /*hidden argument*/Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677_MethodInfo_var);
		__this->set_myCloud_3(L_1);
		__this->set_updateText_4(_stringLiteral1275371535);
		MGR_Cloud_t726529104 * L_2 = __this->get_myCloud_3();
		NullCheck(L_2);
		MGR_Cloud_initFB_m761421013(L_2, _stringLiteral2183870267, _stringLiteral1760029775, _stringLiteral919633924, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m804483696_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m870713862_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4175023932_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t MGR_HoloText_OnGUI_m1016716883_MetadataUsageId;
extern "C"  void MGR_HoloText_OnGUI_m1016716883 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HoloText_OnGUI_m1016716883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_t933071039  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_3();
		NullCheck(L_0);
		Entry_t779635924 * L_1 = L_0->get_address_of_myEntry_2();
		String_t* L_2 = L_1->get_coach_0();
		__this->set_updateText_4(L_2);
		String_t* L_3 = __this->get_updateText_4();
		MGR_Cloud_t726529104 * L_4 = __this->get_myCloud_3();
		NullCheck(L_4);
		Entry_t779635924 * L_5 = L_4->get_address_of_myEntry_2();
		String_t* L_6 = L_5->get_trainee_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_6, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_8 = __this->get_myCloud_3();
		NullCheck(L_8);
		Entry_t779635924 * L_9 = L_8->get_address_of_myEntry_2();
		String_t* L_10 = L_9->get_playlistName_2();
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, L_3, L_7, L_11, /*hidden argument*/NULL);
		__this->set_updateText_4(L_12);
		MGR_Cloud_t726529104 * L_13 = __this->get_myCloud_3();
		NullCheck(L_13);
		Entry_t779635924 * L_14 = L_13->get_address_of_myEntry_2();
		List_1_t1398341365 * L_15 = L_14->get_currentPlaylist_3();
		NullCheck(L_15);
		Enumerator_t933071039  L_16 = List_1_GetEnumerator_m804483696(L_15, /*hidden argument*/List_1_GetEnumerator_m804483696_MethodInfo_var);
		V_1 = L_16;
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009a;
		}

IL_0076:
		{
			String_t* L_17 = Enumerator_get_Current_m870713862((&V_1), /*hidden argument*/Enumerator_get_Current_m870713862_MethodInfo_var);
			V_0 = L_17;
			String_t* L_18 = __this->get_updateText_4();
			String_t* L_19 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_20 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_19, /*hidden argument*/NULL);
			String_t* L_21 = String_Concat_m2596409543(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
			__this->set_updateText_4(L_21);
		}

IL_009a:
		{
			bool L_22 = Enumerator_MoveNext_m4175023932((&V_1), /*hidden argument*/Enumerator_MoveNext_m4175023932_MethodInfo_var);
			if (L_22)
			{
				goto IL_0076;
			}
		}

IL_00a6:
		{
			IL2CPP_LEAVE(0xB9, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_1), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(171)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xB9, IL_00b9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b9:
	{
		TextMesh_t1641806576 * L_23 = __this->get_playlistText_2();
		String_t* L_24 = __this->get_updateText_4();
		NullCheck(L_23);
		TextMesh_set_text_m3390063817(L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::Update()
extern "C"  void MGR_HoloText_Update_m2569076898 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SetAnimationInAnimator::.ctor()
extern "C"  void SetAnimationInAnimator__ctor_m1105520923 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetAnimationInAnimator::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const uint32_t SetAnimationInAnimator_Start_m747789719_MetadataUsageId;
extern "C"  void SetAnimationInAnimator_Start_m747789719 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetAnimationInAnimator_Start_m747789719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set_animator_2(L_0);
		__this->set_currentAnimNum_3(0);
		return;
	}
}
// System.Void SetAnimationInAnimator::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1144830560;
extern Il2CppCodeGenString* _stringLiteral2626903706;
extern const uint32_t SetAnimationInAnimator_Update_m504862234_MetadataUsageId;
extern "C"  void SetAnimationInAnimator_Update_m504862234 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetAnimationInAnimator_Update_m504862234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1749539436(NULL /*static, unused*/, _stringLiteral1144830560, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0046;
		}
	}
	{
		Animator_t69676727 * L_1 = __this->get_animator_2();
		int32_t L_2 = __this->get_currentAnimNum_3();
		NullCheck(L_1);
		Animator_SetInteger_m528582597(L_1, _stringLiteral2626903706, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_currentAnimNum_3();
		__this->set_currentAnimNum_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = __this->get_currentAnimNum_3();
		if ((((int32_t)L_4) < ((int32_t)5)))
		{
			goto IL_0046;
		}
	}
	{
		__this->set_currentAnimNum_3(0);
	}

IL_0046:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
