﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_HoloText
struct MGR_HoloText_t1166120216;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_HoloText::.ctor()
extern "C"  void MGR_HoloText__ctor_m419231409 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_HoloText::Start()
extern "C"  void MGR_HoloText_Start_m3634721657 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_HoloText::OnGUI()
extern "C"  void MGR_HoloText_OnGUI_m1016716883 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_HoloText::Update()
extern "C"  void MGR_HoloText_Update_m2569076898 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
