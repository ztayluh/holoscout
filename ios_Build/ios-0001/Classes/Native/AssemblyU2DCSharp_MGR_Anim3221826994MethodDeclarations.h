﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Anim
struct MGR_Anim_t3221826994;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entry779635924.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MGR_Anim::.ctor()
extern "C"  void MGR_Anim__ctor_m915439051 (MGR_Anim_t3221826994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Anim::Start()
extern "C"  void MGR_Anim_Start_m1643617151 (MGR_Anim_t3221826994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Anim::setEntry(Entry,UnityEngine.Animator,System.Int32)
extern "C"  void MGR_Anim_setEntry_m3451898502 (MGR_Anim_t3221826994 * __this, Entry_t779635924  ___e0, Animator_t69676727 * ___a1, int32_t ___numOfClips2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Anim::playAnims()
extern "C"  void MGR_Anim_playAnims_m3338700699 (MGR_Anim_t3221826994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MGR_Anim::Play()
extern "C"  Il2CppObject * MGR_Anim_Play_m3675863081 (MGR_Anim_t3221826994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Anim::UpdateAnim(System.String,System.String)
extern "C"  void MGR_Anim_UpdateAnim_m248568853 (MGR_Anim_t3221826994 * __this, String_t* ___current0, String_t* ___next1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
