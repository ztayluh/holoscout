﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud/<readTraineeDB>c__Iterator2
struct U3CreadTraineeDBU3Ec__Iterator2_t81797702;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::.ctor()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2__ctor_m1417225815 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Cloud/<readTraineeDB>c__Iterator2::MoveNext()
extern "C"  bool U3CreadTraineeDBU3Ec__Iterator2_MoveNext_m3049364361 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2933058785 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1937447801 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::Dispose()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2_Dispose_m1515506888 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::Reset()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2_Reset_m3846835722 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
