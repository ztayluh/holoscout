﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud/<readCoachDB>c__Iterator1
struct U3CreadCoachDBU3Ec__Iterator1_t3826372699;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::.ctor()
extern "C"  void U3CreadCoachDBU3Ec__Iterator1__ctor_m2869939524 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Cloud/<readCoachDB>c__Iterator1::MoveNext()
extern "C"  bool U3CreadCoachDBU3Ec__Iterator1_MoveNext_m540735720 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readCoachDB>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2325030518 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readCoachDB>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m998574222 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::Dispose()
extern "C"  void U3CreadCoachDBU3Ec__Iterator1_Dispose_m3427768733 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::Reset()
extern "C"  void U3CreadCoachDBU3Ec__Iterator1_Reset_m1753351231 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
