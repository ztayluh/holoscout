﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetAnimationInAnimator
struct  SetAnimationInAnimator_t2420164504  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator SetAnimationInAnimator::animator
	Animator_t69676727 * ___animator_2;
	// System.Int32 SetAnimationInAnimator::currentAnimNum
	int32_t ___currentAnimNum_3;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(SetAnimationInAnimator_t2420164504, ___animator_2)); }
	inline Animator_t69676727 * get_animator_2() const { return ___animator_2; }
	inline Animator_t69676727 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t69676727 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier(&___animator_2, value);
	}

	inline static int32_t get_offset_of_currentAnimNum_3() { return static_cast<int32_t>(offsetof(SetAnimationInAnimator_t2420164504, ___currentAnimNum_3)); }
	inline int32_t get_currentAnimNum_3() const { return ___currentAnimNum_3; }
	inline int32_t* get_address_of_currentAnimNum_3() { return &___currentAnimNum_3; }
	inline void set_currentAnimNum_3(int32_t value)
	{
		___currentAnimNum_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
