﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud
struct MGR_Cloud_t726529104;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MGR_Cloud::.ctor()
extern "C"  void MGR_Cloud__ctor_m1683574021 (MGR_Cloud_t726529104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud::Awake()
extern "C"  void MGR_Cloud_Awake_m4069120028 (MGR_Cloud_t726529104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud::Start()
extern "C"  void MGR_Cloud_Start_m899893525 (MGR_Cloud_t726529104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud::Update()
extern "C"  void MGR_Cloud_Update_m1977723834 (MGR_Cloud_t726529104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud::setEntry(System.String,System.String,System.String)
extern "C"  void MGR_Cloud_setEntry_m1738080337 (MGR_Cloud_t726529104 * __this, String_t* ___cName0, String_t* ___tName1, String_t* ___pName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud::initFB(System.String,System.String,System.String)
extern "C"  void MGR_Cloud_initFB_m761421013 (MGR_Cloud_t726529104 * __this, String_t* ___cName0, String_t* ___tName1, String_t* ___pName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MGR_Cloud::Play()
extern "C"  Il2CppObject * MGR_Cloud_Play_m771278319 (MGR_Cloud_t726529104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MGR_Cloud::readCoachDB()
extern "C"  Il2CppObject * MGR_Cloud_readCoachDB_m3748347533 (MGR_Cloud_t726529104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MGR_Cloud::readTraineeDB(System.String)
extern "C"  Il2CppObject * MGR_Cloud_readTraineeDB_m1908492693 (MGR_Cloud_t726529104 * __this, String_t* ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MGR_Cloud::readPLDB(System.String)
extern "C"  Il2CppObject * MGR_Cloud_readPLDB_m994522313 (MGR_Cloud_t726529104 * __this, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MGR_Cloud::readHashDB(System.String)
extern "C"  Il2CppObject * MGR_Cloud_readHashDB_m3218359951 (MGR_Cloud_t726529104 * __this, String_t* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud::startReadDB()
extern "C"  void MGR_Cloud_startReadDB_m986115961 (MGR_Cloud_t726529104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud::addPL(System.String,System.String,System.String)
extern "C"  void MGR_Cloud_addPL_m3533519414 (MGR_Cloud_t726529104 * __this, String_t* ___c0, String_t* ___t1, String_t* ___pl2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud::addPL()
extern "C"  void MGR_Cloud_addPL_m817357320 (MGR_Cloud_t726529104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
