﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_GUI
struct  MGR_GUI_t2229806790  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean MGR_GUI::coachSelected
	bool ___coachSelected_2;
	// System.Boolean MGR_GUI::traineeSelected
	bool ___traineeSelected_3;
	// System.Boolean MGR_GUI::playlistSelected
	bool ___playlistSelected_4;
	// UnityEngine.UI.InputField MGR_GUI::Input_Coach
	InputField_t1631627530 * ___Input_Coach_5;
	// UnityEngine.UI.InputField MGR_GUI::Input_Trainee
	InputField_t1631627530 * ___Input_Trainee_6;
	// UnityEngine.UI.InputField MGR_GUI::Input_Playlist
	InputField_t1631627530 * ___Input_Playlist_7;
	// System.String MGR_GUI::coach
	String_t* ___coach_8;
	// System.String MGR_GUI::trainee
	String_t* ___trainee_9;
	// System.String MGR_GUI::playlist
	String_t* ___playlist_10;
	// System.String MGR_GUI::i_coach
	String_t* ___i_coach_11;
	// System.String MGR_GUI::i_trainee
	String_t* ___i_trainee_12;
	// System.String MGR_GUI::i_playlist
	String_t* ___i_playlist_13;
	// System.String[] MGR_GUI::clipNames
	StringU5BU5D_t1642385972* ___clipNames_14;
	// System.String MGR_GUI::hlist
	String_t* ___hlist_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MGR_GUI::DictOfAnims
	Dictionary_2_t3986656710 * ___DictOfAnims_16;
	// MGR_Cloud MGR_GUI::myCloud
	MGR_Cloud_t726529104 * ___myCloud_17;
	// UnityEngine.GameObject MGR_GUI::myGO
	GameObject_t1756533147 * ___myGO_18;
	// UnityEngine.Canvas MGR_GUI::myCanvas
	Canvas_t209405766 * ___myCanvas_19;
	// UnityEngine.Animator MGR_GUI::myAnimator
	Animator_t69676727 * ___myAnimator_20;
	// UnityEngine.Vector2 MGR_GUI::coachScrollPosition
	Vector2_t2243707579  ___coachScrollPosition_21;
	// UnityEngine.Vector2 MGR_GUI::traineeScrollPosition
	Vector2_t2243707579  ___traineeScrollPosition_22;
	// UnityEngine.Vector2 MGR_GUI::plScrollPosition
	Vector2_t2243707579  ___plScrollPosition_23;
	// UnityEngine.Vector2 MGR_GUI::hashScrollPosition
	Vector2_t2243707579  ___hashScrollPosition_24;
	// System.Int32 MGR_GUI::count
	int32_t ___count_25;

public:
	inline static int32_t get_offset_of_coachSelected_2() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___coachSelected_2)); }
	inline bool get_coachSelected_2() const { return ___coachSelected_2; }
	inline bool* get_address_of_coachSelected_2() { return &___coachSelected_2; }
	inline void set_coachSelected_2(bool value)
	{
		___coachSelected_2 = value;
	}

	inline static int32_t get_offset_of_traineeSelected_3() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___traineeSelected_3)); }
	inline bool get_traineeSelected_3() const { return ___traineeSelected_3; }
	inline bool* get_address_of_traineeSelected_3() { return &___traineeSelected_3; }
	inline void set_traineeSelected_3(bool value)
	{
		___traineeSelected_3 = value;
	}

	inline static int32_t get_offset_of_playlistSelected_4() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___playlistSelected_4)); }
	inline bool get_playlistSelected_4() const { return ___playlistSelected_4; }
	inline bool* get_address_of_playlistSelected_4() { return &___playlistSelected_4; }
	inline void set_playlistSelected_4(bool value)
	{
		___playlistSelected_4 = value;
	}

	inline static int32_t get_offset_of_Input_Coach_5() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___Input_Coach_5)); }
	inline InputField_t1631627530 * get_Input_Coach_5() const { return ___Input_Coach_5; }
	inline InputField_t1631627530 ** get_address_of_Input_Coach_5() { return &___Input_Coach_5; }
	inline void set_Input_Coach_5(InputField_t1631627530 * value)
	{
		___Input_Coach_5 = value;
		Il2CppCodeGenWriteBarrier(&___Input_Coach_5, value);
	}

	inline static int32_t get_offset_of_Input_Trainee_6() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___Input_Trainee_6)); }
	inline InputField_t1631627530 * get_Input_Trainee_6() const { return ___Input_Trainee_6; }
	inline InputField_t1631627530 ** get_address_of_Input_Trainee_6() { return &___Input_Trainee_6; }
	inline void set_Input_Trainee_6(InputField_t1631627530 * value)
	{
		___Input_Trainee_6 = value;
		Il2CppCodeGenWriteBarrier(&___Input_Trainee_6, value);
	}

	inline static int32_t get_offset_of_Input_Playlist_7() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___Input_Playlist_7)); }
	inline InputField_t1631627530 * get_Input_Playlist_7() const { return ___Input_Playlist_7; }
	inline InputField_t1631627530 ** get_address_of_Input_Playlist_7() { return &___Input_Playlist_7; }
	inline void set_Input_Playlist_7(InputField_t1631627530 * value)
	{
		___Input_Playlist_7 = value;
		Il2CppCodeGenWriteBarrier(&___Input_Playlist_7, value);
	}

	inline static int32_t get_offset_of_coach_8() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___coach_8)); }
	inline String_t* get_coach_8() const { return ___coach_8; }
	inline String_t** get_address_of_coach_8() { return &___coach_8; }
	inline void set_coach_8(String_t* value)
	{
		___coach_8 = value;
		Il2CppCodeGenWriteBarrier(&___coach_8, value);
	}

	inline static int32_t get_offset_of_trainee_9() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___trainee_9)); }
	inline String_t* get_trainee_9() const { return ___trainee_9; }
	inline String_t** get_address_of_trainee_9() { return &___trainee_9; }
	inline void set_trainee_9(String_t* value)
	{
		___trainee_9 = value;
		Il2CppCodeGenWriteBarrier(&___trainee_9, value);
	}

	inline static int32_t get_offset_of_playlist_10() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___playlist_10)); }
	inline String_t* get_playlist_10() const { return ___playlist_10; }
	inline String_t** get_address_of_playlist_10() { return &___playlist_10; }
	inline void set_playlist_10(String_t* value)
	{
		___playlist_10 = value;
		Il2CppCodeGenWriteBarrier(&___playlist_10, value);
	}

	inline static int32_t get_offset_of_i_coach_11() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___i_coach_11)); }
	inline String_t* get_i_coach_11() const { return ___i_coach_11; }
	inline String_t** get_address_of_i_coach_11() { return &___i_coach_11; }
	inline void set_i_coach_11(String_t* value)
	{
		___i_coach_11 = value;
		Il2CppCodeGenWriteBarrier(&___i_coach_11, value);
	}

	inline static int32_t get_offset_of_i_trainee_12() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___i_trainee_12)); }
	inline String_t* get_i_trainee_12() const { return ___i_trainee_12; }
	inline String_t** get_address_of_i_trainee_12() { return &___i_trainee_12; }
	inline void set_i_trainee_12(String_t* value)
	{
		___i_trainee_12 = value;
		Il2CppCodeGenWriteBarrier(&___i_trainee_12, value);
	}

	inline static int32_t get_offset_of_i_playlist_13() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___i_playlist_13)); }
	inline String_t* get_i_playlist_13() const { return ___i_playlist_13; }
	inline String_t** get_address_of_i_playlist_13() { return &___i_playlist_13; }
	inline void set_i_playlist_13(String_t* value)
	{
		___i_playlist_13 = value;
		Il2CppCodeGenWriteBarrier(&___i_playlist_13, value);
	}

	inline static int32_t get_offset_of_clipNames_14() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___clipNames_14)); }
	inline StringU5BU5D_t1642385972* get_clipNames_14() const { return ___clipNames_14; }
	inline StringU5BU5D_t1642385972** get_address_of_clipNames_14() { return &___clipNames_14; }
	inline void set_clipNames_14(StringU5BU5D_t1642385972* value)
	{
		___clipNames_14 = value;
		Il2CppCodeGenWriteBarrier(&___clipNames_14, value);
	}

	inline static int32_t get_offset_of_hlist_15() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___hlist_15)); }
	inline String_t* get_hlist_15() const { return ___hlist_15; }
	inline String_t** get_address_of_hlist_15() { return &___hlist_15; }
	inline void set_hlist_15(String_t* value)
	{
		___hlist_15 = value;
		Il2CppCodeGenWriteBarrier(&___hlist_15, value);
	}

	inline static int32_t get_offset_of_DictOfAnims_16() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___DictOfAnims_16)); }
	inline Dictionary_2_t3986656710 * get_DictOfAnims_16() const { return ___DictOfAnims_16; }
	inline Dictionary_2_t3986656710 ** get_address_of_DictOfAnims_16() { return &___DictOfAnims_16; }
	inline void set_DictOfAnims_16(Dictionary_2_t3986656710 * value)
	{
		___DictOfAnims_16 = value;
		Il2CppCodeGenWriteBarrier(&___DictOfAnims_16, value);
	}

	inline static int32_t get_offset_of_myCloud_17() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___myCloud_17)); }
	inline MGR_Cloud_t726529104 * get_myCloud_17() const { return ___myCloud_17; }
	inline MGR_Cloud_t726529104 ** get_address_of_myCloud_17() { return &___myCloud_17; }
	inline void set_myCloud_17(MGR_Cloud_t726529104 * value)
	{
		___myCloud_17 = value;
		Il2CppCodeGenWriteBarrier(&___myCloud_17, value);
	}

	inline static int32_t get_offset_of_myGO_18() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___myGO_18)); }
	inline GameObject_t1756533147 * get_myGO_18() const { return ___myGO_18; }
	inline GameObject_t1756533147 ** get_address_of_myGO_18() { return &___myGO_18; }
	inline void set_myGO_18(GameObject_t1756533147 * value)
	{
		___myGO_18 = value;
		Il2CppCodeGenWriteBarrier(&___myGO_18, value);
	}

	inline static int32_t get_offset_of_myCanvas_19() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___myCanvas_19)); }
	inline Canvas_t209405766 * get_myCanvas_19() const { return ___myCanvas_19; }
	inline Canvas_t209405766 ** get_address_of_myCanvas_19() { return &___myCanvas_19; }
	inline void set_myCanvas_19(Canvas_t209405766 * value)
	{
		___myCanvas_19 = value;
		Il2CppCodeGenWriteBarrier(&___myCanvas_19, value);
	}

	inline static int32_t get_offset_of_myAnimator_20() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___myAnimator_20)); }
	inline Animator_t69676727 * get_myAnimator_20() const { return ___myAnimator_20; }
	inline Animator_t69676727 ** get_address_of_myAnimator_20() { return &___myAnimator_20; }
	inline void set_myAnimator_20(Animator_t69676727 * value)
	{
		___myAnimator_20 = value;
		Il2CppCodeGenWriteBarrier(&___myAnimator_20, value);
	}

	inline static int32_t get_offset_of_coachScrollPosition_21() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___coachScrollPosition_21)); }
	inline Vector2_t2243707579  get_coachScrollPosition_21() const { return ___coachScrollPosition_21; }
	inline Vector2_t2243707579 * get_address_of_coachScrollPosition_21() { return &___coachScrollPosition_21; }
	inline void set_coachScrollPosition_21(Vector2_t2243707579  value)
	{
		___coachScrollPosition_21 = value;
	}

	inline static int32_t get_offset_of_traineeScrollPosition_22() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___traineeScrollPosition_22)); }
	inline Vector2_t2243707579  get_traineeScrollPosition_22() const { return ___traineeScrollPosition_22; }
	inline Vector2_t2243707579 * get_address_of_traineeScrollPosition_22() { return &___traineeScrollPosition_22; }
	inline void set_traineeScrollPosition_22(Vector2_t2243707579  value)
	{
		___traineeScrollPosition_22 = value;
	}

	inline static int32_t get_offset_of_plScrollPosition_23() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___plScrollPosition_23)); }
	inline Vector2_t2243707579  get_plScrollPosition_23() const { return ___plScrollPosition_23; }
	inline Vector2_t2243707579 * get_address_of_plScrollPosition_23() { return &___plScrollPosition_23; }
	inline void set_plScrollPosition_23(Vector2_t2243707579  value)
	{
		___plScrollPosition_23 = value;
	}

	inline static int32_t get_offset_of_hashScrollPosition_24() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___hashScrollPosition_24)); }
	inline Vector2_t2243707579  get_hashScrollPosition_24() const { return ___hashScrollPosition_24; }
	inline Vector2_t2243707579 * get_address_of_hashScrollPosition_24() { return &___hashScrollPosition_24; }
	inline void set_hashScrollPosition_24(Vector2_t2243707579  value)
	{
		___hashScrollPosition_24 = value;
	}

	inline static int32_t get_offset_of_count_25() { return static_cast<int32_t>(offsetof(MGR_GUI_t2229806790, ___count_25)); }
	inline int32_t get_count_25() const { return ___count_25; }
	inline int32_t* get_address_of_count_25() { return &___count_25; }
	inline void set_count_25(int32_t value)
	{
		___count_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
