﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_CreateMenu
struct MGR_CreateMenu_t817873244;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_CreateMenu::.ctor()
extern "C"  void MGR_CreateMenu__ctor_m645211395 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::Awake()
extern "C"  void MGR_CreateMenu_Awake_m2535073688 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::Update()
extern "C"  void MGR_CreateMenu_Update_m529887162 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::readStates()
extern "C"  void MGR_CreateMenu_readStates_m3857455173 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::OnGUI()
extern "C"  void MGR_CreateMenu_OnGUI_m420939169 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
