﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartSkrollin
struct StartSkrollin_t1317199858;

#include "codegen/il2cpp-codegen.h"

// System.Void StartSkrollin::.ctor()
extern "C"  void StartSkrollin__ctor_m1758154826 (StartSkrollin_t1317199858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSkrollin::Awake()
extern "C"  void StartSkrollin_Awake_m4163082549 (StartSkrollin_t1317199858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSkrollin::Start()
extern "C"  void StartSkrollin_Start_m2806183434 (StartSkrollin_t1317199858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSkrollin::OnGUI()
extern "C"  void StartSkrollin_OnGUI_m1255672610 (StartSkrollin_t1317199858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSkrollin::CreateWindow(System.Int32)
extern "C"  void StartSkrollin_CreateWindow_m2936996973 (StartSkrollin_t1317199858 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSkrollin::CreateGroupWindow(System.Int32)
extern "C"  void StartSkrollin_CreateGroupWindow_m4150283924 (StartSkrollin_t1317199858 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSkrollin::Update()
extern "C"  void StartSkrollin_Update_m4173007945 (StartSkrollin_t1317199858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartSkrollin::Main()
extern "C"  void StartSkrollin_Main_m4110254671 (StartSkrollin_t1317199858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
