﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t1215233438;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Cloud/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m4064257613 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Cloud/<Play>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m260193359 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3924763223 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3219668319 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m2172260480 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<Play>c__Iterator0::Reset()
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m2344447862 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
