﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SetAnimationInAnimator
struct SetAnimationInAnimator_t2420164504;

#include "codegen/il2cpp-codegen.h"

// System.Void SetAnimationInAnimator::.ctor()
extern "C"  void SetAnimationInAnimator__ctor_m1105520923 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetAnimationInAnimator::Start()
extern "C"  void SetAnimationInAnimator_Start_m747789719 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetAnimationInAnimator::Update()
extern "C"  void SetAnimationInAnimator_Update_m504862234 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
