﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "AssemblyU2DUnityScript_StartMenuLevel2952164235.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartSkrollin
struct  StartSkrollin_t1317199858  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GUISkin StartSkrollin::gSkin
	GUISkin_t1436893342 * ___gSkin_2;
	// UnityEngine.TextAsset StartSkrollin::introTextAsset
	TextAsset_t3973159845 * ___introTextAsset_3;
	// UnityEngine.TextAsset StartSkrollin::babaTextAsset
	TextAsset_t3973159845 * ___babaTextAsset_4;
	// UnityEngine.TextAsset StartSkrollin::karoliTextAsset
	TextAsset_t3973159845 * ___karoliTextAsset_5;
	// UnityEngine.TextAsset StartSkrollin::loremTextAsset
	TextAsset_t3973159845 * ___loremTextAsset_6;
	// UnityEngine.TextAsset StartSkrollin::rolandTextAsset
	TextAsset_t3973159845 * ___rolandTextAsset_7;
	// System.String StartSkrollin::introText
	String_t* ___introText_8;
	// System.String StartSkrollin::babaText
	String_t* ___babaText_9;
	// System.String StartSkrollin::karoliText
	String_t* ___karoliText_10;
	// System.String StartSkrollin::loremText
	String_t* ___loremText_11;
	// System.String StartSkrollin::rolandText
	String_t* ___rolandText_12;
	// System.String StartSkrollin::windowText
	String_t* ___windowText_13;
	// UnityEngine.Texture2D StartSkrollin::iconVector
	Texture2D_t3542995729 * ___iconVector_14;
	// UnityEngine.Texture2D StartSkrollin::startBG
	Texture2D_t3542995729 * ___startBG_15;
	// UnityEngine.Texture2D StartSkrollin::charlemagne
	Texture2D_t3542995729 * ___charlemagne_16;
	// UnityEngine.Texture2D StartSkrollin::map
	Texture2D_t3542995729 * ___map_17;
	// UnityEngine.Texture2D StartSkrollin::page
	Texture2D_t3542995729 * ___page_18;
	// UnityEngine.Texture2D StartSkrollin::iconAudio
	Texture2D_t3542995729 * ___iconAudio_19;
	// UnityEngine.Texture2D StartSkrollin::iconAudioNo
	Texture2D_t3542995729 * ___iconAudioNo_20;
	// UnityEngine.Texture2D StartSkrollin::iconMusic
	Texture2D_t3542995729 * ___iconMusic_21;
	// UnityEngine.Texture2D StartSkrollin::iconMusicNo
	Texture2D_t3542995729 * ___iconMusicNo_22;
	// UnityEngine.Texture2D StartSkrollin::iconResetHS
	Texture2D_t3542995729 * ___iconResetHS_23;
	// UnityEngine.Texture2D StartSkrollin::iconResetHSDone
	Texture2D_t3542995729 * ___iconResetHSDone_24;
	// UnityEngine.Texture2D StartSkrollin::iconResetHSWarn
	Texture2D_t3542995729 * ___iconResetHSWarn_25;
	// UnityEngine.Texture2D StartSkrollin::iconResetHSDoneWarn
	Texture2D_t3542995729 * ___iconResetHSDoneWarn_26;
	// UnityEngine.Texture2D StartSkrollin::iconThrustNormal
	Texture2D_t3542995729 * ___iconThrustNormal_27;
	// UnityEngine.Texture2D StartSkrollin::iconThrustReversed
	Texture2D_t3542995729 * ___iconThrustReversed_28;
	// UnityEngine.Texture2D StartSkrollin::iconTurnNormal
	Texture2D_t3542995729 * ___iconTurnNormal_29;
	// UnityEngine.Texture2D StartSkrollin::iconTurnReversed
	Texture2D_t3542995729 * ___iconTurnReversed_30;
	// UnityEngine.Texture2D StartSkrollin::iconInfo
	Texture2D_t3542995729 * ___iconInfo_31;
	// UnityEngine.Texture2D StartSkrollin::iconInfoNo
	Texture2D_t3542995729 * ___iconInfoNo_32;
	// System.Single StartSkrollin::marginSpringiness
	float ___marginSpringiness_33;
	// System.Single StartSkrollin::touchSpeed
	float ___touchSpeed_34;
	// System.Single StartSkrollin::coastSpeed
	float ___coastSpeed_35;
	// System.Single StartSkrollin::scrollMargin
	float ___scrollMargin_36;
	// System.Single StartSkrollin::scrollVector
	float ___scrollVector_37;
	// System.Single StartSkrollin::scrollMin
	float ___scrollMin_38;
	// System.Single StartSkrollin::scrollMax
	float ___scrollMax_39;
	// System.Single StartSkrollin::resistance
	float ___resistance_40;
	// System.Single StartSkrollin::marginPressure
	float ___marginPressure_41;
	// System.Single StartSkrollin::deltaY
	float ___deltaY_42;
	// System.Single StartSkrollin::startTime
	float ___startTime_43;
	// System.Boolean StartSkrollin::hazSkrollin
	bool ___hazSkrollin_44;
	// System.Boolean StartSkrollin::izSkrollin
	bool ___izSkrollin_45;
	// System.Single StartSkrollin::scrollMaxBaba
	float ___scrollMaxBaba_46;
	// System.Single StartSkrollin::scrollMaxKaroli
	float ___scrollMaxKaroli_47;
	// System.Single StartSkrollin::scrollMaxLorem
	float ___scrollMaxLorem_48;
	// System.Single StartSkrollin::scrollMaxRoland
	float ___scrollMaxRoland_49;
	// System.Single StartSkrollin::scrollMaxButtons
	float ___scrollMaxButtons_50;
	// System.Single StartSkrollin::scrollMinBaba
	float ___scrollMinBaba_51;
	// System.Single StartSkrollin::scrollMinKaroli
	float ___scrollMinKaroli_52;
	// System.Single StartSkrollin::scrollMinLorem
	float ___scrollMinLorem_53;
	// System.Single StartSkrollin::scrollMinRoland
	float ___scrollMinRoland_54;
	// System.Single StartSkrollin::scrollMinButtons
	float ___scrollMinButtons_55;
	// System.Single StartSkrollin::windowPad
	float ___windowPad_56;
	// UnityEngine.Rect StartSkrollin::babaWindowRect
	Rect_t3681755626  ___babaWindowRect_57;
	// UnityEngine.Rect StartSkrollin::karoliWindowRect
	Rect_t3681755626  ___karoliWindowRect_58;
	// UnityEngine.Rect StartSkrollin::loremWindowRect
	Rect_t3681755626  ___loremWindowRect_59;
	// UnityEngine.Rect StartSkrollin::rolandWindowRect
	Rect_t3681755626  ___rolandWindowRect_60;
	// UnityEngine.Rect StartSkrollin::buttonsWindowRect
	Rect_t3681755626  ___buttonsWindowRect_61;
	// UnityEngine.Rect StartSkrollin::createdWindow
	Rect_t3681755626  ___createdWindow_62;
	// UnityEngine.Rect StartSkrollin::windowRect
	Rect_t3681755626  ___windowRect_63;
	// System.String StartSkrollin::windowStyle
	String_t* ___windowStyle_64;
	// System.Boolean StartSkrollin::audioOff
	bool ___audioOff_65;
	// System.Boolean StartSkrollin::musicOff
	bool ___musicOff_66;
	// System.Boolean StartSkrollin::thrustReversed
	bool ___thrustReversed_67;
	// System.Boolean StartSkrollin::turnReversed
	bool ___turnReversed_68;
	// System.Boolean StartSkrollin::resetHSCheck
	bool ___resetHSCheck_69;
	// System.Boolean StartSkrollin::hasResetHS
	bool ___hasResetHS_70;
	// System.Boolean StartSkrollin::hideInfo
	bool ___hideInfo_71;
	// StartMenuLevel StartSkrollin::menuLevel
	int32_t ___menuLevel_72;

public:
	inline static int32_t get_offset_of_gSkin_2() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___gSkin_2)); }
	inline GUISkin_t1436893342 * get_gSkin_2() const { return ___gSkin_2; }
	inline GUISkin_t1436893342 ** get_address_of_gSkin_2() { return &___gSkin_2; }
	inline void set_gSkin_2(GUISkin_t1436893342 * value)
	{
		___gSkin_2 = value;
		Il2CppCodeGenWriteBarrier(&___gSkin_2, value);
	}

	inline static int32_t get_offset_of_introTextAsset_3() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___introTextAsset_3)); }
	inline TextAsset_t3973159845 * get_introTextAsset_3() const { return ___introTextAsset_3; }
	inline TextAsset_t3973159845 ** get_address_of_introTextAsset_3() { return &___introTextAsset_3; }
	inline void set_introTextAsset_3(TextAsset_t3973159845 * value)
	{
		___introTextAsset_3 = value;
		Il2CppCodeGenWriteBarrier(&___introTextAsset_3, value);
	}

	inline static int32_t get_offset_of_babaTextAsset_4() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___babaTextAsset_4)); }
	inline TextAsset_t3973159845 * get_babaTextAsset_4() const { return ___babaTextAsset_4; }
	inline TextAsset_t3973159845 ** get_address_of_babaTextAsset_4() { return &___babaTextAsset_4; }
	inline void set_babaTextAsset_4(TextAsset_t3973159845 * value)
	{
		___babaTextAsset_4 = value;
		Il2CppCodeGenWriteBarrier(&___babaTextAsset_4, value);
	}

	inline static int32_t get_offset_of_karoliTextAsset_5() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___karoliTextAsset_5)); }
	inline TextAsset_t3973159845 * get_karoliTextAsset_5() const { return ___karoliTextAsset_5; }
	inline TextAsset_t3973159845 ** get_address_of_karoliTextAsset_5() { return &___karoliTextAsset_5; }
	inline void set_karoliTextAsset_5(TextAsset_t3973159845 * value)
	{
		___karoliTextAsset_5 = value;
		Il2CppCodeGenWriteBarrier(&___karoliTextAsset_5, value);
	}

	inline static int32_t get_offset_of_loremTextAsset_6() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___loremTextAsset_6)); }
	inline TextAsset_t3973159845 * get_loremTextAsset_6() const { return ___loremTextAsset_6; }
	inline TextAsset_t3973159845 ** get_address_of_loremTextAsset_6() { return &___loremTextAsset_6; }
	inline void set_loremTextAsset_6(TextAsset_t3973159845 * value)
	{
		___loremTextAsset_6 = value;
		Il2CppCodeGenWriteBarrier(&___loremTextAsset_6, value);
	}

	inline static int32_t get_offset_of_rolandTextAsset_7() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___rolandTextAsset_7)); }
	inline TextAsset_t3973159845 * get_rolandTextAsset_7() const { return ___rolandTextAsset_7; }
	inline TextAsset_t3973159845 ** get_address_of_rolandTextAsset_7() { return &___rolandTextAsset_7; }
	inline void set_rolandTextAsset_7(TextAsset_t3973159845 * value)
	{
		___rolandTextAsset_7 = value;
		Il2CppCodeGenWriteBarrier(&___rolandTextAsset_7, value);
	}

	inline static int32_t get_offset_of_introText_8() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___introText_8)); }
	inline String_t* get_introText_8() const { return ___introText_8; }
	inline String_t** get_address_of_introText_8() { return &___introText_8; }
	inline void set_introText_8(String_t* value)
	{
		___introText_8 = value;
		Il2CppCodeGenWriteBarrier(&___introText_8, value);
	}

	inline static int32_t get_offset_of_babaText_9() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___babaText_9)); }
	inline String_t* get_babaText_9() const { return ___babaText_9; }
	inline String_t** get_address_of_babaText_9() { return &___babaText_9; }
	inline void set_babaText_9(String_t* value)
	{
		___babaText_9 = value;
		Il2CppCodeGenWriteBarrier(&___babaText_9, value);
	}

	inline static int32_t get_offset_of_karoliText_10() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___karoliText_10)); }
	inline String_t* get_karoliText_10() const { return ___karoliText_10; }
	inline String_t** get_address_of_karoliText_10() { return &___karoliText_10; }
	inline void set_karoliText_10(String_t* value)
	{
		___karoliText_10 = value;
		Il2CppCodeGenWriteBarrier(&___karoliText_10, value);
	}

	inline static int32_t get_offset_of_loremText_11() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___loremText_11)); }
	inline String_t* get_loremText_11() const { return ___loremText_11; }
	inline String_t** get_address_of_loremText_11() { return &___loremText_11; }
	inline void set_loremText_11(String_t* value)
	{
		___loremText_11 = value;
		Il2CppCodeGenWriteBarrier(&___loremText_11, value);
	}

	inline static int32_t get_offset_of_rolandText_12() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___rolandText_12)); }
	inline String_t* get_rolandText_12() const { return ___rolandText_12; }
	inline String_t** get_address_of_rolandText_12() { return &___rolandText_12; }
	inline void set_rolandText_12(String_t* value)
	{
		___rolandText_12 = value;
		Il2CppCodeGenWriteBarrier(&___rolandText_12, value);
	}

	inline static int32_t get_offset_of_windowText_13() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___windowText_13)); }
	inline String_t* get_windowText_13() const { return ___windowText_13; }
	inline String_t** get_address_of_windowText_13() { return &___windowText_13; }
	inline void set_windowText_13(String_t* value)
	{
		___windowText_13 = value;
		Il2CppCodeGenWriteBarrier(&___windowText_13, value);
	}

	inline static int32_t get_offset_of_iconVector_14() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconVector_14)); }
	inline Texture2D_t3542995729 * get_iconVector_14() const { return ___iconVector_14; }
	inline Texture2D_t3542995729 ** get_address_of_iconVector_14() { return &___iconVector_14; }
	inline void set_iconVector_14(Texture2D_t3542995729 * value)
	{
		___iconVector_14 = value;
		Il2CppCodeGenWriteBarrier(&___iconVector_14, value);
	}

	inline static int32_t get_offset_of_startBG_15() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___startBG_15)); }
	inline Texture2D_t3542995729 * get_startBG_15() const { return ___startBG_15; }
	inline Texture2D_t3542995729 ** get_address_of_startBG_15() { return &___startBG_15; }
	inline void set_startBG_15(Texture2D_t3542995729 * value)
	{
		___startBG_15 = value;
		Il2CppCodeGenWriteBarrier(&___startBG_15, value);
	}

	inline static int32_t get_offset_of_charlemagne_16() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___charlemagne_16)); }
	inline Texture2D_t3542995729 * get_charlemagne_16() const { return ___charlemagne_16; }
	inline Texture2D_t3542995729 ** get_address_of_charlemagne_16() { return &___charlemagne_16; }
	inline void set_charlemagne_16(Texture2D_t3542995729 * value)
	{
		___charlemagne_16 = value;
		Il2CppCodeGenWriteBarrier(&___charlemagne_16, value);
	}

	inline static int32_t get_offset_of_map_17() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___map_17)); }
	inline Texture2D_t3542995729 * get_map_17() const { return ___map_17; }
	inline Texture2D_t3542995729 ** get_address_of_map_17() { return &___map_17; }
	inline void set_map_17(Texture2D_t3542995729 * value)
	{
		___map_17 = value;
		Il2CppCodeGenWriteBarrier(&___map_17, value);
	}

	inline static int32_t get_offset_of_page_18() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___page_18)); }
	inline Texture2D_t3542995729 * get_page_18() const { return ___page_18; }
	inline Texture2D_t3542995729 ** get_address_of_page_18() { return &___page_18; }
	inline void set_page_18(Texture2D_t3542995729 * value)
	{
		___page_18 = value;
		Il2CppCodeGenWriteBarrier(&___page_18, value);
	}

	inline static int32_t get_offset_of_iconAudio_19() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconAudio_19)); }
	inline Texture2D_t3542995729 * get_iconAudio_19() const { return ___iconAudio_19; }
	inline Texture2D_t3542995729 ** get_address_of_iconAudio_19() { return &___iconAudio_19; }
	inline void set_iconAudio_19(Texture2D_t3542995729 * value)
	{
		___iconAudio_19 = value;
		Il2CppCodeGenWriteBarrier(&___iconAudio_19, value);
	}

	inline static int32_t get_offset_of_iconAudioNo_20() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconAudioNo_20)); }
	inline Texture2D_t3542995729 * get_iconAudioNo_20() const { return ___iconAudioNo_20; }
	inline Texture2D_t3542995729 ** get_address_of_iconAudioNo_20() { return &___iconAudioNo_20; }
	inline void set_iconAudioNo_20(Texture2D_t3542995729 * value)
	{
		___iconAudioNo_20 = value;
		Il2CppCodeGenWriteBarrier(&___iconAudioNo_20, value);
	}

	inline static int32_t get_offset_of_iconMusic_21() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconMusic_21)); }
	inline Texture2D_t3542995729 * get_iconMusic_21() const { return ___iconMusic_21; }
	inline Texture2D_t3542995729 ** get_address_of_iconMusic_21() { return &___iconMusic_21; }
	inline void set_iconMusic_21(Texture2D_t3542995729 * value)
	{
		___iconMusic_21 = value;
		Il2CppCodeGenWriteBarrier(&___iconMusic_21, value);
	}

	inline static int32_t get_offset_of_iconMusicNo_22() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconMusicNo_22)); }
	inline Texture2D_t3542995729 * get_iconMusicNo_22() const { return ___iconMusicNo_22; }
	inline Texture2D_t3542995729 ** get_address_of_iconMusicNo_22() { return &___iconMusicNo_22; }
	inline void set_iconMusicNo_22(Texture2D_t3542995729 * value)
	{
		___iconMusicNo_22 = value;
		Il2CppCodeGenWriteBarrier(&___iconMusicNo_22, value);
	}

	inline static int32_t get_offset_of_iconResetHS_23() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconResetHS_23)); }
	inline Texture2D_t3542995729 * get_iconResetHS_23() const { return ___iconResetHS_23; }
	inline Texture2D_t3542995729 ** get_address_of_iconResetHS_23() { return &___iconResetHS_23; }
	inline void set_iconResetHS_23(Texture2D_t3542995729 * value)
	{
		___iconResetHS_23 = value;
		Il2CppCodeGenWriteBarrier(&___iconResetHS_23, value);
	}

	inline static int32_t get_offset_of_iconResetHSDone_24() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconResetHSDone_24)); }
	inline Texture2D_t3542995729 * get_iconResetHSDone_24() const { return ___iconResetHSDone_24; }
	inline Texture2D_t3542995729 ** get_address_of_iconResetHSDone_24() { return &___iconResetHSDone_24; }
	inline void set_iconResetHSDone_24(Texture2D_t3542995729 * value)
	{
		___iconResetHSDone_24 = value;
		Il2CppCodeGenWriteBarrier(&___iconResetHSDone_24, value);
	}

	inline static int32_t get_offset_of_iconResetHSWarn_25() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconResetHSWarn_25)); }
	inline Texture2D_t3542995729 * get_iconResetHSWarn_25() const { return ___iconResetHSWarn_25; }
	inline Texture2D_t3542995729 ** get_address_of_iconResetHSWarn_25() { return &___iconResetHSWarn_25; }
	inline void set_iconResetHSWarn_25(Texture2D_t3542995729 * value)
	{
		___iconResetHSWarn_25 = value;
		Il2CppCodeGenWriteBarrier(&___iconResetHSWarn_25, value);
	}

	inline static int32_t get_offset_of_iconResetHSDoneWarn_26() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconResetHSDoneWarn_26)); }
	inline Texture2D_t3542995729 * get_iconResetHSDoneWarn_26() const { return ___iconResetHSDoneWarn_26; }
	inline Texture2D_t3542995729 ** get_address_of_iconResetHSDoneWarn_26() { return &___iconResetHSDoneWarn_26; }
	inline void set_iconResetHSDoneWarn_26(Texture2D_t3542995729 * value)
	{
		___iconResetHSDoneWarn_26 = value;
		Il2CppCodeGenWriteBarrier(&___iconResetHSDoneWarn_26, value);
	}

	inline static int32_t get_offset_of_iconThrustNormal_27() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconThrustNormal_27)); }
	inline Texture2D_t3542995729 * get_iconThrustNormal_27() const { return ___iconThrustNormal_27; }
	inline Texture2D_t3542995729 ** get_address_of_iconThrustNormal_27() { return &___iconThrustNormal_27; }
	inline void set_iconThrustNormal_27(Texture2D_t3542995729 * value)
	{
		___iconThrustNormal_27 = value;
		Il2CppCodeGenWriteBarrier(&___iconThrustNormal_27, value);
	}

	inline static int32_t get_offset_of_iconThrustReversed_28() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconThrustReversed_28)); }
	inline Texture2D_t3542995729 * get_iconThrustReversed_28() const { return ___iconThrustReversed_28; }
	inline Texture2D_t3542995729 ** get_address_of_iconThrustReversed_28() { return &___iconThrustReversed_28; }
	inline void set_iconThrustReversed_28(Texture2D_t3542995729 * value)
	{
		___iconThrustReversed_28 = value;
		Il2CppCodeGenWriteBarrier(&___iconThrustReversed_28, value);
	}

	inline static int32_t get_offset_of_iconTurnNormal_29() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconTurnNormal_29)); }
	inline Texture2D_t3542995729 * get_iconTurnNormal_29() const { return ___iconTurnNormal_29; }
	inline Texture2D_t3542995729 ** get_address_of_iconTurnNormal_29() { return &___iconTurnNormal_29; }
	inline void set_iconTurnNormal_29(Texture2D_t3542995729 * value)
	{
		___iconTurnNormal_29 = value;
		Il2CppCodeGenWriteBarrier(&___iconTurnNormal_29, value);
	}

	inline static int32_t get_offset_of_iconTurnReversed_30() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconTurnReversed_30)); }
	inline Texture2D_t3542995729 * get_iconTurnReversed_30() const { return ___iconTurnReversed_30; }
	inline Texture2D_t3542995729 ** get_address_of_iconTurnReversed_30() { return &___iconTurnReversed_30; }
	inline void set_iconTurnReversed_30(Texture2D_t3542995729 * value)
	{
		___iconTurnReversed_30 = value;
		Il2CppCodeGenWriteBarrier(&___iconTurnReversed_30, value);
	}

	inline static int32_t get_offset_of_iconInfo_31() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconInfo_31)); }
	inline Texture2D_t3542995729 * get_iconInfo_31() const { return ___iconInfo_31; }
	inline Texture2D_t3542995729 ** get_address_of_iconInfo_31() { return &___iconInfo_31; }
	inline void set_iconInfo_31(Texture2D_t3542995729 * value)
	{
		___iconInfo_31 = value;
		Il2CppCodeGenWriteBarrier(&___iconInfo_31, value);
	}

	inline static int32_t get_offset_of_iconInfoNo_32() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___iconInfoNo_32)); }
	inline Texture2D_t3542995729 * get_iconInfoNo_32() const { return ___iconInfoNo_32; }
	inline Texture2D_t3542995729 ** get_address_of_iconInfoNo_32() { return &___iconInfoNo_32; }
	inline void set_iconInfoNo_32(Texture2D_t3542995729 * value)
	{
		___iconInfoNo_32 = value;
		Il2CppCodeGenWriteBarrier(&___iconInfoNo_32, value);
	}

	inline static int32_t get_offset_of_marginSpringiness_33() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___marginSpringiness_33)); }
	inline float get_marginSpringiness_33() const { return ___marginSpringiness_33; }
	inline float* get_address_of_marginSpringiness_33() { return &___marginSpringiness_33; }
	inline void set_marginSpringiness_33(float value)
	{
		___marginSpringiness_33 = value;
	}

	inline static int32_t get_offset_of_touchSpeed_34() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___touchSpeed_34)); }
	inline float get_touchSpeed_34() const { return ___touchSpeed_34; }
	inline float* get_address_of_touchSpeed_34() { return &___touchSpeed_34; }
	inline void set_touchSpeed_34(float value)
	{
		___touchSpeed_34 = value;
	}

	inline static int32_t get_offset_of_coastSpeed_35() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___coastSpeed_35)); }
	inline float get_coastSpeed_35() const { return ___coastSpeed_35; }
	inline float* get_address_of_coastSpeed_35() { return &___coastSpeed_35; }
	inline void set_coastSpeed_35(float value)
	{
		___coastSpeed_35 = value;
	}

	inline static int32_t get_offset_of_scrollMargin_36() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMargin_36)); }
	inline float get_scrollMargin_36() const { return ___scrollMargin_36; }
	inline float* get_address_of_scrollMargin_36() { return &___scrollMargin_36; }
	inline void set_scrollMargin_36(float value)
	{
		___scrollMargin_36 = value;
	}

	inline static int32_t get_offset_of_scrollVector_37() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollVector_37)); }
	inline float get_scrollVector_37() const { return ___scrollVector_37; }
	inline float* get_address_of_scrollVector_37() { return &___scrollVector_37; }
	inline void set_scrollVector_37(float value)
	{
		___scrollVector_37 = value;
	}

	inline static int32_t get_offset_of_scrollMin_38() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMin_38)); }
	inline float get_scrollMin_38() const { return ___scrollMin_38; }
	inline float* get_address_of_scrollMin_38() { return &___scrollMin_38; }
	inline void set_scrollMin_38(float value)
	{
		___scrollMin_38 = value;
	}

	inline static int32_t get_offset_of_scrollMax_39() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMax_39)); }
	inline float get_scrollMax_39() const { return ___scrollMax_39; }
	inline float* get_address_of_scrollMax_39() { return &___scrollMax_39; }
	inline void set_scrollMax_39(float value)
	{
		___scrollMax_39 = value;
	}

	inline static int32_t get_offset_of_resistance_40() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___resistance_40)); }
	inline float get_resistance_40() const { return ___resistance_40; }
	inline float* get_address_of_resistance_40() { return &___resistance_40; }
	inline void set_resistance_40(float value)
	{
		___resistance_40 = value;
	}

	inline static int32_t get_offset_of_marginPressure_41() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___marginPressure_41)); }
	inline float get_marginPressure_41() const { return ___marginPressure_41; }
	inline float* get_address_of_marginPressure_41() { return &___marginPressure_41; }
	inline void set_marginPressure_41(float value)
	{
		___marginPressure_41 = value;
	}

	inline static int32_t get_offset_of_deltaY_42() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___deltaY_42)); }
	inline float get_deltaY_42() const { return ___deltaY_42; }
	inline float* get_address_of_deltaY_42() { return &___deltaY_42; }
	inline void set_deltaY_42(float value)
	{
		___deltaY_42 = value;
	}

	inline static int32_t get_offset_of_startTime_43() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___startTime_43)); }
	inline float get_startTime_43() const { return ___startTime_43; }
	inline float* get_address_of_startTime_43() { return &___startTime_43; }
	inline void set_startTime_43(float value)
	{
		___startTime_43 = value;
	}

	inline static int32_t get_offset_of_hazSkrollin_44() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___hazSkrollin_44)); }
	inline bool get_hazSkrollin_44() const { return ___hazSkrollin_44; }
	inline bool* get_address_of_hazSkrollin_44() { return &___hazSkrollin_44; }
	inline void set_hazSkrollin_44(bool value)
	{
		___hazSkrollin_44 = value;
	}

	inline static int32_t get_offset_of_izSkrollin_45() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___izSkrollin_45)); }
	inline bool get_izSkrollin_45() const { return ___izSkrollin_45; }
	inline bool* get_address_of_izSkrollin_45() { return &___izSkrollin_45; }
	inline void set_izSkrollin_45(bool value)
	{
		___izSkrollin_45 = value;
	}

	inline static int32_t get_offset_of_scrollMaxBaba_46() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMaxBaba_46)); }
	inline float get_scrollMaxBaba_46() const { return ___scrollMaxBaba_46; }
	inline float* get_address_of_scrollMaxBaba_46() { return &___scrollMaxBaba_46; }
	inline void set_scrollMaxBaba_46(float value)
	{
		___scrollMaxBaba_46 = value;
	}

	inline static int32_t get_offset_of_scrollMaxKaroli_47() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMaxKaroli_47)); }
	inline float get_scrollMaxKaroli_47() const { return ___scrollMaxKaroli_47; }
	inline float* get_address_of_scrollMaxKaroli_47() { return &___scrollMaxKaroli_47; }
	inline void set_scrollMaxKaroli_47(float value)
	{
		___scrollMaxKaroli_47 = value;
	}

	inline static int32_t get_offset_of_scrollMaxLorem_48() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMaxLorem_48)); }
	inline float get_scrollMaxLorem_48() const { return ___scrollMaxLorem_48; }
	inline float* get_address_of_scrollMaxLorem_48() { return &___scrollMaxLorem_48; }
	inline void set_scrollMaxLorem_48(float value)
	{
		___scrollMaxLorem_48 = value;
	}

	inline static int32_t get_offset_of_scrollMaxRoland_49() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMaxRoland_49)); }
	inline float get_scrollMaxRoland_49() const { return ___scrollMaxRoland_49; }
	inline float* get_address_of_scrollMaxRoland_49() { return &___scrollMaxRoland_49; }
	inline void set_scrollMaxRoland_49(float value)
	{
		___scrollMaxRoland_49 = value;
	}

	inline static int32_t get_offset_of_scrollMaxButtons_50() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMaxButtons_50)); }
	inline float get_scrollMaxButtons_50() const { return ___scrollMaxButtons_50; }
	inline float* get_address_of_scrollMaxButtons_50() { return &___scrollMaxButtons_50; }
	inline void set_scrollMaxButtons_50(float value)
	{
		___scrollMaxButtons_50 = value;
	}

	inline static int32_t get_offset_of_scrollMinBaba_51() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMinBaba_51)); }
	inline float get_scrollMinBaba_51() const { return ___scrollMinBaba_51; }
	inline float* get_address_of_scrollMinBaba_51() { return &___scrollMinBaba_51; }
	inline void set_scrollMinBaba_51(float value)
	{
		___scrollMinBaba_51 = value;
	}

	inline static int32_t get_offset_of_scrollMinKaroli_52() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMinKaroli_52)); }
	inline float get_scrollMinKaroli_52() const { return ___scrollMinKaroli_52; }
	inline float* get_address_of_scrollMinKaroli_52() { return &___scrollMinKaroli_52; }
	inline void set_scrollMinKaroli_52(float value)
	{
		___scrollMinKaroli_52 = value;
	}

	inline static int32_t get_offset_of_scrollMinLorem_53() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMinLorem_53)); }
	inline float get_scrollMinLorem_53() const { return ___scrollMinLorem_53; }
	inline float* get_address_of_scrollMinLorem_53() { return &___scrollMinLorem_53; }
	inline void set_scrollMinLorem_53(float value)
	{
		___scrollMinLorem_53 = value;
	}

	inline static int32_t get_offset_of_scrollMinRoland_54() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMinRoland_54)); }
	inline float get_scrollMinRoland_54() const { return ___scrollMinRoland_54; }
	inline float* get_address_of_scrollMinRoland_54() { return &___scrollMinRoland_54; }
	inline void set_scrollMinRoland_54(float value)
	{
		___scrollMinRoland_54 = value;
	}

	inline static int32_t get_offset_of_scrollMinButtons_55() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___scrollMinButtons_55)); }
	inline float get_scrollMinButtons_55() const { return ___scrollMinButtons_55; }
	inline float* get_address_of_scrollMinButtons_55() { return &___scrollMinButtons_55; }
	inline void set_scrollMinButtons_55(float value)
	{
		___scrollMinButtons_55 = value;
	}

	inline static int32_t get_offset_of_windowPad_56() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___windowPad_56)); }
	inline float get_windowPad_56() const { return ___windowPad_56; }
	inline float* get_address_of_windowPad_56() { return &___windowPad_56; }
	inline void set_windowPad_56(float value)
	{
		___windowPad_56 = value;
	}

	inline static int32_t get_offset_of_babaWindowRect_57() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___babaWindowRect_57)); }
	inline Rect_t3681755626  get_babaWindowRect_57() const { return ___babaWindowRect_57; }
	inline Rect_t3681755626 * get_address_of_babaWindowRect_57() { return &___babaWindowRect_57; }
	inline void set_babaWindowRect_57(Rect_t3681755626  value)
	{
		___babaWindowRect_57 = value;
	}

	inline static int32_t get_offset_of_karoliWindowRect_58() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___karoliWindowRect_58)); }
	inline Rect_t3681755626  get_karoliWindowRect_58() const { return ___karoliWindowRect_58; }
	inline Rect_t3681755626 * get_address_of_karoliWindowRect_58() { return &___karoliWindowRect_58; }
	inline void set_karoliWindowRect_58(Rect_t3681755626  value)
	{
		___karoliWindowRect_58 = value;
	}

	inline static int32_t get_offset_of_loremWindowRect_59() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___loremWindowRect_59)); }
	inline Rect_t3681755626  get_loremWindowRect_59() const { return ___loremWindowRect_59; }
	inline Rect_t3681755626 * get_address_of_loremWindowRect_59() { return &___loremWindowRect_59; }
	inline void set_loremWindowRect_59(Rect_t3681755626  value)
	{
		___loremWindowRect_59 = value;
	}

	inline static int32_t get_offset_of_rolandWindowRect_60() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___rolandWindowRect_60)); }
	inline Rect_t3681755626  get_rolandWindowRect_60() const { return ___rolandWindowRect_60; }
	inline Rect_t3681755626 * get_address_of_rolandWindowRect_60() { return &___rolandWindowRect_60; }
	inline void set_rolandWindowRect_60(Rect_t3681755626  value)
	{
		___rolandWindowRect_60 = value;
	}

	inline static int32_t get_offset_of_buttonsWindowRect_61() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___buttonsWindowRect_61)); }
	inline Rect_t3681755626  get_buttonsWindowRect_61() const { return ___buttonsWindowRect_61; }
	inline Rect_t3681755626 * get_address_of_buttonsWindowRect_61() { return &___buttonsWindowRect_61; }
	inline void set_buttonsWindowRect_61(Rect_t3681755626  value)
	{
		___buttonsWindowRect_61 = value;
	}

	inline static int32_t get_offset_of_createdWindow_62() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___createdWindow_62)); }
	inline Rect_t3681755626  get_createdWindow_62() const { return ___createdWindow_62; }
	inline Rect_t3681755626 * get_address_of_createdWindow_62() { return &___createdWindow_62; }
	inline void set_createdWindow_62(Rect_t3681755626  value)
	{
		___createdWindow_62 = value;
	}

	inline static int32_t get_offset_of_windowRect_63() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___windowRect_63)); }
	inline Rect_t3681755626  get_windowRect_63() const { return ___windowRect_63; }
	inline Rect_t3681755626 * get_address_of_windowRect_63() { return &___windowRect_63; }
	inline void set_windowRect_63(Rect_t3681755626  value)
	{
		___windowRect_63 = value;
	}

	inline static int32_t get_offset_of_windowStyle_64() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___windowStyle_64)); }
	inline String_t* get_windowStyle_64() const { return ___windowStyle_64; }
	inline String_t** get_address_of_windowStyle_64() { return &___windowStyle_64; }
	inline void set_windowStyle_64(String_t* value)
	{
		___windowStyle_64 = value;
		Il2CppCodeGenWriteBarrier(&___windowStyle_64, value);
	}

	inline static int32_t get_offset_of_audioOff_65() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___audioOff_65)); }
	inline bool get_audioOff_65() const { return ___audioOff_65; }
	inline bool* get_address_of_audioOff_65() { return &___audioOff_65; }
	inline void set_audioOff_65(bool value)
	{
		___audioOff_65 = value;
	}

	inline static int32_t get_offset_of_musicOff_66() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___musicOff_66)); }
	inline bool get_musicOff_66() const { return ___musicOff_66; }
	inline bool* get_address_of_musicOff_66() { return &___musicOff_66; }
	inline void set_musicOff_66(bool value)
	{
		___musicOff_66 = value;
	}

	inline static int32_t get_offset_of_thrustReversed_67() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___thrustReversed_67)); }
	inline bool get_thrustReversed_67() const { return ___thrustReversed_67; }
	inline bool* get_address_of_thrustReversed_67() { return &___thrustReversed_67; }
	inline void set_thrustReversed_67(bool value)
	{
		___thrustReversed_67 = value;
	}

	inline static int32_t get_offset_of_turnReversed_68() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___turnReversed_68)); }
	inline bool get_turnReversed_68() const { return ___turnReversed_68; }
	inline bool* get_address_of_turnReversed_68() { return &___turnReversed_68; }
	inline void set_turnReversed_68(bool value)
	{
		___turnReversed_68 = value;
	}

	inline static int32_t get_offset_of_resetHSCheck_69() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___resetHSCheck_69)); }
	inline bool get_resetHSCheck_69() const { return ___resetHSCheck_69; }
	inline bool* get_address_of_resetHSCheck_69() { return &___resetHSCheck_69; }
	inline void set_resetHSCheck_69(bool value)
	{
		___resetHSCheck_69 = value;
	}

	inline static int32_t get_offset_of_hasResetHS_70() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___hasResetHS_70)); }
	inline bool get_hasResetHS_70() const { return ___hasResetHS_70; }
	inline bool* get_address_of_hasResetHS_70() { return &___hasResetHS_70; }
	inline void set_hasResetHS_70(bool value)
	{
		___hasResetHS_70 = value;
	}

	inline static int32_t get_offset_of_hideInfo_71() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___hideInfo_71)); }
	inline bool get_hideInfo_71() const { return ___hideInfo_71; }
	inline bool* get_address_of_hideInfo_71() { return &___hideInfo_71; }
	inline void set_hideInfo_71(bool value)
	{
		___hideInfo_71 = value;
	}

	inline static int32_t get_offset_of_menuLevel_72() { return static_cast<int32_t>(offsetof(StartSkrollin_t1317199858, ___menuLevel_72)); }
	inline int32_t get_menuLevel_72() const { return ___menuLevel_72; }
	inline int32_t* get_address_of_menuLevel_72() { return &___menuLevel_72; }
	inline void set_menuLevel_72(int32_t value)
	{
		___menuLevel_72 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
