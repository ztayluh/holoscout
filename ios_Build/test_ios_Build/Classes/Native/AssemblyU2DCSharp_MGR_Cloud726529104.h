﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// MGR_Anim
struct MGR_Anim_t3221826994;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Entry779635924.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_Cloud
struct  MGR_Cloud_t726529104  : public MonoBehaviour_t1158329972
{
public:
	// Entry MGR_Cloud::myEntry
	Entry_t779635924  ___myEntry_2;
	// System.Int32 MGR_Cloud::numOfClips
	int32_t ___numOfClips_3;
	// System.Int32 MGR_Cloud::currClip
	int32_t ___currClip_4;
	// System.String MGR_Cloud::currClipName
	String_t* ___currClipName_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MGR_Cloud::traineeRecords
	Dictionary_2_t3943999495 * ___traineeRecords_6;
	// MGR_Anim MGR_Cloud::anims
	MGR_Anim_t3221826994 * ___anims_7;

public:
	inline static int32_t get_offset_of_myEntry_2() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___myEntry_2)); }
	inline Entry_t779635924  get_myEntry_2() const { return ___myEntry_2; }
	inline Entry_t779635924 * get_address_of_myEntry_2() { return &___myEntry_2; }
	inline void set_myEntry_2(Entry_t779635924  value)
	{
		___myEntry_2 = value;
	}

	inline static int32_t get_offset_of_numOfClips_3() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___numOfClips_3)); }
	inline int32_t get_numOfClips_3() const { return ___numOfClips_3; }
	inline int32_t* get_address_of_numOfClips_3() { return &___numOfClips_3; }
	inline void set_numOfClips_3(int32_t value)
	{
		___numOfClips_3 = value;
	}

	inline static int32_t get_offset_of_currClip_4() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___currClip_4)); }
	inline int32_t get_currClip_4() const { return ___currClip_4; }
	inline int32_t* get_address_of_currClip_4() { return &___currClip_4; }
	inline void set_currClip_4(int32_t value)
	{
		___currClip_4 = value;
	}

	inline static int32_t get_offset_of_currClipName_5() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___currClipName_5)); }
	inline String_t* get_currClipName_5() const { return ___currClipName_5; }
	inline String_t** get_address_of_currClipName_5() { return &___currClipName_5; }
	inline void set_currClipName_5(String_t* value)
	{
		___currClipName_5 = value;
		Il2CppCodeGenWriteBarrier(&___currClipName_5, value);
	}

	inline static int32_t get_offset_of_traineeRecords_6() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___traineeRecords_6)); }
	inline Dictionary_2_t3943999495 * get_traineeRecords_6() const { return ___traineeRecords_6; }
	inline Dictionary_2_t3943999495 ** get_address_of_traineeRecords_6() { return &___traineeRecords_6; }
	inline void set_traineeRecords_6(Dictionary_2_t3943999495 * value)
	{
		___traineeRecords_6 = value;
		Il2CppCodeGenWriteBarrier(&___traineeRecords_6, value);
	}

	inline static int32_t get_offset_of_anims_7() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___anims_7)); }
	inline MGR_Anim_t3221826994 * get_anims_7() const { return ___anims_7; }
	inline MGR_Anim_t3221826994 ** get_address_of_anims_7() { return &___anims_7; }
	inline void set_anims_7(MGR_Anim_t3221826994 * value)
	{
		___anims_7 = value;
		Il2CppCodeGenWriteBarrier(&___anims_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
