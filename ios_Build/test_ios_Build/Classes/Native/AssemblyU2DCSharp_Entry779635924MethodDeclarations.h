﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entry
struct Entry_t779635924;
struct Entry_t779635924_marshaled_pinvoke;
struct Entry_t779635924_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Entry_t779635924;
struct Entry_t779635924_marshaled_pinvoke;

extern "C" void Entry_t779635924_marshal_pinvoke(const Entry_t779635924& unmarshaled, Entry_t779635924_marshaled_pinvoke& marshaled);
extern "C" void Entry_t779635924_marshal_pinvoke_back(const Entry_t779635924_marshaled_pinvoke& marshaled, Entry_t779635924& unmarshaled);
extern "C" void Entry_t779635924_marshal_pinvoke_cleanup(Entry_t779635924_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Entry_t779635924;
struct Entry_t779635924_marshaled_com;

extern "C" void Entry_t779635924_marshal_com(const Entry_t779635924& unmarshaled, Entry_t779635924_marshaled_com& marshaled);
extern "C" void Entry_t779635924_marshal_com_back(const Entry_t779635924_marshaled_com& marshaled, Entry_t779635924& unmarshaled);
extern "C" void Entry_t779635924_marshal_com_cleanup(Entry_t779635924_marshaled_com& marshaled);
