﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_StartMenuLevel2952164235.h"
#include "AssemblyU2DUnityScript_StartSkrollin1317199858.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (StartMenuLevel_t2952164235)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1700[7] = 
{
	StartMenuLevel_t2952164235::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (StartSkrollin_t1317199858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[71] = 
{
	StartSkrollin_t1317199858::get_offset_of_gSkin_2(),
	StartSkrollin_t1317199858::get_offset_of_introTextAsset_3(),
	StartSkrollin_t1317199858::get_offset_of_babaTextAsset_4(),
	StartSkrollin_t1317199858::get_offset_of_karoliTextAsset_5(),
	StartSkrollin_t1317199858::get_offset_of_loremTextAsset_6(),
	StartSkrollin_t1317199858::get_offset_of_rolandTextAsset_7(),
	StartSkrollin_t1317199858::get_offset_of_introText_8(),
	StartSkrollin_t1317199858::get_offset_of_babaText_9(),
	StartSkrollin_t1317199858::get_offset_of_karoliText_10(),
	StartSkrollin_t1317199858::get_offset_of_loremText_11(),
	StartSkrollin_t1317199858::get_offset_of_rolandText_12(),
	StartSkrollin_t1317199858::get_offset_of_windowText_13(),
	StartSkrollin_t1317199858::get_offset_of_iconVector_14(),
	StartSkrollin_t1317199858::get_offset_of_startBG_15(),
	StartSkrollin_t1317199858::get_offset_of_charlemagne_16(),
	StartSkrollin_t1317199858::get_offset_of_map_17(),
	StartSkrollin_t1317199858::get_offset_of_page_18(),
	StartSkrollin_t1317199858::get_offset_of_iconAudio_19(),
	StartSkrollin_t1317199858::get_offset_of_iconAudioNo_20(),
	StartSkrollin_t1317199858::get_offset_of_iconMusic_21(),
	StartSkrollin_t1317199858::get_offset_of_iconMusicNo_22(),
	StartSkrollin_t1317199858::get_offset_of_iconResetHS_23(),
	StartSkrollin_t1317199858::get_offset_of_iconResetHSDone_24(),
	StartSkrollin_t1317199858::get_offset_of_iconResetHSWarn_25(),
	StartSkrollin_t1317199858::get_offset_of_iconResetHSDoneWarn_26(),
	StartSkrollin_t1317199858::get_offset_of_iconThrustNormal_27(),
	StartSkrollin_t1317199858::get_offset_of_iconThrustReversed_28(),
	StartSkrollin_t1317199858::get_offset_of_iconTurnNormal_29(),
	StartSkrollin_t1317199858::get_offset_of_iconTurnReversed_30(),
	StartSkrollin_t1317199858::get_offset_of_iconInfo_31(),
	StartSkrollin_t1317199858::get_offset_of_iconInfoNo_32(),
	StartSkrollin_t1317199858::get_offset_of_marginSpringiness_33(),
	StartSkrollin_t1317199858::get_offset_of_touchSpeed_34(),
	StartSkrollin_t1317199858::get_offset_of_coastSpeed_35(),
	StartSkrollin_t1317199858::get_offset_of_scrollMargin_36(),
	StartSkrollin_t1317199858::get_offset_of_scrollVector_37(),
	StartSkrollin_t1317199858::get_offset_of_scrollMin_38(),
	StartSkrollin_t1317199858::get_offset_of_scrollMax_39(),
	StartSkrollin_t1317199858::get_offset_of_resistance_40(),
	StartSkrollin_t1317199858::get_offset_of_marginPressure_41(),
	StartSkrollin_t1317199858::get_offset_of_deltaY_42(),
	StartSkrollin_t1317199858::get_offset_of_startTime_43(),
	StartSkrollin_t1317199858::get_offset_of_hazSkrollin_44(),
	StartSkrollin_t1317199858::get_offset_of_izSkrollin_45(),
	StartSkrollin_t1317199858::get_offset_of_scrollMaxBaba_46(),
	StartSkrollin_t1317199858::get_offset_of_scrollMaxKaroli_47(),
	StartSkrollin_t1317199858::get_offset_of_scrollMaxLorem_48(),
	StartSkrollin_t1317199858::get_offset_of_scrollMaxRoland_49(),
	StartSkrollin_t1317199858::get_offset_of_scrollMaxButtons_50(),
	StartSkrollin_t1317199858::get_offset_of_scrollMinBaba_51(),
	StartSkrollin_t1317199858::get_offset_of_scrollMinKaroli_52(),
	StartSkrollin_t1317199858::get_offset_of_scrollMinLorem_53(),
	StartSkrollin_t1317199858::get_offset_of_scrollMinRoland_54(),
	StartSkrollin_t1317199858::get_offset_of_scrollMinButtons_55(),
	StartSkrollin_t1317199858::get_offset_of_windowPad_56(),
	StartSkrollin_t1317199858::get_offset_of_babaWindowRect_57(),
	StartSkrollin_t1317199858::get_offset_of_karoliWindowRect_58(),
	StartSkrollin_t1317199858::get_offset_of_loremWindowRect_59(),
	StartSkrollin_t1317199858::get_offset_of_rolandWindowRect_60(),
	StartSkrollin_t1317199858::get_offset_of_buttonsWindowRect_61(),
	StartSkrollin_t1317199858::get_offset_of_createdWindow_62(),
	StartSkrollin_t1317199858::get_offset_of_windowRect_63(),
	StartSkrollin_t1317199858::get_offset_of_windowStyle_64(),
	StartSkrollin_t1317199858::get_offset_of_audioOff_65(),
	StartSkrollin_t1317199858::get_offset_of_musicOff_66(),
	StartSkrollin_t1317199858::get_offset_of_thrustReversed_67(),
	StartSkrollin_t1317199858::get_offset_of_turnReversed_68(),
	StartSkrollin_t1317199858::get_offset_of_resetHSCheck_69(),
	StartSkrollin_t1317199858::get_offset_of_hasResetHS_70(),
	StartSkrollin_t1317199858::get_offset_of_hideInfo_71(),
	StartSkrollin_t1317199858::get_offset_of_menuLevel_72(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
