﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_MGR_Menus_MenuStates1946331629.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_Menus
struct  MGR_Menus_t511463637  : public MonoBehaviour_t1158329972
{
public:
	// MGR_Menus/MenuStates MGR_Menus::currentstate
	int32_t ___currentstate_2;
	// UnityEngine.Transform MGR_Menus::myCanvas
	Transform_t3275118058 * ___myCanvas_3;
	// UnityEngine.GameObject MGR_Menus::startMenu
	GameObject_t1756533147 * ___startMenu_4;
	// UnityEngine.GameObject MGR_Menus::createMenu
	GameObject_t1756533147 * ___createMenu_5;
	// UnityEngine.GameObject MGR_Menus::readMenu
	GameObject_t1756533147 * ___readMenu_6;
	// UnityEngine.GameObject[] MGR_Menus::startObjects
	GameObjectU5BU5D_t3057952154* ___startObjects_7;
	// UnityEngine.GameObject[] MGR_Menus::createObjects
	GameObjectU5BU5D_t3057952154* ___createObjects_8;
	// UnityEngine.GameObject[] MGR_Menus::readObjects
	GameObjectU5BU5D_t3057952154* ___readObjects_9;
	// UnityEngine.GameObject MGR_Menus::player
	GameObject_t1756533147 * ___player_10;

public:
	inline static int32_t get_offset_of_currentstate_2() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___currentstate_2)); }
	inline int32_t get_currentstate_2() const { return ___currentstate_2; }
	inline int32_t* get_address_of_currentstate_2() { return &___currentstate_2; }
	inline void set_currentstate_2(int32_t value)
	{
		___currentstate_2 = value;
	}

	inline static int32_t get_offset_of_myCanvas_3() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___myCanvas_3)); }
	inline Transform_t3275118058 * get_myCanvas_3() const { return ___myCanvas_3; }
	inline Transform_t3275118058 ** get_address_of_myCanvas_3() { return &___myCanvas_3; }
	inline void set_myCanvas_3(Transform_t3275118058 * value)
	{
		___myCanvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___myCanvas_3, value);
	}

	inline static int32_t get_offset_of_startMenu_4() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___startMenu_4)); }
	inline GameObject_t1756533147 * get_startMenu_4() const { return ___startMenu_4; }
	inline GameObject_t1756533147 ** get_address_of_startMenu_4() { return &___startMenu_4; }
	inline void set_startMenu_4(GameObject_t1756533147 * value)
	{
		___startMenu_4 = value;
		Il2CppCodeGenWriteBarrier(&___startMenu_4, value);
	}

	inline static int32_t get_offset_of_createMenu_5() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___createMenu_5)); }
	inline GameObject_t1756533147 * get_createMenu_5() const { return ___createMenu_5; }
	inline GameObject_t1756533147 ** get_address_of_createMenu_5() { return &___createMenu_5; }
	inline void set_createMenu_5(GameObject_t1756533147 * value)
	{
		___createMenu_5 = value;
		Il2CppCodeGenWriteBarrier(&___createMenu_5, value);
	}

	inline static int32_t get_offset_of_readMenu_6() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___readMenu_6)); }
	inline GameObject_t1756533147 * get_readMenu_6() const { return ___readMenu_6; }
	inline GameObject_t1756533147 ** get_address_of_readMenu_6() { return &___readMenu_6; }
	inline void set_readMenu_6(GameObject_t1756533147 * value)
	{
		___readMenu_6 = value;
		Il2CppCodeGenWriteBarrier(&___readMenu_6, value);
	}

	inline static int32_t get_offset_of_startObjects_7() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___startObjects_7)); }
	inline GameObjectU5BU5D_t3057952154* get_startObjects_7() const { return ___startObjects_7; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_startObjects_7() { return &___startObjects_7; }
	inline void set_startObjects_7(GameObjectU5BU5D_t3057952154* value)
	{
		___startObjects_7 = value;
		Il2CppCodeGenWriteBarrier(&___startObjects_7, value);
	}

	inline static int32_t get_offset_of_createObjects_8() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___createObjects_8)); }
	inline GameObjectU5BU5D_t3057952154* get_createObjects_8() const { return ___createObjects_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_createObjects_8() { return &___createObjects_8; }
	inline void set_createObjects_8(GameObjectU5BU5D_t3057952154* value)
	{
		___createObjects_8 = value;
		Il2CppCodeGenWriteBarrier(&___createObjects_8, value);
	}

	inline static int32_t get_offset_of_readObjects_9() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___readObjects_9)); }
	inline GameObjectU5BU5D_t3057952154* get_readObjects_9() const { return ___readObjects_9; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_readObjects_9() { return &___readObjects_9; }
	inline void set_readObjects_9(GameObjectU5BU5D_t3057952154* value)
	{
		___readObjects_9 = value;
		Il2CppCodeGenWriteBarrier(&___readObjects_9, value);
	}

	inline static int32_t get_offset_of_player_10() { return static_cast<int32_t>(offsetof(MGR_Menus_t511463637, ___player_10)); }
	inline GameObject_t1756533147 * get_player_10() const { return ___player_10; }
	inline GameObject_t1756533147 ** get_address_of_player_10() { return &___player_10; }
	inline void set_player_10(GameObject_t1756533147 * value)
	{
		___player_10 = value;
		Il2CppCodeGenWriteBarrier(&___player_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
