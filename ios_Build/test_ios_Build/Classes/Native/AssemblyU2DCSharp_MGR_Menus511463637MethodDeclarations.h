﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Menus
struct MGR_Menus_t511463637;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Menus::.ctor()
extern "C"  void MGR_Menus__ctor_m4221427840 (MGR_Menus_t511463637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Menus::Awake()
extern "C"  void MGR_Menus_Awake_m45193489 (MGR_Menus_t511463637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Menus::hideMenu(UnityEngine.GameObject[])
extern "C"  void MGR_Menus_hideMenu_m3562369893 (MGR_Menus_t511463637 * __this, GameObjectU5BU5D_t3057952154* ___G0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Menus::showMenu(UnityEngine.GameObject[])
extern "C"  void MGR_Menus_showMenu_m1990371462 (MGR_Menus_t511463637 * __this, GameObjectU5BU5D_t3057952154* ___G0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Menus::Update()
extern "C"  void MGR_Menus_Update_m3885601141 (MGR_Menus_t511463637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Menus::StartMenu()
extern "C"  void MGR_Menus_StartMenu_m1676692927 (MGR_Menus_t511463637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Menus::CreateMenu()
extern "C"  void MGR_Menus_CreateMenu_m1027925027 (MGR_Menus_t511463637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Menus::ReadMenu()
extern "C"  void MGR_Menus_ReadMenu_m1846344601 (MGR_Menus_t511463637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
