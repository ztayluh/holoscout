﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation1571958496.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode1081683921.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage2749640213.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D1156185964.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar3248359358.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction3696775921.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEven1794825321.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis2427050347.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRe4156771994.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect1199013257.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementTy905360158.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_Scrollbar3834843475.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRec3529018992.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition605142169.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection3187567897.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility4019374597.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction1525323322.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2111116400.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis375128448.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1353336012.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial1630303189.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE3157325053.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_GUIUtils2318875170.h"
#include "AssemblyU2DCSharp_MGR_Anim3221826994.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator02856749856.h"
#include "AssemblyU2DCSharp_Entry779635924.h"
#include "AssemblyU2DCSharp_MGR_Cloud726529104.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CPlayU3Ec__Iterator01215233438.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadCoachDBU3Ec__It3826372699.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadTraineeDBU3Ec__It81797702.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadPLDBU3Ec__Iterat728369779.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadHashDBU3Ec__Ite2884699250.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu817873244.h"
#include "AssemblyU2DCSharp_MGR_GUI2229806790.h"
#include "AssemblyU2DCSharp_MGR_HLGUI3333927834.h"
#include "AssemblyU2DCSharp_MGR_HoloText1166120216.h"
#include "AssemblyU2DCSharp_MGR_Menus511463637.h"
#include "AssemblyU2DCSharp_MGR_Menus_MenuStates1946331629.h"
#include "AssemblyU2DCSharp_SetAnimationInAnimator2420164504.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (Navigation_t1571958496)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1600[5] = 
{
	Navigation_t1571958496::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (Mode_t1081683921)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1601[6] = 
{
	Mode_t1081683921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (RawImage_t2749640213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[2] = 
{
	RawImage_t2749640213::get_offset_of_m_Texture_28(),
	RawImage_t2749640213::get_offset_of_m_UVRect_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (RectMask2D_t1156185964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[8] = 
{
	RectMask2D_t1156185964::get_offset_of_m_VertexClipper_2(),
	RectMask2D_t1156185964::get_offset_of_m_RectTransform_3(),
	RectMask2D_t1156185964::get_offset_of_m_ClipTargets_4(),
	RectMask2D_t1156185964::get_offset_of_m_ShouldRecalculateClipRects_5(),
	RectMask2D_t1156185964::get_offset_of_m_Clippers_6(),
	RectMask2D_t1156185964::get_offset_of_m_LastClipRectCanvasSpace_7(),
	RectMask2D_t1156185964::get_offset_of_m_LastValidClipRect_8(),
	RectMask2D_t1156185964::get_offset_of_m_ForceClip_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (Scrollbar_t3248359358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[11] = 
{
	Scrollbar_t3248359358::get_offset_of_m_HandleRect_16(),
	Scrollbar_t3248359358::get_offset_of_m_Direction_17(),
	Scrollbar_t3248359358::get_offset_of_m_Value_18(),
	Scrollbar_t3248359358::get_offset_of_m_Size_19(),
	Scrollbar_t3248359358::get_offset_of_m_NumberOfSteps_20(),
	Scrollbar_t3248359358::get_offset_of_m_OnValueChanged_21(),
	Scrollbar_t3248359358::get_offset_of_m_ContainerRect_22(),
	Scrollbar_t3248359358::get_offset_of_m_Offset_23(),
	Scrollbar_t3248359358::get_offset_of_m_Tracker_24(),
	Scrollbar_t3248359358::get_offset_of_m_PointerDownRepeat_25(),
	Scrollbar_t3248359358::get_offset_of_isPointerDownAndNotDragging_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (Direction_t3696775921)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1605[5] = 
{
	Direction_t3696775921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (ScrollEvent_t1794825321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (Axis_t2427050347)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1607[3] = 
{
	Axis_t2427050347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (U3CClickRepeatU3Ec__Iterator0_t4156771994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[5] = 
{
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_U24this_1(),
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_U24current_2(),
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_U24disposing_3(),
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (ScrollRect_t1199013257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[36] = 
{
	ScrollRect_t1199013257::get_offset_of_m_Content_2(),
	ScrollRect_t1199013257::get_offset_of_m_Horizontal_3(),
	ScrollRect_t1199013257::get_offset_of_m_Vertical_4(),
	ScrollRect_t1199013257::get_offset_of_m_MovementType_5(),
	ScrollRect_t1199013257::get_offset_of_m_Elasticity_6(),
	ScrollRect_t1199013257::get_offset_of_m_Inertia_7(),
	ScrollRect_t1199013257::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t1199013257::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t1199013257::get_offset_of_m_Viewport_10(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t1199013257::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t1199013257::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t1199013257::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t1199013257::get_offset_of_m_ViewRect_20(),
	ScrollRect_t1199013257::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t1199013257::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t1199013257::get_offset_of_m_Velocity_23(),
	ScrollRect_t1199013257::get_offset_of_m_Dragging_24(),
	ScrollRect_t1199013257::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t1199013257::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t1199013257::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t1199013257::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t1199013257::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t1199013257::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t1199013257::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t1199013257::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t1199013257::get_offset_of_m_Rect_33(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t1199013257::get_offset_of_m_Tracker_36(),
	ScrollRect_t1199013257::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (MovementType_t905360158)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1610[4] = 
{
	MovementType_t905360158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (ScrollbarVisibility_t3834843475)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1611[4] = 
{
	ScrollbarVisibility_t3834843475::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (ScrollRectEvent_t3529018992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (Selectable_t1490392188), -1, sizeof(Selectable_t1490392188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1613[14] = 
{
	Selectable_t1490392188_StaticFields::get_offset_of_s_List_2(),
	Selectable_t1490392188::get_offset_of_m_Navigation_3(),
	Selectable_t1490392188::get_offset_of_m_Transition_4(),
	Selectable_t1490392188::get_offset_of_m_Colors_5(),
	Selectable_t1490392188::get_offset_of_m_SpriteState_6(),
	Selectable_t1490392188::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t1490392188::get_offset_of_m_Interactable_8(),
	Selectable_t1490392188::get_offset_of_m_TargetGraphic_9(),
	Selectable_t1490392188::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t1490392188::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t1490392188::get_offset_of_U3CisPointerInsideU3Ek__BackingField_12(),
	Selectable_t1490392188::get_offset_of_U3CisPointerDownU3Ek__BackingField_13(),
	Selectable_t1490392188::get_offset_of_U3ChasSelectionU3Ek__BackingField_14(),
	Selectable_t1490392188::get_offset_of_m_CanvasGroupCache_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (Transition_t605142169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1614[5] = 
{
	Transition_t605142169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (SelectionState_t3187567897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1615[5] = 
{
	SelectionState_t3187567897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (SetPropertyUtility_t4019374597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (Slider_t297367283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[15] = 
{
	Slider_t297367283::get_offset_of_m_FillRect_16(),
	Slider_t297367283::get_offset_of_m_HandleRect_17(),
	Slider_t297367283::get_offset_of_m_Direction_18(),
	Slider_t297367283::get_offset_of_m_MinValue_19(),
	Slider_t297367283::get_offset_of_m_MaxValue_20(),
	Slider_t297367283::get_offset_of_m_WholeNumbers_21(),
	Slider_t297367283::get_offset_of_m_Value_22(),
	Slider_t297367283::get_offset_of_m_OnValueChanged_23(),
	Slider_t297367283::get_offset_of_m_FillImage_24(),
	Slider_t297367283::get_offset_of_m_FillTransform_25(),
	Slider_t297367283::get_offset_of_m_FillContainerRect_26(),
	Slider_t297367283::get_offset_of_m_HandleTransform_27(),
	Slider_t297367283::get_offset_of_m_HandleContainerRect_28(),
	Slider_t297367283::get_offset_of_m_Offset_29(),
	Slider_t297367283::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (Direction_t1525323322)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1618[5] = 
{
	Direction_t1525323322::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (SliderEvent_t2111116400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (Axis_t375128448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1620[3] = 
{
	Axis_t375128448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (SpriteState_t1353336012)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1621[3] = 
{
	SpriteState_t1353336012::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (StencilMaterial_t1630303189), -1, sizeof(StencilMaterial_t1630303189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1622[1] = 
{
	StencilMaterial_t1630303189_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (MatEntry_t3157325053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1623[10] = 
{
	MatEntry_t3157325053::get_offset_of_baseMat_0(),
	MatEntry_t3157325053::get_offset_of_customMat_1(),
	MatEntry_t3157325053::get_offset_of_count_2(),
	MatEntry_t3157325053::get_offset_of_stencilId_3(),
	MatEntry_t3157325053::get_offset_of_operation_4(),
	MatEntry_t3157325053::get_offset_of_compareFunction_5(),
	MatEntry_t3157325053::get_offset_of_readMask_6(),
	MatEntry_t3157325053::get_offset_of_writeMask_7(),
	MatEntry_t3157325053::get_offset_of_useAlphaClip_8(),
	MatEntry_t3157325053::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (Text_t356221433), -1, sizeof(Text_t356221433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1624[7] = 
{
	Text_t356221433::get_offset_of_m_FontData_28(),
	Text_t356221433::get_offset_of_m_Text_29(),
	Text_t356221433::get_offset_of_m_TextCache_30(),
	Text_t356221433::get_offset_of_m_TextCacheForLayout_31(),
	Text_t356221433_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t356221433::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t356221433::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1625[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1626[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1628[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1629[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1633[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1635[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1636[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1637[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1638[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1639[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1640[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1641[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1643[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1644[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1645[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1656[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1657[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1660[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1663[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1668[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1674[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1678[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (GUIUtils_t2318875170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (MGR_Anim_t3221826994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[7] = 
{
	MGR_Anim_t3221826994::get_offset_of_myGO_2(),
	MGR_Anim_t3221826994::get_offset_of_myAnimator_3(),
	MGR_Anim_t3221826994::get_offset_of_entry_4(),
	MGR_Anim_t3221826994::get_offset_of_myCloud_5(),
	MGR_Anim_t3221826994::get_offset_of_currPLItem_6(),
	MGR_Anim_t3221826994::get_offset_of_currHashID_7(),
	MGR_Anim_t3221826994::get_offset_of_myhashPL_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (U3CPlayU3Ec__Iterator0_t2856749856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[7] = 
{
	U3CPlayU3Ec__Iterator0_t2856749856::get_offset_of_U3CcurrInfoU3E__0_0(),
	U3CPlayU3Ec__Iterator0_t2856749856::get_offset_of_U3CiU3E__1_1(),
	U3CPlayU3Ec__Iterator0_t2856749856::get_offset_of_U3CtimeU3E__2_2(),
	U3CPlayU3Ec__Iterator0_t2856749856::get_offset_of_U24this_3(),
	U3CPlayU3Ec__Iterator0_t2856749856::get_offset_of_U24current_4(),
	U3CPlayU3Ec__Iterator0_t2856749856::get_offset_of_U24disposing_5(),
	U3CPlayU3Ec__Iterator0_t2856749856::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (Entry_t779635924)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1685[13] = 
{
	Entry_t779635924::get_offset_of_coach_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_trainee_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_playlistName_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_currentPlaylist_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_playlists_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_coaches_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_trainees_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_hashString_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_pSize_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_cSize_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_tSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_hSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t779635924::get_offset_of_numOfPlaylists_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (MGR_Cloud_t726529104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[6] = 
{
	MGR_Cloud_t726529104::get_offset_of_myEntry_2(),
	MGR_Cloud_t726529104::get_offset_of_numOfClips_3(),
	MGR_Cloud_t726529104::get_offset_of_currClip_4(),
	MGR_Cloud_t726529104::get_offset_of_currClipName_5(),
	MGR_Cloud_t726529104::get_offset_of_traineeRecords_6(),
	MGR_Cloud_t726529104::get_offset_of_anims_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (U3CPlayU3Ec__Iterator0_t1215233438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[7] = 
{
	U3CPlayU3Ec__Iterator0_t1215233438::get_offset_of_U3CcurrInfoU3E__0_0(),
	U3CPlayU3Ec__Iterator0_t1215233438::get_offset_of_U3CiU3E__1_1(),
	U3CPlayU3Ec__Iterator0_t1215233438::get_offset_of_U3CtimeU3E__2_2(),
	U3CPlayU3Ec__Iterator0_t1215233438::get_offset_of_U24this_3(),
	U3CPlayU3Ec__Iterator0_t1215233438::get_offset_of_U24current_4(),
	U3CPlayU3Ec__Iterator0_t1215233438::get_offset_of_U24disposing_5(),
	U3CPlayU3Ec__Iterator0_t1215233438::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (U3CreadCoachDBU3Ec__Iterator1_t3826372699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1688[7] = 
{
	U3CreadCoachDBU3Ec__Iterator1_t3826372699::get_offset_of_U3CurlU3E__0_0(),
	U3CreadCoachDBU3Ec__Iterator1_t3826372699::get_offset_of_U3Ccu_getU3E__1_1(),
	U3CreadCoachDBU3Ec__Iterator1_t3826372699::get_offset_of_U3CdataU3E__2_2(),
	U3CreadCoachDBU3Ec__Iterator1_t3826372699::get_offset_of_U24this_3(),
	U3CreadCoachDBU3Ec__Iterator1_t3826372699::get_offset_of_U24current_4(),
	U3CreadCoachDBU3Ec__Iterator1_t3826372699::get_offset_of_U24disposing_5(),
	U3CreadCoachDBU3Ec__Iterator1_t3826372699::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (U3CreadTraineeDBU3Ec__Iterator2_t81797702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1689[8] = 
{
	U3CreadTraineeDBU3Ec__Iterator2_t81797702::get_offset_of_c_0(),
	U3CreadTraineeDBU3Ec__Iterator2_t81797702::get_offset_of_U3CurlU3E__0_1(),
	U3CreadTraineeDBU3Ec__Iterator2_t81797702::get_offset_of_U3Ccu_getU3E__1_2(),
	U3CreadTraineeDBU3Ec__Iterator2_t81797702::get_offset_of_U3CdataU3E__2_3(),
	U3CreadTraineeDBU3Ec__Iterator2_t81797702::get_offset_of_U24this_4(),
	U3CreadTraineeDBU3Ec__Iterator2_t81797702::get_offset_of_U24current_5(),
	U3CreadTraineeDBU3Ec__Iterator2_t81797702::get_offset_of_U24disposing_6(),
	U3CreadTraineeDBU3Ec__Iterator2_t81797702::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (U3CreadPLDBU3Ec__Iterator3_t728369779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[8] = 
{
	U3CreadPLDBU3Ec__Iterator3_t728369779::get_offset_of_t_0(),
	U3CreadPLDBU3Ec__Iterator3_t728369779::get_offset_of_U3CurlU3E__0_1(),
	U3CreadPLDBU3Ec__Iterator3_t728369779::get_offset_of_U3Ccu_getU3E__1_2(),
	U3CreadPLDBU3Ec__Iterator3_t728369779::get_offset_of_U3CdataU3E__2_3(),
	U3CreadPLDBU3Ec__Iterator3_t728369779::get_offset_of_U24this_4(),
	U3CreadPLDBU3Ec__Iterator3_t728369779::get_offset_of_U24current_5(),
	U3CreadPLDBU3Ec__Iterator3_t728369779::get_offset_of_U24disposing_6(),
	U3CreadPLDBU3Ec__Iterator3_t728369779::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (U3CreadHashDBU3Ec__Iterator4_t2884699250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[8] = 
{
	U3CreadHashDBU3Ec__Iterator4_t2884699250::get_offset_of_p_0(),
	U3CreadHashDBU3Ec__Iterator4_t2884699250::get_offset_of_U3CurlU3E__0_1(),
	U3CreadHashDBU3Ec__Iterator4_t2884699250::get_offset_of_U3Ccu_getU3E__1_2(),
	U3CreadHashDBU3Ec__Iterator4_t2884699250::get_offset_of_U3CdataU3E__2_3(),
	U3CreadHashDBU3Ec__Iterator4_t2884699250::get_offset_of_U24this_4(),
	U3CreadHashDBU3Ec__Iterator4_t2884699250::get_offset_of_U24current_5(),
	U3CreadHashDBU3Ec__Iterator4_t2884699250::get_offset_of_U24disposing_6(),
	U3CreadHashDBU3Ec__Iterator4_t2884699250::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (MGR_CreateMenu_t817873244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[4] = 
{
	MGR_CreateMenu_t817873244::get_offset_of_clipNames_2(),
	MGR_CreateMenu_t817873244::get_offset_of_myGO_3(),
	MGR_CreateMenu_t817873244::get_offset_of_myAnimator_4(),
	MGR_CreateMenu_t817873244::get_offset_of_playlistItems_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (MGR_GUI_t2229806790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1693[24] = 
{
	MGR_GUI_t2229806790::get_offset_of_coachSelected_2(),
	MGR_GUI_t2229806790::get_offset_of_traineeSelected_3(),
	MGR_GUI_t2229806790::get_offset_of_playlistSelected_4(),
	MGR_GUI_t2229806790::get_offset_of_Input_Coach_5(),
	MGR_GUI_t2229806790::get_offset_of_Input_Trainee_6(),
	MGR_GUI_t2229806790::get_offset_of_Input_Playlist_7(),
	MGR_GUI_t2229806790::get_offset_of_coach_8(),
	MGR_GUI_t2229806790::get_offset_of_trainee_9(),
	MGR_GUI_t2229806790::get_offset_of_playlist_10(),
	MGR_GUI_t2229806790::get_offset_of_i_coach_11(),
	MGR_GUI_t2229806790::get_offset_of_i_trainee_12(),
	MGR_GUI_t2229806790::get_offset_of_i_playlist_13(),
	MGR_GUI_t2229806790::get_offset_of_clipNames_14(),
	MGR_GUI_t2229806790::get_offset_of_hlist_15(),
	MGR_GUI_t2229806790::get_offset_of_DictOfAnims_16(),
	MGR_GUI_t2229806790::get_offset_of_myCloud_17(),
	MGR_GUI_t2229806790::get_offset_of_myGO_18(),
	MGR_GUI_t2229806790::get_offset_of_myCanvas_19(),
	MGR_GUI_t2229806790::get_offset_of_myAnimator_20(),
	MGR_GUI_t2229806790::get_offset_of_coachScrollPosition_21(),
	MGR_GUI_t2229806790::get_offset_of_traineeScrollPosition_22(),
	MGR_GUI_t2229806790::get_offset_of_plScrollPosition_23(),
	MGR_GUI_t2229806790::get_offset_of_hashScrollPosition_24(),
	MGR_GUI_t2229806790::get_offset_of_count_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (MGR_HLGUI_t3333927834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[21] = 
{
	MGR_HLGUI_t3333927834::get_offset_of_Input_Coach_2(),
	MGR_HLGUI_t3333927834::get_offset_of_Input_Trainee_3(),
	MGR_HLGUI_t3333927834::get_offset_of_Input_Playlist_4(),
	MGR_HLGUI_t3333927834::get_offset_of_coach_5(),
	MGR_HLGUI_t3333927834::get_offset_of_trainee_6(),
	MGR_HLGUI_t3333927834::get_offset_of_playlist_7(),
	MGR_HLGUI_t3333927834::get_offset_of_i_coach_8(),
	MGR_HLGUI_t3333927834::get_offset_of_i_trainee_9(),
	MGR_HLGUI_t3333927834::get_offset_of_i_playlist_10(),
	MGR_HLGUI_t3333927834::get_offset_of_clipNames_11(),
	MGR_HLGUI_t3333927834::get_offset_of_DictOfAnims_12(),
	MGR_HLGUI_t3333927834::get_offset_of_myCloud_13(),
	MGR_HLGUI_t3333927834::get_offset_of_myAnim_14(),
	MGR_HLGUI_t3333927834::get_offset_of_myGO_15(),
	MGR_HLGUI_t3333927834::get_offset_of_myCanvas_16(),
	MGR_HLGUI_t3333927834::get_offset_of_myAnimator_17(),
	MGR_HLGUI_t3333927834::get_offset_of_coachScrollPosition_18(),
	MGR_HLGUI_t3333927834::get_offset_of_traineeScrollPosition_19(),
	MGR_HLGUI_t3333927834::get_offset_of_plScrollPosition_20(),
	MGR_HLGUI_t3333927834::get_offset_of_hashScrollPosition_21(),
	MGR_HLGUI_t3333927834::get_offset_of_count_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (MGR_HoloText_t1166120216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[3] = 
{
	MGR_HoloText_t1166120216::get_offset_of_playlistText_2(),
	MGR_HoloText_t1166120216::get_offset_of_myCloud_3(),
	MGR_HoloText_t1166120216::get_offset_of_updateText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (MGR_Menus_t511463637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[9] = 
{
	MGR_Menus_t511463637::get_offset_of_currentstate_2(),
	MGR_Menus_t511463637::get_offset_of_myCanvas_3(),
	MGR_Menus_t511463637::get_offset_of_startMenu_4(),
	MGR_Menus_t511463637::get_offset_of_createMenu_5(),
	MGR_Menus_t511463637::get_offset_of_readMenu_6(),
	MGR_Menus_t511463637::get_offset_of_startObjects_7(),
	MGR_Menus_t511463637::get_offset_of_createObjects_8(),
	MGR_Menus_t511463637::get_offset_of_readObjects_9(),
	MGR_Menus_t511463637::get_offset_of_player_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (MenuStates_t1946331629)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1697[4] = 
{
	MenuStates_t1946331629::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (SetAnimationInAnimator_t2420164504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[2] = 
{
	SetAnimationInAnimator_t2420164504::get_offset_of_animator_2(),
	SetAnimationInAnimator_t2420164504::get_offset_of_currentAnimNum_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
