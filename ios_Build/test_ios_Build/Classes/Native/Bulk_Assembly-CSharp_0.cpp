﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AssemblyCSharp.GUIUtils
struct GUIUtils_t2318875170;
// MGR_Anim
struct MGR_Anim_t3221826994;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// System.Object
struct Il2CppObject;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// MGR_Anim/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t2856749856;
// MGR_Cloud/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t1215233438;
// MGR_Cloud/<readCoachDB>c__Iterator1
struct U3CreadCoachDBU3Ec__Iterator1_t3826372699;
// MGR_Cloud/<readHashDB>c__Iterator4
struct U3CreadHashDBU3Ec__Iterator4_t2884699250;
// MGR_Cloud/<readPLDB>c__Iterator3
struct U3CreadPLDBU3Ec__Iterator3_t728369779;
// MGR_Cloud/<readTraineeDB>c__Iterator2
struct U3CreadTraineeDBU3Ec__Iterator2_t81797702;
// MGR_CreateMenu
struct MGR_CreateMenu_t817873244;
// MGR_GUI
struct MGR_GUI_t2229806790;
// UnityEngine.Canvas
struct Canvas_t209405766;
// MGR_HLGUI
struct MGR_HLGUI_t3333927834;
// MGR_HoloText
struct MGR_HoloText_t1166120216;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// MGR_Menus
struct MGR_Menus_t511463637;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// SetAnimationInAnimator
struct SetAnimationInAnimator_t2420164504;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_GUIUtils2318875170.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_GUIUtils2318875170MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_Entry779635924.h"
#include "AssemblyU2DCSharp_Entry779635924MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_MGR_Anim3221826994.h"
#include "AssemblyU2DCSharp_MGR_Anim3221826994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_MGR_Cloud726529104.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator02856749856MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator02856749856.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_MGR_Cloud726529104MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CPlayU3Ec__Iterator01215233438MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CPlayU3Ec__Iterator01215233438.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadCoachDBU3Ec__It3826372699MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadCoachDBU3Ec__It3826372699.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadTraineeDBU3Ec__It81797702MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadTraineeDBU3Ec__It81797702.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadPLDBU3Ec__Iterat728369779MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadPLDBU3Ec__Iterat728369779.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadHashDBU3Ec__Ite2884699250MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadHashDBU3Ec__Ite2884699250.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu817873244.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu817873244MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_GUI2229806790.h"
#include "AssemblyU2DCSharp_MGR_GUI2229806790MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_HLGUI3333927834.h"
#include "AssemblyU2DCSharp_MGR_HLGUI3333927834MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_HoloText1166120216.h"
#include "AssemblyU2DCSharp_MGR_HoloText1166120216MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Menus511463637.h"
#include "AssemblyU2DCSharp_MGR_Menus511463637MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "AssemblyU2DCSharp_MGR_Menus_MenuStates1946331629.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharp_MGR_Menus_MenuStates1946331629MethodDeclarations.h"
#include "AssemblyU2DCSharp_SetAnimationInAnimator2420164504.h"
#include "AssemblyU2DCSharp_SetAnimationInAnimator2420164504MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MGR_Cloud>()
#define GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(__this, method) ((  MGR_Cloud_t726529104 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MGR_Anim>()
#define Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191(__this, method) ((  MGR_Anim_t3221826994 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, method) ((  Canvas_t209405766 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MGR_Anim>()
#define GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355(__this, method) ((  MGR_Anim_t3221826994 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
#define Component_GetComponent_TisTextMesh_t1641806576_m4177416886(__this, method) ((  TextMesh_t1641806576 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MGR_Cloud>()
#define Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677(__this, method) ((  MGR_Cloud_t726529104 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t257310565_m1312615893(__this, method) ((  Renderer_t257310565 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AssemblyCSharp.GUIUtils::.ctor()
extern "C"  void GUIUtils__ctor_m880850310 (GUIUtils_t2318875170 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: Entry
extern "C" void Entry_t779635924_marshal_pinvoke(const Entry_t779635924& unmarshaled, Entry_t779635924_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
extern "C" void Entry_t779635924_marshal_pinvoke_back(const Entry_t779635924_marshaled_pinvoke& marshaled, Entry_t779635924& unmarshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
// Conversion method for clean up from marshalling of: Entry
extern "C" void Entry_t779635924_marshal_pinvoke_cleanup(Entry_t779635924_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Entry
extern "C" void Entry_t779635924_marshal_com(const Entry_t779635924& unmarshaled, Entry_t779635924_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
extern "C" void Entry_t779635924_marshal_com_back(const Entry_t779635924_marshaled_com& marshaled, Entry_t779635924& unmarshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
// Conversion method for clean up from marshalling of: Entry
extern "C" void Entry_t779635924_marshal_com_cleanup(Entry_t779635924_marshaled_com& marshaled)
{
}
// System.Void MGR_Anim::.ctor()
extern "C"  void MGR_Anim__ctor_m915439051 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	{
		__this->set_currPLItem_6(0);
		__this->set_currHashID_7(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Anim::Start()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const uint32_t MGR_Anim_Start_m1643617151_MetadataUsageId;
extern "C"  void MGR_Anim_Start_m1643617151 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_Start_m1643617151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_myGO_2();
		NullCheck(L_0);
		MGR_Cloud_t726529104 * L_1 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_0, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_5(L_1);
		GameObject_t1756533147 * L_2 = __this->get_myGO_2();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_3(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_4->set_playlists_4(L_5);
		Entry_t779635924 * L_6 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_7 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_7, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_6->set_currentPlaylist_3(L_7);
		Entry_t779635924 * L_8 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_9 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_9, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_8->set_trainees_6(L_9);
		Entry_t779635924 * L_10 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_11 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_11, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_10->set_coaches_5(L_11);
		Entry_t779635924 * L_12 = __this->get_address_of_entry_4();
		List_1_t1440998580 * L_13 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_13, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		L_12->set_hashString_7(L_13);
		GameObject_t1756533147 * L_14 = __this->get_myGO_2();
		NullCheck(L_14);
		Animator_t69676727 * L_15 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_14, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_3(L_15);
		return;
	}
}
// System.Void MGR_Anim::setEntry(Entry,UnityEngine.Animator,System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m555649161_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1722794520;
extern const uint32_t MGR_Anim_setEntry_m3451898502_MetadataUsageId;
extern "C"  void MGR_Anim_setEntry_m3451898502 (MGR_Anim_t3221826994 * __this, Entry_t779635924  ___e0, Animator_t69676727 * ___a1, int32_t ___numOfClips2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_setEntry_m3451898502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Animator_t69676727 * L_0 = ___a1;
		__this->set_myAnimator_3(L_0);
		int32_t L_1 = ___numOfClips2;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1722794520, L_3, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Entry_t779635924 * L_5 = __this->get_address_of_entry_4();
		String_t* L_6 = (&___e0)->get_coach_0();
		L_5->set_coach_0(L_6);
		Entry_t779635924 * L_7 = __this->get_address_of_entry_4();
		String_t* L_8 = (&___e0)->get_trainee_1();
		L_7->set_trainee_1(L_8);
		MGR_Cloud_t726529104 * L_9 = __this->get_myCloud_5();
		NullCheck(L_9);
		Entry_t779635924 * L_10 = L_9->get_address_of_myEntry_2();
		int32_t L_11 = ___numOfClips2;
		L_10->set_hSize_11(L_11);
		int32_t L_12 = ___numOfClips2;
		List_1_t1440998580 * L_13 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m555649161(L_13, L_12, /*hidden argument*/List_1__ctor_m555649161_MethodInfo_var);
		__this->set_myhashPL_8(L_13);
		V_0 = 0;
		goto IL_00a0;
	}

IL_0065:
	{
		Entry_t779635924 * L_14 = __this->get_address_of_entry_4();
		List_1_t1440998580 * L_15 = L_14->get_hashString_7();
		List_1_t1440998580 * L_16 = (&___e0)->get_hashString_7();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = List_1_get_Item_m1921196075(L_16, L_17, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		NullCheck(L_15);
		List_1_Add_m2828939739(L_15, L_18, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		List_1_t1440998580 * L_19 = __this->get_myhashPL_8();
		List_1_t1440998580 * L_20 = (&___e0)->get_hashString_7();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		int32_t L_22 = List_1_get_Item_m1921196075(L_20, L_21, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		NullCheck(L_19);
		List_1_Add_m2828939739(L_19, L_22, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_24 = V_0;
		int32_t L_25 = ___numOfClips2;
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0065;
		}
	}
	{
		return;
	}
}
// System.Void MGR_Anim::playAnims()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3819797390;
extern const uint32_t MGR_Anim_playAnims_m3338700699_MetadataUsageId;
extern "C"  void MGR_Anim_playAnims_m3338700699 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_playAnims_m3338700699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_5();
		NullCheck(L_0);
		Entry_t779635924 * L_1 = L_0->get_address_of_myEntry_2();
		int32_t L_2 = L_1->get_hSize_11();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3819797390, L_4, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Il2CppObject * L_6 = MGR_Anim_Play_m3675863081(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MGR_Anim::Play()
extern Il2CppClass* U3CPlayU3Ec__Iterator0_t2856749856_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Anim_Play_m3675863081_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Anim_Play_m3675863081 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_Play_m3675863081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPlayU3Ec__Iterator0_t2856749856 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CPlayU3Ec__Iterator0_t2856749856 * L_0 = (U3CPlayU3Ec__Iterator0_t2856749856 *)il2cpp_codegen_object_new(U3CPlayU3Ec__Iterator0_t2856749856_il2cpp_TypeInfo_var);
		U3CPlayU3Ec__Iterator0__ctor_m1257570099(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayU3Ec__Iterator0_t2856749856 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CPlayU3Ec__Iterator0_t2856749856 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Void MGR_Anim::UpdateAnim(System.String,System.String)
extern "C"  void MGR_Anim_UpdateAnim_m248568853 (MGR_Anim_t3221826994 * __this, String_t* ___current0, String_t* ___next1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m1257570099 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Anim/<Play>c__Iterator0::MoveNext()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1955728068;
extern Il2CppCodeGenString* _stringLiteral3611362455;
extern Il2CppCodeGenString* _stringLiteral3611362452;
extern const uint32_t U3CPlayU3Ec__Iterator0_MoveNext_m4201796769_MetadataUsageId;
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m4201796769 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_MoveNext_m4201796769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0193;
		}
	}
	{
		goto IL_01dc;
	}

IL_0021:
	{
		MGR_Anim_t3221826994 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = L_2->get_myAnimator_3();
		NullCheck(L_3);
		AnimatorStateInfo_t2577870592  L_4 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_3, 0, /*hidden argument*/NULL);
		__this->set_U3CcurrInfoU3E__0_0(L_4);
		MGR_Anim_t3221826994 * L_5 = __this->get_U24this_3();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_currPLItem_6();
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_9 = __this->get_U24this_3();
		NullCheck(L_9);
		MGR_Cloud_t726529104 * L_10 = L_9->get_myCloud_5();
		NullCheck(L_10);
		Entry_t779635924 * L_11 = L_10->get_address_of_myEntry_2();
		int32_t L_12 = L_11->get_hSize_11();
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1955728068, L_14, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		__this->set_U3CiU3E__1_1(1);
		goto IL_01b5;
	}

IL_0083:
	{
		MGR_Anim_t3221826994 * L_16 = __this->get_U24this_3();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_currPLItem_6();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_20 = __this->get_U24this_3();
		NullCheck(L_20);
		int32_t L_21 = L_20->get_currPLItem_6();
		MGR_Anim_t3221826994 * L_22 = __this->get_U24this_3();
		NullCheck(L_22);
		MGR_Cloud_t726529104 * L_23 = L_22->get_myCloud_5();
		NullCheck(L_23);
		Entry_t779635924 * L_24 = L_23->get_address_of_myEntry_2();
		int32_t L_25 = L_24->get_hSize_11();
		if ((((int32_t)L_21) >= ((int32_t)L_25)))
		{
			goto IL_00cf;
		}
	}
	{
		MGR_Anim_t3221826994 * L_26 = __this->get_U24this_3();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_currPLItem_6();
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_00dd;
		}
	}

IL_00cf:
	{
		MGR_Anim_t3221826994 * L_28 = __this->get_U24this_3();
		NullCheck(L_28);
		L_28->set_currPLItem_6(0);
	}

IL_00dd:
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral3611362455, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_29 = __this->get_U24this_3();
		MGR_Anim_t3221826994 * L_30 = __this->get_U24this_3();
		NullCheck(L_30);
		MGR_Cloud_t726529104 * L_31 = L_30->get_myCloud_5();
		NullCheck(L_31);
		Entry_t779635924 * L_32 = L_31->get_address_of_myEntry_2();
		List_1_t1440998580 * L_33 = L_32->get_hashString_7();
		MGR_Anim_t3221826994 * L_34 = __this->get_U24this_3();
		NullCheck(L_34);
		int32_t L_35 = L_34->get_currPLItem_6();
		NullCheck(L_33);
		int32_t L_36 = List_1_get_Item_m1921196075(L_33, L_35, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		NullCheck(L_29);
		L_29->set_currHashID_7(L_36);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral3611362452, /*hidden argument*/NULL);
		AnimatorStateInfo_t2577870592 * L_37 = __this->get_address_of_U3CcurrInfoU3E__0_0();
		float L_38 = AnimatorStateInfo_get_length_m3151009408(L_37, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__2_2(L_38);
		MGR_Anim_t3221826994 * L_39 = __this->get_U24this_3();
		NullCheck(L_39);
		int32_t L_40 = L_39->get_currHashID_7();
		int32_t L_41 = L_40;
		Il2CppObject * L_42 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_41);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_43 = __this->get_U24this_3();
		NullCheck(L_43);
		Animator_t69676727 * L_44 = L_43->get_myAnimator_3();
		MGR_Anim_t3221826994 * L_45 = __this->get_U24this_3();
		NullCheck(L_45);
		int32_t L_46 = L_45->get_currHashID_7();
		NullCheck(L_44);
		Animator_CrossFade_m1425728227(L_44, L_46, (1.0f), 0, /*hidden argument*/NULL);
		float L_47 = __this->get_U3CtimeU3E__2_2();
		WaitForSeconds_t3839502067 * L_48 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_48, ((float)((float)L_47-(float)(1.0f))), /*hidden argument*/NULL);
		__this->set_U24current_4(L_48);
		bool L_49 = __this->get_U24disposing_5();
		if (L_49)
		{
			goto IL_018e;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_018e:
	{
		goto IL_01de;
	}

IL_0193:
	{
		MGR_Anim_t3221826994 * L_50 = __this->get_U24this_3();
		MGR_Anim_t3221826994 * L_51 = L_50;
		NullCheck(L_51);
		int32_t L_52 = L_51->get_currPLItem_6();
		NullCheck(L_51);
		L_51->set_currPLItem_6(((int32_t)((int32_t)L_52+(int32_t)1)));
		int32_t L_53 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)((int32_t)L_53+(int32_t)1)));
	}

IL_01b5:
	{
		int32_t L_54 = __this->get_U3CiU3E__1_1();
		MGR_Anim_t3221826994 * L_55 = __this->get_U24this_3();
		NullCheck(L_55);
		MGR_Cloud_t726529104 * L_56 = L_55->get_myCloud_5();
		NullCheck(L_56);
		Entry_t779635924 * L_57 = L_56->get_address_of_myEntry_2();
		int32_t L_58 = L_57->get_hSize_11();
		if ((((int32_t)L_54) < ((int32_t)L_58)))
		{
			goto IL_0083;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_01dc:
	{
		return (bool)0;
	}

IL_01de:
	{
		return (bool)1;
	}
}
// System.Object MGR_Anim/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m504323601 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Anim/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2596265641 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m2829949778 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_Reset_m1939598112_MetadataUsageId;
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m1939598112 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_Reset_m1939598112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud::.ctor()
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t MGR_Cloud__ctor_m1683574021_MetadataUsageId;
extern "C"  void MGR_Cloud__ctor_m1683574021 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud__ctor_m1683574021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_currClipName_5(_stringLiteral371857150);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Cloud::Awake()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m28427054_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const uint32_t MGR_Cloud_Awake_m4069120028_MetadataUsageId;
extern "C"  void MGR_Cloud_Awake_m4069120028 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Awake_m4069120028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Anim_t3221826994 * L_0 = Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191(__this, /*hidden argument*/Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191_MethodInfo_var);
		__this->set_anims_7(L_0);
		Dictionary_2_t3943999495 * L_1 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m28427054(L_1, /*hidden argument*/Dictionary_2__ctor_m28427054_MethodInfo_var);
		__this->set_traineeRecords_6(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_3 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_3, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_2->set_playlists_4(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_4->set_currentPlaylist_3(L_5);
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_7 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_7, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_6->set_trainees_6(L_7);
		Entry_t779635924 * L_8 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_9 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_9, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_8->set_coaches_5(L_9);
		Entry_t779635924 * L_10 = __this->get_address_of_myEntry_2();
		List_1_t1440998580 * L_11 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_11, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		L_10->set_hashString_7(L_11);
		return;
	}
}
// System.Void MGR_Cloud::Start()
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t MGR_Cloud_Start_m899893525_MetadataUsageId;
extern "C"  void MGR_Cloud_Start_m899893525 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Start_m899893525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		L_0->set_coach_0(_stringLiteral371857150);
		Entry_t779635924 * L_1 = __this->get_address_of_myEntry_2();
		L_1->set_trainee_1(_stringLiteral371857150);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		L_2->set_playlistName_2(_stringLiteral371857150);
		Entry_t779635924 * L_3 = __this->get_address_of_myEntry_2();
		L_3->set_cSize_9(0);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		L_4->set_pSize_8(0);
		Entry_t779635924 * L_5 = __this->get_address_of_myEntry_2();
		L_5->set_tSize_10(0);
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		L_6->set_hSize_11(0);
		Entry_t779635924 * L_7 = __this->get_address_of_myEntry_2();
		L_7->set_numOfPlaylists_12(0);
		MGR_Cloud_startReadDB_m986115961(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Cloud::Update()
extern "C"  void MGR_Cloud_Update_m1977723834 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_Cloud::setEntry(System.String,System.String,System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m1170681985_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3686231158_MethodInfo_var;
extern const uint32_t MGR_Cloud_setEntry_m1738080337_MetadataUsageId;
extern "C"  void MGR_Cloud_setEntry_m1738080337 (MGR_Cloud_t726529104 * __this, String_t* ___cName0, String_t* ___tName1, String_t* ___pName2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_setEntry_m1738080337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		String_t* L_1 = ___pName2;
		L_0->set_playlistName_2(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = ___cName0;
		L_2->set_coach_0(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		String_t* L_5 = ___tName1;
		L_4->set_trainee_1(L_5);
		Dictionary_2_t3943999495 * L_6 = __this->get_traineeRecords_6();
		String_t* L_7 = ___tName1;
		NullCheck(L_6);
		bool L_8 = Dictionary_2_ContainsKey_m1170681985(L_6, L_7, /*hidden argument*/Dictionary_2_ContainsKey_m1170681985_MethodInfo_var);
		if (L_8)
		{
			goto IL_0058;
		}
	}
	{
		Dictionary_2_t3943999495 * L_9 = __this->get_traineeRecords_6();
		String_t* L_10 = ___tName1;
		String_t* L_11 = ___cName0;
		NullCheck(L_9);
		Dictionary_2_Add_m3686231158(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		Entry_t779635924 * L_12 = __this->get_address_of_myEntry_2();
		Entry_t779635924 * L_13 = L_12;
		int32_t L_14 = L_13->get_cSize_9();
		L_13->set_cSize_9(((int32_t)((int32_t)L_14+(int32_t)1)));
	}

IL_0058:
	{
		return;
	}
}
// System.Void MGR_Cloud::initFB(System.String,System.String,System.String)
extern "C"  void MGR_Cloud_initFB_m761421013 (MGR_Cloud_t726529104 * __this, String_t* ___cName0, String_t* ___tName1, String_t* ___pName2, const MethodInfo* method)
{
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		String_t* L_1 = ___pName2;
		L_0->set_playlistName_2(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = ___cName0;
		L_2->set_coach_0(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		String_t* L_5 = ___tName1;
		L_4->set_trainee_1(L_5);
		return;
	}
}
// System.Collections.IEnumerator MGR_Cloud::Play()
extern Il2CppClass* U3CPlayU3Ec__Iterator0_t1215233438_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_Play_m771278319_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_Play_m771278319 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Play_m771278319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPlayU3Ec__Iterator0_t1215233438 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CPlayU3Ec__Iterator0_t1215233438 * L_0 = (U3CPlayU3Ec__Iterator0_t1215233438 *)il2cpp_codegen_object_new(U3CPlayU3Ec__Iterator0_t1215233438_il2cpp_TypeInfo_var);
		U3CPlayU3Ec__Iterator0__ctor_m4064257613(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayU3Ec__Iterator0_t1215233438 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CPlayU3Ec__Iterator0_t1215233438 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readCoachDB()
extern Il2CppClass* U3CreadCoachDBU3Ec__Iterator1_t3826372699_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readCoachDB_m3748347533_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readCoachDB_m3748347533 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readCoachDB_m3748347533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadCoachDBU3Ec__Iterator1_t3826372699 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CreadCoachDBU3Ec__Iterator1_t3826372699 * L_0 = (U3CreadCoachDBU3Ec__Iterator1_t3826372699 *)il2cpp_codegen_object_new(U3CreadCoachDBU3Ec__Iterator1_t3826372699_il2cpp_TypeInfo_var);
		U3CreadCoachDBU3Ec__Iterator1__ctor_m2869939524(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadCoachDBU3Ec__Iterator1_t3826372699 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CreadCoachDBU3Ec__Iterator1_t3826372699 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readTraineeDB(System.String)
extern Il2CppClass* U3CreadTraineeDBU3Ec__Iterator2_t81797702_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readTraineeDB_m1908492693_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readTraineeDB_m1908492693 (MGR_Cloud_t726529104 * __this, String_t* ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readTraineeDB_m1908492693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadTraineeDBU3Ec__Iterator2_t81797702 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CreadTraineeDBU3Ec__Iterator2_t81797702 * L_0 = (U3CreadTraineeDBU3Ec__Iterator2_t81797702 *)il2cpp_codegen_object_new(U3CreadTraineeDBU3Ec__Iterator2_t81797702_il2cpp_TypeInfo_var);
		U3CreadTraineeDBU3Ec__Iterator2__ctor_m1417225815(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadTraineeDBU3Ec__Iterator2_t81797702 * L_1 = V_0;
		String_t* L_2 = ___c0;
		NullCheck(L_1);
		L_1->set_c_0(L_2);
		U3CreadTraineeDBU3Ec__Iterator2_t81797702 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadTraineeDBU3Ec__Iterator2_t81797702 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readPLDB(System.String)
extern Il2CppClass* U3CreadPLDBU3Ec__Iterator3_t728369779_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readPLDB_m994522313_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readPLDB_m994522313 (MGR_Cloud_t726529104 * __this, String_t* ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readPLDB_m994522313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadPLDBU3Ec__Iterator3_t728369779 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CreadPLDBU3Ec__Iterator3_t728369779 * L_0 = (U3CreadPLDBU3Ec__Iterator3_t728369779 *)il2cpp_codegen_object_new(U3CreadPLDBU3Ec__Iterator3_t728369779_il2cpp_TypeInfo_var);
		U3CreadPLDBU3Ec__Iterator3__ctor_m3324892502(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadPLDBU3Ec__Iterator3_t728369779 * L_1 = V_0;
		String_t* L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CreadPLDBU3Ec__Iterator3_t728369779 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadPLDBU3Ec__Iterator3_t728369779 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readHashDB(System.String)
extern Il2CppClass* U3CreadHashDBU3Ec__Iterator4_t2884699250_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readHashDB_m3218359951_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readHashDB_m3218359951 (MGR_Cloud_t726529104 * __this, String_t* ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readHashDB_m3218359951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadHashDBU3Ec__Iterator4_t2884699250 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CreadHashDBU3Ec__Iterator4_t2884699250 * L_0 = (U3CreadHashDBU3Ec__Iterator4_t2884699250 *)il2cpp_codegen_object_new(U3CreadHashDBU3Ec__Iterator4_t2884699250_il2cpp_TypeInfo_var);
		U3CreadHashDBU3Ec__Iterator4__ctor_m3255918799(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadHashDBU3Ec__Iterator4_t2884699250 * L_1 = V_0;
		String_t* L_2 = ___p0;
		NullCheck(L_1);
		L_1->set_p_0(L_2);
		U3CreadHashDBU3Ec__Iterator4_t2884699250 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadHashDBU3Ec__Iterator4_t2884699250 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Void MGR_Cloud::startReadDB()
extern "C"  void MGR_Cloud_startReadDB_m986115961 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = MGR_Cloud_readCoachDB_m3748347533(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Cloud::addPL(System.String,System.String,System.String,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1333684107;
extern Il2CppCodeGenString* _stringLiteral2032843076;
extern Il2CppCodeGenString* _stringLiteral439627463;
extern Il2CppCodeGenString* _stringLiteral947094105;
extern Il2CppCodeGenString* _stringLiteral545555276;
extern const uint32_t MGR_Cloud_addPL_m2038133962_MetadataUsageId;
extern "C"  void MGR_Cloud_addPL_m2038133962 (MGR_Cloud_t726529104 * __this, String_t* ___c0, String_t* ___t1, String_t* ___pl2, String_t* ___hs3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_addPL_m2038133962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	WWW_t2919945039 * V_2 = NULL;
	{
		V_0 = _stringLiteral1333684107;
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2032843076);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2032843076);
		StringU5BU5D_t1642385972* L_1 = L_0;
		String_t* L_2 = ___c0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral439627463);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral439627463);
		StringU5BU5D_t1642385972* L_4 = L_3;
		String_t* L_5 = ___t1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral947094105);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral947094105);
		StringU5BU5D_t1642385972* L_7 = L_6;
		String_t* L_8 = ___pl2;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_8);
		StringU5BU5D_t1642385972* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral545555276);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral545555276);
		StringU5BU5D_t1642385972* L_10 = L_9;
		String_t* L_11 = ___hs3;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m626692867(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13 = V_1;
		WWW_t2919945039 * L_14 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_14, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		return;
	}
}
// System.Void MGR_Cloud::addPL()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3287239316;
extern Il2CppCodeGenString* _stringLiteral2032843076;
extern Il2CppCodeGenString* _stringLiteral439627463;
extern Il2CppCodeGenString* _stringLiteral947094105;
extern Il2CppCodeGenString* _stringLiteral545555276;
extern const uint32_t MGR_Cloud_addPL_m817357320_MetadataUsageId;
extern "C"  void MGR_Cloud_addPL_m817357320 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_addPL_m817357320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	WWW_t2919945039 * V_2 = NULL;
	{
		V_0 = _stringLiteral3287239316;
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2032843076);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2032843076);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = L_2->get_coach_0();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral439627463);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral439627463);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		String_t* L_7 = L_6->get_trainee_1();
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral947094105);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral947094105);
		StringU5BU5D_t1642385972* L_9 = L_8;
		Entry_t779635924 * L_10 = __this->get_address_of_myEntry_2();
		String_t* L_11 = L_10->get_playlistName_2();
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_9;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral545555276);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral545555276);
		StringU5BU5D_t1642385972* L_13 = L_12;
		String_t* L_14 = V_0;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m626692867(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_15;
		String_t* L_16 = V_1;
		WWW_t2919945039 * L_17 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_17, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		return;
	}
}
// System.Void MGR_Cloud/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m4064257613 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<Play>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_MoveNext_m260193359_MetadataUsageId;
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m260193359 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_MoveNext_m260193359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0115;
		}
	}
	{
		goto IL_0159;
	}

IL_0021:
	{
		MGR_Cloud_t726529104 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		MGR_Anim_t3221826994 * L_3 = L_2->get_anims_7();
		NullCheck(L_3);
		Animator_t69676727 * L_4 = L_3->get_myAnimator_3();
		NullCheck(L_4);
		AnimatorStateInfo_t2577870592  L_5 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_4, 0, /*hidden argument*/NULL);
		__this->set_U3CcurrInfoU3E__0_0(L_5);
		__this->set_U3CiU3E__1_1(0);
		goto IL_0137;
	}

IL_004a:
	{
		MGR_Cloud_t726529104 * L_6 = __this->get_U24this_3();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_currClip_4();
		MGR_Cloud_t726529104 * L_8 = __this->get_U24this_3();
		NullCheck(L_8);
		Entry_t779635924 * L_9 = L_8->get_address_of_myEntry_2();
		int32_t L_10 = L_9->get_pSize_8();
		if ((((int32_t)L_7) >= ((int32_t)L_10)))
		{
			goto IL_007c;
		}
	}
	{
		MGR_Cloud_t726529104 * L_11 = __this->get_U24this_3();
		NullCheck(L_11);
		int32_t L_12 = L_11->get_currClip_4();
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_0088;
		}
	}

IL_007c:
	{
		MGR_Cloud_t726529104 * L_13 = __this->get_U24this_3();
		NullCheck(L_13);
		L_13->set_currClip_4(0);
	}

IL_0088:
	{
		MGR_Cloud_t726529104 * L_14 = __this->get_U24this_3();
		MGR_Cloud_t726529104 * L_15 = __this->get_U24this_3();
		NullCheck(L_15);
		Entry_t779635924 * L_16 = L_15->get_address_of_myEntry_2();
		List_1_t1398341365 * L_17 = L_16->get_currentPlaylist_3();
		MGR_Cloud_t726529104 * L_18 = __this->get_U24this_3();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_currClip_4();
		NullCheck(L_17);
		String_t* L_20 = List_1_get_Item_m1112119647(L_17, L_19, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_14);
		L_14->set_currClipName_5(L_20);
		AnimatorStateInfo_t2577870592 * L_21 = __this->get_address_of_U3CcurrInfoU3E__0_0();
		float L_22 = AnimatorStateInfo_get_length_m3151009408(L_21, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__2_2(L_22);
		MGR_Cloud_t726529104 * L_23 = __this->get_U24this_3();
		NullCheck(L_23);
		MGR_Anim_t3221826994 * L_24 = L_23->get_anims_7();
		NullCheck(L_24);
		Animator_t69676727 * L_25 = L_24->get_myAnimator_3();
		MGR_Cloud_t726529104 * L_26 = __this->get_U24this_3();
		NullCheck(L_26);
		String_t* L_27 = L_26->get_currClipName_5();
		NullCheck(L_25);
		Animator_CrossFade_m1558283744(L_25, L_27, (1.0f), 0, /*hidden argument*/NULL);
		float L_28 = __this->get_U3CtimeU3E__2_2();
		WaitForSeconds_t3839502067 * L_29 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_29, ((float)((float)L_28-(float)(0.5f))), /*hidden argument*/NULL);
		__this->set_U24current_4(L_29);
		bool L_30 = __this->get_U24disposing_5();
		if (L_30)
		{
			goto IL_0110;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0110:
	{
		goto IL_015b;
	}

IL_0115:
	{
		MGR_Cloud_t726529104 * L_31 = __this->get_U24this_3();
		MGR_Cloud_t726529104 * L_32 = L_31;
		NullCheck(L_32);
		int32_t L_33 = L_32->get_currClip_4();
		NullCheck(L_32);
		L_32->set_currClip_4(((int32_t)((int32_t)L_33+(int32_t)1)));
		int32_t L_34 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)((int32_t)L_34+(int32_t)1)));
	}

IL_0137:
	{
		int32_t L_35 = __this->get_U3CiU3E__1_1();
		MGR_Cloud_t726529104 * L_36 = __this->get_U24this_3();
		NullCheck(L_36);
		Entry_t779635924 * L_37 = L_36->get_address_of_myEntry_2();
		int32_t L_38 = L_37->get_pSize_8();
		if ((((int32_t)L_35) < ((int32_t)L_38)))
		{
			goto IL_004a;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_0159:
	{
		return (bool)0;
	}

IL_015b:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3924763223 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3219668319 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m2172260480 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_Cloud/<Play>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_Reset_m2344447862_MetadataUsageId;
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m2344447862 (U3CPlayU3Ec__Iterator0_t1215233438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_Reset_m2344447862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::.ctor()
extern "C"  void U3CreadCoachDBU3Ec__Iterator1__ctor_m2869939524 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readCoachDB>c__Iterator1::MoveNext()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1695502239;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern const uint32_t U3CreadCoachDBU3Ec__Iterator1_MoveNext_m540735720_MetadataUsageId;
extern "C"  bool U3CreadCoachDBU3Ec__Iterator1_MoveNext_m540735720 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadCoachDBU3Ec__Iterator1_MoveNext_m540735720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0069;
		}
	}
	{
		goto IL_012d;
	}

IL_0021:
	{
		int32_t L_2 = 1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1695502239, L_3, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_0(L_4);
		String_t* L_5 = __this->get_U3CurlU3E__0_0();
		WWW_t2919945039 * L_6 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_1(L_6);
		WWW_t2919945039 * L_7 = __this->get_U3Ccu_getU3E__1_1();
		__this->set_U24current_4(L_7);
		bool L_8 = __this->get_U24disposing_5();
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0064:
	{
		goto IL_012f;
	}

IL_0069:
	{
		WWW_t2919945039 * L_9 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m3092701216(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_009a;
		}
	}
	{
		WWW_t2919945039 * L_11 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_11);
		String_t* L_12 = WWW_get_error_m3092701216(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_12, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		goto IL_0126;
	}

IL_009a:
	{
		WWW_t2919945039 * L_14 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_text_m1558985139(L_14, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_2(L_15);
		String_t* L_16 = __this->get_U3CdataU3E__2_2();
		CharU5BU5D_t1328083999* L_17 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_16);
		StringU5BU5D_t1642385972* L_18 = String_Split_m3326265864(L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		MGR_Cloud_t726529104 * L_19 = __this->get_U24this_3();
		NullCheck(L_19);
		Entry_t779635924 * L_20 = L_19->get_address_of_myEntry_2();
		List_1_t1398341365 * L_21 = L_20->get_coaches_5();
		NullCheck(L_21);
		List_1_Clear_m2928955906(L_21, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_22 = __this->get_U24this_3();
		NullCheck(L_22);
		Entry_t779635924 * L_23 = L_22->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_24 = V_1;
		NullCheck(L_24);
		L_23->set_cSize_9(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_011a;
	}

IL_00f4:
	{
		MGR_Cloud_t726529104 * L_25 = __this->get_U24this_3();
		NullCheck(L_25);
		Entry_t779635924 * L_26 = L_25->get_address_of_myEntry_2();
		List_1_t1398341365 * L_27 = L_26->get_coaches_5();
		StringU5BU5D_t1642385972* L_28 = V_1;
		int32_t L_29 = V_2;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		String_t* L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_27);
		List_1_Add_m4061286785(L_27, L_31, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		StringU5BU5D_t1642385972* L_32 = V_1;
		int32_t L_33 = V_2;
		NullCheck(L_32);
		int32_t L_34 = L_33;
		String_t* L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		int32_t L_36 = V_2;
		V_2 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_37 = V_2;
		StringU5BU5D_t1642385972* L_38 = V_1;
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length))))-(int32_t)1)))))
		{
			goto IL_00f4;
		}
	}
	{
	}

IL_0126:
	{
		__this->set_U24PC_6((-1));
	}

IL_012d:
	{
		return (bool)0;
	}

IL_012f:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readCoachDB>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2325030518 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<readCoachDB>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m998574222 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::Dispose()
extern "C"  void U3CreadCoachDBU3Ec__Iterator1_Dispose_m3427768733 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadCoachDBU3Ec__Iterator1_Reset_m1753351231_MetadataUsageId;
extern "C"  void U3CreadCoachDBU3Ec__Iterator1_Reset_m1753351231 (U3CreadCoachDBU3Ec__Iterator1_t3826372699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadCoachDBU3Ec__Iterator1_Reset_m1753351231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator4::.ctor()
extern "C"  void U3CreadHashDBU3Ec__Iterator4__ctor_m3255918799 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readHashDB>c__Iterator4::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m3644677550_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3854425081;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral419949503;
extern const uint32_t U3CreadHashDBU3Ec__Iterator4_MoveNext_m2665498253_MetadataUsageId;
extern "C"  bool U3CreadHashDBU3Ec__Iterator4_MoveNext_m2665498253 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHashDBU3Ec__Iterator4_MoveNext_m2665498253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0091;
		}
	}
	{
		goto IL_016b;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral3854425081);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3854425081);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_p_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 4;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		String_t* L_11 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_12 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_12, L_11, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_12);
		WWW_t2919945039 * L_13 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_13);
		bool L_14 = __this->get_U24disposing_6();
		if (L_14)
		{
			goto IL_008c;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_008c:
	{
		goto IL_016d;
	}

IL_0091:
	{
		WWW_t2919945039 * L_15 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_15);
		String_t* L_16 = WWW_get_error_m3092701216(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00cd;
		}
	}
	{
		WWW_t2919945039 * L_17 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_17);
		String_t* L_18 = WWW_get_error_m3092701216(L_17, /*hidden argument*/NULL);
		String_t* L_19 = __this->get_U3CurlU3E__0_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral2285603187, L_18, _stringLiteral372029352, L_19, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_00cd:
	{
		WWW_t2919945039 * L_21 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_21);
		String_t* L_22 = WWW_get_text_m1558985139(L_21, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_22);
		String_t* L_23 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_24 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_23);
		StringU5BU5D_t1642385972* L_25 = String_Split_m3326265864(L_23, L_24, /*hidden argument*/NULL);
		V_1 = L_25;
		MGR_Cloud_t726529104 * L_26 = __this->get_U24this_4();
		NullCheck(L_26);
		Entry_t779635924 * L_27 = L_26->get_address_of_myEntry_2();
		List_1_t1440998580 * L_28 = L_27->get_hashString_7();
		NullCheck(L_28);
		List_1_Clear_m3644677550(L_28, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		MGR_Cloud_t726529104 * L_29 = __this->get_U24this_4();
		NullCheck(L_29);
		Entry_t779635924 * L_30 = L_29->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_31 = V_1;
		NullCheck(L_31);
		L_30->set_hSize_11((((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))));
		V_2 = 0;
		goto IL_015a;
	}

IL_0125:
	{
		MGR_Cloud_t726529104 * L_32 = __this->get_U24this_4();
		NullCheck(L_32);
		Entry_t779635924 * L_33 = L_32->get_address_of_myEntry_2();
		List_1_t1440998580 * L_34 = L_33->get_hashString_7();
		StringU5BU5D_t1642385972* L_35 = V_1;
		int32_t L_36 = V_2;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		String_t* L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_39 = Convert_ToInt32_m2593311249(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		NullCheck(L_34);
		List_1_Add_m2828939739(L_34, L_39, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		StringU5BU5D_t1642385972* L_40 = V_1;
		int32_t L_41 = V_2;
		NullCheck(L_40);
		int32_t L_42 = L_41;
		String_t* L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral419949503, L_43, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_2;
		V_2 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_015a:
	{
		int32_t L_46 = V_2;
		StringU5BU5D_t1642385972* L_47 = V_1;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0125;
		}
	}
	{
	}

IL_0164:
	{
		__this->set_U24PC_7((-1));
	}

IL_016b:
	{
		return (bool)0;
	}

IL_016d:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readHashDB>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m761012313 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<readHashDB>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3044320145 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator4::Dispose()
extern "C"  void U3CreadHashDBU3Ec__Iterator4_Dispose_m2996611800 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator4::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadHashDBU3Ec__Iterator4_Reset_m3436070926_MetadataUsageId;
extern "C"  void U3CreadHashDBU3Ec__Iterator4_Reset_m3436070926 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHashDBU3Ec__Iterator4_Reset_m3436070926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator3::.ctor()
extern "C"  void U3CreadPLDBU3Ec__Iterator3__ctor_m3324892502 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readPLDB>c__Iterator3::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral859641999;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern Il2CppCodeGenString* _stringLiteral2543474434;
extern Il2CppCodeGenString* _stringLiteral419949503;
extern const uint32_t U3CreadPLDBU3Ec__Iterator3_MoveNext_m741270346_MetadataUsageId;
extern "C"  bool U3CreadPLDBU3Ec__Iterator3_MoveNext_m741270346 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadPLDBU3Ec__Iterator3_MoveNext_m741270346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0091;
		}
	}
	{
		goto IL_0186;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral859641999);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral859641999);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_t_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 3;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		String_t* L_11 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_12 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_12, L_11, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_12);
		WWW_t2919945039 * L_13 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_13);
		bool L_14 = __this->get_U24disposing_6();
		if (L_14)
		{
			goto IL_008c;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_008c:
	{
		goto IL_0188;
	}

IL_0091:
	{
		WWW_t2919945039 * L_15 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_15);
		String_t* L_16 = WWW_get_error_m3092701216(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c2;
		}
	}
	{
		WWW_t2919945039 * L_17 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_17);
		String_t* L_18 = WWW_get_error_m3092701216(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_18, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_00c2:
	{
		WWW_t2919945039 * L_20 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_20);
		String_t* L_21 = WWW_get_text_m1558985139(L_20, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_21);
		String_t* L_22 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_23 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_22);
		StringU5BU5D_t1642385972* L_24 = String_Split_m3326265864(L_22, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral2543474434, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_25 = __this->get_U24this_4();
		NullCheck(L_25);
		Entry_t779635924 * L_26 = L_25->get_address_of_myEntry_2();
		List_1_t1398341365 * L_27 = L_26->get_currentPlaylist_3();
		NullCheck(L_27);
		List_1_Clear_m2928955906(L_27, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_28 = __this->get_U24this_4();
		NullCheck(L_28);
		Entry_t779635924 * L_29 = L_28->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_30 = V_1;
		NullCheck(L_30);
		L_29->set_pSize_8(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_0173;
	}

IL_0126:
	{
		MGR_Cloud_t726529104 * L_31 = __this->get_U24this_4();
		NullCheck(L_31);
		Entry_t779635924 * L_32 = L_31->get_address_of_myEntry_2();
		List_1_t1398341365 * L_33 = L_32->get_currentPlaylist_3();
		StringU5BU5D_t1642385972* L_34 = V_1;
		int32_t L_35 = V_2;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_33);
		List_1_Add_m4061286785(L_33, L_37, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		MGR_Cloud_t726529104 * L_38 = __this->get_U24this_4();
		NullCheck(L_38);
		Entry_t779635924 * L_39 = L_38->get_address_of_myEntry_2();
		List_1_t1440998580 * L_40 = L_39->get_hashString_7();
		StringU5BU5D_t1642385972* L_41 = V_1;
		int32_t L_42 = V_2;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		String_t* L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_45 = Convert_ToInt32_m2593311249(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		NullCheck(L_40);
		List_1_Add_m2828939739(L_40, L_45, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		StringU5BU5D_t1642385972* L_46 = V_1;
		int32_t L_47 = V_2;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		String_t* L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral419949503, L_49, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		int32_t L_51 = V_2;
		V_2 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_0173:
	{
		int32_t L_52 = V_2;
		StringU5BU5D_t1642385972* L_53 = V_1;
		NullCheck(L_53);
		if ((((int32_t)L_52) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_53)->max_length))))-(int32_t)1)))))
		{
			goto IL_0126;
		}
	}
	{
	}

IL_017f:
	{
		__this->set_U24PC_7((-1));
	}

IL_0186:
	{
		return (bool)0;
	}

IL_0188:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readPLDB>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadPLDBU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m342871002 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<readPLDB>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadPLDBU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1769159186 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator3::Dispose()
extern "C"  void U3CreadPLDBU3Ec__Iterator3_Dispose_m1736126113 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadPLDBU3Ec__Iterator3_Reset_m2254435207_MetadataUsageId;
extern "C"  void U3CreadPLDBU3Ec__Iterator3_Reset_m2254435207 (U3CreadPLDBU3Ec__Iterator3_t728369779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadPLDBU3Ec__Iterator3_Reset_m2254435207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::.ctor()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2__ctor_m1417225815 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readTraineeDB>c__Iterator2::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2054597853;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern const uint32_t U3CreadTraineeDBU3Ec__Iterator2_MoveNext_m3049364361_MetadataUsageId;
extern "C"  bool U3CreadTraineeDBU3Ec__Iterator2_MoveNext_m3049364361 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadTraineeDBU3Ec__Iterator2_MoveNext_m3049364361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0086;
		}
	}
	{
		goto IL_014a;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral2054597853);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2054597853);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_c_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 2;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_11);
		WWW_t2919945039 * L_12 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_12);
		bool L_13 = __this->get_U24disposing_6();
		if (L_13)
		{
			goto IL_0081;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0081:
	{
		goto IL_014c;
	}

IL_0086:
	{
		WWW_t2919945039 * L_14 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_error_m3092701216(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00b7;
		}
	}
	{
		WWW_t2919945039 * L_16 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m3092701216(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_17, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_00b7:
	{
		WWW_t2919945039 * L_19 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_19);
		String_t* L_20 = WWW_get_text_m1558985139(L_19, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_20);
		String_t* L_21 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_22 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_21);
		StringU5BU5D_t1642385972* L_23 = String_Split_m3326265864(L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		MGR_Cloud_t726529104 * L_24 = __this->get_U24this_4();
		NullCheck(L_24);
		Entry_t779635924 * L_25 = L_24->get_address_of_myEntry_2();
		List_1_t1398341365 * L_26 = L_25->get_trainees_6();
		NullCheck(L_26);
		List_1_Clear_m2928955906(L_26, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_27 = __this->get_U24this_4();
		NullCheck(L_27);
		Entry_t779635924 * L_28 = L_27->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_29 = V_1;
		NullCheck(L_29);
		L_28->set_tSize_10(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_0137;
	}

IL_0111:
	{
		MGR_Cloud_t726529104 * L_30 = __this->get_U24this_4();
		NullCheck(L_30);
		Entry_t779635924 * L_31 = L_30->get_address_of_myEntry_2();
		List_1_t1398341365 * L_32 = L_31->get_trainees_6();
		StringU5BU5D_t1642385972* L_33 = V_1;
		int32_t L_34 = V_2;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		String_t* L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_32);
		List_1_Add_m4061286785(L_32, L_36, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		StringU5BU5D_t1642385972* L_37 = V_1;
		int32_t L_38 = V_2;
		NullCheck(L_37);
		int32_t L_39 = L_38;
		String_t* L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_0137:
	{
		int32_t L_42 = V_2;
		StringU5BU5D_t1642385972* L_43 = V_1;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length))))-(int32_t)1)))))
		{
			goto IL_0111;
		}
	}
	{
	}

IL_0143:
	{
		__this->set_U24PC_7((-1));
	}

IL_014a:
	{
		return (bool)0;
	}

IL_014c:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2933058785 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1937447801 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::Dispose()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2_Dispose_m1515506888 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadTraineeDBU3Ec__Iterator2_Reset_m3846835722_MetadataUsageId;
extern "C"  void U3CreadTraineeDBU3Ec__Iterator2_Reset_m3846835722 (U3CreadTraineeDBU3Ec__Iterator2_t81797702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadTraineeDBU3Ec__Iterator2_Reset_m3846835722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_CreateMenu::.ctor()
extern "C"  void MGR_CreateMenu__ctor_m645211395 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_CreateMenu::Awake()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const uint32_t MGR_CreateMenu_Awake_m2535073688_MetadataUsageId;
extern "C"  void MGR_CreateMenu_Awake_m2535073688 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_Awake_m2535073688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_playlistItems_5(L_0);
		GameObject_t1756533147 * L_1 = __this->get_myGO_3();
		NullCheck(L_1);
		Animator_t69676727 * L_2 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_4(L_2);
		MGR_CreateMenu_readStates_m3857455173(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_CreateMenu::Update()
extern "C"  void MGR_CreateMenu_Update_m529887162 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_CreateMenu::readStates()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const uint32_t MGR_CreateMenu_readStates_m3857455173_MetadataUsageId;
extern "C"  void MGR_CreateMenu_readStates_m3857455173 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_readStates_m3857455173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AnimationClip_t3510324950 * V_1 = NULL;
	AnimationClipU5BU5D_t3936083219* V_2 = NULL;
	int32_t V_3 = 0;
	AnimationClip_t3510324950 * V_4 = NULL;
	AnimationClipU5BU5D_t3936083219* V_5 = NULL;
	int32_t V_6 = 0;
	{
		V_0 = 0;
		Animator_t69676727 * L_0 = __this->get_myAnimator_4();
		NullCheck(L_0);
		RuntimeAnimatorController_t670468573 * L_1 = Animator_get_runtimeAnimatorController_m652575931(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationClipU5BU5D_t3936083219* L_2 = RuntimeAnimatorController_get_animationClips_m3074969690(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_002a;
	}

IL_001c:
	{
		AnimationClipU5BU5D_t3936083219* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AnimationClip_t3510324950 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_3;
		AnimationClipU5BU5D_t3936083219* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->set_clipNames_2(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_0 = 0;
		Animator_t69676727 * L_12 = __this->get_myAnimator_4();
		NullCheck(L_12);
		RuntimeAnimatorController_t670468573 * L_13 = Animator_get_runtimeAnimatorController_m652575931(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationClipU5BU5D_t3936083219* L_14 = RuntimeAnimatorController_get_animationClips_m3074969690(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		V_6 = 0;
		goto IL_0083;
	}

IL_005c:
	{
		AnimationClipU5BU5D_t3936083219* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		AnimationClip_t3510324950 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		StringU5BU5D_t1642385972* L_19 = __this->get_clipNames_2();
		int32_t L_20 = V_0;
		AnimationClip_t3510324950 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (String_t*)L_23);
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_6;
		V_6 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_26 = V_6;
		AnimationClipU5BU5D_t3936083219* L_27 = V_5;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		return;
	}
}
// System.Void MGR_CreateMenu::OnGUI()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t MGR_CreateMenu_OnGUI_m420939169_MetadataUsageId;
extern "C"  void MGR_CreateMenu_OnGUI_m420939169 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_OnGUI_m420939169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_0 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t1799908754 * L_1 = GUISkin_get_button_m797402546(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_set_fontSize_m4015341543(L_1, ((int32_t)36), /*hidden argument*/NULL);
		V_0 = _stringLiteral371857150;
		V_1 = 0;
		goto IL_00b5;
	}

IL_001f:
	{
		StringU5BU5D_t1642385972* L_2 = __this->get_clipNames_2();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		String_t* L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		GUILayoutOptionU5BU5D_t2108882777* L_6 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_7 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_8 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_7/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_8);
		GUILayoutOptionU5BU5D_t2108882777* L_9 = L_6;
		int32_t L_10 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_11 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_10)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_11);
		bool L_12 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_5, L_9, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b0;
		}
	}
	{
		List_1_t1398341365 * L_13 = __this->get_playlistItems_5();
		StringU5BU5D_t1642385972* L_14 = __this->get_clipNames_2();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		String_t* L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_13);
		List_1_Add_m4061286785(L_13, L_17, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_18 = __this->get_playlistItems_5();
		StringU5BU5D_t1642385972* L_19 = __this->get_clipNames_2();
		int32_t L_20 = V_1;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m2596409543(NULL /*static, unused*/, L_22, _stringLiteral372029352, /*hidden argument*/NULL);
		String_Concat_m56707527(NULL /*static, unused*/, L_18, L_23, /*hidden argument*/NULL);
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, (50.0f), (50.0f), (50.0f), (200.0f), /*hidden argument*/NULL);
		String_t* L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_TextArea_m2093454620(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00b5:
	{
		int32_t L_27 = V_1;
		StringU5BU5D_t1642385972* L_28 = __this->get_clipNames_2();
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_001f;
		}
	}
	{
		return;
	}
}
// System.Void MGR_GUI::.ctor()
extern "C"  void MGR_GUI__ctor_m1251067095 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	{
		__this->set_coachSelected_2((bool)0);
		__this->set_traineeSelected_3((bool)0);
		__this->set_playlistSelected_4((bool)0);
		__this->set_count_25(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_GUI::Awake()
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2063026683_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const uint32_t MGR_GUI_Awake_m2956493910_MetadataUsageId;
extern "C"  void MGR_GUI_Awake_m2956493910 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_Awake_m2956493910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_myGO_18();
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_20(L_1);
		Dictionary_2_t3986656710 * L_2 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2063026683(L_2, /*hidden argument*/Dictionary_2__ctor_m2063026683_MethodInfo_var);
		__this->set_DictOfAnims_16(L_2);
		MGR_GUI_readStates_m194826505(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_myGO_18();
		NullCheck(L_3);
		MGR_Cloud_t726529104 * L_4 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_3, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_17(L_4);
		Canvas_t209405766 * L_5 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		__this->set_myCanvas_19(L_5);
		return;
	}
}
// System.Void MGR_GUI::readStates()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3588976330_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t MGR_GUI_readStates_m194826505_MetadataUsageId;
extern "C"  void MGR_GUI_readStates_m194826505 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_readStates_m194826505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AnimationClip_t3510324950 * V_1 = NULL;
	AnimationClipU5BU5D_t3936083219* V_2 = NULL;
	int32_t V_3 = 0;
	AnimationClip_t3510324950 * V_4 = NULL;
	AnimationClipU5BU5D_t3936083219* V_5 = NULL;
	int32_t V_6 = 0;
	{
		V_0 = 0;
		Animator_t69676727 * L_0 = __this->get_myAnimator_20();
		NullCheck(L_0);
		RuntimeAnimatorController_t670468573 * L_1 = Animator_get_runtimeAnimatorController_m652575931(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationClipU5BU5D_t3936083219* L_2 = RuntimeAnimatorController_get_animationClips_m3074969690(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_002a;
	}

IL_001c:
	{
		AnimationClipU5BU5D_t3936083219* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AnimationClip_t3510324950 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_3;
		AnimationClipU5BU5D_t3936083219* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->set_clipNames_14(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_0 = 0;
		Animator_t69676727 * L_12 = __this->get_myAnimator_20();
		NullCheck(L_12);
		RuntimeAnimatorController_t670468573 * L_13 = Animator_get_runtimeAnimatorController_m652575931(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationClipU5BU5D_t3936083219* L_14 = RuntimeAnimatorController_get_animationClips_m3074969690(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		V_6 = 0;
		goto IL_00df;
	}

IL_005c:
	{
		AnimationClipU5BU5D_t3936083219* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		AnimationClip_t3510324950 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		StringU5BU5D_t1642385972* L_19 = __this->get_clipNames_14();
		int32_t L_20 = V_0;
		AnimationClip_t3510324950 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (String_t*)L_23);
		Dictionary_2_t3986656710 * L_24 = __this->get_DictOfAnims_16();
		StringU5BU5D_t1642385972* L_25 = __this->get_clipNames_14();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		bool L_29 = Dictionary_2_ContainsKey_m3588976330(L_24, L_28, /*hidden argument*/Dictionary_2_ContainsKey_m3588976330_MethodInfo_var);
		if (L_29)
		{
			goto IL_00d4;
		}
	}
	{
		AnimationClip_t3510324950 * L_30 = V_4;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_30);
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		StringU5BU5D_t1642385972* L_34 = __this->get_clipNames_14();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2000667605(NULL /*static, unused*/, L_33, _stringLiteral372029310, L_38, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		Dictionary_2_t3986656710 * L_40 = __this->get_DictOfAnims_16();
		StringU5BU5D_t1642385972* L_41 = __this->get_clipNames_14();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		String_t* L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		AnimationClip_t3510324950 * L_45 = V_4;
		NullCheck(L_45);
		int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_45);
		NullCheck(L_40);
		Dictionary_2_Add_m1209957957(L_40, L_44, L_46, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
	}

IL_00d4:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)((int32_t)L_47+(int32_t)1));
		int32_t L_48 = V_6;
		V_6 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_00df:
	{
		int32_t L_49 = V_6;
		AnimationClipU5BU5D_t3936083219* L_50 = V_5;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		return;
	}
}
// System.Void MGR_GUI::addDBEntry()
extern "C"  void MGR_GUI_addDBEntry_m376179630 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	{
		InputField_t1631627530 * L_0 = __this->get_Input_Coach_5();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		__this->set_i_coach_11(L_1);
		InputField_t1631627530 * L_2 = __this->get_Input_Trainee_6();
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m409351770(L_2, /*hidden argument*/NULL);
		__this->set_i_trainee_12(L_3);
		InputField_t1631627530 * L_4 = __this->get_Input_Playlist_7();
		NullCheck(L_4);
		String_t* L_5 = InputField_get_text_m409351770(L_4, /*hidden argument*/NULL);
		__this->set_i_playlist_13(L_5);
		MGR_Cloud_t726529104 * L_6 = __this->get_myCloud_17();
		String_t* L_7 = __this->get_i_coach_11();
		String_t* L_8 = __this->get_i_trainee_12();
		String_t* L_9 = __this->get_i_playlist_13();
		String_t* L_10 = __this->get_hlist_15();
		NullCheck(L_6);
		MGR_Cloud_addPL_m2038133962(L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_GUI::OnGUI()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029319;
extern Il2CppCodeGenString* _stringLiteral2077193552;
extern Il2CppCodeGenString* _stringLiteral20327090;
extern const uint32_t MGR_GUI_OnGUI_m858956101_MetadataUsageId;
extern "C"  void MGR_GUI_OnGUI_m858956101 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_OnGUI_m858956101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_0 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t1799908754 * L_1 = GUISkin_get_button_m797402546(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_set_fontSize_m4015341543(L_1, ((int32_t)36), /*hidden argument*/NULL);
		int32_t L_2 = __this->get_count_25();
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0040;
		}
	}
	{
		MGR_Cloud_t726529104 * L_3 = __this->get_myCloud_17();
		NullCheck(L_3);
		Il2CppObject * L_4 = MGR_Cloud_readCoachDB_m3748347533(L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_count_25();
		__this->set_count_25(((int32_t)((int32_t)L_5+(int32_t)1)));
	}

IL_0040:
	{
		int32_t L_6 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m1220545469(&L_8, (0.0f), (0.0f), (((float)((float)L_6))), (((float)((float)L_7))), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3297699023(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = __this->get_coachScrollPosition_21();
		GUILayoutOptionU5BU5D_t2108882777* L_10 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_11 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_12 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_11/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_12);
		GUILayoutOptionU5BU5D_t2108882777* L_13 = L_10;
		int32_t L_14 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_15 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_14)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_15);
		Vector2_t2243707579  L_16 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_9, L_13, /*hidden argument*/NULL);
		__this->set_coachScrollPosition_21(L_16);
		GUILayoutOptionU5BU5D_t2108882777* L_17 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_18 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_19 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_18/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_19);
		GUILayoutOptionU5BU5D_t2108882777* L_20 = L_17;
		int32_t L_21 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_22 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_21)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_22);
		bool L_23 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_20, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00ed;
		}
	}
	{
	}

IL_00ed:
	{
		V_0 = 0;
		goto IL_01a0;
	}

IL_00f4:
	{
		MGR_Cloud_t726529104 * L_24 = __this->get_myCloud_17();
		NullCheck(L_24);
		Entry_t779635924 * L_25 = L_24->get_address_of_myEntry_2();
		List_1_t1398341365 * L_26 = L_25->get_coaches_5();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		String_t* L_28 = List_1_get_Item_m1112119647(L_26, L_27, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_29 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_30 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_31 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_30/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_31);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_31);
		GUILayoutOptionU5BU5D_t2108882777* L_32 = L_29;
		int32_t L_33 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_34 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_33)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_34);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_34);
		bool L_35 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_28, L_32, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_019b;
		}
	}
	{
		__this->set_coachSelected_2((bool)1);
		__this->set_traineeSelected_3((bool)0);
		__this->set_playlistSelected_4((bool)0);
		MGR_Cloud_t726529104 * L_36 = __this->get_myCloud_17();
		NullCheck(L_36);
		Entry_t779635924 * L_37 = L_36->get_address_of_myEntry_2();
		List_1_t1398341365 * L_38 = L_37->get_coaches_5();
		int32_t L_39 = V_0;
		NullCheck(L_38);
		String_t* L_40 = List_1_get_Item_m1112119647(L_38, L_39, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_coach_8(L_40);
		MGR_Cloud_t726529104 * L_41 = __this->get_myCloud_17();
		MGR_Cloud_t726529104 * L_42 = __this->get_myCloud_17();
		NullCheck(L_42);
		Entry_t779635924 * L_43 = L_42->get_address_of_myEntry_2();
		List_1_t1398341365 * L_44 = L_43->get_coaches_5();
		int32_t L_45 = V_0;
		NullCheck(L_44);
		String_t* L_46 = List_1_get_Item_m1112119647(L_44, L_45, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_41);
		Il2CppObject * L_47 = MGR_Cloud_readTraineeDB_m1908492693(L_41, L_46, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_47, /*hidden argument*/NULL);
	}

IL_019b:
	{
		int32_t L_48 = V_0;
		V_0 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_01a0:
	{
		int32_t L_49 = V_0;
		MGR_Cloud_t726529104 * L_50 = __this->get_myCloud_17();
		NullCheck(L_50);
		Entry_t779635924 * L_51 = L_50->get_address_of_myEntry_2();
		int32_t L_52 = L_51->get_cSize_9();
		if ((((int32_t)L_49) < ((int32_t)L_52)))
		{
			goto IL_00f4;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_53 = __this->get_coachSelected_2();
		if (!L_53)
		{
			goto IL_033b;
		}
	}
	{
		Vector2_t2243707579  L_54 = __this->get_traineeScrollPosition_22();
		GUILayoutOptionU5BU5D_t2108882777* L_55 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_56 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_57 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_56/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_57);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_57);
		GUILayoutOptionU5BU5D_t2108882777* L_58 = L_55;
		int32_t L_59 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_60 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_59)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_58);
		ArrayElementTypeCheck (L_58, L_60);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_60);
		Vector2_t2243707579  L_61 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_54, L_58, /*hidden argument*/NULL);
		__this->set_traineeScrollPosition_22(L_61);
		GUILayoutOptionU5BU5D_t2108882777* L_62 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_63 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_64 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_63/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_62);
		ArrayElementTypeCheck (L_62, L_64);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_64);
		GUILayoutOptionU5BU5D_t2108882777* L_65 = L_62;
		int32_t L_66 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_67 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_66)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_65);
		ArrayElementTypeCheck (L_65, L_67);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_67);
		bool L_68 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_65, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_024e;
		}
	}
	{
	}

IL_024e:
	{
		V_1 = 0;
		goto IL_031f;
	}

IL_0255:
	{
		MGR_Cloud_t726529104 * L_69 = __this->get_myCloud_17();
		NullCheck(L_69);
		Entry_t779635924 * L_70 = L_69->get_address_of_myEntry_2();
		List_1_t1398341365 * L_71 = L_70->get_trainees_6();
		int32_t L_72 = V_1;
		NullCheck(L_71);
		String_t* L_73 = List_1_get_Item_m1112119647(L_71, L_72, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_74 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_75 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_76 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_75/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_76);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_76);
		GUILayoutOptionU5BU5D_t2108882777* L_77 = L_74;
		int32_t L_78 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_79 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_78)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_77);
		ArrayElementTypeCheck (L_77, L_79);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_79);
		bool L_80 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_73, L_77, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_031a;
		}
	}
	{
		__this->set_traineeSelected_3((bool)1);
		__this->set_playlistSelected_4((bool)0);
		MGR_Cloud_t726529104 * L_81 = __this->get_myCloud_17();
		NullCheck(L_81);
		Entry_t779635924 * L_82 = L_81->get_address_of_myEntry_2();
		List_1_t1398341365 * L_83 = L_82->get_trainees_6();
		int32_t L_84 = V_1;
		NullCheck(L_83);
		String_t* L_85 = List_1_get_Item_m1112119647(L_83, L_84, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_trainee_9(L_85);
		MGR_Cloud_t726529104 * L_86 = __this->get_myCloud_17();
		NullCheck(L_86);
		Entry_t779635924 * L_87 = L_86->get_address_of_myEntry_2();
		List_1_t1398341365 * L_88 = L_87->get_trainees_6();
		int32_t L_89 = V_1;
		NullCheck(L_88);
		String_t* L_90 = List_1_get_Item_m1112119647(L_88, L_89, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_91 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2077193552, L_90, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_92 = __this->get_myCloud_17();
		MGR_Cloud_t726529104 * L_93 = __this->get_myCloud_17();
		NullCheck(L_93);
		Entry_t779635924 * L_94 = L_93->get_address_of_myEntry_2();
		List_1_t1398341365 * L_95 = L_94->get_trainees_6();
		int32_t L_96 = V_1;
		NullCheck(L_95);
		String_t* L_97 = List_1_get_Item_m1112119647(L_95, L_96, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_92);
		Il2CppObject * L_98 = MGR_Cloud_readPLDB_m994522313(L_92, L_97, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_98, /*hidden argument*/NULL);
	}

IL_031a:
	{
		int32_t L_99 = V_1;
		V_1 = ((int32_t)((int32_t)L_99+(int32_t)1));
	}

IL_031f:
	{
		int32_t L_100 = V_1;
		MGR_Cloud_t726529104 * L_101 = __this->get_myCloud_17();
		NullCheck(L_101);
		Entry_t779635924 * L_102 = L_101->get_address_of_myEntry_2();
		int32_t L_103 = L_102->get_tSize_10();
		if ((((int32_t)L_100) < ((int32_t)L_103)))
		{
			goto IL_0255;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_033b:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_104 = __this->get_traineeSelected_3();
		if (!L_104)
		{
			goto IL_04b4;
		}
	}
	{
		Vector2_t2243707579  L_105 = __this->get_plScrollPosition_23();
		GUILayoutOptionU5BU5D_t2108882777* L_106 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_107 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_108 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_107/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_106);
		ArrayElementTypeCheck (L_106, L_108);
		(L_106)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_108);
		GUILayoutOptionU5BU5D_t2108882777* L_109 = L_106;
		int32_t L_110 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_111 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_110)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_109);
		ArrayElementTypeCheck (L_109, L_111);
		(L_109)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_111);
		Vector2_t2243707579  L_112 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_105, L_109, /*hidden argument*/NULL);
		__this->set_plScrollPosition_23(L_112);
		GUILayoutOptionU5BU5D_t2108882777* L_113 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_114 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_115 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_114/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_113);
		ArrayElementTypeCheck (L_113, L_115);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_115);
		GUILayoutOptionU5BU5D_t2108882777* L_116 = L_113;
		int32_t L_117 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_118 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_117)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_116);
		ArrayElementTypeCheck (L_116, L_118);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_118);
		bool L_119 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_116, /*hidden argument*/NULL);
		if (!L_119)
		{
			goto IL_03ce;
		}
	}
	{
	}

IL_03ce:
	{
		V_2 = 0;
		goto IL_0498;
	}

IL_03d5:
	{
		MGR_Cloud_t726529104 * L_120 = __this->get_myCloud_17();
		NullCheck(L_120);
		Entry_t779635924 * L_121 = L_120->get_address_of_myEntry_2();
		List_1_t1398341365 * L_122 = L_121->get_currentPlaylist_3();
		int32_t L_123 = V_2;
		NullCheck(L_122);
		String_t* L_124 = List_1_get_Item_m1112119647(L_122, L_123, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_125 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_126 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_127 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_126/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_125);
		ArrayElementTypeCheck (L_125, L_127);
		(L_125)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_127);
		GUILayoutOptionU5BU5D_t2108882777* L_128 = L_125;
		int32_t L_129 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_130 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_129)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_128);
		ArrayElementTypeCheck (L_128, L_130);
		(L_128)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_130);
		bool L_131 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_124, L_128, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_0493;
		}
	}
	{
		__this->set_playlistSelected_4((bool)1);
		MGR_Cloud_t726529104 * L_132 = __this->get_myCloud_17();
		NullCheck(L_132);
		Entry_t779635924 * L_133 = L_132->get_address_of_myEntry_2();
		List_1_t1398341365 * L_134 = L_133->get_currentPlaylist_3();
		int32_t L_135 = V_2;
		NullCheck(L_134);
		String_t* L_136 = List_1_get_Item_m1112119647(L_134, L_135, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_playlist_10(L_136);
		MGR_Cloud_t726529104 * L_137 = __this->get_myCloud_17();
		NullCheck(L_137);
		Entry_t779635924 * L_138 = L_137->get_address_of_myEntry_2();
		List_1_t1398341365 * L_139 = L_138->get_currentPlaylist_3();
		int32_t L_140 = V_2;
		NullCheck(L_139);
		String_t* L_141 = List_1_get_Item_m1112119647(L_139, L_140, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_142 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral20327090, L_141, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_142, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_143 = __this->get_myCloud_17();
		MGR_Cloud_t726529104 * L_144 = __this->get_myCloud_17();
		NullCheck(L_144);
		Entry_t779635924 * L_145 = L_144->get_address_of_myEntry_2();
		List_1_t1398341365 * L_146 = L_145->get_currentPlaylist_3();
		int32_t L_147 = V_2;
		NullCheck(L_146);
		String_t* L_148 = List_1_get_Item_m1112119647(L_146, L_147, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_143);
		Il2CppObject * L_149 = MGR_Cloud_readHashDB_m3218359951(L_143, L_148, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_149, /*hidden argument*/NULL);
	}

IL_0493:
	{
		int32_t L_150 = V_2;
		V_2 = ((int32_t)((int32_t)L_150+(int32_t)1));
	}

IL_0498:
	{
		int32_t L_151 = V_2;
		MGR_Cloud_t726529104 * L_152 = __this->get_myCloud_17();
		NullCheck(L_152);
		Entry_t779635924 * L_153 = L_152->get_address_of_myEntry_2();
		int32_t L_154 = L_153->get_pSize_8();
		if ((((int32_t)L_151) < ((int32_t)L_154)))
		{
			goto IL_03d5;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_04b4:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_155 = __this->get_playlistSelected_4();
		if (!L_155)
		{
			goto IL_0632;
		}
	}
	{
		Vector2_t2243707579  L_156 = __this->get_hashScrollPosition_24();
		GUILayoutOptionU5BU5D_t2108882777* L_157 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_158 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_159 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_158/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_157);
		ArrayElementTypeCheck (L_157, L_159);
		(L_157)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_159);
		GUILayoutOptionU5BU5D_t2108882777* L_160 = L_157;
		int32_t L_161 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_162 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_161)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_160);
		ArrayElementTypeCheck (L_160, L_162);
		(L_160)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_162);
		Vector2_t2243707579  L_163 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_156, L_160, /*hidden argument*/NULL);
		__this->set_hashScrollPosition_24(L_163);
		GUILayoutOptionU5BU5D_t2108882777* L_164 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_165 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_166 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_165/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_164);
		ArrayElementTypeCheck (L_164, L_166);
		(L_164)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_166);
		GUILayoutOptionU5BU5D_t2108882777* L_167 = L_164;
		int32_t L_168 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_169 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_168)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_167);
		ArrayElementTypeCheck (L_167, L_169);
		(L_167)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_169);
		bool L_170 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_167, /*hidden argument*/NULL);
		if (!L_170)
		{
			goto IL_0547;
		}
	}
	{
	}

IL_0547:
	{
		V_3 = 0;
		goto IL_0616;
	}

IL_054e:
	{
		MGR_Cloud_t726529104 * L_171 = __this->get_myCloud_17();
		NullCheck(L_171);
		Entry_t779635924 * L_172 = L_171->get_address_of_myEntry_2();
		List_1_t1440998580 * L_173 = L_172->get_hashString_7();
		int32_t L_174 = V_3;
		NullCheck(L_173);
		int32_t L_175 = List_1_get_Item_m1921196075(L_173, L_174, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_4 = L_175;
		String_t* L_176 = Int32_ToString_m2960866144((&V_4), /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_177 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_178 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_179 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_178/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_177);
		ArrayElementTypeCheck (L_177, L_179);
		(L_177)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_179);
		GUILayoutOptionU5BU5D_t2108882777* L_180 = L_177;
		int32_t L_181 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_182 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_181)))/(float)(4.25f))), /*hidden argument*/NULL);
		NullCheck(L_180);
		ArrayElementTypeCheck (L_180, L_182);
		(L_180)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_182);
		bool L_183 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_176, L_180, /*hidden argument*/NULL);
		if (!L_183)
		{
			goto IL_0611;
		}
	}
	{
		__this->set_playlistSelected_4((bool)1);
		MGR_Cloud_t726529104 * L_184 = __this->get_myCloud_17();
		NullCheck(L_184);
		Entry_t779635924 * L_185 = L_184->get_address_of_myEntry_2();
		List_1_t1440998580 * L_186 = L_185->get_hashString_7();
		int32_t L_187 = V_3;
		NullCheck(L_186);
		int32_t L_188 = List_1_get_Item_m1921196075(L_186, L_187, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_5 = L_188;
		String_t* L_189 = Int32_ToString_m2960866144((&V_5), /*hidden argument*/NULL);
		__this->set_playlist_10(L_189);
		MGR_Cloud_t726529104 * L_190 = __this->get_myCloud_17();
		NullCheck(L_190);
		Entry_t779635924 * L_191 = L_190->get_address_of_myEntry_2();
		List_1_t1440998580 * L_192 = L_191->get_hashString_7();
		int32_t L_193 = V_3;
		NullCheck(L_192);
		int32_t L_194 = List_1_get_Item_m1921196075(L_192, L_193, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_6 = L_194;
		String_t* L_195 = Int32_ToString_m2960866144((&V_6), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_196 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral20327090, L_195, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_196, /*hidden argument*/NULL);
	}

IL_0611:
	{
		int32_t L_197 = V_3;
		V_3 = ((int32_t)((int32_t)L_197+(int32_t)1));
	}

IL_0616:
	{
		int32_t L_198 = V_3;
		MGR_Cloud_t726529104 * L_199 = __this->get_myCloud_17();
		NullCheck(L_199);
		Entry_t779635924 * L_200 = L_199->get_address_of_myEntry_2();
		int32_t L_201 = L_200->get_hSize_11();
		if ((((int32_t)L_198) < ((int32_t)L_201)))
		{
			goto IL_054e;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0632:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2019304577(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1904221074(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::.ctor()
extern "C"  void MGR_HLGUI__ctor_m1143505859 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	{
		__this->set_count_22(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::Awake()
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2063026683_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1568294237;
extern const uint32_t MGR_HLGUI_Awake_m2690111322_MetadataUsageId;
extern "C"  void MGR_HLGUI_Awake_m2690111322 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_Awake_m2690111322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_myGO_15();
		NullCheck(L_0);
		MGR_Anim_t3221826994 * L_1 = GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355(L_0, /*hidden argument*/GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355_MethodInfo_var);
		__this->set_myAnim_14(L_1);
		GameObject_t1756533147 * L_2 = __this->get_myGO_15();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_17(L_3);
		Dictionary_2_t3986656710 * L_4 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2063026683(L_4, /*hidden argument*/Dictionary_2__ctor_m2063026683_MethodInfo_var);
		__this->set_DictOfAnims_12(L_4);
		GameObject_t1756533147 * L_5 = __this->get_myGO_15();
		NullCheck(L_5);
		MGR_Cloud_t726529104 * L_6 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_5, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_13(L_6);
		MGR_HLGUI_updatePL_m557645580(__this, /*hidden argument*/NULL);
		Canvas_t209405766 * L_7 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		__this->set_myCanvas_16(L_7);
		MGR_HLGUI_helpME_m62156382(__this, _stringLiteral1568294237, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::updatePL()
extern Il2CppCodeGenString* _stringLiteral4225798434;
extern Il2CppCodeGenString* _stringLiteral3680455850;
extern const uint32_t MGR_HLGUI_updatePL_m557645580_MetadataUsageId;
extern "C"  void MGR_HLGUI_updatePL_m557645580 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_updatePL_m557645580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_13();
		NullCheck(L_0);
		Il2CppObject * L_1 = MGR_Cloud_readHashDB_m3218359951(L_0, _stringLiteral4225798434, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
		MGR_HLGUI_helpME_m62156382(__this, _stringLiteral3680455850, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::helpME(System.String)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1214590000;
extern const uint32_t MGR_HLGUI_helpME_m62156382_MetadataUsageId;
extern "C"  void MGR_HLGUI_helpME_m62156382 (MGR_HLGUI_t3333927834 * __this, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_helpME_m62156382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___s0;
		MGR_Cloud_t726529104 * L_1 = __this->get_myCloud_13();
		NullCheck(L_1);
		Entry_t779635924 * L_2 = L_1->get_address_of_myEntry_2();
		int32_t L_3 = L_2->get_hSize_11();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2000667605(NULL /*static, unused*/, L_0, _stringLiteral1214590000, L_5, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0033;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_8 = V_0;
		MGR_Cloud_t726529104 * L_9 = __this->get_myCloud_13();
		NullCheck(L_9);
		Entry_t779635924 * L_10 = L_9->get_address_of_myEntry_2();
		int32_t L_11 = L_10->get_hSize_11();
		if ((((int32_t)L_8) < ((int32_t)L_11)))
		{
			goto IL_002d;
		}
	}
	{
		return;
	}
}
// System.Void MGR_HLGUI::readStates()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3588976330_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t MGR_HLGUI_readStates_m3798139293_MetadataUsageId;
extern "C"  void MGR_HLGUI_readStates_m3798139293 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_readStates_m3798139293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AnimationClip_t3510324950 * V_1 = NULL;
	AnimationClipU5BU5D_t3936083219* V_2 = NULL;
	int32_t V_3 = 0;
	AnimationClip_t3510324950 * V_4 = NULL;
	AnimationClipU5BU5D_t3936083219* V_5 = NULL;
	int32_t V_6 = 0;
	{
		V_0 = 0;
		Animator_t69676727 * L_0 = __this->get_myAnimator_17();
		NullCheck(L_0);
		RuntimeAnimatorController_t670468573 * L_1 = Animator_get_runtimeAnimatorController_m652575931(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationClipU5BU5D_t3936083219* L_2 = RuntimeAnimatorController_get_animationClips_m3074969690(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_002a;
	}

IL_001c:
	{
		AnimationClipU5BU5D_t3936083219* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AnimationClip_t3510324950 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_3;
		AnimationClipU5BU5D_t3936083219* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->set_clipNames_11(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_0 = 0;
		Animator_t69676727 * L_12 = __this->get_myAnimator_17();
		NullCheck(L_12);
		RuntimeAnimatorController_t670468573 * L_13 = Animator_get_runtimeAnimatorController_m652575931(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationClipU5BU5D_t3936083219* L_14 = RuntimeAnimatorController_get_animationClips_m3074969690(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		V_6 = 0;
		goto IL_00df;
	}

IL_005c:
	{
		AnimationClipU5BU5D_t3936083219* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		AnimationClip_t3510324950 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		StringU5BU5D_t1642385972* L_19 = __this->get_clipNames_11();
		int32_t L_20 = V_0;
		AnimationClip_t3510324950 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (String_t*)L_23);
		Dictionary_2_t3986656710 * L_24 = __this->get_DictOfAnims_12();
		StringU5BU5D_t1642385972* L_25 = __this->get_clipNames_11();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		bool L_29 = Dictionary_2_ContainsKey_m3588976330(L_24, L_28, /*hidden argument*/Dictionary_2_ContainsKey_m3588976330_MethodInfo_var);
		if (L_29)
		{
			goto IL_00d4;
		}
	}
	{
		AnimationClip_t3510324950 * L_30 = V_4;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_30);
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		StringU5BU5D_t1642385972* L_34 = __this->get_clipNames_11();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2000667605(NULL /*static, unused*/, L_33, _stringLiteral372029310, L_38, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		Dictionary_2_t3986656710 * L_40 = __this->get_DictOfAnims_12();
		StringU5BU5D_t1642385972* L_41 = __this->get_clipNames_11();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		String_t* L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		AnimationClip_t3510324950 * L_45 = V_4;
		NullCheck(L_45);
		int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_45);
		NullCheck(L_40);
		Dictionary_2_Add_m1209957957(L_40, L_44, L_46, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
	}

IL_00d4:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)((int32_t)L_47+(int32_t)1));
		int32_t L_48 = V_6;
		V_6 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_00df:
	{
		int32_t L_49 = V_6;
		AnimationClipU5BU5D_t3936083219* L_50 = V_5;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		return;
	}
}
// System.Void MGR_HLGUI::addDBEntry()
extern "C"  void MGR_HLGUI_addDBEntry_m3452731226 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	{
		InputField_t1631627530 * L_0 = __this->get_Input_Coach_2();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		__this->set_i_coach_8(L_1);
		InputField_t1631627530 * L_2 = __this->get_Input_Trainee_3();
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m409351770(L_2, /*hidden argument*/NULL);
		__this->set_i_trainee_9(L_3);
		InputField_t1631627530 * L_4 = __this->get_Input_Playlist_4();
		NullCheck(L_4);
		String_t* L_5 = InputField_get_text_m409351770(L_4, /*hidden argument*/NULL);
		__this->set_i_playlist_10(L_5);
		return;
	}
}
// System.Void MGR_HLGUI::buttonPressed()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral232122944;
extern Il2CppCodeGenString* _stringLiteral806467172;
extern const uint32_t MGR_HLGUI_buttonPressed_m1867206739_MetadataUsageId;
extern "C"  void MGR_HLGUI_buttonPressed_m1867206739 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_buttonPressed_m1867206739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_13();
		NullCheck(L_0);
		Entry_t779635924 * L_1 = L_0->get_address_of_myEntry_2();
		int32_t L_2 = L_1->get_hSize_11();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral232122944, L_4, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		MGR_HLGUI_helpME_m62156382(__this, _stringLiteral806467172, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_6 = __this->get_myAnim_14();
		NullCheck(L_6);
		MGR_Anim_playAnims_m3338700699(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::OnGUI()
extern "C"  void MGR_HLGUI_OnGUI_m4057632545 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_HoloText::.ctor()
extern "C"  void MGR_HoloText__ctor_m419231409 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::Start()
extern const MethodInfo* Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1275371535;
extern Il2CppCodeGenString* _stringLiteral2183870267;
extern Il2CppCodeGenString* _stringLiteral1760029775;
extern Il2CppCodeGenString* _stringLiteral919633924;
extern const uint32_t MGR_HoloText_Start_m3634721657_MetadataUsageId;
extern "C"  void MGR_HoloText_Start_m3634721657 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HoloText_Start_m3634721657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t1641806576 * L_0 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		__this->set_playlistText_2(L_0);
		MGR_Cloud_t726529104 * L_1 = Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677(__this, /*hidden argument*/Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677_MethodInfo_var);
		__this->set_myCloud_3(L_1);
		__this->set_updateText_4(_stringLiteral1275371535);
		MGR_Cloud_t726529104 * L_2 = __this->get_myCloud_3();
		NullCheck(L_2);
		MGR_Cloud_initFB_m761421013(L_2, _stringLiteral2183870267, _stringLiteral1760029775, _stringLiteral919633924, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m804483696_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m870713862_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4175023932_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t MGR_HoloText_OnGUI_m1016716883_MetadataUsageId;
extern "C"  void MGR_HoloText_OnGUI_m1016716883 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HoloText_OnGUI_m1016716883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_t933071039  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_3();
		NullCheck(L_0);
		Entry_t779635924 * L_1 = L_0->get_address_of_myEntry_2();
		String_t* L_2 = L_1->get_coach_0();
		__this->set_updateText_4(L_2);
		String_t* L_3 = __this->get_updateText_4();
		MGR_Cloud_t726529104 * L_4 = __this->get_myCloud_3();
		NullCheck(L_4);
		Entry_t779635924 * L_5 = L_4->get_address_of_myEntry_2();
		String_t* L_6 = L_5->get_trainee_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_6, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_8 = __this->get_myCloud_3();
		NullCheck(L_8);
		Entry_t779635924 * L_9 = L_8->get_address_of_myEntry_2();
		String_t* L_10 = L_9->get_playlistName_2();
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, L_3, L_7, L_11, /*hidden argument*/NULL);
		__this->set_updateText_4(L_12);
		MGR_Cloud_t726529104 * L_13 = __this->get_myCloud_3();
		NullCheck(L_13);
		Entry_t779635924 * L_14 = L_13->get_address_of_myEntry_2();
		List_1_t1398341365 * L_15 = L_14->get_currentPlaylist_3();
		NullCheck(L_15);
		Enumerator_t933071039  L_16 = List_1_GetEnumerator_m804483696(L_15, /*hidden argument*/List_1_GetEnumerator_m804483696_MethodInfo_var);
		V_1 = L_16;
	}

IL_0073:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009e;
		}

IL_0078:
		{
			String_t* L_17 = Enumerator_get_Current_m870713862((&V_1), /*hidden argument*/Enumerator_get_Current_m870713862_MethodInfo_var);
			V_0 = L_17;
			String_t* L_18 = __this->get_updateText_4();
			String_t* L_19 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_20 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_19, /*hidden argument*/NULL);
			String_t* L_21 = String_Concat_m2596409543(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
			__this->set_updateText_4(L_21);
		}

IL_009e:
		{
			bool L_22 = Enumerator_MoveNext_m4175023932((&V_1), /*hidden argument*/Enumerator_MoveNext_m4175023932_MethodInfo_var);
			if (L_22)
			{
				goto IL_0078;
			}
		}

IL_00aa:
		{
			IL2CPP_LEAVE(0xBD, FINALLY_00af);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00af;
	}

FINALLY_00af:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_1), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(175)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(175)
	{
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00bd:
	{
		TextMesh_t1641806576 * L_23 = __this->get_playlistText_2();
		String_t* L_24 = __this->get_updateText_4();
		NullCheck(L_23);
		TextMesh_set_text_m3390063817(L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::Update()
extern "C"  void MGR_HoloText_Update_m2569076898 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_Menus::.ctor()
extern "C"  void MGR_Menus__ctor_m4221427840 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Menus::Awake()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral214955626;
extern Il2CppCodeGenString* _stringLiteral3844169588;
extern Il2CppCodeGenString* _stringLiteral2666598912;
extern const uint32_t MGR_Menus_Awake_m45193489_MetadataUsageId;
extern "C"  void MGR_Menus_Awake_m45193489 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Menus_Awake_m45193489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_player_10();
		NullCheck(L_0);
		Renderer_t257310565 * L_1 = GameObject_GetComponent_TisRenderer_t257310565_m1312615893(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var);
		bool L_2 = ((bool)0);
		Il2CppObject * L_3 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_1);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_1, L_3);
		GameObjectU5BU5D_t3057952154* L_4 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral214955626, /*hidden argument*/NULL);
		__this->set_startObjects_7(L_4);
		GameObjectU5BU5D_t3057952154* L_5 = __this->get_startObjects_7();
		MGR_Menus_hideMenu_m3562369893(__this, L_5, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_6 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral3844169588, /*hidden argument*/NULL);
		__this->set_createObjects_8(L_6);
		GameObjectU5BU5D_t3057952154* L_7 = __this->get_createObjects_8();
		MGR_Menus_hideMenu_m3562369893(__this, L_7, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_8 = GameObject_FindGameObjectsWithTag_m2154478296(NULL /*static, unused*/, _stringLiteral2666598912, /*hidden argument*/NULL);
		__this->set_readObjects_9(L_8);
		GameObjectU5BU5D_t3057952154* L_9 = __this->get_readObjects_9();
		MGR_Menus_hideMenu_m3562369893(__this, L_9, /*hidden argument*/NULL);
		__this->set_currentstate_2(0);
		return;
	}
}
// System.Void MGR_Menus::hideMenu(UnityEngine.GameObject[])
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Menus_hideMenu_m3562369893_MetadataUsageId;
extern "C"  void MGR_Menus_hideMenu_m3562369893 (MGR_Menus_t511463637 * __this, GameObjectU5BU5D_t3057952154* ___G0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Menus_hideMenu_m3562369893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = ___G0;
		V_1 = L_0;
		V_2 = 0;
		goto IL_002a;
	}

IL_000b:
	{
		GameObjectU5BU5D_t3057952154* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0025;
		}
	}
	{
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_0025:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_2;
		GameObjectU5BU5D_t3057952154* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
// System.Void MGR_Menus::showMenu(UnityEngine.GameObject[])
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Menus_showMenu_m1990371462_MetadataUsageId;
extern "C"  void MGR_Menus_showMenu_m1990371462 (MGR_Menus_t511463637 * __this, GameObjectU5BU5D_t3057952154* ___G0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Menus_showMenu_m1990371462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = ___G0;
		V_1 = L_0;
		V_2 = 0;
		goto IL_002a;
	}

IL_000b:
	{
		GameObjectU5BU5D_t3057952154* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0025;
		}
	}
	{
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)1, /*hidden argument*/NULL);
	}

IL_0025:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_2;
		GameObjectU5BU5D_t3057952154* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
// System.Void MGR_Menus::Update()
extern "C"  void MGR_Menus_Update_m3885601141 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_currentstate_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_007f;
		}
	}
	{
		goto IL_00a8;
	}

IL_0021:
	{
		GameObjectU5BU5D_t3057952154* L_4 = __this->get_startObjects_7();
		MGR_Menus_showMenu_m1990371462(__this, L_4, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_5 = __this->get_createObjects_8();
		MGR_Menus_hideMenu_m3562369893(__this, L_5, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_6 = __this->get_readObjects_9();
		MGR_Menus_hideMenu_m3562369893(__this, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_startMenu_4();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)1, /*hidden argument*/NULL);
		goto IL_00a8;
	}

IL_0056:
	{
		GameObjectU5BU5D_t3057952154* L_8 = __this->get_createObjects_8();
		MGR_Menus_showMenu_m1990371462(__this, L_8, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_9 = __this->get_startObjects_7();
		MGR_Menus_hideMenu_m3562369893(__this, L_9, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_10 = __this->get_readObjects_9();
		MGR_Menus_hideMenu_m3562369893(__this, L_10, /*hidden argument*/NULL);
		goto IL_00a8;
	}

IL_007f:
	{
		GameObjectU5BU5D_t3057952154* L_11 = __this->get_readObjects_9();
		MGR_Menus_showMenu_m1990371462(__this, L_11, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_12 = __this->get_createObjects_8();
		MGR_Menus_hideMenu_m3562369893(__this, L_12, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_13 = __this->get_startObjects_7();
		MGR_Menus_hideMenu_m3562369893(__this, L_13, /*hidden argument*/NULL);
		goto IL_00a8;
	}

IL_00a8:
	{
		return;
	}
}
// System.Void MGR_Menus::StartMenu()
extern "C"  void MGR_Menus_StartMenu_m1676692927 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	{
		__this->set_currentstate_2(0);
		return;
	}
}
// System.Void MGR_Menus::CreateMenu()
extern "C"  void MGR_Menus_CreateMenu_m1027925027 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	{
		__this->set_currentstate_2(1);
		return;
	}
}
// System.Void MGR_Menus::ReadMenu()
extern "C"  void MGR_Menus_ReadMenu_m1846344601 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	{
		__this->set_currentstate_2(2);
		return;
	}
}
// System.Void SetAnimationInAnimator::.ctor()
extern "C"  void SetAnimationInAnimator__ctor_m1105520923 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetAnimationInAnimator::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const uint32_t SetAnimationInAnimator_Start_m747789719_MetadataUsageId;
extern "C"  void SetAnimationInAnimator_Start_m747789719 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetAnimationInAnimator_Start_m747789719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set_animator_2(L_0);
		__this->set_currentAnimNum_3(0);
		return;
	}
}
// System.Void SetAnimationInAnimator::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1144830560;
extern Il2CppCodeGenString* _stringLiteral2626903706;
extern const uint32_t SetAnimationInAnimator_Update_m504862234_MetadataUsageId;
extern "C"  void SetAnimationInAnimator_Update_m504862234 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetAnimationInAnimator_Update_m504862234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1749539436(NULL /*static, unused*/, _stringLiteral1144830560, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_004b;
		}
	}
	{
		Animator_t69676727 * L_1 = __this->get_animator_2();
		int32_t L_2 = __this->get_currentAnimNum_3();
		NullCheck(L_1);
		Animator_SetInteger_m528582597(L_1, _stringLiteral2626903706, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_currentAnimNum_3();
		__this->set_currentAnimNum_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = __this->get_currentAnimNum_3();
		if ((((int32_t)L_4) < ((int32_t)5)))
		{
			goto IL_004a;
		}
	}
	{
		__this->set_currentAnimNum_3(0);
	}

IL_004a:
	{
	}

IL_004b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
