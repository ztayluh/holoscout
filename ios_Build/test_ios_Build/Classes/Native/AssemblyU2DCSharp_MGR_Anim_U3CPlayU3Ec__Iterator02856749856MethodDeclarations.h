﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Anim/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t2856749856;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Anim/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m1257570099 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Anim/<Play>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m4201796769 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Anim/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m504323601 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Anim/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2596265641 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Anim/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m2829949778 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Anim/<Play>c__Iterator0::Reset()
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m1939598112 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
