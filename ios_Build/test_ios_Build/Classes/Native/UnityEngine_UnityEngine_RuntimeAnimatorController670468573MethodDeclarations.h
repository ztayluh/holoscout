﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t670468573;
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t3936083219;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationClip[] UnityEngine.RuntimeAnimatorController::get_animationClips()
extern "C"  AnimationClipU5BU5D_t3936083219* RuntimeAnimatorController_get_animationClips_m3074969690 (RuntimeAnimatorController_t670468573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
