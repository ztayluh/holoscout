﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_CreateMenu
struct  MGR_CreateMenu_t817873244  : public MonoBehaviour_t1158329972
{
public:
	// System.String[] MGR_CreateMenu::clipNames
	StringU5BU5D_t1642385972* ___clipNames_2;
	// UnityEngine.GameObject MGR_CreateMenu::myGO
	GameObject_t1756533147 * ___myGO_3;
	// UnityEngine.Animator MGR_CreateMenu::myAnimator
	Animator_t69676727 * ___myAnimator_4;
	// System.Collections.Generic.List`1<System.String> MGR_CreateMenu::playlistItems
	List_1_t1398341365 * ___playlistItems_5;

public:
	inline static int32_t get_offset_of_clipNames_2() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___clipNames_2)); }
	inline StringU5BU5D_t1642385972* get_clipNames_2() const { return ___clipNames_2; }
	inline StringU5BU5D_t1642385972** get_address_of_clipNames_2() { return &___clipNames_2; }
	inline void set_clipNames_2(StringU5BU5D_t1642385972* value)
	{
		___clipNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___clipNames_2, value);
	}

	inline static int32_t get_offset_of_myGO_3() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___myGO_3)); }
	inline GameObject_t1756533147 * get_myGO_3() const { return ___myGO_3; }
	inline GameObject_t1756533147 ** get_address_of_myGO_3() { return &___myGO_3; }
	inline void set_myGO_3(GameObject_t1756533147 * value)
	{
		___myGO_3 = value;
		Il2CppCodeGenWriteBarrier(&___myGO_3, value);
	}

	inline static int32_t get_offset_of_myAnimator_4() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___myAnimator_4)); }
	inline Animator_t69676727 * get_myAnimator_4() const { return ___myAnimator_4; }
	inline Animator_t69676727 ** get_address_of_myAnimator_4() { return &___myAnimator_4; }
	inline void set_myAnimator_4(Animator_t69676727 * value)
	{
		___myAnimator_4 = value;
		Il2CppCodeGenWriteBarrier(&___myAnimator_4, value);
	}

	inline static int32_t get_offset_of_playlistItems_5() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___playlistItems_5)); }
	inline List_1_t1398341365 * get_playlistItems_5() const { return ___playlistItems_5; }
	inline List_1_t1398341365 ** get_address_of_playlistItems_5() { return &___playlistItems_5; }
	inline void set_playlistItems_5(List_1_t1398341365 * value)
	{
		___playlistItems_5 = value;
		Il2CppCodeGenWriteBarrier(&___playlistItems_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
