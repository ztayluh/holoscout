﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud/<readHashDB>c__Iterator4
struct U3CreadHashDBU3Ec__Iterator4_t2884699250;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Cloud/<readHashDB>c__Iterator4::.ctor()
extern "C"  void U3CreadHashDBU3Ec__Iterator4__ctor_m3255918799 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Cloud/<readHashDB>c__Iterator4::MoveNext()
extern "C"  bool U3CreadHashDBU3Ec__Iterator4_MoveNext_m2665498253 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readHashDB>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m761012313 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readHashDB>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3044320145 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readHashDB>c__Iterator4::Dispose()
extern "C"  void U3CreadHashDBU3Ec__Iterator4_Dispose_m2996611800 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readHashDB>c__Iterator4::Reset()
extern "C"  void U3CreadHashDBU3Ec__Iterator4_Reset_m3436070926 (U3CreadHashDBU3Ec__Iterator4_t2884699250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
