﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// StartSkrollin
struct StartSkrollin_t1317199858;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DUnityScript_StartMenuLevel2952164235.h"
#include "AssemblyU2DUnityScript_StartMenuLevel2952164235MethodDeclarations.h"
#include "AssemblyU2DUnityScript_StartSkrollin1317199858.h"
#include "AssemblyU2DUnityScript_StartSkrollin1317199858MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void StartSkrollin::.ctor()
extern "C"  void StartSkrollin__ctor_m1758154826 (StartSkrollin_t1317199858 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartSkrollin::Awake()
extern "C"  void StartSkrollin_Awake_m4163082549 (StartSkrollin_t1317199858 * __this, const MethodInfo* method)
{
	{
		__this->set_menuLevel_72(0);
		__this->set_deltaY_42((((float)((float)0))));
		__this->set_hazSkrollin_44((bool)0);
		return;
	}
}
// System.Void StartSkrollin::Start()
extern "C"  void StartSkrollin_Start_m2806183434 (StartSkrollin_t1317199858 * __this, const MethodInfo* method)
{
	{
		TextAsset_t3973159845 * L_0 = __this->get_introTextAsset_3();
		NullCheck(L_0);
		String_t* L_1 = TextAsset_get_text_m2589865997(L_0, /*hidden argument*/NULL);
		__this->set_introText_8(L_1);
		TextAsset_t3973159845 * L_2 = __this->get_babaTextAsset_4();
		NullCheck(L_2);
		String_t* L_3 = TextAsset_get_text_m2589865997(L_2, /*hidden argument*/NULL);
		__this->set_babaText_9(L_3);
		TextAsset_t3973159845 * L_4 = __this->get_karoliTextAsset_5();
		NullCheck(L_4);
		String_t* L_5 = TextAsset_get_text_m2589865997(L_4, /*hidden argument*/NULL);
		__this->set_karoliText_10(L_5);
		TextAsset_t3973159845 * L_6 = __this->get_loremTextAsset_6();
		NullCheck(L_6);
		String_t* L_7 = TextAsset_get_text_m2589865997(L_6, /*hidden argument*/NULL);
		__this->set_loremText_11(L_7);
		TextAsset_t3973159845 * L_8 = __this->get_rolandTextAsset_7();
		NullCheck(L_8);
		String_t* L_9 = TextAsset_get_text_m2589865997(L_8, /*hidden argument*/NULL);
		__this->set_rolandText_12(L_9);
		return;
	}
}
// System.Void StartSkrollin::OnGUI()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* WindowFunction_t3486805455_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1057720270;
extern Il2CppCodeGenString* _stringLiteral1006879746;
extern Il2CppCodeGenString* _stringLiteral2435266816;
extern Il2CppCodeGenString* _stringLiteral2671768982;
extern Il2CppCodeGenString* _stringLiteral3138505722;
extern Il2CppCodeGenString* _stringLiteral2233079820;
extern Il2CppCodeGenString* _stringLiteral1757116141;
extern Il2CppCodeGenString* _stringLiteral3829823908;
extern Il2CppCodeGenString* _stringLiteral1281338459;
extern Il2CppCodeGenString* _stringLiteral2103682387;
extern Il2CppCodeGenString* _stringLiteral3161336182;
extern Il2CppCodeGenString* _stringLiteral634842547;
extern Il2CppCodeGenString* _stringLiteral3173230063;
extern Il2CppCodeGenString* _stringLiteral3889149889;
extern Il2CppCodeGenString* _stringLiteral2401532284;
extern Il2CppCodeGenString* _stringLiteral2963722944;
extern Il2CppCodeGenString* _stringLiteral2778558511;
extern Il2CppCodeGenString* _stringLiteral1383890222;
extern Il2CppCodeGenString* _stringLiteral4235102328;
extern Il2CppCodeGenString* _stringLiteral1982654410;
extern Il2CppCodeGenString* _stringLiteral3852433285;
extern Il2CppCodeGenString* _stringLiteral1371885191;
extern Il2CppCodeGenString* _stringLiteral3414426824;
extern Il2CppCodeGenString* _stringLiteral963635650;
extern Il2CppCodeGenString* _stringLiteral2504383212;
extern Il2CppCodeGenString* _stringLiteral490568597;
extern Il2CppCodeGenString* _stringLiteral2374011071;
extern const uint32_t StartSkrollin_OnGUI_m1255672610_MetadataUsageId;
extern "C"  void StartSkrollin_OnGUI_m1255672610 (StartSkrollin_t1317199858 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartSkrollin_OnGUI_m1255672610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GUIStyle_t1799908754 * V_1 = NULL;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GUISkin_t1436893342 * L_0 = __this->get_gSkin_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		GUISkin_t1436893342 * L_2 = __this->get_gSkin_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_skin_m3391676555(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_002a;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1057720270, /*hidden argument*/NULL);
	}

IL_002a:
	{
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469(&L_3, (((float)((float)0))), (((float)((float)0))), (((float)((float)((int32_t)320)))), (((float)((float)((int32_t)480)))), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_4 = __this->get_startBG_15();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_menuLevel_72();
		V_0 = L_5;
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)0))))
		{
			goto IL_0333;
		}
	}
	{
		int32_t L_7 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m1220545469(&L_8, ((float)((float)(((float)((float)L_7)))*(float)(0.05f))), (((float)((float)((int32_t)60)))), (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_9 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2435266816, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_8, _stringLiteral1006879746, L_9, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Rect__ctor_m1220545469(&L_11, ((float)((float)(((float)((float)L_10)))*(float)(0.1f))), (((float)((float)((int32_t)80)))), (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_12 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral3138505722, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_11, _stringLiteral2671768982, L_12, /*hidden argument*/NULL);
		Rect_t3681755626  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Rect__ctor_m1220545469(&L_13, (((float)((float)((int32_t)160)))), (((float)((float)((int32_t)295)))), (((float)((float)((int32_t)150)))), (((float)((float)((int32_t)150)))), /*hidden argument*/NULL);
		String_t* L_14 = __this->get_introText_8();
		GUI_Label_m2412846501(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Rect_t3681755626  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Rect__ctor_m1220545469(&L_15, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)175)))), (((float)((float)((int32_t)100)))), (((float)((float)((int32_t)35)))), /*hidden argument*/NULL);
		bool L_16 = GUI_Button_m3054448581(NULL /*static, unused*/, L_15, _stringLiteral2233079820, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_015a;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_17 = __this->get_scrollMaxBaba_46();
		__this->set_scrollMax_39(L_17);
		float L_18 = __this->get_scrollMaxBaba_46();
		__this->set_scrollVector_37(L_18);
		float L_19 = __this->get_scrollMinBaba_51();
		__this->set_scrollMin_38(L_19);
		String_t* L_20 = __this->get_babaText_9();
		__this->set_windowText_13(L_20);
		Rect_t3681755626  L_21 = __this->get_babaWindowRect_57();
		__this->set_windowRect_63(L_21);
		__this->set_windowStyle_64(_stringLiteral1757116141);
		__this->set_menuLevel_72(1);
	}

IL_015a:
	{
		Rect_t3681755626  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Rect__ctor_m1220545469(&L_22, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)230)))), (((float)((float)((int32_t)100)))), (((float)((float)((int32_t)35)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_23 = GUI_Button_m3054448581(NULL /*static, unused*/, L_22, _stringLiteral3829823908, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_01d2;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_24 = __this->get_scrollMaxKaroli_47();
		__this->set_scrollMax_39(L_24);
		float L_25 = __this->get_scrollMaxKaroli_47();
		__this->set_scrollVector_37(L_25);
		float L_26 = __this->get_scrollMinKaroli_52();
		__this->set_scrollMin_38(L_26);
		String_t* L_27 = __this->get_karoliText_10();
		__this->set_windowText_13(L_27);
		Rect_t3681755626  L_28 = __this->get_karoliWindowRect_58();
		__this->set_windowRect_63(L_28);
		__this->set_windowStyle_64(_stringLiteral1281338459);
		__this->set_menuLevel_72(2);
	}

IL_01d2:
	{
		Rect_t3681755626  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Rect__ctor_m1220545469(&L_29, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)285)))), (((float)((float)((int32_t)100)))), (((float)((float)((int32_t)35)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_30 = GUI_Button_m3054448581(NULL /*static, unused*/, L_29, _stringLiteral2103682387, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_024a;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_31 = __this->get_scrollMaxLorem_48();
		__this->set_scrollMax_39(L_31);
		float L_32 = __this->get_scrollMaxLorem_48();
		__this->set_scrollVector_37(L_32);
		float L_33 = __this->get_scrollMinLorem_53();
		__this->set_scrollMin_38(L_33);
		String_t* L_34 = __this->get_loremText_11();
		__this->set_windowText_13(L_34);
		Rect_t3681755626  L_35 = __this->get_loremWindowRect_59();
		__this->set_windowRect_63(L_35);
		__this->set_windowStyle_64(_stringLiteral3161336182);
		__this->set_menuLevel_72(3);
	}

IL_024a:
	{
		Rect_t3681755626  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Rect__ctor_m1220545469(&L_36, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)340)))), (((float)((float)((int32_t)100)))), (((float)((float)((int32_t)35)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_37 = GUI_Button_m3054448581(NULL /*static, unused*/, L_36, _stringLiteral634842547, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_02c2;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_38 = __this->get_scrollMaxRoland_49();
		__this->set_scrollMax_39(L_38);
		float L_39 = __this->get_scrollMaxRoland_49();
		__this->set_scrollVector_37(L_39);
		float L_40 = __this->get_scrollMinRoland_54();
		__this->set_scrollMin_38(L_40);
		String_t* L_41 = __this->get_rolandText_12();
		__this->set_windowText_13(L_41);
		Rect_t3681755626  L_42 = __this->get_rolandWindowRect_60();
		__this->set_windowRect_63(L_42);
		__this->set_windowStyle_64(_stringLiteral3173230063);
		__this->set_menuLevel_72(4);
	}

IL_02c2:
	{
		Rect_t3681755626  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Rect__ctor_m1220545469(&L_43, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)395)))), (((float)((float)((int32_t)100)))), (((float)((float)((int32_t)35)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_44 = GUI_Button_m3054448581(NULL /*static, unused*/, L_43, _stringLiteral3889149889, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_032e;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_45 = __this->get_scrollMaxButtons_50();
		__this->set_scrollMax_39(L_45);
		float L_46 = __this->get_scrollMaxButtons_50();
		__this->set_scrollVector_37(L_46);
		float L_47 = __this->get_scrollMinButtons_55();
		__this->set_scrollMin_38(L_47);
		Rect_t3681755626  L_48 = __this->get_buttonsWindowRect_61();
		__this->set_windowRect_63(L_48);
		__this->set_windowStyle_64(_stringLiteral2401532284);
		__this->set_menuLevel_72(5);
	}

IL_032e:
	{
		goto IL_0c50;
	}

IL_0333:
	{
		int32_t L_49 = V_0;
		if ((!(((uint32_t)L_49) == ((uint32_t)1))))
		{
			goto IL_04ee;
		}
	}
	{
		GUIStyle_t1799908754 * L_50 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_50, /*hidden argument*/NULL);
		V_1 = L_50;
		Rect_t3681755626  L_51 = __this->get_windowRect_63();
		IntPtr_t L_52;
		L_52.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 7));
		WindowFunction_t3486805455 * L_53 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_53, __this, L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_55 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_51, L_53, L_54, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_55);
		int32_t L_56 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_57;
		memset(&L_57, 0, sizeof(L_57));
		Rect__ctor_m1220545469(&L_57, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_56/(int32_t)2))-(int32_t)((int32_t)100)))))), (((float)((float)0))), (((float)((float)((int32_t)200)))), (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_58 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2963722944, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_57, _stringLiteral2233079820, L_58, /*hidden argument*/NULL);
		int32_t L_59 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_60;
		memset(&L_60, 0, sizeof(L_60));
		Rect__ctor_m1220545469(&L_60, ((float)((float)((float)((float)(((float)((float)L_59)))*(float)(0.5f)))-(float)(((float)((float)((int32_t)70)))))), (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)140)))), (((float)((float)((int32_t)70)))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_61 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral1383890222, /*hidden argument*/NULL);
		bool L_62 = GUI_Button_m2147724592(NULL /*static, unused*/, L_60, _stringLiteral2778558511, L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_03de;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_menuLevel_72(0);
	}

IL_03de:
	{
		bool L_63 = __this->get_izSkrollin_45();
		if (!L_63)
		{
			goto IL_04e9;
		}
	}
	{
		GUIStyle_t1799908754 * L_64 = V_1;
		NullCheck(L_64);
		GUIStyleState_t3801000545 * L_65 = GUIStyle_get_normal_m2789468942(L_64, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_66 = __this->get_iconVector_14();
		NullCheck(L_65);
		GUIStyleState_set_background_m3931679679(L_65, L_66, /*hidden argument*/NULL);
		Rect_t3681755626 * L_67 = __this->get_address_of_windowRect_63();
		float L_68 = Rect_get_x_m1393582490(L_67, /*hidden argument*/NULL);
		Rect_t3681755626 * L_69 = __this->get_address_of_windowRect_63();
		float L_70 = Rect_get_width_m1138015702(L_69, /*hidden argument*/NULL);
		Rect_t3681755626 * L_71 = __this->get_address_of_windowRect_63();
		float L_72 = Rect_get_y_m1393582395(L_71, /*hidden argument*/NULL);
		float L_73 = __this->get_scrollVector_37();
		float L_74 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_75 = __this->get_address_of_windowRect_63();
		float L_76 = Rect_get_height_m3128694305(L_75, /*hidden argument*/NULL);
		Vector2_t2243707579  L_77;
		memset(&L_77, 0, sizeof(L_77));
		Vector2__ctor_m3067419446(&L_77, ((float)((float)((float)((float)L_68+(float)L_70))-(float)(((float)((float)((int32_t)13)))))), ((float)((float)L_72-(float)((float)((float)L_73/(float)((float)((float)L_74/(float)((float)((float)L_76-(float)(((float)((float)((int32_t)25)))))))))))), /*hidden argument*/NULL);
		V_2 = L_77;
		float L_78 = (&V_2)->get_y_1();
		float L_79 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_80 = __this->get_address_of_windowRect_63();
		float L_81 = Rect_get_y_m1393582395(L_80, /*hidden argument*/NULL);
		if ((((float)L_78) >= ((float)((float)((float)L_79+(float)L_81)))))
		{
			goto IL_047b;
		}
	}
	{
		float L_82 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_83 = __this->get_address_of_windowRect_63();
		float L_84 = Rect_get_y_m1393582395(L_83, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)L_82+(float)L_84)));
	}

IL_047b:
	{
		float L_85 = (&V_2)->get_y_1();
		Rect_t3681755626 * L_86 = __this->get_address_of_windowRect_63();
		float L_87 = Rect_get_y_m1393582395(L_86, /*hidden argument*/NULL);
		Rect_t3681755626 * L_88 = __this->get_address_of_windowRect_63();
		float L_89 = Rect_get_height_m3128694305(L_88, /*hidden argument*/NULL);
		if ((((float)L_85) <= ((float)((float)((float)((float)((float)L_87+(float)L_89))-(float)(((float)((float)((int32_t)25)))))))))
		{
			goto IL_04c4;
		}
	}
	{
		Rect_t3681755626 * L_90 = __this->get_address_of_windowRect_63();
		float L_91 = Rect_get_y_m1393582395(L_90, /*hidden argument*/NULL);
		Rect_t3681755626 * L_92 = __this->get_address_of_windowRect_63();
		float L_93 = Rect_get_height_m3128694305(L_92, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)((float)((float)L_91+(float)L_93))-(float)(((float)((float)((int32_t)25)))))));
	}

IL_04c4:
	{
		float L_94 = (&V_2)->get_x_0();
		float L_95 = (&V_2)->get_y_1();
		Rect_t3681755626  L_96;
		memset(&L_96, 0, sizeof(L_96));
		Rect__ctor_m1220545469(&L_96, L_94, L_95, (((float)((float)((int32_t)12)))), (((float)((float)((int32_t)25)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_98 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_96, L_97, L_98, /*hidden argument*/NULL);
	}

IL_04e9:
	{
		goto IL_0c50;
	}

IL_04ee:
	{
		int32_t L_99 = V_0;
		if ((!(((uint32_t)L_99) == ((uint32_t)2))))
		{
			goto IL_06cc;
		}
	}
	{
		GUIStyle_t1799908754 * L_100 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_100, /*hidden argument*/NULL);
		V_1 = L_100;
		Rect_t3681755626  L_101 = __this->get_windowRect_63();
		IntPtr_t L_102;
		L_102.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 7));
		WindowFunction_t3486805455 * L_103 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_103, __this, L_102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_104 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_105 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_101, L_103, L_104, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_105);
		int32_t L_106 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_107;
		memset(&L_107, 0, sizeof(L_107));
		Rect__ctor_m1220545469(&L_107, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_106/(int32_t)2))-(int32_t)((int32_t)150)))))), (((float)((float)0))), (((float)((float)((int32_t)300)))), (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_108 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral4235102328, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_107, _stringLiteral3829823908, L_108, /*hidden argument*/NULL);
		Rect_t3681755626  L_109;
		memset(&L_109, 0, sizeof(L_109));
		Rect__ctor_m1220545469(&L_109, (((float)((float)((int32_t)175)))), (((float)((float)((int32_t)110)))), (((float)((float)((int32_t)200)))), (((float)((float)((int32_t)275)))), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_110 = __this->get_charlemagne_16();
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		int32_t L_111 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_112;
		memset(&L_112, 0, sizeof(L_112));
		Rect__ctor_m1220545469(&L_112, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_111/(int32_t)2))-(int32_t)((int32_t)70)))))), (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)140)))), (((float)((float)((int32_t)70)))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_113 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral1982654410, /*hidden argument*/NULL);
		bool L_114 = GUI_Button_m2147724592(NULL /*static, unused*/, L_112, _stringLiteral2778558511, L_113, /*hidden argument*/NULL);
		if (!L_114)
		{
			goto IL_05bc;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_menuLevel_72(0);
	}

IL_05bc:
	{
		bool L_115 = __this->get_izSkrollin_45();
		if (!L_115)
		{
			goto IL_06c7;
		}
	}
	{
		GUIStyle_t1799908754 * L_116 = V_1;
		NullCheck(L_116);
		GUIStyleState_t3801000545 * L_117 = GUIStyle_get_normal_m2789468942(L_116, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_118 = __this->get_iconVector_14();
		NullCheck(L_117);
		GUIStyleState_set_background_m3931679679(L_117, L_118, /*hidden argument*/NULL);
		Rect_t3681755626 * L_119 = __this->get_address_of_windowRect_63();
		float L_120 = Rect_get_x_m1393582490(L_119, /*hidden argument*/NULL);
		Rect_t3681755626 * L_121 = __this->get_address_of_windowRect_63();
		float L_122 = Rect_get_width_m1138015702(L_121, /*hidden argument*/NULL);
		Rect_t3681755626 * L_123 = __this->get_address_of_windowRect_63();
		float L_124 = Rect_get_y_m1393582395(L_123, /*hidden argument*/NULL);
		float L_125 = __this->get_scrollVector_37();
		float L_126 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_127 = __this->get_address_of_windowRect_63();
		float L_128 = Rect_get_height_m3128694305(L_127, /*hidden argument*/NULL);
		Vector2_t2243707579  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Vector2__ctor_m3067419446(&L_129, ((float)((float)((float)((float)L_120+(float)L_122))-(float)(((float)((float)((int32_t)13)))))), ((float)((float)L_124-(float)((float)((float)L_125/(float)((float)((float)L_126/(float)((float)((float)L_128-(float)(((float)((float)((int32_t)25)))))))))))), /*hidden argument*/NULL);
		V_2 = L_129;
		float L_130 = (&V_2)->get_y_1();
		float L_131 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_132 = __this->get_address_of_windowRect_63();
		float L_133 = Rect_get_y_m1393582395(L_132, /*hidden argument*/NULL);
		if ((((float)L_130) >= ((float)((float)((float)L_131+(float)L_133)))))
		{
			goto IL_0659;
		}
	}
	{
		float L_134 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_135 = __this->get_address_of_windowRect_63();
		float L_136 = Rect_get_y_m1393582395(L_135, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)L_134+(float)L_136)));
	}

IL_0659:
	{
		float L_137 = (&V_2)->get_y_1();
		Rect_t3681755626 * L_138 = __this->get_address_of_windowRect_63();
		float L_139 = Rect_get_y_m1393582395(L_138, /*hidden argument*/NULL);
		Rect_t3681755626 * L_140 = __this->get_address_of_windowRect_63();
		float L_141 = Rect_get_height_m3128694305(L_140, /*hidden argument*/NULL);
		if ((((float)L_137) <= ((float)((float)((float)((float)((float)L_139+(float)L_141))-(float)(((float)((float)((int32_t)25)))))))))
		{
			goto IL_06a2;
		}
	}
	{
		Rect_t3681755626 * L_142 = __this->get_address_of_windowRect_63();
		float L_143 = Rect_get_y_m1393582395(L_142, /*hidden argument*/NULL);
		Rect_t3681755626 * L_144 = __this->get_address_of_windowRect_63();
		float L_145 = Rect_get_height_m3128694305(L_144, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)((float)((float)L_143+(float)L_145))-(float)(((float)((float)((int32_t)25)))))));
	}

IL_06a2:
	{
		float L_146 = (&V_2)->get_x_0();
		float L_147 = (&V_2)->get_y_1();
		Rect_t3681755626  L_148;
		memset(&L_148, 0, sizeof(L_148));
		Rect__ctor_m1220545469(&L_148, L_146, L_147, (((float)((float)((int32_t)12)))), (((float)((float)((int32_t)25)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_149 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_150 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_148, L_149, L_150, /*hidden argument*/NULL);
	}

IL_06c7:
	{
		goto IL_0c50;
	}

IL_06cc:
	{
		int32_t L_151 = V_0;
		if ((!(((uint32_t)L_151) == ((uint32_t)3))))
		{
			goto IL_08a9;
		}
	}
	{
		GUIStyle_t1799908754 * L_152 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_152, /*hidden argument*/NULL);
		V_1 = L_152;
		Rect_t3681755626  L_153 = __this->get_windowRect_63();
		IntPtr_t L_154;
		L_154.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 7));
		WindowFunction_t3486805455 * L_155 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_155, __this, L_154, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_156 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_157 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_153, L_155, L_156, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_157);
		int32_t L_158 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_159;
		memset(&L_159, 0, sizeof(L_159));
		Rect__ctor_m1220545469(&L_159, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_158/(int32_t)2))-(int32_t)((int32_t)100)))))), (((float)((float)0))), (((float)((float)((int32_t)200)))), (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_160 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral3852433285, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_159, _stringLiteral2103682387, L_160, /*hidden argument*/NULL);
		Rect_t3681755626  L_161;
		memset(&L_161, 0, sizeof(L_161));
		Rect__ctor_m1220545469(&L_161, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)315)))), (((float)((float)((int32_t)300)))), (((float)((float)((int32_t)75)))), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_162 = __this->get_map_17();
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_161, L_162, /*hidden argument*/NULL);
		int32_t L_163 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_164;
		memset(&L_164, 0, sizeof(L_164));
		Rect__ctor_m1220545469(&L_164, ((float)((float)((float)((float)(((float)((float)L_163)))*(float)(0.5f)))-(float)(((float)((float)((int32_t)70)))))), (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)140)))), (((float)((float)((int32_t)70)))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_165 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral1371885191, /*hidden argument*/NULL);
		bool L_166 = GUI_Button_m2147724592(NULL /*static, unused*/, L_164, _stringLiteral2778558511, L_165, /*hidden argument*/NULL);
		if (!L_166)
		{
			goto IL_0799;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_menuLevel_72(0);
	}

IL_0799:
	{
		bool L_167 = __this->get_izSkrollin_45();
		if (!L_167)
		{
			goto IL_08a4;
		}
	}
	{
		GUIStyle_t1799908754 * L_168 = V_1;
		NullCheck(L_168);
		GUIStyleState_t3801000545 * L_169 = GUIStyle_get_normal_m2789468942(L_168, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_170 = __this->get_iconVector_14();
		NullCheck(L_169);
		GUIStyleState_set_background_m3931679679(L_169, L_170, /*hidden argument*/NULL);
		Rect_t3681755626 * L_171 = __this->get_address_of_windowRect_63();
		float L_172 = Rect_get_x_m1393582490(L_171, /*hidden argument*/NULL);
		Rect_t3681755626 * L_173 = __this->get_address_of_windowRect_63();
		float L_174 = Rect_get_width_m1138015702(L_173, /*hidden argument*/NULL);
		Rect_t3681755626 * L_175 = __this->get_address_of_windowRect_63();
		float L_176 = Rect_get_y_m1393582395(L_175, /*hidden argument*/NULL);
		float L_177 = __this->get_scrollVector_37();
		float L_178 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_179 = __this->get_address_of_windowRect_63();
		float L_180 = Rect_get_height_m3128694305(L_179, /*hidden argument*/NULL);
		Vector2_t2243707579  L_181;
		memset(&L_181, 0, sizeof(L_181));
		Vector2__ctor_m3067419446(&L_181, ((float)((float)((float)((float)L_172+(float)L_174))-(float)(((float)((float)((int32_t)13)))))), ((float)((float)L_176-(float)((float)((float)L_177/(float)((float)((float)L_178/(float)((float)((float)L_180-(float)(((float)((float)((int32_t)25)))))))))))), /*hidden argument*/NULL);
		V_2 = L_181;
		float L_182 = (&V_2)->get_y_1();
		float L_183 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_184 = __this->get_address_of_windowRect_63();
		float L_185 = Rect_get_y_m1393582395(L_184, /*hidden argument*/NULL);
		if ((((float)L_182) >= ((float)((float)((float)L_183+(float)L_185)))))
		{
			goto IL_0836;
		}
	}
	{
		float L_186 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_187 = __this->get_address_of_windowRect_63();
		float L_188 = Rect_get_y_m1393582395(L_187, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)L_186+(float)L_188)));
	}

IL_0836:
	{
		float L_189 = (&V_2)->get_y_1();
		Rect_t3681755626 * L_190 = __this->get_address_of_windowRect_63();
		float L_191 = Rect_get_y_m1393582395(L_190, /*hidden argument*/NULL);
		Rect_t3681755626 * L_192 = __this->get_address_of_windowRect_63();
		float L_193 = Rect_get_height_m3128694305(L_192, /*hidden argument*/NULL);
		if ((((float)L_189) <= ((float)((float)((float)((float)((float)L_191+(float)L_193))-(float)(((float)((float)((int32_t)25)))))))))
		{
			goto IL_087f;
		}
	}
	{
		Rect_t3681755626 * L_194 = __this->get_address_of_windowRect_63();
		float L_195 = Rect_get_y_m1393582395(L_194, /*hidden argument*/NULL);
		Rect_t3681755626 * L_196 = __this->get_address_of_windowRect_63();
		float L_197 = Rect_get_height_m3128694305(L_196, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)((float)((float)L_195+(float)L_197))-(float)(((float)((float)((int32_t)25)))))));
	}

IL_087f:
	{
		float L_198 = (&V_2)->get_x_0();
		float L_199 = (&V_2)->get_y_1();
		Rect_t3681755626  L_200;
		memset(&L_200, 0, sizeof(L_200));
		Rect__ctor_m1220545469(&L_200, L_198, L_199, (((float)((float)((int32_t)12)))), (((float)((float)((int32_t)25)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_201 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_202 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_200, L_201, L_202, /*hidden argument*/NULL);
	}

IL_08a4:
	{
		goto IL_0c50;
	}

IL_08a9:
	{
		int32_t L_203 = V_0;
		if ((!(((uint32_t)L_203) == ((uint32_t)4))))
		{
			goto IL_0a83;
		}
	}
	{
		GUIStyle_t1799908754 * L_204 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_204, /*hidden argument*/NULL);
		V_1 = L_204;
		Rect_t3681755626  L_205 = __this->get_windowRect_63();
		IntPtr_t L_206;
		L_206.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 7));
		WindowFunction_t3486805455 * L_207 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_207, __this, L_206, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_208 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_209 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_205, L_207, L_208, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_209);
		int32_t L_210 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_211;
		memset(&L_211, 0, sizeof(L_211));
		Rect__ctor_m1220545469(&L_211, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_210/(int32_t)2))-(int32_t)((int32_t)100)))))), (((float)((float)0))), (((float)((float)((int32_t)200)))), (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_212 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral963635650, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_211, _stringLiteral3414426824, L_212, /*hidden argument*/NULL);
		Rect_t3681755626  L_213;
		memset(&L_213, 0, sizeof(L_213));
		Rect__ctor_m1220545469(&L_213, (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)95)))), (((float)((float)((int32_t)300)))), (((float)((float)((int32_t)90)))), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_214 = __this->get_page_18();
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_213, L_214, /*hidden argument*/NULL);
		int32_t L_215 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_216;
		memset(&L_216, 0, sizeof(L_216));
		Rect__ctor_m1220545469(&L_216, ((float)((float)((float)((float)(((float)((float)L_215)))*(float)(0.5f)))-(float)(((float)((float)((int32_t)70)))))), (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)140)))), (((float)((float)((int32_t)70)))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_217 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2504383212, /*hidden argument*/NULL);
		bool L_218 = GUI_Button_m2147724592(NULL /*static, unused*/, L_216, _stringLiteral2778558511, L_217, /*hidden argument*/NULL);
		if (!L_218)
		{
			goto IL_0973;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_menuLevel_72(0);
	}

IL_0973:
	{
		bool L_219 = __this->get_izSkrollin_45();
		if (!L_219)
		{
			goto IL_0a7e;
		}
	}
	{
		GUIStyle_t1799908754 * L_220 = V_1;
		NullCheck(L_220);
		GUIStyleState_t3801000545 * L_221 = GUIStyle_get_normal_m2789468942(L_220, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_222 = __this->get_iconVector_14();
		NullCheck(L_221);
		GUIStyleState_set_background_m3931679679(L_221, L_222, /*hidden argument*/NULL);
		Rect_t3681755626 * L_223 = __this->get_address_of_windowRect_63();
		float L_224 = Rect_get_x_m1393582490(L_223, /*hidden argument*/NULL);
		Rect_t3681755626 * L_225 = __this->get_address_of_windowRect_63();
		float L_226 = Rect_get_width_m1138015702(L_225, /*hidden argument*/NULL);
		Rect_t3681755626 * L_227 = __this->get_address_of_windowRect_63();
		float L_228 = Rect_get_y_m1393582395(L_227, /*hidden argument*/NULL);
		float L_229 = __this->get_scrollVector_37();
		float L_230 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_231 = __this->get_address_of_windowRect_63();
		float L_232 = Rect_get_height_m3128694305(L_231, /*hidden argument*/NULL);
		Vector2_t2243707579  L_233;
		memset(&L_233, 0, sizeof(L_233));
		Vector2__ctor_m3067419446(&L_233, ((float)((float)((float)((float)L_224+(float)L_226))-(float)(((float)((float)((int32_t)13)))))), ((float)((float)L_228-(float)((float)((float)L_229/(float)((float)((float)L_230/(float)((float)((float)L_232-(float)(((float)((float)((int32_t)25)))))))))))), /*hidden argument*/NULL);
		V_2 = L_233;
		float L_234 = (&V_2)->get_y_1();
		float L_235 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_236 = __this->get_address_of_windowRect_63();
		float L_237 = Rect_get_y_m1393582395(L_236, /*hidden argument*/NULL);
		if ((((float)L_234) >= ((float)((float)((float)L_235+(float)L_237)))))
		{
			goto IL_0a10;
		}
	}
	{
		float L_238 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_239 = __this->get_address_of_windowRect_63();
		float L_240 = Rect_get_y_m1393582395(L_239, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)L_238+(float)L_240)));
	}

IL_0a10:
	{
		float L_241 = (&V_2)->get_y_1();
		Rect_t3681755626 * L_242 = __this->get_address_of_windowRect_63();
		float L_243 = Rect_get_y_m1393582395(L_242, /*hidden argument*/NULL);
		Rect_t3681755626 * L_244 = __this->get_address_of_windowRect_63();
		float L_245 = Rect_get_height_m3128694305(L_244, /*hidden argument*/NULL);
		if ((((float)L_241) <= ((float)((float)((float)((float)((float)L_243+(float)L_245))-(float)(((float)((float)((int32_t)25)))))))))
		{
			goto IL_0a59;
		}
	}
	{
		Rect_t3681755626 * L_246 = __this->get_address_of_windowRect_63();
		float L_247 = Rect_get_y_m1393582395(L_246, /*hidden argument*/NULL);
		Rect_t3681755626 * L_248 = __this->get_address_of_windowRect_63();
		float L_249 = Rect_get_height_m3128694305(L_248, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)((float)((float)L_247+(float)L_249))-(float)(((float)((float)((int32_t)25)))))));
	}

IL_0a59:
	{
		float L_250 = (&V_2)->get_x_0();
		float L_251 = (&V_2)->get_y_1();
		Rect_t3681755626  L_252;
		memset(&L_252, 0, sizeof(L_252));
		Rect__ctor_m1220545469(&L_252, L_250, L_251, (((float)((float)((int32_t)12)))), (((float)((float)((int32_t)25)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_253 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_254 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_252, L_253, L_254, /*hidden argument*/NULL);
	}

IL_0a7e:
	{
		goto IL_0c50;
	}

IL_0a83:
	{
		int32_t L_255 = V_0;
		if ((!(((uint32_t)L_255) == ((uint32_t)5))))
		{
			goto IL_0c50;
		}
	}
	{
		GUIStyle_t1799908754 * L_256 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_256, /*hidden argument*/NULL);
		V_1 = L_256;
		int32_t L_257 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_258;
		memset(&L_258, 0, sizeof(L_258));
		Rect__ctor_m1220545469(&L_258, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_257/(int32_t)2))-(int32_t)((int32_t)100)))))), (((float)((float)((int32_t)25)))), (((float)((float)((int32_t)200)))), (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_259 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral490568597, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_258, _stringLiteral3889149889, L_259, /*hidden argument*/NULL);
		Rect_t3681755626  L_260 = __this->get_windowRect_63();
		IntPtr_t L_261;
		L_261.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 8));
		WindowFunction_t3486805455 * L_262 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_262, __this, L_261, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_263 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Rect_t3681755626  L_264 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_260, L_262, L_263, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_264);
		int32_t L_265 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_266 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_267;
		memset(&L_267, 0, sizeof(L_267));
		Rect__ctor_m1220545469(&L_267, ((float)((float)((float)((float)(((float)((float)L_265)))*(float)(0.5f)))-(float)(((float)((float)((int32_t)70)))))), (((float)((float)((int32_t)((int32_t)L_266-(int32_t)((int32_t)80)))))), (((float)((float)((int32_t)140)))), (((float)((float)((int32_t)70)))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_268 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2374011071, /*hidden argument*/NULL);
		bool L_269 = GUI_Button_m2147724592(NULL /*static, unused*/, L_267, _stringLiteral2778558511, L_268, /*hidden argument*/NULL);
		if (!L_269)
		{
			goto IL_0b40;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_hasResetHS_70((bool)0);
		__this->set_resetHSCheck_69((bool)0);
		__this->set_menuLevel_72(0);
	}

IL_0b40:
	{
		bool L_270 = __this->get_izSkrollin_45();
		if (!L_270)
		{
			goto IL_0c4b;
		}
	}
	{
		GUIStyle_t1799908754 * L_271 = V_1;
		NullCheck(L_271);
		GUIStyleState_t3801000545 * L_272 = GUIStyle_get_normal_m2789468942(L_271, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_273 = __this->get_iconVector_14();
		NullCheck(L_272);
		GUIStyleState_set_background_m3931679679(L_272, L_273, /*hidden argument*/NULL);
		Rect_t3681755626 * L_274 = __this->get_address_of_windowRect_63();
		float L_275 = Rect_get_x_m1393582490(L_274, /*hidden argument*/NULL);
		Rect_t3681755626 * L_276 = __this->get_address_of_windowRect_63();
		float L_277 = Rect_get_width_m1138015702(L_276, /*hidden argument*/NULL);
		Rect_t3681755626 * L_278 = __this->get_address_of_windowRect_63();
		float L_279 = Rect_get_y_m1393582395(L_278, /*hidden argument*/NULL);
		float L_280 = __this->get_scrollVector_37();
		float L_281 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_282 = __this->get_address_of_windowRect_63();
		float L_283 = Rect_get_height_m3128694305(L_282, /*hidden argument*/NULL);
		Vector2_t2243707579  L_284;
		memset(&L_284, 0, sizeof(L_284));
		Vector2__ctor_m3067419446(&L_284, ((float)((float)((float)((float)L_275+(float)L_277))-(float)(((float)((float)((int32_t)13)))))), ((float)((float)L_279-(float)((float)((float)L_280/(float)((float)((float)L_281/(float)((float)((float)L_283-(float)(((float)((float)((int32_t)25)))))))))))), /*hidden argument*/NULL);
		V_2 = L_284;
		float L_285 = (&V_2)->get_y_1();
		float L_286 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_287 = __this->get_address_of_windowRect_63();
		float L_288 = Rect_get_y_m1393582395(L_287, /*hidden argument*/NULL);
		if ((((float)L_285) >= ((float)((float)((float)L_286+(float)L_288)))))
		{
			goto IL_0bdd;
		}
	}
	{
		float L_289 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_290 = __this->get_address_of_windowRect_63();
		float L_291 = Rect_get_y_m1393582395(L_290, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)L_289+(float)L_291)));
	}

IL_0bdd:
	{
		float L_292 = (&V_2)->get_y_1();
		Rect_t3681755626 * L_293 = __this->get_address_of_windowRect_63();
		float L_294 = Rect_get_y_m1393582395(L_293, /*hidden argument*/NULL);
		Rect_t3681755626 * L_295 = __this->get_address_of_windowRect_63();
		float L_296 = Rect_get_height_m3128694305(L_295, /*hidden argument*/NULL);
		if ((((float)L_292) <= ((float)((float)((float)((float)((float)L_294+(float)L_296))-(float)(((float)((float)((int32_t)25)))))))))
		{
			goto IL_0c26;
		}
	}
	{
		Rect_t3681755626 * L_297 = __this->get_address_of_windowRect_63();
		float L_298 = Rect_get_y_m1393582395(L_297, /*hidden argument*/NULL);
		Rect_t3681755626 * L_299 = __this->get_address_of_windowRect_63();
		float L_300 = Rect_get_height_m3128694305(L_299, /*hidden argument*/NULL);
		(&V_2)->set_y_1(((float)((float)((float)((float)L_298+(float)L_300))-(float)(((float)((float)((int32_t)25)))))));
	}

IL_0c26:
	{
		float L_301 = (&V_2)->get_x_0();
		float L_302 = (&V_2)->get_y_1();
		Rect_t3681755626  L_303;
		memset(&L_303, 0, sizeof(L_303));
		Rect__ctor_m1220545469(&L_303, L_301, L_302, (((float)((float)((int32_t)12)))), (((float)((float)((int32_t)25)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_304 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_305 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_303, L_304, L_305, /*hidden argument*/NULL);
	}

IL_0c4b:
	{
		goto IL_0c50;
	}

IL_0c50:
	{
		return;
	}
}
// System.Void StartSkrollin::CreateWindow(System.Int32)
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t StartSkrollin_CreateWindow_m2936996973_MetadataUsageId;
extern "C"  void StartSkrollin_CreateWindow_m2936996973 (StartSkrollin_t1317199858 * __this, int32_t ___windowID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartSkrollin_CreateWindow_m2936996973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_windowPad_56();
		float L_1 = __this->get_scrollVector_37();
		Rect_t3681755626 * L_2 = __this->get_address_of_windowRect_63();
		float L_3 = Rect_get_width_m1138015702(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_windowPad_56();
		float L_5 = __this->get_scrollMin_38();
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, L_0, L_1, ((float)((float)L_3-(float)((float)((float)L_4*(float)(((float)((float)2))))))), L_5, /*hidden argument*/NULL);
		String_t* L_7 = __this->get_windowText_13();
		String_t* L_8 = __this->get_windowStyle_64();
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_9 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_6, L_7, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartSkrollin::CreateGroupWindow(System.Int32)
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1412472851;
extern Il2CppCodeGenString* _stringLiteral2401532284;
extern Il2CppCodeGenString* _stringLiteral3387269145;
extern Il2CppCodeGenString* _stringLiteral3021629903;
extern Il2CppCodeGenString* _stringLiteral3759753276;
extern Il2CppCodeGenString* _stringLiteral1496915101;
extern Il2CppCodeGenString* _stringLiteral2939335172;
extern Il2CppCodeGenString* _stringLiteral3387431150;
extern Il2CppCodeGenString* _stringLiteral991689925;
extern Il2CppCodeGenString* _stringLiteral669848589;
extern Il2CppCodeGenString* _stringLiteral3929823022;
extern Il2CppCodeGenString* _stringLiteral2742042351;
extern Il2CppCodeGenString* _stringLiteral2629128627;
extern Il2CppCodeGenString* _stringLiteral3177285002;
extern Il2CppCodeGenString* _stringLiteral1649436034;
extern Il2CppCodeGenString* _stringLiteral4139802140;
extern Il2CppCodeGenString* _stringLiteral4203522109;
extern Il2CppCodeGenString* _stringLiteral868933909;
extern Il2CppCodeGenString* _stringLiteral1410940430;
extern Il2CppCodeGenString* _stringLiteral997113313;
extern Il2CppCodeGenString* _stringLiteral3778989995;
extern Il2CppCodeGenString* _stringLiteral505509294;
extern const uint32_t StartSkrollin_CreateGroupWindow_m4150283924_MetadataUsageId;
extern "C"  void StartSkrollin_CreateGroupWindow_m4150283924 (StartSkrollin_t1317199858 * __this, int32_t ___windowID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartSkrollin_CreateGroupWindow_m4150283924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUIStyle_t1799908754 * V_0 = NULL;
	{
		float L_0 = __this->get_windowPad_56();
		float L_1 = __this->get_scrollVector_37();
		Rect_t3681755626 * L_2 = __this->get_address_of_windowRect_63();
		float L_3 = Rect_get_width_m1138015702(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_windowPad_56();
		Rect_t3681755626 * L_5 = __this->get_address_of_windowRect_63();
		float L_6 = Rect_get_height_m3128694305(L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_scrollMin_38();
		Rect_t3681755626  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m1220545469(&L_8, L_0, L_1, ((float)((float)L_3-(float)((float)((float)L_4*(float)(((float)((float)2))))))), ((float)((float)L_6+(float)L_7)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_9 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_9, /*hidden argument*/NULL);
		V_0 = L_9;
		bool L_10 = __this->get_audioOff_65();
		if (L_10)
		{
			goto IL_00bd;
		}
	}
	{
		GUIStyle_t1799908754 * L_11 = V_0;
		NullCheck(L_11);
		GUIStyleState_t3801000545 * L_12 = GUIStyle_get_normal_m2789468942(L_11, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_13 = __this->get_iconAudio_19();
		NullCheck(L_12);
		GUIStyleState_set_background_m3931679679(L_12, L_13, /*hidden argument*/NULL);
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)15)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_15 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_14, _stringLiteral1412472851, L_15, /*hidden argument*/NULL);
		Rect_t3681755626  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m1220545469(&L_16, (((float)((float)0))), (((float)((float)((int32_t)20)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_18 = V_0;
		bool L_19 = GUI_Button_m2147724592(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00bd;
		}
	}
	{
		__this->set_audioOff_65((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3387269145, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		bool L_20 = __this->get_audioOff_65();
		if (!L_20)
		{
			goto IL_0137;
		}
	}
	{
		GUIStyle_t1799908754 * L_21 = V_0;
		NullCheck(L_21);
		GUIStyleState_t3801000545 * L_22 = GUIStyle_get_normal_m2789468942(L_21, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_23 = __this->get_iconAudioNo_20();
		NullCheck(L_22);
		GUIStyleState_set_background_m3931679679(L_22, L_23, /*hidden argument*/NULL);
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)15)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_25 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_24, _stringLiteral3759753276, L_25, /*hidden argument*/NULL);
		Rect_t3681755626  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Rect__ctor_m1220545469(&L_26, (((float)((float)0))), (((float)((float)((int32_t)20)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_28 = V_0;
		bool L_29 = GUI_Button_m2147724592(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0137;
		}
	}
	{
		__this->set_audioOff_65((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3387269145, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_0137:
	{
		bool L_30 = __this->get_musicOff_66();
		if (L_30)
		{
			goto IL_01b1;
		}
	}
	{
		GUIStyle_t1799908754 * L_31 = V_0;
		NullCheck(L_31);
		GUIStyleState_t3801000545 * L_32 = GUIStyle_get_normal_m2789468942(L_31, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_33 = __this->get_iconMusic_21();
		NullCheck(L_32);
		GUIStyleState_set_background_m3931679679(L_32, L_33, /*hidden argument*/NULL);
		Rect_t3681755626  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Rect__ctor_m1220545469(&L_34, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)70)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_35 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_34, _stringLiteral2939335172, L_35, /*hidden argument*/NULL);
		Rect_t3681755626  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Rect__ctor_m1220545469(&L_36, (((float)((float)0))), (((float)((float)((int32_t)75)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_38 = V_0;
		bool L_39 = GUI_Button_m2147724592(NULL /*static, unused*/, L_36, L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_01b1;
		}
	}
	{
		__this->set_musicOff_66((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3387431150, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_01b1:
	{
		bool L_40 = __this->get_musicOff_66();
		if (!L_40)
		{
			goto IL_022b;
		}
	}
	{
		GUIStyle_t1799908754 * L_41 = V_0;
		NullCheck(L_41);
		GUIStyleState_t3801000545 * L_42 = GUIStyle_get_normal_m2789468942(L_41, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_43 = __this->get_iconMusicNo_22();
		NullCheck(L_42);
		GUIStyleState_set_background_m3931679679(L_42, L_43, /*hidden argument*/NULL);
		Rect_t3681755626  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Rect__ctor_m1220545469(&L_44, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)70)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_45 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_44, _stringLiteral991689925, L_45, /*hidden argument*/NULL);
		Rect_t3681755626  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Rect__ctor_m1220545469(&L_46, (((float)((float)0))), (((float)((float)((int32_t)75)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_48 = V_0;
		bool L_49 = GUI_Button_m2147724592(NULL /*static, unused*/, L_46, L_47, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_022b;
		}
	}
	{
		__this->set_musicOff_66((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3387431150, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_022b:
	{
		bool L_50 = __this->get_hideInfo_71();
		if (L_50)
		{
			goto IL_02ab;
		}
	}
	{
		GUIStyle_t1799908754 * L_51 = V_0;
		NullCheck(L_51);
		GUIStyleState_t3801000545 * L_52 = GUIStyle_get_normal_m2789468942(L_51, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_53 = __this->get_iconInfo_31();
		NullCheck(L_52);
		GUIStyleState_set_background_m3931679679(L_52, L_53, /*hidden argument*/NULL);
		Rect_t3681755626  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Rect__ctor_m1220545469(&L_54, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)130)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_55 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_54, _stringLiteral669848589, L_55, /*hidden argument*/NULL);
		Rect_t3681755626  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Rect__ctor_m1220545469(&L_56, (((float)((float)0))), (((float)((float)((int32_t)135)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_58 = V_0;
		bool L_59 = GUI_Button_m2147724592(NULL /*static, unused*/, L_56, L_57, L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_02ab;
		}
	}
	{
		__this->set_hideInfo_71((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3929823022, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_02ab:
	{
		bool L_60 = __this->get_hideInfo_71();
		if (!L_60)
		{
			goto IL_032b;
		}
	}
	{
		GUIStyle_t1799908754 * L_61 = V_0;
		NullCheck(L_61);
		GUIStyleState_t3801000545 * L_62 = GUIStyle_get_normal_m2789468942(L_61, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_63 = __this->get_iconInfoNo_32();
		NullCheck(L_62);
		GUIStyleState_set_background_m3931679679(L_62, L_63, /*hidden argument*/NULL);
		Rect_t3681755626  L_64;
		memset(&L_64, 0, sizeof(L_64));
		Rect__ctor_m1220545469(&L_64, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)130)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_65 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_64, _stringLiteral2742042351, L_65, /*hidden argument*/NULL);
		Rect_t3681755626  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Rect__ctor_m1220545469(&L_66, (((float)((float)0))), (((float)((float)((int32_t)135)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_67 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_68 = V_0;
		bool L_69 = GUI_Button_m2147724592(NULL /*static, unused*/, L_66, L_67, L_68, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_032b;
		}
	}
	{
		__this->set_hideInfo_71((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3929823022, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_032b:
	{
		bool L_70 = __this->get_thrustReversed_67();
		if (L_70)
		{
			goto IL_03ab;
		}
	}
	{
		GUIStyle_t1799908754 * L_71 = V_0;
		NullCheck(L_71);
		GUIStyleState_t3801000545 * L_72 = GUIStyle_get_normal_m2789468942(L_71, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_73 = __this->get_iconThrustNormal_27();
		NullCheck(L_72);
		GUIStyleState_set_background_m3931679679(L_72, L_73, /*hidden argument*/NULL);
		Rect_t3681755626  L_74;
		memset(&L_74, 0, sizeof(L_74));
		Rect__ctor_m1220545469(&L_74, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)185)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_75 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_74, _stringLiteral2629128627, L_75, /*hidden argument*/NULL);
		Rect_t3681755626  L_76;
		memset(&L_76, 0, sizeof(L_76));
		Rect__ctor_m1220545469(&L_76, (((float)((float)0))), (((float)((float)((int32_t)190)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_78 = V_0;
		bool L_79 = GUI_Button_m2147724592(NULL /*static, unused*/, L_76, L_77, L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_03ab;
		}
	}
	{
		__this->set_thrustReversed_67((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3177285002, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_03ab:
	{
		bool L_80 = __this->get_thrustReversed_67();
		if (!L_80)
		{
			goto IL_042b;
		}
	}
	{
		GUIStyle_t1799908754 * L_81 = V_0;
		NullCheck(L_81);
		GUIStyleState_t3801000545 * L_82 = GUIStyle_get_normal_m2789468942(L_81, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_83 = __this->get_iconThrustReversed_28();
		NullCheck(L_82);
		GUIStyleState_set_background_m3931679679(L_82, L_83, /*hidden argument*/NULL);
		Rect_t3681755626  L_84;
		memset(&L_84, 0, sizeof(L_84));
		Rect__ctor_m1220545469(&L_84, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)185)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_85 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_84, _stringLiteral1649436034, L_85, /*hidden argument*/NULL);
		Rect_t3681755626  L_86;
		memset(&L_86, 0, sizeof(L_86));
		Rect__ctor_m1220545469(&L_86, (((float)((float)0))), (((float)((float)((int32_t)190)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_88 = V_0;
		bool L_89 = GUI_Button_m2147724592(NULL /*static, unused*/, L_86, L_87, L_88, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_042b;
		}
	}
	{
		__this->set_thrustReversed_67((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3177285002, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_042b:
	{
		bool L_90 = __this->get_turnReversed_68();
		if (L_90)
		{
			goto IL_04ab;
		}
	}
	{
		GUIStyle_t1799908754 * L_91 = V_0;
		NullCheck(L_91);
		GUIStyleState_t3801000545 * L_92 = GUIStyle_get_normal_m2789468942(L_91, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_93 = __this->get_iconTurnNormal_29();
		NullCheck(L_92);
		GUIStyleState_set_background_m3931679679(L_92, L_93, /*hidden argument*/NULL);
		Rect_t3681755626  L_94;
		memset(&L_94, 0, sizeof(L_94));
		Rect__ctor_m1220545469(&L_94, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)240)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_95 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_94, _stringLiteral4139802140, L_95, /*hidden argument*/NULL);
		Rect_t3681755626  L_96;
		memset(&L_96, 0, sizeof(L_96));
		Rect__ctor_m1220545469(&L_96, (((float)((float)0))), (((float)((float)((int32_t)245)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_98 = V_0;
		bool L_99 = GUI_Button_m2147724592(NULL /*static, unused*/, L_96, L_97, L_98, /*hidden argument*/NULL);
		if (!L_99)
		{
			goto IL_04ab;
		}
	}
	{
		__this->set_turnReversed_68((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral4203522109, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_04ab:
	{
		bool L_100 = __this->get_turnReversed_68();
		if (!L_100)
		{
			goto IL_052b;
		}
	}
	{
		GUIStyle_t1799908754 * L_101 = V_0;
		NullCheck(L_101);
		GUIStyleState_t3801000545 * L_102 = GUIStyle_get_normal_m2789468942(L_101, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_103 = __this->get_iconTurnReversed_30();
		NullCheck(L_102);
		GUIStyleState_set_background_m3931679679(L_102, L_103, /*hidden argument*/NULL);
		Rect_t3681755626  L_104;
		memset(&L_104, 0, sizeof(L_104));
		Rect__ctor_m1220545469(&L_104, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)240)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_105 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_104, _stringLiteral868933909, L_105, /*hidden argument*/NULL);
		Rect_t3681755626  L_106;
		memset(&L_106, 0, sizeof(L_106));
		Rect__ctor_m1220545469(&L_106, (((float)((float)0))), (((float)((float)((int32_t)245)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_107 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_108 = V_0;
		bool L_109 = GUI_Button_m2147724592(NULL /*static, unused*/, L_106, L_107, L_108, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_052b;
		}
	}
	{
		__this->set_turnReversed_68((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral4203522109, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_052b:
	{
		bool L_110 = __this->get_hasResetHS_70();
		if (L_110)
		{
			goto IL_065d;
		}
	}
	{
		bool L_111 = __this->get_resetHSCheck_69();
		if (L_111)
		{
			goto IL_05a7;
		}
	}
	{
		GUIStyle_t1799908754 * L_112 = V_0;
		NullCheck(L_112);
		GUIStyleState_t3801000545 * L_113 = GUIStyle_get_normal_m2789468942(L_112, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_114 = __this->get_iconResetHS_23();
		NullCheck(L_113);
		GUIStyleState_set_background_m3931679679(L_113, L_114, /*hidden argument*/NULL);
		Rect_t3681755626  L_115;
		memset(&L_115, 0, sizeof(L_115));
		Rect__ctor_m1220545469(&L_115, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)295)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_116 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_115, _stringLiteral1410940430, L_116, /*hidden argument*/NULL);
		Rect_t3681755626  L_117;
		memset(&L_117, 0, sizeof(L_117));
		Rect__ctor_m1220545469(&L_117, (((float)((float)0))), (((float)((float)((int32_t)300)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_118 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_119 = V_0;
		bool L_120 = GUI_Button_m2147724592(NULL /*static, unused*/, L_117, L_118, L_119, /*hidden argument*/NULL);
		if (!L_120)
		{
			goto IL_05a7;
		}
	}
	{
		__this->set_resetHSCheck_69((bool)1);
	}

IL_05a7:
	{
		bool L_121 = __this->get_resetHSCheck_69();
		if (!L_121)
		{
			goto IL_065d;
		}
	}
	{
		GUIStyle_t1799908754 * L_122 = V_0;
		NullCheck(L_122);
		GUIStyleState_t3801000545 * L_123 = GUIStyle_get_normal_m2789468942(L_122, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_124 = __this->get_iconResetHSWarn_25();
		NullCheck(L_123);
		GUIStyleState_set_background_m3931679679(L_123, L_124, /*hidden argument*/NULL);
		Rect_t3681755626  L_125;
		memset(&L_125, 0, sizeof(L_125));
		Rect__ctor_m1220545469(&L_125, (((float)((float)0))), (((float)((float)((int32_t)296)))), (((float)((float)((int32_t)265)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_126 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral3778989995, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_125, _stringLiteral997113313, L_126, /*hidden argument*/NULL);
		Rect_t3681755626  L_127;
		memset(&L_127, 0, sizeof(L_127));
		Rect__ctor_m1220545469(&L_127, (((float)((float)0))), (((float)((float)((int32_t)300)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_128 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_129 = V_0;
		bool L_130 = GUI_Button_m2147724592(NULL /*static, unused*/, L_127, L_128, L_129, /*hidden argument*/NULL);
		if (!L_130)
		{
			goto IL_0617;
		}
	}
	{
		__this->set_resetHSCheck_69((bool)0);
	}

IL_0617:
	{
		GUIStyle_t1799908754 * L_131 = V_0;
		NullCheck(L_131);
		GUIStyleState_t3801000545 * L_132 = GUIStyle_get_normal_m2789468942(L_131, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_133 = __this->get_iconResetHSDoneWarn_26();
		NullCheck(L_132);
		GUIStyleState_set_background_m3931679679(L_132, L_133, /*hidden argument*/NULL);
		Rect_t3681755626  L_134;
		memset(&L_134, 0, sizeof(L_134));
		Rect__ctor_m1220545469(&L_134, (((float)((float)((int32_t)220)))), (((float)((float)((int32_t)300)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_135 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_136 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_137 = GUI_Button_m2147724592(NULL /*static, unused*/, L_134, L_135, L_136, /*hidden argument*/NULL);
		if (!L_137)
		{
			goto IL_065d;
		}
	}
	{
		__this->set_resetHSCheck_69((bool)0);
		__this->set_hasResetHS_70((bool)1);
	}

IL_065d:
	{
		bool L_138 = __this->get_hasResetHS_70();
		if (!L_138)
		{
			goto IL_06ce;
		}
	}
	{
		GUIStyle_t1799908754 * L_139 = V_0;
		NullCheck(L_139);
		GUIStyleState_t3801000545 * L_140 = GUIStyle_get_normal_m2789468942(L_139, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_141 = __this->get_iconResetHSDone_24();
		NullCheck(L_140);
		GUIStyleState_set_background_m3931679679(L_140, L_141, /*hidden argument*/NULL);
		Rect_t3681755626  L_142;
		memset(&L_142, 0, sizeof(L_142));
		Rect__ctor_m1220545469(&L_142, (((float)((float)((int32_t)55)))), (((float)((float)((int32_t)295)))), (((float)((float)((int32_t)250)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_143 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_142, _stringLiteral505509294, L_143, /*hidden argument*/NULL);
		Rect_t3681755626  L_144;
		memset(&L_144, 0, sizeof(L_144));
		Rect__ctor_m1220545469(&L_144, (((float)((float)0))), (((float)((float)((int32_t)300)))), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)50)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_145 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		GUIStyle_t1799908754 * L_146 = V_0;
		bool L_147 = GUI_Button_m2147724592(NULL /*static, unused*/, L_144, L_145, L_146, /*hidden argument*/NULL);
		if (!L_147)
		{
			goto IL_06ce;
		}
	}
	{
		__this->set_resetHSCheck_69((bool)1);
	}

IL_06ce:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartSkrollin::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t StartSkrollin_Update_m4173007945_MetadataUsageId;
extern "C"  void StartSkrollin_Update_m4173007945 (StartSkrollin_t1317199858 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartSkrollin_Update_m4173007945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Touch_t407273883  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_hazSkrollin_44();
		if (!L_0)
		{
			goto IL_0210;
		}
	}
	{
		float L_1 = __this->get_scrollVector_37();
		V_0 = L_1;
		__this->set_resistance_40((1.0f));
		__this->set_marginPressure_41((((float)((float)0))));
		float L_2 = __this->get_scrollVector_37();
		float L_3 = __this->get_scrollMax_39();
		if ((((float)L_2) < ((float)L_3)))
		{
			goto IL_0086;
		}
	}
	{
		float L_4 = __this->get_scrollVector_37();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = fabsf(L_4);
		float L_6 = __this->get_scrollMax_39();
		float L_7 = __this->get_scrollMargin_36();
		float L_8 = fabsf(((float)((float)((float)((float)L_5-(float)L_6))/(float)L_7)));
		float L_9 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (1.0f), (((float)((float)0))), L_8, /*hidden argument*/NULL);
		__this->set_resistance_40(L_9);
		float L_10 = __this->get_resistance_40();
		float L_11 = __this->get_marginSpringiness_33();
		float L_12 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_marginPressure_41(((-((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_10))*(float)L_11))*(float)L_12)))));
	}

IL_0086:
	{
		float L_13 = __this->get_scrollVector_37();
		float L_14 = __this->get_scrollMin_38();
		if ((((float)L_13) > ((float)((-L_14)))))
		{
			goto IL_00e7;
		}
	}
	{
		float L_15 = __this->get_scrollVector_37();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_16 = fabsf(L_15);
		float L_17 = __this->get_scrollMin_38();
		float L_18 = __this->get_scrollMargin_36();
		float L_19 = fabsf(((float)((float)((float)((float)L_16-(float)L_17))/(float)L_18)));
		float L_20 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (1.0f), (((float)((float)0))), L_19, /*hidden argument*/NULL);
		__this->set_resistance_40(L_20);
		float L_21 = __this->get_resistance_40();
		float L_22 = __this->get_marginSpringiness_33();
		float L_23 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_marginPressure_41(((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_21))*(float)L_22))*(float)L_23)));
	}

IL_00e7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_24 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_24) <= ((int32_t)0)))
		{
			goto IL_0169;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_25 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_25;
		int32_t L_26 = Touch_get_phase_m196706494((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_0169;
		}
	}
	{
		Vector2_t2243707579  L_27 = Touch_get_deltaPosition_m97688791((&V_1), /*hidden argument*/NULL);
		V_3 = L_27;
		float L_28 = (&V_3)->get_y_1();
		V_2 = L_28;
		float L_29 = V_2;
		float L_30 = __this->get_touchSpeed_34();
		float L_31 = __this->get_resistance_40();
		__this->set_deltaY_42(((float)((float)((float)((float)L_29*(float)L_30))*(float)L_31)));
		float L_32 = __this->get_scrollVector_37();
		float L_33 = __this->get_deltaY_42();
		float L_34 = __this->get_scrollMin_38();
		float L_35 = __this->get_scrollMargin_36();
		float L_36 = __this->get_scrollMax_39();
		float L_37 = __this->get_scrollMargin_36();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_38 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_32+(float)L_33)), ((-((float)((float)L_34+(float)L_35)))), ((float)((float)L_36+(float)L_37)), /*hidden argument*/NULL);
		__this->set_scrollVector_37(L_38);
		float L_39 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_43(L_39);
	}

IL_0169:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_40 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_40) >= ((int32_t)1)))
		{
			goto IL_01db;
		}
	}
	{
		float L_41 = __this->get_deltaY_42();
		float L_42 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_43 = __this->get_startTime_43();
		float L_44 = __this->get_coastSpeed_35();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_45 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_41, (((float)((float)0))), ((float)((float)((float)((float)L_42-(float)L_43))*(float)L_44)), /*hidden argument*/NULL);
		float L_46 = __this->get_resistance_40();
		__this->set_deltaY_42(((float)((float)L_45*(float)L_46)));
		float L_47 = __this->get_scrollVector_37();
		float L_48 = __this->get_deltaY_42();
		float L_49 = __this->get_scrollMin_38();
		float L_50 = __this->get_scrollMargin_36();
		float L_51 = __this->get_scrollMax_39();
		float L_52 = __this->get_scrollMargin_36();
		float L_53 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_47+(float)L_48)), ((-((float)((float)L_49+(float)L_50)))), ((float)((float)L_51+(float)L_52)), /*hidden argument*/NULL);
		float L_54 = __this->get_marginPressure_41();
		__this->set_scrollVector_37(((float)((float)L_53+(float)L_54)));
	}

IL_01db:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_55 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_55) > ((int32_t)0)))
		{
			goto IL_01fd;
		}
	}
	{
		float L_56 = V_0;
		float L_57 = __this->get_scrollVector_37();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_58 = fabsf(((float)((float)L_56-(float)L_57)));
		if ((((float)L_58) <= ((float)(0.01f))))
		{
			goto IL_0209;
		}
	}

IL_01fd:
	{
		__this->set_izSkrollin_45((bool)1);
		goto IL_0210;
	}

IL_0209:
	{
		__this->set_izSkrollin_45((bool)0);
	}

IL_0210:
	{
		return;
	}
}
// System.Void StartSkrollin::Main()
extern "C"  void StartSkrollin_Main_m4110254671 (StartSkrollin_t1317199858 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
