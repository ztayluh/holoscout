﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud/<readPLDB>c__Iterator2
struct U3CreadPLDBU3Ec__Iterator2_t2294453720;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Cloud/<readPLDB>c__Iterator2::.ctor()
extern "C"  void U3CreadPLDBU3Ec__Iterator2__ctor_m938752507 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Cloud/<readPLDB>c__Iterator2::MoveNext()
extern "C"  bool U3CreadPLDBU3Ec__Iterator2_MoveNext_m1249169289 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readPLDB>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadPLDBU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1657717909 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readPLDB>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadPLDBU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m758923853 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readPLDB>c__Iterator2::Dispose()
extern "C"  void U3CreadPLDBU3Ec__Iterator2_Dispose_m2708295014 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readPLDB>c__Iterator2::Reset()
extern "C"  void U3CreadPLDBU3Ec__Iterator2_Reset_m4163262508 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
