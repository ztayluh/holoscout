﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssemblyCSharp.GUIUtils
struct GUIUtils_t2318875170;

#include "codegen/il2cpp-codegen.h"

// System.Void AssemblyCSharp.GUIUtils::.ctor()
extern "C"  void GUIUtils__ctor_m880850310 (GUIUtils_t2318875170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
