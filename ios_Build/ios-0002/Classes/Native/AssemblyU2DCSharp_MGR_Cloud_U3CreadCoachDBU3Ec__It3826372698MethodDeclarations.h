﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud/<readCoachDB>c__Iterator0
struct U3CreadCoachDBU3Ec__Iterator0_t3826372698;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Cloud/<readCoachDB>c__Iterator0::.ctor()
extern "C"  void U3CreadCoachDBU3Ec__Iterator0__ctor_m2866453635 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Cloud/<readCoachDB>c__Iterator0::MoveNext()
extern "C"  bool U3CreadCoachDBU3Ec__Iterator0_MoveNext_m1720357261 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readCoachDB>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3563481621 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readCoachDB>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2351408045 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readCoachDB>c__Iterator0::Dispose()
extern "C"  void U3CreadCoachDBU3Ec__Iterator0_Dispose_m3313015228 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readCoachDB>c__Iterator0::Reset()
extern "C"  void U3CreadCoachDBU3Ec__Iterator0_Reset_m1749873790 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
