﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud/<readHashDB>c__Iterator3
struct U3CreadHashDBU3Ec__Iterator3_t962384949;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Cloud/<readHashDB>c__Iterator3::.ctor()
extern "C"  void U3CreadHashDBU3Ec__Iterator3__ctor_m3315340498 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Cloud/<readHashDB>c__Iterator3::MoveNext()
extern "C"  bool U3CreadHashDBU3Ec__Iterator3_MoveNext_m443546314 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readHashDB>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4155094814 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readHashDB>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3808693974 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readHashDB>c__Iterator3::Dispose()
extern "C"  void U3CreadHashDBU3Ec__Iterator3_Dispose_m1756324991 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readHashDB>c__Iterator3::Reset()
extern "C"  void U3CreadHashDBU3Ec__Iterator3_Reset_m738018153 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
