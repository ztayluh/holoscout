﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// MGR_Anim
struct MGR_Anim_t3221826994;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_HLGUI
struct  MGR_HLGUI_t3333927834  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField MGR_HLGUI::Input_Coach
	InputField_t1631627530 * ___Input_Coach_2;
	// UnityEngine.UI.InputField MGR_HLGUI::Input_Trainee
	InputField_t1631627530 * ___Input_Trainee_3;
	// UnityEngine.UI.InputField MGR_HLGUI::Input_Playlist
	InputField_t1631627530 * ___Input_Playlist_4;
	// System.String MGR_HLGUI::coach
	String_t* ___coach_5;
	// System.String MGR_HLGUI::trainee
	String_t* ___trainee_6;
	// System.String MGR_HLGUI::playlist
	String_t* ___playlist_7;
	// System.String MGR_HLGUI::i_coach
	String_t* ___i_coach_8;
	// System.String MGR_HLGUI::i_trainee
	String_t* ___i_trainee_9;
	// System.String MGR_HLGUI::i_playlist
	String_t* ___i_playlist_10;
	// System.String[] MGR_HLGUI::clipNames
	StringU5BU5D_t1642385972* ___clipNames_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MGR_HLGUI::DictOfAnims
	Dictionary_2_t3986656710 * ___DictOfAnims_12;
	// MGR_Cloud MGR_HLGUI::myCloud
	MGR_Cloud_t726529104 * ___myCloud_13;
	// MGR_Anim MGR_HLGUI::myAnim
	MGR_Anim_t3221826994 * ___myAnim_14;
	// UnityEngine.GameObject MGR_HLGUI::myGO
	GameObject_t1756533147 * ___myGO_15;
	// UnityEngine.Canvas MGR_HLGUI::myCanvas
	Canvas_t209405766 * ___myCanvas_16;
	// UnityEngine.Animator MGR_HLGUI::myAnimator
	Animator_t69676727 * ___myAnimator_17;
	// UnityEngine.Vector2 MGR_HLGUI::coachScrollPosition
	Vector2_t2243707579  ___coachScrollPosition_18;
	// UnityEngine.Vector2 MGR_HLGUI::traineeScrollPosition
	Vector2_t2243707579  ___traineeScrollPosition_19;
	// UnityEngine.Vector2 MGR_HLGUI::plScrollPosition
	Vector2_t2243707579  ___plScrollPosition_20;
	// UnityEngine.Vector2 MGR_HLGUI::hashScrollPosition
	Vector2_t2243707579  ___hashScrollPosition_21;
	// System.Int32 MGR_HLGUI::count
	int32_t ___count_22;

public:
	inline static int32_t get_offset_of_Input_Coach_2() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___Input_Coach_2)); }
	inline InputField_t1631627530 * get_Input_Coach_2() const { return ___Input_Coach_2; }
	inline InputField_t1631627530 ** get_address_of_Input_Coach_2() { return &___Input_Coach_2; }
	inline void set_Input_Coach_2(InputField_t1631627530 * value)
	{
		___Input_Coach_2 = value;
		Il2CppCodeGenWriteBarrier(&___Input_Coach_2, value);
	}

	inline static int32_t get_offset_of_Input_Trainee_3() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___Input_Trainee_3)); }
	inline InputField_t1631627530 * get_Input_Trainee_3() const { return ___Input_Trainee_3; }
	inline InputField_t1631627530 ** get_address_of_Input_Trainee_3() { return &___Input_Trainee_3; }
	inline void set_Input_Trainee_3(InputField_t1631627530 * value)
	{
		___Input_Trainee_3 = value;
		Il2CppCodeGenWriteBarrier(&___Input_Trainee_3, value);
	}

	inline static int32_t get_offset_of_Input_Playlist_4() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___Input_Playlist_4)); }
	inline InputField_t1631627530 * get_Input_Playlist_4() const { return ___Input_Playlist_4; }
	inline InputField_t1631627530 ** get_address_of_Input_Playlist_4() { return &___Input_Playlist_4; }
	inline void set_Input_Playlist_4(InputField_t1631627530 * value)
	{
		___Input_Playlist_4 = value;
		Il2CppCodeGenWriteBarrier(&___Input_Playlist_4, value);
	}

	inline static int32_t get_offset_of_coach_5() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___coach_5)); }
	inline String_t* get_coach_5() const { return ___coach_5; }
	inline String_t** get_address_of_coach_5() { return &___coach_5; }
	inline void set_coach_5(String_t* value)
	{
		___coach_5 = value;
		Il2CppCodeGenWriteBarrier(&___coach_5, value);
	}

	inline static int32_t get_offset_of_trainee_6() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___trainee_6)); }
	inline String_t* get_trainee_6() const { return ___trainee_6; }
	inline String_t** get_address_of_trainee_6() { return &___trainee_6; }
	inline void set_trainee_6(String_t* value)
	{
		___trainee_6 = value;
		Il2CppCodeGenWriteBarrier(&___trainee_6, value);
	}

	inline static int32_t get_offset_of_playlist_7() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___playlist_7)); }
	inline String_t* get_playlist_7() const { return ___playlist_7; }
	inline String_t** get_address_of_playlist_7() { return &___playlist_7; }
	inline void set_playlist_7(String_t* value)
	{
		___playlist_7 = value;
		Il2CppCodeGenWriteBarrier(&___playlist_7, value);
	}

	inline static int32_t get_offset_of_i_coach_8() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___i_coach_8)); }
	inline String_t* get_i_coach_8() const { return ___i_coach_8; }
	inline String_t** get_address_of_i_coach_8() { return &___i_coach_8; }
	inline void set_i_coach_8(String_t* value)
	{
		___i_coach_8 = value;
		Il2CppCodeGenWriteBarrier(&___i_coach_8, value);
	}

	inline static int32_t get_offset_of_i_trainee_9() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___i_trainee_9)); }
	inline String_t* get_i_trainee_9() const { return ___i_trainee_9; }
	inline String_t** get_address_of_i_trainee_9() { return &___i_trainee_9; }
	inline void set_i_trainee_9(String_t* value)
	{
		___i_trainee_9 = value;
		Il2CppCodeGenWriteBarrier(&___i_trainee_9, value);
	}

	inline static int32_t get_offset_of_i_playlist_10() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___i_playlist_10)); }
	inline String_t* get_i_playlist_10() const { return ___i_playlist_10; }
	inline String_t** get_address_of_i_playlist_10() { return &___i_playlist_10; }
	inline void set_i_playlist_10(String_t* value)
	{
		___i_playlist_10 = value;
		Il2CppCodeGenWriteBarrier(&___i_playlist_10, value);
	}

	inline static int32_t get_offset_of_clipNames_11() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___clipNames_11)); }
	inline StringU5BU5D_t1642385972* get_clipNames_11() const { return ___clipNames_11; }
	inline StringU5BU5D_t1642385972** get_address_of_clipNames_11() { return &___clipNames_11; }
	inline void set_clipNames_11(StringU5BU5D_t1642385972* value)
	{
		___clipNames_11 = value;
		Il2CppCodeGenWriteBarrier(&___clipNames_11, value);
	}

	inline static int32_t get_offset_of_DictOfAnims_12() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___DictOfAnims_12)); }
	inline Dictionary_2_t3986656710 * get_DictOfAnims_12() const { return ___DictOfAnims_12; }
	inline Dictionary_2_t3986656710 ** get_address_of_DictOfAnims_12() { return &___DictOfAnims_12; }
	inline void set_DictOfAnims_12(Dictionary_2_t3986656710 * value)
	{
		___DictOfAnims_12 = value;
		Il2CppCodeGenWriteBarrier(&___DictOfAnims_12, value);
	}

	inline static int32_t get_offset_of_myCloud_13() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___myCloud_13)); }
	inline MGR_Cloud_t726529104 * get_myCloud_13() const { return ___myCloud_13; }
	inline MGR_Cloud_t726529104 ** get_address_of_myCloud_13() { return &___myCloud_13; }
	inline void set_myCloud_13(MGR_Cloud_t726529104 * value)
	{
		___myCloud_13 = value;
		Il2CppCodeGenWriteBarrier(&___myCloud_13, value);
	}

	inline static int32_t get_offset_of_myAnim_14() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___myAnim_14)); }
	inline MGR_Anim_t3221826994 * get_myAnim_14() const { return ___myAnim_14; }
	inline MGR_Anim_t3221826994 ** get_address_of_myAnim_14() { return &___myAnim_14; }
	inline void set_myAnim_14(MGR_Anim_t3221826994 * value)
	{
		___myAnim_14 = value;
		Il2CppCodeGenWriteBarrier(&___myAnim_14, value);
	}

	inline static int32_t get_offset_of_myGO_15() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___myGO_15)); }
	inline GameObject_t1756533147 * get_myGO_15() const { return ___myGO_15; }
	inline GameObject_t1756533147 ** get_address_of_myGO_15() { return &___myGO_15; }
	inline void set_myGO_15(GameObject_t1756533147 * value)
	{
		___myGO_15 = value;
		Il2CppCodeGenWriteBarrier(&___myGO_15, value);
	}

	inline static int32_t get_offset_of_myCanvas_16() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___myCanvas_16)); }
	inline Canvas_t209405766 * get_myCanvas_16() const { return ___myCanvas_16; }
	inline Canvas_t209405766 ** get_address_of_myCanvas_16() { return &___myCanvas_16; }
	inline void set_myCanvas_16(Canvas_t209405766 * value)
	{
		___myCanvas_16 = value;
		Il2CppCodeGenWriteBarrier(&___myCanvas_16, value);
	}

	inline static int32_t get_offset_of_myAnimator_17() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___myAnimator_17)); }
	inline Animator_t69676727 * get_myAnimator_17() const { return ___myAnimator_17; }
	inline Animator_t69676727 ** get_address_of_myAnimator_17() { return &___myAnimator_17; }
	inline void set_myAnimator_17(Animator_t69676727 * value)
	{
		___myAnimator_17 = value;
		Il2CppCodeGenWriteBarrier(&___myAnimator_17, value);
	}

	inline static int32_t get_offset_of_coachScrollPosition_18() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___coachScrollPosition_18)); }
	inline Vector2_t2243707579  get_coachScrollPosition_18() const { return ___coachScrollPosition_18; }
	inline Vector2_t2243707579 * get_address_of_coachScrollPosition_18() { return &___coachScrollPosition_18; }
	inline void set_coachScrollPosition_18(Vector2_t2243707579  value)
	{
		___coachScrollPosition_18 = value;
	}

	inline static int32_t get_offset_of_traineeScrollPosition_19() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___traineeScrollPosition_19)); }
	inline Vector2_t2243707579  get_traineeScrollPosition_19() const { return ___traineeScrollPosition_19; }
	inline Vector2_t2243707579 * get_address_of_traineeScrollPosition_19() { return &___traineeScrollPosition_19; }
	inline void set_traineeScrollPosition_19(Vector2_t2243707579  value)
	{
		___traineeScrollPosition_19 = value;
	}

	inline static int32_t get_offset_of_plScrollPosition_20() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___plScrollPosition_20)); }
	inline Vector2_t2243707579  get_plScrollPosition_20() const { return ___plScrollPosition_20; }
	inline Vector2_t2243707579 * get_address_of_plScrollPosition_20() { return &___plScrollPosition_20; }
	inline void set_plScrollPosition_20(Vector2_t2243707579  value)
	{
		___plScrollPosition_20 = value;
	}

	inline static int32_t get_offset_of_hashScrollPosition_21() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___hashScrollPosition_21)); }
	inline Vector2_t2243707579  get_hashScrollPosition_21() const { return ___hashScrollPosition_21; }
	inline Vector2_t2243707579 * get_address_of_hashScrollPosition_21() { return &___hashScrollPosition_21; }
	inline void set_hashScrollPosition_21(Vector2_t2243707579  value)
	{
		___hashScrollPosition_21 = value;
	}

	inline static int32_t get_offset_of_count_22() { return static_cast<int32_t>(offsetof(MGR_HLGUI_t3333927834, ___count_22)); }
	inline int32_t get_count_22() const { return ___count_22; }
	inline int32_t* get_address_of_count_22() { return &___count_22; }
	inline void set_count_22(int32_t value)
	{
		___count_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
