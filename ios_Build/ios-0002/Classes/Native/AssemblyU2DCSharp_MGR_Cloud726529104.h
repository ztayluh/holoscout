﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MGR_Anim
struct MGR_Anim_t3221826994;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Entry779635924.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_Cloud
struct  MGR_Cloud_t726529104  : public MonoBehaviour_t1158329972
{
public:
	// Entry MGR_Cloud::myEntry
	Entry_t779635924  ___myEntry_2;
	// MGR_Anim MGR_Cloud::anims
	MGR_Anim_t3221826994 * ___anims_3;
	// System.Int32 MGR_Cloud::numOfClips
	int32_t ___numOfClips_4;
	// System.Int32 MGR_Cloud::currClip
	int32_t ___currClip_5;
	// System.String MGR_Cloud::currClipName
	String_t* ___currClipName_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MGR_Cloud::traineeRecords
	Dictionary_2_t3943999495 * ___traineeRecords_7;

public:
	inline static int32_t get_offset_of_myEntry_2() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___myEntry_2)); }
	inline Entry_t779635924  get_myEntry_2() const { return ___myEntry_2; }
	inline Entry_t779635924 * get_address_of_myEntry_2() { return &___myEntry_2; }
	inline void set_myEntry_2(Entry_t779635924  value)
	{
		___myEntry_2 = value;
	}

	inline static int32_t get_offset_of_anims_3() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___anims_3)); }
	inline MGR_Anim_t3221826994 * get_anims_3() const { return ___anims_3; }
	inline MGR_Anim_t3221826994 ** get_address_of_anims_3() { return &___anims_3; }
	inline void set_anims_3(MGR_Anim_t3221826994 * value)
	{
		___anims_3 = value;
		Il2CppCodeGenWriteBarrier(&___anims_3, value);
	}

	inline static int32_t get_offset_of_numOfClips_4() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___numOfClips_4)); }
	inline int32_t get_numOfClips_4() const { return ___numOfClips_4; }
	inline int32_t* get_address_of_numOfClips_4() { return &___numOfClips_4; }
	inline void set_numOfClips_4(int32_t value)
	{
		___numOfClips_4 = value;
	}

	inline static int32_t get_offset_of_currClip_5() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___currClip_5)); }
	inline int32_t get_currClip_5() const { return ___currClip_5; }
	inline int32_t* get_address_of_currClip_5() { return &___currClip_5; }
	inline void set_currClip_5(int32_t value)
	{
		___currClip_5 = value;
	}

	inline static int32_t get_offset_of_currClipName_6() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___currClipName_6)); }
	inline String_t* get_currClipName_6() const { return ___currClipName_6; }
	inline String_t** get_address_of_currClipName_6() { return &___currClipName_6; }
	inline void set_currClipName_6(String_t* value)
	{
		___currClipName_6 = value;
		Il2CppCodeGenWriteBarrier(&___currClipName_6, value);
	}

	inline static int32_t get_offset_of_traineeRecords_7() { return static_cast<int32_t>(offsetof(MGR_Cloud_t726529104, ___traineeRecords_7)); }
	inline Dictionary_2_t3943999495 * get_traineeRecords_7() const { return ___traineeRecords_7; }
	inline Dictionary_2_t3943999495 ** get_address_of_traineeRecords_7() { return &___traineeRecords_7; }
	inline void set_traineeRecords_7(Dictionary_2_t3943999495 * value)
	{
		___traineeRecords_7 = value;
		Il2CppCodeGenWriteBarrier(&___traineeRecords_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
