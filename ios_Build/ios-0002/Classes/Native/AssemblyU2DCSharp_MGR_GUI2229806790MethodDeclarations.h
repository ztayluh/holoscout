﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_GUI
struct MGR_GUI_t2229806790;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_GUI::.ctor()
extern "C"  void MGR_GUI__ctor_m1251067095 (MGR_GUI_t2229806790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_GUI::Awake()
extern "C"  void MGR_GUI_Awake_m2956493910 (MGR_GUI_t2229806790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_GUI::readStates()
extern "C"  void MGR_GUI_readStates_m194826505 (MGR_GUI_t2229806790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_GUI::addDBEntry()
extern "C"  void MGR_GUI_addDBEntry_m376179630 (MGR_GUI_t2229806790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_GUI::PlayAnims()
extern "C"  void MGR_GUI_PlayAnims_m77098791 (MGR_GUI_t2229806790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_GUI::OnGUI()
extern "C"  void MGR_GUI_OnGUI_m858956101 (MGR_GUI_t2229806790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
