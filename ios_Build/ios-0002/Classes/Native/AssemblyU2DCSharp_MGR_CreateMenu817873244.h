﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// MGR_GUI
struct MGR_GUI_t2229806790;
// System.String
struct String_t;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_CreateMenu
struct  MGR_CreateMenu_t817873244  : public MonoBehaviour_t1158329972
{
public:
	// System.String[] MGR_CreateMenu::clipNames
	StringU5BU5D_t1642385972* ___clipNames_2;
	// UnityEngine.GameObject MGR_CreateMenu::myGO
	GameObject_t1756533147 * ___myGO_3;
	// UnityEngine.Animator MGR_CreateMenu::myAnimator
	Animator_t69676727 * ___myAnimator_4;
	// System.Collections.Generic.List`1<System.String> MGR_CreateMenu::playlistItems
	List_1_t1398341365 * ___playlistItems_5;
	// MGR_Cloud MGR_CreateMenu::myCloud
	MGR_Cloud_t726529104 * ___myCloud_6;
	// MGR_GUI MGR_CreateMenu::myGUI
	MGR_GUI_t2229806790 * ___myGUI_7;
	// System.String MGR_CreateMenu::coach
	String_t* ___coach_8;
	// System.String MGR_CreateMenu::trainee
	String_t* ___trainee_9;
	// System.String MGR_CreateMenu::pl
	String_t* ___pl_10;
	// System.String MGR_CreateMenu::hs
	String_t* ___hs_11;
	// UnityEngine.UI.InputField MGR_CreateMenu::coachIF
	InputField_t1631627530 * ___coachIF_12;
	// UnityEngine.UI.InputField MGR_CreateMenu::traineeIF
	InputField_t1631627530 * ___traineeIF_13;
	// UnityEngine.UI.InputField MGR_CreateMenu::plIF
	InputField_t1631627530 * ___plIF_14;
	// System.Int32 MGR_CreateMenu::currPLItem
	int32_t ___currPLItem_15;
	// UnityEngine.Vector2 MGR_CreateMenu::coachScrollPosition
	Vector2_t2243707579  ___coachScrollPosition_16;
	// UnityEngine.Vector2 MGR_CreateMenu::coachScrollPosition2
	Vector2_t2243707579  ___coachScrollPosition2_17;

public:
	inline static int32_t get_offset_of_clipNames_2() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___clipNames_2)); }
	inline StringU5BU5D_t1642385972* get_clipNames_2() const { return ___clipNames_2; }
	inline StringU5BU5D_t1642385972** get_address_of_clipNames_2() { return &___clipNames_2; }
	inline void set_clipNames_2(StringU5BU5D_t1642385972* value)
	{
		___clipNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___clipNames_2, value);
	}

	inline static int32_t get_offset_of_myGO_3() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___myGO_3)); }
	inline GameObject_t1756533147 * get_myGO_3() const { return ___myGO_3; }
	inline GameObject_t1756533147 ** get_address_of_myGO_3() { return &___myGO_3; }
	inline void set_myGO_3(GameObject_t1756533147 * value)
	{
		___myGO_3 = value;
		Il2CppCodeGenWriteBarrier(&___myGO_3, value);
	}

	inline static int32_t get_offset_of_myAnimator_4() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___myAnimator_4)); }
	inline Animator_t69676727 * get_myAnimator_4() const { return ___myAnimator_4; }
	inline Animator_t69676727 ** get_address_of_myAnimator_4() { return &___myAnimator_4; }
	inline void set_myAnimator_4(Animator_t69676727 * value)
	{
		___myAnimator_4 = value;
		Il2CppCodeGenWriteBarrier(&___myAnimator_4, value);
	}

	inline static int32_t get_offset_of_playlistItems_5() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___playlistItems_5)); }
	inline List_1_t1398341365 * get_playlistItems_5() const { return ___playlistItems_5; }
	inline List_1_t1398341365 ** get_address_of_playlistItems_5() { return &___playlistItems_5; }
	inline void set_playlistItems_5(List_1_t1398341365 * value)
	{
		___playlistItems_5 = value;
		Il2CppCodeGenWriteBarrier(&___playlistItems_5, value);
	}

	inline static int32_t get_offset_of_myCloud_6() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___myCloud_6)); }
	inline MGR_Cloud_t726529104 * get_myCloud_6() const { return ___myCloud_6; }
	inline MGR_Cloud_t726529104 ** get_address_of_myCloud_6() { return &___myCloud_6; }
	inline void set_myCloud_6(MGR_Cloud_t726529104 * value)
	{
		___myCloud_6 = value;
		Il2CppCodeGenWriteBarrier(&___myCloud_6, value);
	}

	inline static int32_t get_offset_of_myGUI_7() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___myGUI_7)); }
	inline MGR_GUI_t2229806790 * get_myGUI_7() const { return ___myGUI_7; }
	inline MGR_GUI_t2229806790 ** get_address_of_myGUI_7() { return &___myGUI_7; }
	inline void set_myGUI_7(MGR_GUI_t2229806790 * value)
	{
		___myGUI_7 = value;
		Il2CppCodeGenWriteBarrier(&___myGUI_7, value);
	}

	inline static int32_t get_offset_of_coach_8() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___coach_8)); }
	inline String_t* get_coach_8() const { return ___coach_8; }
	inline String_t** get_address_of_coach_8() { return &___coach_8; }
	inline void set_coach_8(String_t* value)
	{
		___coach_8 = value;
		Il2CppCodeGenWriteBarrier(&___coach_8, value);
	}

	inline static int32_t get_offset_of_trainee_9() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___trainee_9)); }
	inline String_t* get_trainee_9() const { return ___trainee_9; }
	inline String_t** get_address_of_trainee_9() { return &___trainee_9; }
	inline void set_trainee_9(String_t* value)
	{
		___trainee_9 = value;
		Il2CppCodeGenWriteBarrier(&___trainee_9, value);
	}

	inline static int32_t get_offset_of_pl_10() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___pl_10)); }
	inline String_t* get_pl_10() const { return ___pl_10; }
	inline String_t** get_address_of_pl_10() { return &___pl_10; }
	inline void set_pl_10(String_t* value)
	{
		___pl_10 = value;
		Il2CppCodeGenWriteBarrier(&___pl_10, value);
	}

	inline static int32_t get_offset_of_hs_11() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___hs_11)); }
	inline String_t* get_hs_11() const { return ___hs_11; }
	inline String_t** get_address_of_hs_11() { return &___hs_11; }
	inline void set_hs_11(String_t* value)
	{
		___hs_11 = value;
		Il2CppCodeGenWriteBarrier(&___hs_11, value);
	}

	inline static int32_t get_offset_of_coachIF_12() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___coachIF_12)); }
	inline InputField_t1631627530 * get_coachIF_12() const { return ___coachIF_12; }
	inline InputField_t1631627530 ** get_address_of_coachIF_12() { return &___coachIF_12; }
	inline void set_coachIF_12(InputField_t1631627530 * value)
	{
		___coachIF_12 = value;
		Il2CppCodeGenWriteBarrier(&___coachIF_12, value);
	}

	inline static int32_t get_offset_of_traineeIF_13() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___traineeIF_13)); }
	inline InputField_t1631627530 * get_traineeIF_13() const { return ___traineeIF_13; }
	inline InputField_t1631627530 ** get_address_of_traineeIF_13() { return &___traineeIF_13; }
	inline void set_traineeIF_13(InputField_t1631627530 * value)
	{
		___traineeIF_13 = value;
		Il2CppCodeGenWriteBarrier(&___traineeIF_13, value);
	}

	inline static int32_t get_offset_of_plIF_14() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___plIF_14)); }
	inline InputField_t1631627530 * get_plIF_14() const { return ___plIF_14; }
	inline InputField_t1631627530 ** get_address_of_plIF_14() { return &___plIF_14; }
	inline void set_plIF_14(InputField_t1631627530 * value)
	{
		___plIF_14 = value;
		Il2CppCodeGenWriteBarrier(&___plIF_14, value);
	}

	inline static int32_t get_offset_of_currPLItem_15() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___currPLItem_15)); }
	inline int32_t get_currPLItem_15() const { return ___currPLItem_15; }
	inline int32_t* get_address_of_currPLItem_15() { return &___currPLItem_15; }
	inline void set_currPLItem_15(int32_t value)
	{
		___currPLItem_15 = value;
	}

	inline static int32_t get_offset_of_coachScrollPosition_16() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___coachScrollPosition_16)); }
	inline Vector2_t2243707579  get_coachScrollPosition_16() const { return ___coachScrollPosition_16; }
	inline Vector2_t2243707579 * get_address_of_coachScrollPosition_16() { return &___coachScrollPosition_16; }
	inline void set_coachScrollPosition_16(Vector2_t2243707579  value)
	{
		___coachScrollPosition_16 = value;
	}

	inline static int32_t get_offset_of_coachScrollPosition2_17() { return static_cast<int32_t>(offsetof(MGR_CreateMenu_t817873244, ___coachScrollPosition2_17)); }
	inline Vector2_t2243707579  get_coachScrollPosition2_17() const { return ___coachScrollPosition2_17; }
	inline Vector2_t2243707579 * get_address_of_coachScrollPosition2_17() { return &___coachScrollPosition2_17; }
	inline void set_coachScrollPosition2_17(Vector2_t2243707579  value)
	{
		___coachScrollPosition2_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
