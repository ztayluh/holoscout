﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_CreateMenu
struct MGR_CreateMenu_t817873244;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_CreateMenu::.ctor()
extern "C"  void MGR_CreateMenu__ctor_m645211395 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::Start()
extern "C"  void MGR_CreateMenu_Start_m931694471 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::Update()
extern "C"  void MGR_CreateMenu_Update_m529887162 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::displayWorkingPL()
extern "C"  void MGR_CreateMenu_displayWorkingPL_m1446641882 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::readStates()
extern "C"  void MGR_CreateMenu_readStates_m3857455173 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::OnGUI()
extern "C"  void MGR_CreateMenu_OnGUI_m420939169 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::playAnims()
extern "C"  void MGR_CreateMenu_playAnims_m3011770835 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MGR_CreateMenu::Play()
extern "C"  Il2CppObject * MGR_CreateMenu_Play_m3856428385 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::resetPL()
extern "C"  void MGR_CreateMenu_resetPL_m2861834464 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu::submitPL()
extern "C"  void MGR_CreateMenu_submitPL_m2648786843 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
