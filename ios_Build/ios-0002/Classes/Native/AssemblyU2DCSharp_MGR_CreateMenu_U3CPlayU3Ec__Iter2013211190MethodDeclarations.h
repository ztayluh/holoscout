﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_CreateMenu/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t2013211190;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_CreateMenu/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m2881918555 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_CreateMenu/<Play>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m2206641169 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_CreateMenu/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m296993137 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_CreateMenu/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1260047433 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m317506136 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_CreateMenu/<Play>c__Iterator0::Reset()
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m1194165590 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
