﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AssemblyCSharp.GUIUtils
struct GUIUtils_t2318875170;
// MGR_Anim
struct MGR_Anim_t3221826994;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// System.Object
struct Il2CppObject;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// MGR_Anim/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t2856749856;
// MGR_Anim/<Play>c__Iterator1
struct U3CPlayU3Ec__Iterator1_t2856749855;
// MGR_Cloud/<readCoachDB>c__Iterator0
struct U3CreadCoachDBU3Ec__Iterator0_t3826372698;
// MGR_Cloud/<readHashDB>c__Iterator3
struct U3CreadHashDBU3Ec__Iterator3_t962384949;
// MGR_Cloud/<readPLDB>c__Iterator2
struct U3CreadPLDBU3Ec__Iterator2_t2294453720;
// MGR_Cloud/<readTraineeDB>c__Iterator1
struct U3CreadTraineeDBU3Ec__Iterator1_t81797705;
// MGR_CreateMenu
struct MGR_CreateMenu_t817873244;
// MGR_CreateMenu/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t2013211190;
// MGR_GUI
struct MGR_GUI_t2229806790;
// UnityEngine.Canvas
struct Canvas_t209405766;
// MGR_HLGUI
struct MGR_HLGUI_t3333927834;
// MGR_HoloText
struct MGR_HoloText_t1166120216;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// MGR_Menus
struct MGR_Menus_t511463637;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// MGR_Scroll
struct MGR_Scroll_t3416609842;
// SetAnimationInAnimator
struct SetAnimationInAnimator_t2420164504;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_GUIUtils2318875170.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_GUIUtils2318875170MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_Entry779635924.h"
#include "AssemblyU2DCSharp_Entry779635924MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_MGR_Anim3221826994.h"
#include "AssemblyU2DCSharp_MGR_Anim3221826994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Int64909078037.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen278199169MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_MGR_Cloud726529104.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_List_1_gen278199169.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator02856749856MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator02856749856.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator12856749855MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Anim_U3CPlayU3Ec__Iterator12856749855.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_MGR_Cloud726529104MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadCoachDBU3Ec__It3826372698MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadCoachDBU3Ec__It3826372698.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadTraineeDBU3Ec__It81797705MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadTraineeDBU3Ec__It81797705.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadPLDBU3Ec__Itera2294453720MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadPLDBU3Ec__Itera2294453720.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadHashDBU3Ec__Iter962384949MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadHashDBU3Ec__Iter962384949.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu817873244.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu817873244MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu_U3CPlayU3Ec__Iter2013211190MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu_U3CPlayU3Ec__Iter2013211190.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2823857299MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_GUI2229806790.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2823857299.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"
#include "AssemblyU2DCSharp_MGR_GUI2229806790MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3018738635MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3018738635.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_HLGUI3333927834.h"
#include "AssemblyU2DCSharp_MGR_HLGUI3333927834MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710.h"
#include "AssemblyU2DCSharp_MGR_HoloText1166120216.h"
#include "AssemblyU2DCSharp_MGR_HoloText1166120216MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Menus511463637.h"
#include "AssemblyU2DCSharp_MGR_Menus511463637MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "AssemblyU2DCSharp_MGR_Menus_MenuStates1946331629.h"
#include "AssemblyU2DCSharp_MGR_Menus_MenuStates1946331629MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Scroll3416609842.h"
#include "AssemblyU2DCSharp_MGR_Scroll3416609842MethodDeclarations.h"
#include "AssemblyU2DCSharp_MGR_Scroll_StartMenuLevel3121722732.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "AssemblyU2DCSharp_MGR_Scroll_StartMenuLevel3121722732MethodDeclarations.h"
#include "AssemblyU2DCSharp_SetAnimationInAnimator2420164504.h"
#include "AssemblyU2DCSharp_SetAnimationInAnimator2420164504MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MGR_Cloud>()
#define GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(__this, method) ((  MGR_Cloud_t726529104 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MGR_Anim>()
#define Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191(__this, method) ((  MGR_Anim_t3221826994 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, method) ((  Canvas_t209405766 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MGR_Anim>()
#define GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355(__this, method) ((  MGR_Anim_t3221826994 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
#define Component_GetComponent_TisTextMesh_t1641806576_m4177416886(__this, method) ((  TextMesh_t1641806576 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MGR_Cloud>()
#define Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677(__this, method) ((  MGR_Cloud_t726529104 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t257310565_m1312615893(__this, method) ((  Renderer_t257310565 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AssemblyCSharp.GUIUtils::.ctor()
extern "C"  void GUIUtils__ctor_m880850310 (GUIUtils_t2318875170 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: Entry
extern "C" void Entry_t779635924_marshal_pinvoke(const Entry_t779635924& unmarshaled, Entry_t779635924_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
extern "C" void Entry_t779635924_marshal_pinvoke_back(const Entry_t779635924_marshaled_pinvoke& marshaled, Entry_t779635924& unmarshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
// Conversion method for clean up from marshalling of: Entry
extern "C" void Entry_t779635924_marshal_pinvoke_cleanup(Entry_t779635924_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Entry
extern "C" void Entry_t779635924_marshal_com(const Entry_t779635924& unmarshaled, Entry_t779635924_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
extern "C" void Entry_t779635924_marshal_com_back(const Entry_t779635924_marshaled_com& marshaled, Entry_t779635924& unmarshaled)
{
	Il2CppCodeGenException* ___currentPlaylist_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'currentPlaylist' of type 'Entry'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___currentPlaylist_3Exception);
}
// Conversion method for clean up from marshalling of: Entry
extern "C" void Entry_t779635924_marshal_com_cleanup(Entry_t779635924_marshaled_com& marshaled)
{
}
// System.Void MGR_Anim::.ctor()
extern "C"  void MGR_Anim__ctor_m915439051 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	{
		__this->set_currPLItem_6(0);
		__this->set_currHashID_7((((int64_t)((int64_t)0))));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Anim::Start()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t278199169_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2168168579_MethodInfo_var;
extern const uint32_t MGR_Anim_Start_m1643617151_MetadataUsageId;
extern "C"  void MGR_Anim_Start_m1643617151 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_Start_m1643617151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_myGO_2();
		NullCheck(L_0);
		MGR_Cloud_t726529104 * L_1 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_0, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_5(L_1);
		GameObject_t1756533147 * L_2 = __this->get_myGO_2();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_3(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_4->set_playlists_4(L_5);
		Entry_t779635924 * L_6 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_7 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_7, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_6->set_currentPlaylist_3(L_7);
		Entry_t779635924 * L_8 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_9 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_9, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_8->set_trainees_6(L_9);
		Entry_t779635924 * L_10 = __this->get_address_of_entry_4();
		List_1_t1398341365 * L_11 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_11, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_10->set_coaches_5(L_11);
		Entry_t779635924 * L_12 = __this->get_address_of_entry_4();
		List_1_t278199169 * L_13 = (List_1_t278199169 *)il2cpp_codegen_object_new(List_1_t278199169_il2cpp_TypeInfo_var);
		List_1__ctor_m2168168579(L_13, /*hidden argument*/List_1__ctor_m2168168579_MethodInfo_var);
		L_12->set_hashString_7(L_13);
		GameObject_t1756533147 * L_14 = __this->get_myGO_2();
		NullCheck(L_14);
		Animator_t69676727 * L_15 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_14, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_3(L_15);
		return;
	}
}
// System.Void MGR_Anim::setEntry(Entry,UnityEngine.Animator,System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t278199169_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3892806282_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1950531896_MethodInfo_var;
extern const MethodInfo* List_1_Add_m236055543_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1722794520;
extern const uint32_t MGR_Anim_setEntry_m3451898502_MetadataUsageId;
extern "C"  void MGR_Anim_setEntry_m3451898502 (MGR_Anim_t3221826994 * __this, Entry_t779635924  ___e0, Animator_t69676727 * ___a1, int32_t ___numOfClips2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_setEntry_m3451898502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Animator_t69676727 * L_0 = ___a1;
		__this->set_myAnimator_3(L_0);
		int32_t L_1 = ___numOfClips2;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1722794520, L_3, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Entry_t779635924 * L_5 = __this->get_address_of_entry_4();
		String_t* L_6 = (&___e0)->get_coach_0();
		L_5->set_coach_0(L_6);
		Entry_t779635924 * L_7 = __this->get_address_of_entry_4();
		String_t* L_8 = (&___e0)->get_trainee_1();
		L_7->set_trainee_1(L_8);
		MGR_Cloud_t726529104 * L_9 = __this->get_myCloud_5();
		NullCheck(L_9);
		Entry_t779635924 * L_10 = L_9->get_address_of_myEntry_2();
		int32_t L_11 = ___numOfClips2;
		L_10->set_hSize_11(L_11);
		int32_t L_12 = ___numOfClips2;
		List_1_t278199169 * L_13 = (List_1_t278199169 *)il2cpp_codegen_object_new(List_1_t278199169_il2cpp_TypeInfo_var);
		List_1__ctor_m3892806282(L_13, L_12, /*hidden argument*/List_1__ctor_m3892806282_MethodInfo_var);
		__this->set_myhashPL_8(L_13);
		V_0 = 0;
		goto IL_00a0;
	}

IL_0065:
	{
		Entry_t779635924 * L_14 = __this->get_address_of_entry_4();
		List_1_t278199169 * L_15 = L_14->get_hashString_7();
		List_1_t278199169 * L_16 = (&___e0)->get_hashString_7();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int64_t L_18 = List_1_get_Item_m1950531896(L_16, L_17, /*hidden argument*/List_1_get_Item_m1950531896_MethodInfo_var);
		NullCheck(L_15);
		List_1_Add_m236055543(L_15, L_18, /*hidden argument*/List_1_Add_m236055543_MethodInfo_var);
		List_1_t278199169 * L_19 = __this->get_myhashPL_8();
		List_1_t278199169 * L_20 = (&___e0)->get_hashString_7();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		int64_t L_22 = List_1_get_Item_m1950531896(L_20, L_21, /*hidden argument*/List_1_get_Item_m1950531896_MethodInfo_var);
		NullCheck(L_19);
		List_1_Add_m236055543(L_19, L_22, /*hidden argument*/List_1_Add_m236055543_MethodInfo_var);
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_24 = V_0;
		int32_t L_25 = ___numOfClips2;
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0065;
		}
	}
	{
		return;
	}
}
// System.Void MGR_Anim::playAnims()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3819797390;
extern const uint32_t MGR_Anim_playAnims_m3338700699_MetadataUsageId;
extern "C"  void MGR_Anim_playAnims_m3338700699 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_playAnims_m3338700699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_5();
		NullCheck(L_0);
		Entry_t779635924 * L_1 = L_0->get_address_of_myEntry_2();
		int32_t L_2 = L_1->get_hSize_11();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3819797390, L_4, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Il2CppObject * L_6 = MGR_Anim_Play_m3675863081(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Anim::previewAnims(Entry)
extern "C"  void MGR_Anim_previewAnims_m2850797577 (MGR_Anim_t3221826994 * __this, Entry_t779635924  ___e0, const MethodInfo* method)
{
	{
		Entry_t779635924  L_0 = ___e0;
		Il2CppObject * L_1 = MGR_Anim_Play_m3487651337(__this, L_0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MGR_Anim::Play(Entry)
extern Il2CppClass* U3CPlayU3Ec__Iterator0_t2856749856_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Anim_Play_m3487651337_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Anim_Play_m3487651337 (MGR_Anim_t3221826994 * __this, Entry_t779635924  ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_Play_m3487651337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPlayU3Ec__Iterator0_t2856749856 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CPlayU3Ec__Iterator0_t2856749856 * L_0 = (U3CPlayU3Ec__Iterator0_t2856749856 *)il2cpp_codegen_object_new(U3CPlayU3Ec__Iterator0_t2856749856_il2cpp_TypeInfo_var);
		U3CPlayU3Ec__Iterator0__ctor_m1257570099(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayU3Ec__Iterator0_t2856749856 * L_1 = V_0;
		Entry_t779635924  L_2 = ___e0;
		NullCheck(L_1);
		L_1->set_e_2(L_2);
		U3CPlayU3Ec__Iterator0_t2856749856 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CPlayU3Ec__Iterator0_t2856749856 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Collections.IEnumerator MGR_Anim::Play()
extern Il2CppClass* U3CPlayU3Ec__Iterator1_t2856749855_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Anim_Play_m3675863081_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Anim_Play_m3675863081 (MGR_Anim_t3221826994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Anim_Play_m3675863081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPlayU3Ec__Iterator1_t2856749855 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CPlayU3Ec__Iterator1_t2856749855 * L_0 = (U3CPlayU3Ec__Iterator1_t2856749855 *)il2cpp_codegen_object_new(U3CPlayU3Ec__Iterator1_t2856749855_il2cpp_TypeInfo_var);
		U3CPlayU3Ec__Iterator1__ctor_m1254084210(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayU3Ec__Iterator1_t2856749855 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CPlayU3Ec__Iterator1_t2856749855 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Void MGR_Anim::UpdateAnim(System.String,System.String)
extern "C"  void MGR_Anim_UpdateAnim_m248568853 (MGR_Anim_t3221826994 * __this, String_t* ___current0, String_t* ___next1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m1257570099 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Anim/<Play>c__Iterator0::MoveNext()
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1950531896_MethodInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_MoveNext_m4201796769_MetadataUsageId;
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m4201796769 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_MoveNext_m4201796769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00f7;
		}
	}
	{
		goto IL_0136;
	}

IL_0021:
	{
		MGR_Anim_t3221826994 * L_2 = __this->get_U24this_4();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = L_2->get_myAnimator_3();
		NullCheck(L_3);
		AnimatorStateInfo_t2577870592  L_4 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_3, 0, /*hidden argument*/NULL);
		__this->set_U3CcurrInfoU3E__0_0(L_4);
		__this->set_U3CiU3E__1_1(1);
		goto IL_0119;
	}

IL_0045:
	{
		MGR_Anim_t3221826994 * L_5 = __this->get_U24this_4();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_currPLItem_6();
		Entry_t779635924 * L_7 = __this->get_address_of_e_2();
		int32_t L_8 = L_7->get_hSize_11();
		if ((((int32_t)L_6) >= ((int32_t)L_8)))
		{
			goto IL_0072;
		}
	}
	{
		MGR_Anim_t3221826994 * L_9 = __this->get_U24this_4();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_currPLItem_6();
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}

IL_0072:
	{
		MGR_Anim_t3221826994 * L_11 = __this->get_U24this_4();
		NullCheck(L_11);
		L_11->set_currPLItem_6(0);
	}

IL_0080:
	{
		MGR_Anim_t3221826994 * L_12 = __this->get_U24this_4();
		Entry_t779635924 * L_13 = __this->get_address_of_e_2();
		List_1_t278199169 * L_14 = L_13->get_hashString_7();
		MGR_Anim_t3221826994 * L_15 = __this->get_U24this_4();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_currPLItem_6();
		NullCheck(L_14);
		int64_t L_17 = List_1_get_Item_m1950531896(L_14, L_16, /*hidden argument*/List_1_get_Item_m1950531896_MethodInfo_var);
		NullCheck(L_12);
		L_12->set_currHashID_7(L_17);
		AnimatorStateInfo_t2577870592 * L_18 = __this->get_address_of_U3CcurrInfoU3E__0_0();
		float L_19 = AnimatorStateInfo_get_length_m3151009408(L_18, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__2_3(L_19);
		MGR_Anim_t3221826994 * L_20 = __this->get_U24this_4();
		NullCheck(L_20);
		int64_t L_21 = L_20->get_currHashID_7();
		int64_t L_22 = L_21;
		Il2CppObject * L_23 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_22);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		float L_24 = __this->get_U3CtimeU3E__2_3();
		WaitForSeconds_t3839502067 * L_25 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_25, ((float)((float)L_24-(float)(1.0f))), /*hidden argument*/NULL);
		__this->set_U24current_5(L_25);
		bool L_26 = __this->get_U24disposing_6();
		if (L_26)
		{
			goto IL_00f2;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_00f2:
	{
		goto IL_0138;
	}

IL_00f7:
	{
		MGR_Anim_t3221826994 * L_27 = __this->get_U24this_4();
		MGR_Anim_t3221826994 * L_28 = L_27;
		NullCheck(L_28);
		int32_t L_29 = L_28->get_currPLItem_6();
		NullCheck(L_28);
		L_28->set_currPLItem_6(((int32_t)((int32_t)L_29+(int32_t)1)));
		int32_t L_30 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)((int32_t)L_30+(int32_t)1)));
	}

IL_0119:
	{
		int32_t L_31 = __this->get_U3CiU3E__1_1();
		Entry_t779635924 * L_32 = __this->get_address_of_e_2();
		int32_t L_33 = L_32->get_hSize_11();
		if ((((int32_t)L_31) < ((int32_t)L_33)))
		{
			goto IL_0045;
		}
	}
	{
		__this->set_U24PC_7((-1));
	}

IL_0136:
	{
		return (bool)0;
	}

IL_0138:
	{
		return (bool)1;
	}
}
// System.Object MGR_Anim/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m504323601 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Anim/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2596265641 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m2829949778 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_Reset_m1939598112_MetadataUsageId;
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m1939598112 (U3CPlayU3Ec__Iterator0_t2856749856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_Reset_m1939598112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Anim/<Play>c__Iterator1::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator1__ctor_m1254084210 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Anim/<Play>c__Iterator1::MoveNext()
extern Il2CppClass* AnimatorStateInfo_t2577870592_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1950531896_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1955728068;
extern const uint32_t U3CPlayU3Ec__Iterator1_MoveNext_m1086451014_MetadataUsageId;
extern "C"  bool U3CPlayU3Ec__Iterator1_MoveNext_m1086451014 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator1_MoveNext_m1086451014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0144;
		}
	}
	{
		goto IL_018d;
	}

IL_0021:
	{
		MGR_Anim_t3221826994 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = L_2->get_myAnimator_3();
		NullCheck(L_3);
		AnimatorStateInfo_t2577870592  L_4 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_3, 0, /*hidden argument*/NULL);
		__this->set_U3CcurrInfoU3E__0_0(L_4);
		AnimatorStateInfo_t2577870592  L_5 = __this->get_U3CcurrInfoU3E__0_0();
		AnimatorStateInfo_t2577870592  L_6 = L_5;
		Il2CppObject * L_7 = Box(AnimatorStateInfo_t2577870592_il2cpp_TypeInfo_var, &L_6);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_8 = __this->get_U24this_3();
		NullCheck(L_8);
		MGR_Cloud_t726529104 * L_9 = L_8->get_myCloud_5();
		NullCheck(L_9);
		Entry_t779635924 * L_10 = L_9->get_address_of_myEntry_2();
		int32_t L_11 = L_10->get_hSize_11();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1955728068, L_13, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		__this->set_U3CiU3E__1_1(1);
		goto IL_0166;
	}

IL_007e:
	{
		MGR_Anim_t3221826994 * L_15 = __this->get_U24this_3();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_currPLItem_6();
		MGR_Anim_t3221826994 * L_17 = __this->get_U24this_3();
		NullCheck(L_17);
		MGR_Cloud_t726529104 * L_18 = L_17->get_myCloud_5();
		NullCheck(L_18);
		Entry_t779635924 * L_19 = L_18->get_address_of_myEntry_2();
		int32_t L_20 = L_19->get_hSize_11();
		if ((((int32_t)L_16) >= ((int32_t)L_20)))
		{
			goto IL_00b5;
		}
	}
	{
		MGR_Anim_t3221826994 * L_21 = __this->get_U24this_3();
		NullCheck(L_21);
		int32_t L_22 = L_21->get_currPLItem_6();
		if ((((int32_t)L_22) >= ((int32_t)0)))
		{
			goto IL_00c3;
		}
	}

IL_00b5:
	{
		MGR_Anim_t3221826994 * L_23 = __this->get_U24this_3();
		NullCheck(L_23);
		L_23->set_currPLItem_6(0);
	}

IL_00c3:
	{
		MGR_Anim_t3221826994 * L_24 = __this->get_U24this_3();
		MGR_Anim_t3221826994 * L_25 = __this->get_U24this_3();
		NullCheck(L_25);
		MGR_Cloud_t726529104 * L_26 = L_25->get_myCloud_5();
		NullCheck(L_26);
		Entry_t779635924 * L_27 = L_26->get_address_of_myEntry_2();
		List_1_t278199169 * L_28 = L_27->get_hashString_7();
		MGR_Anim_t3221826994 * L_29 = __this->get_U24this_3();
		NullCheck(L_29);
		int32_t L_30 = L_29->get_currPLItem_6();
		NullCheck(L_28);
		int64_t L_31 = List_1_get_Item_m1950531896(L_28, L_30, /*hidden argument*/List_1_get_Item_m1950531896_MethodInfo_var);
		NullCheck(L_24);
		L_24->set_currHashID_7(L_31);
		AnimatorStateInfo_t2577870592 * L_32 = __this->get_address_of_U3CcurrInfoU3E__0_0();
		float L_33 = AnimatorStateInfo_get_length_m3151009408(L_32, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__2_2(L_33);
		MGR_Anim_t3221826994 * L_34 = __this->get_U24this_3();
		NullCheck(L_34);
		int64_t L_35 = L_34->get_currHashID_7();
		int64_t L_36 = L_35;
		Il2CppObject * L_37 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_36);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		float L_38 = __this->get_U3CtimeU3E__2_2();
		WaitForSeconds_t3839502067 * L_39 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_39, ((float)((float)L_38-(float)(1.0f))), /*hidden argument*/NULL);
		__this->set_U24current_4(L_39);
		bool L_40 = __this->get_U24disposing_5();
		if (L_40)
		{
			goto IL_013f;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_013f:
	{
		goto IL_018f;
	}

IL_0144:
	{
		MGR_Anim_t3221826994 * L_41 = __this->get_U24this_3();
		MGR_Anim_t3221826994 * L_42 = L_41;
		NullCheck(L_42);
		int32_t L_43 = L_42->get_currPLItem_6();
		NullCheck(L_42);
		L_42->set_currPLItem_6(((int32_t)((int32_t)L_43+(int32_t)1)));
		int32_t L_44 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)((int32_t)L_44+(int32_t)1)));
	}

IL_0166:
	{
		int32_t L_45 = __this->get_U3CiU3E__1_1();
		MGR_Anim_t3221826994 * L_46 = __this->get_U24this_3();
		NullCheck(L_46);
		MGR_Cloud_t726529104 * L_47 = L_46->get_myCloud_5();
		NullCheck(L_47);
		Entry_t779635924 * L_48 = L_47->get_address_of_myEntry_2();
		int32_t L_49 = L_48->get_hSize_11();
		if ((((int32_t)L_45) < ((int32_t)L_49)))
		{
			goto IL_007e;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_018d:
	{
		return (bool)0;
	}

IL_018f:
	{
		return (bool)1;
	}
}
// System.Object MGR_Anim/<Play>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499146672 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Anim/<Play>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3559029192 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator1::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator1_Dispose_m2715196273 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_Anim/<Play>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator1_Reset_m1936120671_MetadataUsageId;
extern "C"  void U3CPlayU3Ec__Iterator1_Reset_m1936120671 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator1_Reset_m1936120671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud::.ctor()
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t MGR_Cloud__ctor_m1683574021_MetadataUsageId;
extern "C"  void MGR_Cloud__ctor_m1683574021 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud__ctor_m1683574021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_currClipName_6(_stringLiteral371857150);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Cloud::Awake()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t278199169_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m28427054_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2168168579_MethodInfo_var;
extern const uint32_t MGR_Cloud_Awake_m4069120028_MetadataUsageId;
extern "C"  void MGR_Cloud_Awake_m4069120028 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Awake_m4069120028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Anim_t3221826994 * L_0 = Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191(__this, /*hidden argument*/Component_GetComponent_TisMGR_Anim_t3221826994_m1935195191_MethodInfo_var);
		__this->set_anims_3(L_0);
		Dictionary_2_t3943999495 * L_1 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m28427054(L_1, /*hidden argument*/Dictionary_2__ctor_m28427054_MethodInfo_var);
		__this->set_traineeRecords_7(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_3 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_3, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_2->set_playlists_4(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_4->set_currentPlaylist_3(L_5);
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_7 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_7, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_6->set_trainees_6(L_7);
		Entry_t779635924 * L_8 = __this->get_address_of_myEntry_2();
		List_1_t1398341365 * L_9 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_9, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		L_8->set_coaches_5(L_9);
		Entry_t779635924 * L_10 = __this->get_address_of_myEntry_2();
		List_1_t278199169 * L_11 = (List_1_t278199169 *)il2cpp_codegen_object_new(List_1_t278199169_il2cpp_TypeInfo_var);
		List_1__ctor_m2168168579(L_11, /*hidden argument*/List_1__ctor_m2168168579_MethodInfo_var);
		L_10->set_hashString_7(L_11);
		return;
	}
}
// System.Void MGR_Cloud::Start()
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t MGR_Cloud_Start_m899893525_MetadataUsageId;
extern "C"  void MGR_Cloud_Start_m899893525 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_Start_m899893525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		L_0->set_coach_0(_stringLiteral371857150);
		Entry_t779635924 * L_1 = __this->get_address_of_myEntry_2();
		L_1->set_trainee_1(_stringLiteral371857150);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		L_2->set_playlistName_2(_stringLiteral371857150);
		Entry_t779635924 * L_3 = __this->get_address_of_myEntry_2();
		L_3->set_cSize_9(0);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		L_4->set_pSize_8(0);
		Entry_t779635924 * L_5 = __this->get_address_of_myEntry_2();
		L_5->set_tSize_10(0);
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		L_6->set_hSize_11(0);
		Entry_t779635924 * L_7 = __this->get_address_of_myEntry_2();
		L_7->set_numOfPlaylists_12(0);
		MGR_Cloud_startReadDB_m986115961(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Cloud::Update()
extern "C"  void MGR_Cloud_Update_m1977723834 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_Cloud::setEntry(System.String,System.String,System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m1170681985_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3686231158_MethodInfo_var;
extern const uint32_t MGR_Cloud_setEntry_m1738080337_MetadataUsageId;
extern "C"  void MGR_Cloud_setEntry_m1738080337 (MGR_Cloud_t726529104 * __this, String_t* ___cName0, String_t* ___tName1, String_t* ___pName2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_setEntry_m1738080337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		String_t* L_1 = ___pName2;
		L_0->set_playlistName_2(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = ___cName0;
		L_2->set_coach_0(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		String_t* L_5 = ___tName1;
		L_4->set_trainee_1(L_5);
		Dictionary_2_t3943999495 * L_6 = __this->get_traineeRecords_7();
		String_t* L_7 = ___tName1;
		NullCheck(L_6);
		bool L_8 = Dictionary_2_ContainsKey_m1170681985(L_6, L_7, /*hidden argument*/Dictionary_2_ContainsKey_m1170681985_MethodInfo_var);
		if (L_8)
		{
			goto IL_0058;
		}
	}
	{
		Dictionary_2_t3943999495 * L_9 = __this->get_traineeRecords_7();
		String_t* L_10 = ___tName1;
		String_t* L_11 = ___cName0;
		NullCheck(L_9);
		Dictionary_2_Add_m3686231158(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		Entry_t779635924 * L_12 = __this->get_address_of_myEntry_2();
		Entry_t779635924 * L_13 = L_12;
		int32_t L_14 = L_13->get_cSize_9();
		L_13->set_cSize_9(((int32_t)((int32_t)L_14+(int32_t)1)));
	}

IL_0058:
	{
		return;
	}
}
// System.Void MGR_Cloud::initFB(System.String,System.String,System.String)
extern "C"  void MGR_Cloud_initFB_m761421013 (MGR_Cloud_t726529104 * __this, String_t* ___cName0, String_t* ___tName1, String_t* ___pName2, const MethodInfo* method)
{
	{
		Entry_t779635924 * L_0 = __this->get_address_of_myEntry_2();
		String_t* L_1 = ___pName2;
		L_0->set_playlistName_2(L_1);
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = ___cName0;
		L_2->set_coach_0(L_3);
		Entry_t779635924 * L_4 = __this->get_address_of_myEntry_2();
		String_t* L_5 = ___tName1;
		L_4->set_trainee_1(L_5);
		return;
	}
}
// System.Void MGR_Cloud::playPlay()
extern "C"  void MGR_Cloud_playPlay_m4061453909 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	{
		MGR_Anim_t3221826994 * L_0 = __this->get_anims_3();
		NullCheck(L_0);
		MGR_Anim_playAnims_m3338700699(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readCoachDB()
extern Il2CppClass* U3CreadCoachDBU3Ec__Iterator0_t3826372698_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readCoachDB_m3748347533_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readCoachDB_m3748347533 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readCoachDB_m3748347533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadCoachDBU3Ec__Iterator0_t3826372698 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CreadCoachDBU3Ec__Iterator0_t3826372698 * L_0 = (U3CreadCoachDBU3Ec__Iterator0_t3826372698 *)il2cpp_codegen_object_new(U3CreadCoachDBU3Ec__Iterator0_t3826372698_il2cpp_TypeInfo_var);
		U3CreadCoachDBU3Ec__Iterator0__ctor_m2866453635(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadCoachDBU3Ec__Iterator0_t3826372698 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CreadCoachDBU3Ec__Iterator0_t3826372698 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readTraineeDB(System.String)
extern Il2CppClass* U3CreadTraineeDBU3Ec__Iterator1_t81797705_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readTraineeDB_m1908492693_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readTraineeDB_m1908492693 (MGR_Cloud_t726529104 * __this, String_t* ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readTraineeDB_m1908492693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadTraineeDBU3Ec__Iterator1_t81797705 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CreadTraineeDBU3Ec__Iterator1_t81797705 * L_0 = (U3CreadTraineeDBU3Ec__Iterator1_t81797705 *)il2cpp_codegen_object_new(U3CreadTraineeDBU3Ec__Iterator1_t81797705_il2cpp_TypeInfo_var);
		U3CreadTraineeDBU3Ec__Iterator1__ctor_m1422939798(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadTraineeDBU3Ec__Iterator1_t81797705 * L_1 = V_0;
		String_t* L_2 = ___c0;
		NullCheck(L_1);
		L_1->set_c_0(L_2);
		U3CreadTraineeDBU3Ec__Iterator1_t81797705 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadTraineeDBU3Ec__Iterator1_t81797705 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readPLDB(System.String)
extern Il2CppClass* U3CreadPLDBU3Ec__Iterator2_t2294453720_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readPLDB_m994522313_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readPLDB_m994522313 (MGR_Cloud_t726529104 * __this, String_t* ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readPLDB_m994522313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadPLDBU3Ec__Iterator2_t2294453720 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CreadPLDBU3Ec__Iterator2_t2294453720 * L_0 = (U3CreadPLDBU3Ec__Iterator2_t2294453720 *)il2cpp_codegen_object_new(U3CreadPLDBU3Ec__Iterator2_t2294453720_il2cpp_TypeInfo_var);
		U3CreadPLDBU3Ec__Iterator2__ctor_m938752507(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadPLDBU3Ec__Iterator2_t2294453720 * L_1 = V_0;
		String_t* L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CreadPLDBU3Ec__Iterator2_t2294453720 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadPLDBU3Ec__Iterator2_t2294453720 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Collections.IEnumerator MGR_Cloud::readHashDB(System.String)
extern Il2CppClass* U3CreadHashDBU3Ec__Iterator3_t962384949_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Cloud_readHashDB_m3218359951_MetadataUsageId;
extern "C"  Il2CppObject * MGR_Cloud_readHashDB_m3218359951 (MGR_Cloud_t726529104 * __this, String_t* ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_readHashDB_m3218359951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CreadHashDBU3Ec__Iterator3_t962384949 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CreadHashDBU3Ec__Iterator3_t962384949 * L_0 = (U3CreadHashDBU3Ec__Iterator3_t962384949 *)il2cpp_codegen_object_new(U3CreadHashDBU3Ec__Iterator3_t962384949_il2cpp_TypeInfo_var);
		U3CreadHashDBU3Ec__Iterator3__ctor_m3315340498(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadHashDBU3Ec__Iterator3_t962384949 * L_1 = V_0;
		String_t* L_2 = ___p0;
		NullCheck(L_1);
		L_1->set_p_0(L_2);
		U3CreadHashDBU3Ec__Iterator3_t962384949 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CreadHashDBU3Ec__Iterator3_t962384949 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Void MGR_Cloud::startReadDB()
extern "C"  void MGR_Cloud_startReadDB_m986115961 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = MGR_Cloud_readCoachDB_m3748347533(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Cloud::addPL(System.String,System.String,System.String,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2032843076;
extern Il2CppCodeGenString* _stringLiteral439627463;
extern Il2CppCodeGenString* _stringLiteral947094105;
extern Il2CppCodeGenString* _stringLiteral545555276;
extern const uint32_t MGR_Cloud_addPL_m2038133962_MetadataUsageId;
extern "C"  void MGR_Cloud_addPL_m2038133962 (MGR_Cloud_t726529104 * __this, String_t* ___c0, String_t* ___t1, String_t* ___pl2, String_t* ___hs3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_addPL_m2038133962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	WWW_t2919945039 * V_1 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2032843076);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2032843076);
		StringU5BU5D_t1642385972* L_1 = L_0;
		String_t* L_2 = ___c0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral439627463);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral439627463);
		StringU5BU5D_t1642385972* L_4 = L_3;
		String_t* L_5 = ___t1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral947094105);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral947094105);
		StringU5BU5D_t1642385972* L_7 = L_6;
		String_t* L_8 = ___pl2;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_8);
		StringU5BU5D_t1642385972* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral545555276);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral545555276);
		StringU5BU5D_t1642385972* L_10 = L_9;
		String_t* L_11 = ___hs3;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m626692867(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13 = V_0;
		WWW_t2919945039 * L_14 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_14, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		return;
	}
}
// System.Void MGR_Cloud::addPL()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1333684107;
extern Il2CppCodeGenString* _stringLiteral2032843076;
extern Il2CppCodeGenString* _stringLiteral439627463;
extern Il2CppCodeGenString* _stringLiteral947094105;
extern Il2CppCodeGenString* _stringLiteral545555276;
extern const uint32_t MGR_Cloud_addPL_m817357320_MetadataUsageId;
extern "C"  void MGR_Cloud_addPL_m817357320 (MGR_Cloud_t726529104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Cloud_addPL_m817357320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	WWW_t2919945039 * V_2 = NULL;
	{
		V_0 = _stringLiteral1333684107;
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2032843076);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2032843076);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Entry_t779635924 * L_2 = __this->get_address_of_myEntry_2();
		String_t* L_3 = L_2->get_coach_0();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral439627463);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral439627463);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Entry_t779635924 * L_6 = __this->get_address_of_myEntry_2();
		String_t* L_7 = L_6->get_trainee_1();
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral947094105);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral947094105);
		StringU5BU5D_t1642385972* L_9 = L_8;
		Entry_t779635924 * L_10 = __this->get_address_of_myEntry_2();
		String_t* L_11 = L_10->get_playlistName_2();
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_9;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral545555276);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral545555276);
		StringU5BU5D_t1642385972* L_13 = L_12;
		String_t* L_14 = V_0;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m626692867(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_15;
		String_t* L_16 = V_1;
		WWW_t2919945039 * L_17 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_17, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		return;
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator0::.ctor()
extern "C"  void U3CreadCoachDBU3Ec__Iterator0__ctor_m2866453635 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readCoachDB>c__Iterator0::MoveNext()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1695502239;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern const uint32_t U3CreadCoachDBU3Ec__Iterator0_MoveNext_m1720357261_MetadataUsageId;
extern "C"  bool U3CreadCoachDBU3Ec__Iterator0_MoveNext_m1720357261 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadCoachDBU3Ec__Iterator0_MoveNext_m1720357261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0069;
		}
	}
	{
		goto IL_0125;
	}

IL_0021:
	{
		int32_t L_2 = 1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1695502239, L_3, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_0(L_4);
		String_t* L_5 = __this->get_U3CurlU3E__0_0();
		WWW_t2919945039 * L_6 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_1(L_6);
		WWW_t2919945039 * L_7 = __this->get_U3Ccu_getU3E__1_1();
		__this->set_U24current_4(L_7);
		bool L_8 = __this->get_U24disposing_5();
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0064:
	{
		goto IL_0127;
	}

IL_0069:
	{
		WWW_t2919945039 * L_9 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m3092701216(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_009a;
		}
	}
	{
		WWW_t2919945039 * L_11 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_11);
		String_t* L_12 = WWW_get_error_m3092701216(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_12, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_009a:
	{
		WWW_t2919945039 * L_14 = __this->get_U3Ccu_getU3E__1_1();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_text_m1558985139(L_14, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_2(L_15);
		String_t* L_16 = __this->get_U3CdataU3E__2_2();
		CharU5BU5D_t1328083999* L_17 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_16);
		StringU5BU5D_t1642385972* L_18 = String_Split_m3326265864(L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		MGR_Cloud_t726529104 * L_19 = __this->get_U24this_3();
		NullCheck(L_19);
		Entry_t779635924 * L_20 = L_19->get_address_of_myEntry_2();
		List_1_t1398341365 * L_21 = L_20->get_coaches_5();
		NullCheck(L_21);
		List_1_Clear_m2928955906(L_21, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_22 = __this->get_U24this_3();
		NullCheck(L_22);
		Entry_t779635924 * L_23 = L_22->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_24 = V_1;
		NullCheck(L_24);
		L_23->set_cSize_9(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_0112;
	}

IL_00f4:
	{
		MGR_Cloud_t726529104 * L_25 = __this->get_U24this_3();
		NullCheck(L_25);
		Entry_t779635924 * L_26 = L_25->get_address_of_myEntry_2();
		List_1_t1398341365 * L_27 = L_26->get_coaches_5();
		StringU5BU5D_t1642385972* L_28 = V_1;
		int32_t L_29 = V_2;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		String_t* L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_27);
		List_1_Add_m4061286785(L_27, L_31, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0112:
	{
		int32_t L_33 = V_2;
		StringU5BU5D_t1642385972* L_34 = V_1;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length))))-(int32_t)1)))))
		{
			goto IL_00f4;
		}
	}
	{
	}

IL_011e:
	{
		__this->set_U24PC_6((-1));
	}

IL_0125:
	{
		return (bool)0;
	}

IL_0127:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readCoachDB>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3563481621 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<readCoachDB>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadCoachDBU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2351408045 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator0::Dispose()
extern "C"  void U3CreadCoachDBU3Ec__Iterator0_Dispose_m3313015228 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readCoachDB>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadCoachDBU3Ec__Iterator0_Reset_m1749873790_MetadataUsageId;
extern "C"  void U3CreadCoachDBU3Ec__Iterator0_Reset_m1749873790 (U3CreadCoachDBU3Ec__Iterator0_t3826372698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadCoachDBU3Ec__Iterator0_Reset_m1749873790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator3::.ctor()
extern "C"  void U3CreadHashDBU3Ec__Iterator3__ctor_m3315340498 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readHashDB>c__Iterator3::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m3117563614_MethodInfo_var;
extern const MethodInfo* List_1_Add_m236055543_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3854425081;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t U3CreadHashDBU3Ec__Iterator3_MoveNext_m443546314_MetadataUsageId;
extern "C"  bool U3CreadHashDBU3Ec__Iterator3_MoveNext_m443546314 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHashDBU3Ec__Iterator3_MoveNext_m443546314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0091;
		}
	}
	{
		goto IL_0161;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral3854425081);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3854425081);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_p_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 4;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		String_t* L_11 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_12 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_12, L_11, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_12);
		WWW_t2919945039 * L_13 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_13);
		bool L_14 = __this->get_U24disposing_6();
		if (L_14)
		{
			goto IL_008c;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_008c:
	{
		goto IL_0163;
	}

IL_0091:
	{
		WWW_t2919945039 * L_15 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_15);
		String_t* L_16 = WWW_get_error_m3092701216(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00cd;
		}
	}
	{
		WWW_t2919945039 * L_17 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_17);
		String_t* L_18 = WWW_get_error_m3092701216(L_17, /*hidden argument*/NULL);
		String_t* L_19 = __this->get_U3CurlU3E__0_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral2285603187, L_18, _stringLiteral372029352, L_19, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_00cd:
	{
		WWW_t2919945039 * L_21 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_21);
		String_t* L_22 = WWW_get_text_m1558985139(L_21, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_22);
		String_t* L_23 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_24 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_23);
		StringU5BU5D_t1642385972* L_25 = String_Split_m3326265864(L_23, L_24, /*hidden argument*/NULL);
		V_1 = L_25;
		MGR_Cloud_t726529104 * L_26 = __this->get_U24this_4();
		NullCheck(L_26);
		Entry_t779635924 * L_27 = L_26->get_address_of_myEntry_2();
		List_1_t278199169 * L_28 = L_27->get_hashString_7();
		NullCheck(L_28);
		List_1_Clear_m3117563614(L_28, /*hidden argument*/List_1_Clear_m3117563614_MethodInfo_var);
		MGR_Cloud_t726529104 * L_29 = __this->get_U24this_4();
		NullCheck(L_29);
		Entry_t779635924 * L_30 = L_29->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_31 = V_1;
		NullCheck(L_31);
		L_30->set_hSize_11((((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))));
		V_2 = 0;
		goto IL_0150;
	}

IL_0125:
	{
		StringU5BU5D_t1642385972* L_32 = V_1;
		int32_t L_33 = V_2;
		NullCheck(L_32);
		int32_t L_34 = L_33;
		String_t* L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_36 = __this->get_U24this_4();
		NullCheck(L_36);
		Entry_t779635924 * L_37 = L_36->get_address_of_myEntry_2();
		List_1_t278199169 * L_38 = L_37->get_hashString_7();
		StringU5BU5D_t1642385972* L_39 = V_1;
		int32_t L_40 = V_2;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		String_t* L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int64_t L_43 = Convert_ToInt64_m3181519185(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		NullCheck(L_38);
		List_1_Add_m236055543(L_38, L_43, /*hidden argument*/List_1_Add_m236055543_MethodInfo_var);
		int32_t L_44 = V_2;
		V_2 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_0150:
	{
		int32_t L_45 = V_2;
		StringU5BU5D_t1642385972* L_46 = V_1;
		NullCheck(L_46);
		if ((((int32_t)L_45) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_46)->max_length)))))))
		{
			goto IL_0125;
		}
	}
	{
	}

IL_015a:
	{
		__this->set_U24PC_7((-1));
	}

IL_0161:
	{
		return (bool)0;
	}

IL_0163:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readHashDB>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4155094814 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<readHashDB>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadHashDBU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3808693974 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator3::Dispose()
extern "C"  void U3CreadHashDBU3Ec__Iterator3_Dispose_m1756324991 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readHashDB>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadHashDBU3Ec__Iterator3_Reset_m738018153_MetadataUsageId;
extern "C"  void U3CreadHashDBU3Ec__Iterator3_Reset_m738018153 (U3CreadHashDBU3Ec__Iterator3_t962384949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHashDBU3Ec__Iterator3_Reset_m738018153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator2::.ctor()
extern "C"  void U3CreadPLDBU3Ec__Iterator2__ctor_m938752507 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readPLDB>c__Iterator2::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral859641999;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern Il2CppCodeGenString* _stringLiteral2543474434;
extern const uint32_t U3CreadPLDBU3Ec__Iterator2_MoveNext_m1249169289_MetadataUsageId;
extern "C"  bool U3CreadPLDBU3Ec__Iterator2_MoveNext_m1249169289 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadPLDBU3Ec__Iterator2_MoveNext_m1249169289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0091;
		}
	}
	{
		goto IL_015f;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral859641999);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral859641999);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_t_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 3;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		String_t* L_11 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_12 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_12, L_11, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_12);
		WWW_t2919945039 * L_13 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_13);
		bool L_14 = __this->get_U24disposing_6();
		if (L_14)
		{
			goto IL_008c;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_008c:
	{
		goto IL_0161;
	}

IL_0091:
	{
		WWW_t2919945039 * L_15 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_15);
		String_t* L_16 = WWW_get_error_m3092701216(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c2;
		}
	}
	{
		WWW_t2919945039 * L_17 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_17);
		String_t* L_18 = WWW_get_error_m3092701216(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_18, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		goto IL_0158;
	}

IL_00c2:
	{
		WWW_t2919945039 * L_20 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_20);
		String_t* L_21 = WWW_get_text_m1558985139(L_20, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_21);
		String_t* L_22 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_23 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_22);
		StringU5BU5D_t1642385972* L_24 = String_Split_m3326265864(L_22, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral2543474434, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_25 = __this->get_U24this_4();
		NullCheck(L_25);
		Entry_t779635924 * L_26 = L_25->get_address_of_myEntry_2();
		List_1_t1398341365 * L_27 = L_26->get_currentPlaylist_3();
		NullCheck(L_27);
		List_1_Clear_m2928955906(L_27, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_28 = __this->get_U24this_4();
		NullCheck(L_28);
		Entry_t779635924 * L_29 = L_28->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_30 = V_1;
		NullCheck(L_30);
		L_29->set_pSize_8(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_014c;
	}

IL_0126:
	{
		MGR_Cloud_t726529104 * L_31 = __this->get_U24this_4();
		NullCheck(L_31);
		Entry_t779635924 * L_32 = L_31->get_address_of_myEntry_2();
		List_1_t1398341365 * L_33 = L_32->get_currentPlaylist_3();
		StringU5BU5D_t1642385972* L_34 = V_1;
		int32_t L_35 = V_2;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_33);
		List_1_Add_m4061286785(L_33, L_37, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		StringU5BU5D_t1642385972* L_38 = V_1;
		int32_t L_39 = V_2;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		String_t* L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		int32_t L_42 = V_2;
		V_2 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_014c:
	{
		int32_t L_43 = V_2;
		StringU5BU5D_t1642385972* L_44 = V_1;
		NullCheck(L_44);
		if ((((int32_t)L_43) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_44)->max_length))))-(int32_t)1)))))
		{
			goto IL_0126;
		}
	}
	{
	}

IL_0158:
	{
		__this->set_U24PC_7((-1));
	}

IL_015f:
	{
		return (bool)0;
	}

IL_0161:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readPLDB>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadPLDBU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1657717909 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<readPLDB>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadPLDBU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m758923853 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator2::Dispose()
extern "C"  void U3CreadPLDBU3Ec__Iterator2_Dispose_m2708295014 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readPLDB>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadPLDBU3Ec__Iterator2_Reset_m4163262508_MetadataUsageId;
extern "C"  void U3CreadPLDBU3Ec__Iterator2_Reset_m4163262508 (U3CreadPLDBU3Ec__Iterator2_t2294453720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadPLDBU3Ec__Iterator2_Reset_m4163262508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator1::.ctor()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator1__ctor_m1422939798 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_Cloud/<readTraineeDB>c__Iterator1::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2054597853;
extern Il2CppCodeGenString* _stringLiteral627712325;
extern Il2CppCodeGenString* _stringLiteral2285603187;
extern const uint32_t U3CreadTraineeDBU3Ec__Iterator1_MoveNext_m838431130_MetadataUsageId;
extern "C"  bool U3CreadTraineeDBU3Ec__Iterator1_MoveNext_m838431130 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadTraineeDBU3Ec__Iterator1_MoveNext_m838431130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0086;
		}
	}
	{
		goto IL_0142;
	}

IL_0021:
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral2054597853);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2054597853);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = __this->get_c_0();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral627712325);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral627712325);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = 2;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_9);
		String_t* L_10 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_11, L_10, /*hidden argument*/NULL);
		__this->set_U3Ccu_getU3E__1_2(L_11);
		WWW_t2919945039 * L_12 = __this->get_U3Ccu_getU3E__1_2();
		__this->set_U24current_5(L_12);
		bool L_13 = __this->get_U24disposing_6();
		if (L_13)
		{
			goto IL_0081;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0081:
	{
		goto IL_0144;
	}

IL_0086:
	{
		WWW_t2919945039 * L_14 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_14);
		String_t* L_15 = WWW_get_error_m3092701216(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00b7;
		}
	}
	{
		WWW_t2919945039 * L_16 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_16);
		String_t* L_17 = WWW_get_error_m3092701216(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2285603187, L_17, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_00b7:
	{
		WWW_t2919945039 * L_19 = __this->get_U3Ccu_getU3E__1_2();
		NullCheck(L_19);
		String_t* L_20 = WWW_get_text_m1558985139(L_19, /*hidden argument*/NULL);
		__this->set_U3CdataU3E__2_3(L_20);
		String_t* L_21 = __this->get_U3CdataU3E__2_3();
		CharU5BU5D_t1328083999* L_22 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_21);
		StringU5BU5D_t1642385972* L_23 = String_Split_m3326265864(L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		MGR_Cloud_t726529104 * L_24 = __this->get_U24this_4();
		NullCheck(L_24);
		Entry_t779635924 * L_25 = L_24->get_address_of_myEntry_2();
		List_1_t1398341365 * L_26 = L_25->get_trainees_6();
		NullCheck(L_26);
		List_1_Clear_m2928955906(L_26, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		MGR_Cloud_t726529104 * L_27 = __this->get_U24this_4();
		NullCheck(L_27);
		Entry_t779635924 * L_28 = L_27->get_address_of_myEntry_2();
		StringU5BU5D_t1642385972* L_29 = V_1;
		NullCheck(L_29);
		L_28->set_tSize_10(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length))))-(int32_t)1)));
		V_2 = 0;
		goto IL_012f;
	}

IL_0111:
	{
		MGR_Cloud_t726529104 * L_30 = __this->get_U24this_4();
		NullCheck(L_30);
		Entry_t779635924 * L_31 = L_30->get_address_of_myEntry_2();
		List_1_t1398341365 * L_32 = L_31->get_trainees_6();
		StringU5BU5D_t1642385972* L_33 = V_1;
		int32_t L_34 = V_2;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		String_t* L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_32);
		List_1_Add_m4061286785(L_32, L_36, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		int32_t L_37 = V_2;
		V_2 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_012f:
	{
		int32_t L_38 = V_2;
		StringU5BU5D_t1642385972* L_39 = V_1;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length))))-(int32_t)1)))))
		{
			goto IL_0111;
		}
	}
	{
	}

IL_013b:
	{
		__this->set_U24PC_7((-1));
	}

IL_0142:
	{
		return (bool)0;
	}

IL_0144:
	{
		return (bool)1;
	}
}
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3733750148 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1190909852 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator1::Dispose()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator1_Dispose_m1703927019 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CreadTraineeDBU3Ec__Iterator1_Reset_m3852545481_MetadataUsageId;
extern "C"  void U3CreadTraineeDBU3Ec__Iterator1_Reset_m3852545481 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CreadTraineeDBU3Ec__Iterator1_Reset_m3852545481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_CreateMenu::.ctor()
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t MGR_CreateMenu__ctor_m645211395_MetadataUsageId;
extern "C"  void MGR_CreateMenu__ctor_m645211395 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu__ctor_m645211395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_hs_11(_stringLiteral371857150);
		__this->set_currPLItem_15(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_CreateMenu::Start()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3611321335;
extern Il2CppCodeGenString* _stringLiteral3611321332;
extern const uint32_t MGR_CreateMenu_Start_m931694471_MetadataUsageId;
extern "C"  void MGR_CreateMenu_Start_m931694471 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_Start_m931694471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_playlistItems_5(L_0);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral3611321335, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_myGO_3();
		NullCheck(L_1);
		Animator_t69676727 * L_2 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_4(L_2);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral3611321332, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_myGO_3();
		NullCheck(L_3);
		MGR_Cloud_t726529104 * L_4 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_3, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_6(L_4);
		MGR_CreateMenu_readStates_m3857455173(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_CreateMenu::Update()
extern "C"  void MGR_CreateMenu_Update_m529887162 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_CreateMenu::displayWorkingPL()
extern "C"  void MGR_CreateMenu_displayWorkingPL_m1446641882 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_CreateMenu::readStates()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const uint32_t MGR_CreateMenu_readStates_m3857455173_MetadataUsageId;
extern "C"  void MGR_CreateMenu_readStates_m3857455173 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_readStates_m3857455173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AnimationClip_t3510324950 * V_1 = NULL;
	AnimationClipU5BU5D_t3936083219* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Animator_t69676727 * L_0 = __this->get_myAnimator_4();
		NullCheck(L_0);
		RuntimeAnimatorController_t670468573 * L_1 = Animator_get_runtimeAnimatorController_m652575931(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationClipU5BU5D_t3936083219* L_2 = RuntimeAnimatorController_get_animationClips_m3074969690(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		__this->set_clipNames_2(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))));
		V_0 = 0;
		Animator_t69676727 * L_3 = __this->get_myAnimator_4();
		NullCheck(L_3);
		RuntimeAnimatorController_t670468573 * L_4 = Animator_get_runtimeAnimatorController_m652575931(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		AnimationClipU5BU5D_t3936083219* L_5 = RuntimeAnimatorController_get_animationClips_m3074969690(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		V_3 = 0;
		goto IL_0065;
	}

IL_0039:
	{
		AnimationClipU5BU5D_t3936083219* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		AnimationClip_t3510324950 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		StringU5BU5D_t1642385972* L_10 = __this->get_clipNames_2();
		int32_t L_11 = V_0;
		AnimationClip_t3510324950 * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = Object_get_name_m2079638459(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_14);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (String_t*)L_14);
		AnimationClip_t3510324950 * L_15 = V_1;
		NullCheck(L_15);
		String_t* L_16 = Object_get_name_m2079638459(L_15, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_0;
		V_0 = ((int32_t)((int32_t)L_17+(int32_t)1));
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0065:
	{
		int32_t L_19 = V_3;
		AnimationClipU5BU5D_t3936083219* L_20 = V_2;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0039;
		}
	}
	{
		return;
	}
}
// System.Void MGR_CreateMenu::OnGUI()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m3019523498_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t MGR_CreateMenu_OnGUI_m420939169_MetadataUsageId;
extern "C"  void MGR_CreateMenu_OnGUI_m420939169 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_OnGUI_m420939169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_0 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t1799908754 * L_1 = GUISkin_get_button_m797402546(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_set_fontSize_m4015341543(L_1, ((int32_t)24), /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, (10.0f), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_2/(int32_t)((int32_t)15)))+(int32_t)((int32_t)10)))))), (((float)((float)((int32_t)((int32_t)L_3/(int32_t)3))))), (((float)((float)((int32_t)((int32_t)L_4-(int32_t)((int32_t)((int32_t)L_5/(int32_t)((int32_t)14)))))))), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3297699023(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = __this->get_coachScrollPosition_16();
		GUILayoutOptionU5BU5D_t2108882777* L_8 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_9 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_10 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_9/(int32_t)3))))), /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_10);
		GUILayoutOptionU5BU5D_t2108882777* L_11 = L_8;
		int32_t L_12 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_13 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_12)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_13);
		Vector2_t2243707579  L_14 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		__this->set_coachScrollPosition_16(L_14);
		V_0 = 0;
		goto IL_00fc;
	}

IL_0086:
	{
		StringU5BU5D_t1642385972* L_15 = __this->get_clipNames_2();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		String_t* L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		GUILayoutOptionU5BU5D_t2108882777* L_19 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_20 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_21 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_20/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_21);
		GUILayoutOptionU5BU5D_t2108882777* L_22 = L_19;
		int32_t L_23 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_24 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_23)))/(float)(3.4f))), /*hidden argument*/NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_24);
		bool L_25 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_18, L_22, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00f7;
		}
	}
	{
		List_1_t1398341365 * L_26 = __this->get_playlistItems_5();
		StringU5BU5D_t1642385972* L_27 = __this->get_clipNames_2();
		int32_t L_28 = V_0;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		String_t* L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_26);
		List_1_Add_m4061286785(L_26, L_30, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		String_t* L_31 = __this->get_hs_11();
		StringU5BU5D_t1642385972* L_32 = __this->get_clipNames_2();
		int32_t L_33 = V_0;
		NullCheck(L_32);
		int32_t L_34 = L_33;
		String_t* L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m2596409543(NULL /*static, unused*/, L_35, _stringLiteral372029352, /*hidden argument*/NULL);
		String_Concat_m2596409543(NULL /*static, unused*/, L_31, L_36, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		int32_t L_37 = V_0;
		V_0 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00fc:
	{
		int32_t L_38 = V_0;
		StringU5BU5D_t1642385972* L_39 = __this->get_clipNames_2();
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))))))
		{
			goto IL_0086;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1904221074(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_40 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_41 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_42 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_43 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_44 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_45 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Rect__ctor_m1220545469(&L_46, ((float)((float)((float)((float)(((float)((float)((int32_t)((int32_t)L_40/(int32_t)((int32_t)15))))))+(float)((float)((float)(((float)((float)L_41)))/(float)(3.4f)))))+(float)(100.0f))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_42/(int32_t)((int32_t)15)))+(int32_t)((int32_t)10)))))), (((float)((float)((int32_t)((int32_t)L_43/(int32_t)3))))), (((float)((float)((int32_t)((int32_t)L_44-(int32_t)((int32_t)((int32_t)L_45/(int32_t)((int32_t)14)))))))), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3297699023(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		Vector2_t2243707579  L_47 = __this->get_coachScrollPosition2_17();
		GUILayoutOptionU5BU5D_t2108882777* L_48 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_49 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_50 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_49/(int32_t)3))))), /*hidden argument*/NULL);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_50);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_50);
		GUILayoutOptionU5BU5D_t2108882777* L_51 = L_48;
		int32_t L_52 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_53 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_52)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_53);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_53);
		Vector2_t2243707579  L_54 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_47, L_51, /*hidden argument*/NULL);
		__this->set_coachScrollPosition2_17(L_54);
		V_1 = 0;
		goto IL_0200;
	}

IL_019f:
	{
		List_1_t1398341365 * L_55 = __this->get_playlistItems_5();
		int32_t L_56 = V_1;
		NullCheck(L_55);
		String_t* L_57 = List_1_get_Item_m1112119647(L_55, L_56, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_58 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_59 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_60 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_59/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_58);
		ArrayElementTypeCheck (L_58, L_60);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_60);
		GUILayoutOptionU5BU5D_t2108882777* L_61 = L_58;
		int32_t L_62 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_63 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_62)))/(float)(3.4f))), /*hidden argument*/NULL);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_63);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_63);
		bool L_64 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_57, L_61, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01fb;
		}
	}
	{
		List_1_t1398341365 * L_65 = __this->get_playlistItems_5();
		List_1_t1398341365 * L_66 = __this->get_playlistItems_5();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		String_t* L_68 = List_1_get_Item_m1112119647(L_66, L_67, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_65);
		List_1_Remove_m3019523498(L_65, L_68, /*hidden argument*/List_1_Remove_m3019523498_MethodInfo_var);
	}

IL_01fb:
	{
		int32_t L_69 = V_1;
		V_1 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_0200:
	{
		int32_t L_70 = V_1;
		List_1_t1398341365 * L_71 = __this->get_playlistItems_5();
		NullCheck(L_71);
		int32_t L_72 = List_1_get_Count_m780127360(L_71, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_70) < ((int32_t)L_72)))
		{
			goto IL_019f;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1904221074(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_CreateMenu::playAnims()
extern "C"  void MGR_CreateMenu_playAnims_m3011770835 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = MGR_CreateMenu_Play_m3856428385(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MGR_CreateMenu::Play()
extern Il2CppClass* U3CPlayU3Ec__Iterator0_t2013211190_il2cpp_TypeInfo_var;
extern const uint32_t MGR_CreateMenu_Play_m3856428385_MetadataUsageId;
extern "C"  Il2CppObject * MGR_CreateMenu_Play_m3856428385 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_Play_m3856428385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPlayU3Ec__Iterator0_t2013211190 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CPlayU3Ec__Iterator0_t2013211190 * L_0 = (U3CPlayU3Ec__Iterator0_t2013211190 *)il2cpp_codegen_object_new(U3CPlayU3Ec__Iterator0_t2013211190_il2cpp_TypeInfo_var);
		U3CPlayU3Ec__Iterator0__ctor_m2881918555(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayU3Ec__Iterator0_t2013211190 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_3(__this);
		U3CPlayU3Ec__Iterator0_t2013211190 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Void MGR_CreateMenu::resetPL()
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const uint32_t MGR_CreateMenu_resetPL_m2861834464_MetadataUsageId;
extern "C"  void MGR_CreateMenu_resetPL_m2861834464 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_resetPL_m2861834464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1398341365 * L_0 = __this->get_playlistItems_5();
		NullCheck(L_0);
		List_1_Clear_m2928955906(L_0, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		return;
	}
}
// System.Void MGR_CreateMenu::submitPL()
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m2685735877_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern const uint32_t MGR_CreateMenu_submitPL_m2648786843_MetadataUsageId;
extern "C"  void MGR_CreateMenu_submitPL_m2648786843 (MGR_CreateMenu_t817873244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_CreateMenu_submitPL_m2648786843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int64_t V_1 = 0;
	{
		__this->set_hs_11(_stringLiteral371857150);
		V_0 = 0;
		goto IL_0087;
	}

IL_0013:
	{
		MGR_GUI_t2229806790 * L_0 = __this->get_myGUI_7();
		NullCheck(L_0);
		Dictionary_2_t2823857299 * L_1 = L_0->get_DictOfAnims_16();
		List_1_t1398341365 * L_2 = __this->get_playlistItems_5();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		String_t* L_4 = List_1_get_Item_m1112119647(L_2, L_3, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_1);
		Dictionary_2_TryGetValue_m2685735877(L_1, L_4, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m2685735877_MethodInfo_var);
		int32_t L_5 = V_0;
		List_1_t1398341365 * L_6 = __this->get_playlistItems_5();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m780127360(L_6, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_5) >= ((int32_t)((int32_t)((int32_t)L_7-(int32_t)1)))))
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_8 = __this->get_hs_11();
		int64_t L_9 = V_1;
		int64_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2000667605(NULL /*static, unused*/, L_8, L_11, _stringLiteral372029314, /*hidden argument*/NULL);
		__this->set_hs_11(L_12);
		goto IL_0082;
	}

IL_0069:
	{
		String_t* L_13 = __this->get_hs_11();
		int64_t L_14 = V_1;
		int64_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m56707527(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		__this->set_hs_11(L_17);
	}

IL_0082:
	{
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0087:
	{
		int32_t L_19 = V_0;
		List_1_t1398341365 * L_20 = __this->get_playlistItems_5();
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m780127360(L_20, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0013;
		}
	}
	{
		InputField_t1631627530 * L_22 = __this->get_coachIF_12();
		NullCheck(L_22);
		String_t* L_23 = InputField_get_text_m409351770(L_22, /*hidden argument*/NULL);
		__this->set_coach_8(L_23);
		InputField_t1631627530 * L_24 = __this->get_traineeIF_13();
		NullCheck(L_24);
		String_t* L_25 = InputField_get_text_m409351770(L_24, /*hidden argument*/NULL);
		__this->set_trainee_9(L_25);
		InputField_t1631627530 * L_26 = __this->get_plIF_14();
		NullCheck(L_26);
		String_t* L_27 = InputField_get_text_m409351770(L_26, /*hidden argument*/NULL);
		__this->set_pl_10(L_27);
		MGR_Cloud_t726529104 * L_28 = __this->get_myCloud_6();
		String_t* L_29 = __this->get_coach_8();
		String_t* L_30 = __this->get_trainee_9();
		String_t* L_31 = __this->get_pl_10();
		String_t* L_32 = __this->get_hs_11();
		NullCheck(L_28);
		MGR_Cloud_addPL_m2038133962(L_28, L_29, L_30, L_31, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_CreateMenu/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m2881918555 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean MGR_CreateMenu/<Play>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_MoveNext_m2206641169_MetadataUsageId;
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m2206641169 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_MoveNext_m2206641169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ed;
		}
	}
	{
		goto IL_0131;
	}

IL_0021:
	{
		MGR_CreateMenu_t817873244 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = L_2->get_myAnimator_4();
		NullCheck(L_3);
		AnimatorStateInfo_t2577870592  L_4 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_3, 0, /*hidden argument*/NULL);
		__this->set_U3CcurrInfoU3E__0_0(L_4);
		__this->set_U3CiU3E__1_1(1);
		goto IL_010f;
	}

IL_0045:
	{
		MGR_CreateMenu_t817873244 * L_5 = __this->get_U24this_3();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_currPLItem_15();
		MGR_CreateMenu_t817873244 * L_7 = __this->get_U24this_3();
		NullCheck(L_7);
		List_1_t1398341365 * L_8 = L_7->get_playlistItems_5();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m780127360(L_8, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_6) >= ((int32_t)L_9)))
		{
			goto IL_0077;
		}
	}
	{
		MGR_CreateMenu_t817873244 * L_10 = __this->get_U24this_3();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_currPLItem_15();
		if ((((int32_t)L_11) >= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}

IL_0077:
	{
		MGR_CreateMenu_t817873244 * L_12 = __this->get_U24this_3();
		NullCheck(L_12);
		L_12->set_currPLItem_15(0);
	}

IL_0085:
	{
		AnimatorStateInfo_t2577870592 * L_13 = __this->get_address_of_U3CcurrInfoU3E__0_0();
		float L_14 = AnimatorStateInfo_get_length_m3151009408(L_13, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__2_2(L_14);
		MGR_CreateMenu_t817873244 * L_15 = __this->get_U24this_3();
		NullCheck(L_15);
		Animator_t69676727 * L_16 = L_15->get_myAnimator_4();
		MGR_CreateMenu_t817873244 * L_17 = __this->get_U24this_3();
		NullCheck(L_17);
		List_1_t1398341365 * L_18 = L_17->get_playlistItems_5();
		int32_t L_19 = __this->get_U3CiU3E__1_1();
		NullCheck(L_18);
		String_t* L_20 = List_1_get_Item_m1112119647(L_18, L_19, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_16);
		Animator_CrossFade_m1558283744(L_16, L_20, (1.0f), 0, /*hidden argument*/NULL);
		float L_21 = __this->get_U3CtimeU3E__2_2();
		WaitForSeconds_t3839502067 * L_22 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_22, ((float)((float)L_21-(float)(1.0f))), /*hidden argument*/NULL);
		__this->set_U24current_4(L_22);
		bool L_23 = __this->get_U24disposing_5();
		if (L_23)
		{
			goto IL_00e8;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_00e8:
	{
		goto IL_0133;
	}

IL_00ed:
	{
		MGR_CreateMenu_t817873244 * L_24 = __this->get_U24this_3();
		MGR_CreateMenu_t817873244 * L_25 = L_24;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_currPLItem_15();
		NullCheck(L_25);
		L_25->set_currPLItem_15(((int32_t)((int32_t)L_26+(int32_t)1)));
		int32_t L_27 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)((int32_t)L_27+(int32_t)1)));
	}

IL_010f:
	{
		int32_t L_28 = __this->get_U3CiU3E__1_1();
		MGR_CreateMenu_t817873244 * L_29 = __this->get_U24this_3();
		NullCheck(L_29);
		List_1_t1398341365 * L_30 = L_29->get_playlistItems_5();
		NullCheck(L_30);
		int32_t L_31 = List_1_get_Count_m780127360(L_30, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_28) < ((int32_t)L_31)))
		{
			goto IL_0045;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_0131:
	{
		return (bool)0;
	}

IL_0133:
	{
		return (bool)1;
	}
}
// System.Object MGR_CreateMenu/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m296993137 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object MGR_CreateMenu/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1260047433 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void MGR_CreateMenu/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m317506136 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void MGR_CreateMenu/<Play>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlayU3Ec__Iterator0_Reset_m1194165590_MetadataUsageId;
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m1194165590 (U3CPlayU3Ec__Iterator0_t2013211190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayU3Ec__Iterator0_Reset_m1194165590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MGR_GUI::.ctor()
extern "C"  void MGR_GUI__ctor_m1251067095 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	{
		__this->set_coachSelected_2((bool)0);
		__this->set_traineeSelected_3((bool)0);
		__this->set_playlistSelected_4((bool)0);
		__this->set_count_26(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_GUI::Awake()
extern Il2CppClass* Dictionary_2_t3018738635_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2823857299_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1120707728_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1746140668_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const uint32_t MGR_GUI_Awake_m2956493910_MetadataUsageId;
extern "C"  void MGR_GUI_Awake_m2956493910 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_Awake_m2956493910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_myGO_19();
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_21(L_1);
		Dictionary_2_t3018738635 * L_2 = (Dictionary_2_t3018738635 *)il2cpp_codegen_object_new(Dictionary_2_t3018738635_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1120707728(L_2, /*hidden argument*/Dictionary_2__ctor_m1120707728_MethodInfo_var);
		__this->set_AnimsOfDick_17(L_2);
		Dictionary_2_t2823857299 * L_3 = (Dictionary_2_t2823857299 *)il2cpp_codegen_object_new(Dictionary_2_t2823857299_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1746140668(L_3, /*hidden argument*/Dictionary_2__ctor_m1746140668_MethodInfo_var);
		__this->set_DictOfAnims_16(L_3);
		MGR_GUI_readStates_m194826505(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_myGO_19();
		NullCheck(L_4);
		MGR_Cloud_t726529104 * L_5 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_4, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_18(L_5);
		Canvas_t209405766 * L_6 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		__this->set_myCanvas_20(L_6);
		return;
	}
}
// System.Void MGR_GUI::readStates()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m526555557_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m139970648_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1712687620_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t MGR_GUI_readStates_m194826505_MetadataUsageId;
extern "C"  void MGR_GUI_readStates_m194826505 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_readStates_m194826505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AnimationClip_t3510324950 * V_1 = NULL;
	AnimationClipU5BU5D_t3936083219* V_2 = NULL;
	int32_t V_3 = 0;
	AnimationClip_t3510324950 * V_4 = NULL;
	AnimationClipU5BU5D_t3936083219* V_5 = NULL;
	int32_t V_6 = 0;
	{
		V_0 = 0;
		Animator_t69676727 * L_0 = __this->get_myAnimator_21();
		NullCheck(L_0);
		RuntimeAnimatorController_t670468573 * L_1 = Animator_get_runtimeAnimatorController_m652575931(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationClipU5BU5D_t3936083219* L_2 = RuntimeAnimatorController_get_animationClips_m3074969690(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_002a;
	}

IL_001c:
	{
		AnimationClipU5BU5D_t3936083219* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AnimationClip_t3510324950 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_3;
		AnimationClipU5BU5D_t3936083219* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->set_clipNames_14(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_0 = 0;
		Animator_t69676727 * L_12 = __this->get_myAnimator_21();
		NullCheck(L_12);
		RuntimeAnimatorController_t670468573 * L_13 = Animator_get_runtimeAnimatorController_m652575931(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationClipU5BU5D_t3936083219* L_14 = RuntimeAnimatorController_get_animationClips_m3074969690(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		V_6 = 0;
		goto IL_00fb;
	}

IL_005c:
	{
		AnimationClipU5BU5D_t3936083219* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		AnimationClip_t3510324950 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		StringU5BU5D_t1642385972* L_19 = __this->get_clipNames_14();
		int32_t L_20 = V_0;
		AnimationClip_t3510324950 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (String_t*)L_23);
		Dictionary_2_t2823857299 * L_24 = __this->get_DictOfAnims_16();
		StringU5BU5D_t1642385972* L_25 = __this->get_clipNames_14();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		bool L_29 = Dictionary_2_ContainsKey_m526555557(L_24, L_28, /*hidden argument*/Dictionary_2_ContainsKey_m526555557_MethodInfo_var);
		if (L_29)
		{
			goto IL_00f0;
		}
	}
	{
		AnimationClip_t3510324950 * L_30 = V_4;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_30);
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		StringU5BU5D_t1642385972* L_34 = __this->get_clipNames_14();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2000667605(NULL /*static, unused*/, L_33, _stringLiteral372029310, L_38, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		Dictionary_2_t3018738635 * L_40 = __this->get_AnimsOfDick_17();
		AnimationClip_t3510324950 * L_41 = V_4;
		NullCheck(L_41);
		int32_t L_42 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_41);
		StringU5BU5D_t1642385972* L_43 = __this->get_clipNames_14();
		int32_t L_44 = V_0;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		String_t* L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_40);
		Dictionary_2_Add_m139970648(L_40, (((int64_t)((int64_t)L_42))), L_46, /*hidden argument*/Dictionary_2_Add_m139970648_MethodInfo_var);
		Dictionary_2_t2823857299 * L_47 = __this->get_DictOfAnims_16();
		StringU5BU5D_t1642385972* L_48 = __this->get_clipNames_14();
		int32_t L_49 = V_0;
		NullCheck(L_48);
		int32_t L_50 = L_49;
		String_t* L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		AnimationClip_t3510324950 * L_52 = V_4;
		NullCheck(L_52);
		int32_t L_53 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_52);
		NullCheck(L_47);
		Dictionary_2_Add_m1712687620(L_47, L_51, (((int64_t)((int64_t)L_53))), /*hidden argument*/Dictionary_2_Add_m1712687620_MethodInfo_var);
	}

IL_00f0:
	{
		int32_t L_54 = V_0;
		V_0 = ((int32_t)((int32_t)L_54+(int32_t)1));
		int32_t L_55 = V_6;
		V_6 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_00fb:
	{
		int32_t L_56 = V_6;
		AnimationClipU5BU5D_t3936083219* L_57 = V_5;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		return;
	}
}
// System.Void MGR_GUI::addDBEntry()
extern "C"  void MGR_GUI_addDBEntry_m376179630 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	{
		InputField_t1631627530 * L_0 = __this->get_Input_Coach_5();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		__this->set_i_coach_11(L_1);
		InputField_t1631627530 * L_2 = __this->get_Input_Trainee_6();
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m409351770(L_2, /*hidden argument*/NULL);
		__this->set_i_trainee_12(L_3);
		InputField_t1631627530 * L_4 = __this->get_Input_Playlist_7();
		NullCheck(L_4);
		String_t* L_5 = InputField_get_text_m409351770(L_4, /*hidden argument*/NULL);
		__this->set_i_playlist_13(L_5);
		MGR_Cloud_t726529104 * L_6 = __this->get_myCloud_18();
		String_t* L_7 = __this->get_i_coach_11();
		String_t* L_8 = __this->get_i_trainee_12();
		String_t* L_9 = __this->get_i_playlist_13();
		String_t* L_10 = __this->get_hlist_15();
		NullCheck(L_6);
		MGR_Cloud_addPL_m2038133962(L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_GUI::PlayAnims()
extern "C"  void MGR_GUI_PlayAnims_m77098791 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_GUI::OnGUI()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1950531896_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m3941258745_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1149623516_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029319;
extern Il2CppCodeGenString* _stringLiteral2077193552;
extern Il2CppCodeGenString* _stringLiteral20327090;
extern Il2CppCodeGenString* _stringLiteral56436829;
extern Il2CppCodeGenString* _stringLiteral1842768;
extern const uint32_t MGR_GUI_OnGUI_m858956101_MetadataUsageId;
extern "C"  void MGR_GUI_OnGUI_m858956101 (MGR_GUI_t2229806790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_GUI_OnGUI_m858956101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	int64_t V_5 = 0;
	int64_t V_6 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_0 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t1799908754 * L_1 = GUISkin_get_button_m797402546(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_set_fontSize_m4015341543(L_1, ((int32_t)24), /*hidden argument*/NULL);
		int32_t L_2 = __this->get_count_26();
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0040;
		}
	}
	{
		MGR_Cloud_t726529104 * L_3 = __this->get_myCloud_18();
		NullCheck(L_3);
		Il2CppObject * L_4 = MGR_Cloud_readCoachDB_m3748347533(L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_count_26();
		__this->set_count_26(((int32_t)((int32_t)L_5+(int32_t)1)));
	}

IL_0040:
	{
		int32_t L_6 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Rect__ctor_m1220545469(&L_9, (10.0f), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_6/(int32_t)((int32_t)15)))+(int32_t)((int32_t)10)))))), (((float)((float)L_7))), (((float)((float)L_8))), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3297699023(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = __this->get_coachScrollPosition_22();
		GUILayoutOptionU5BU5D_t2108882777* L_11 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_12 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_13 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_12/(int32_t)4))-(int32_t)((int32_t)20)))))), /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_13);
		GUILayoutOptionU5BU5D_t2108882777* L_14 = L_11;
		int32_t L_15 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_16 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_15)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_16);
		Vector2_t2243707579  L_17 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_10, L_14, /*hidden argument*/NULL);
		__this->set_coachScrollPosition_22(L_17);
		GUILayoutOptionU5BU5D_t2108882777* L_18 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_19 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_20 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_19/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_20);
		GUILayoutOptionU5BU5D_t2108882777* L_21 = L_18;
		int32_t L_22 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_23 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_22)))/(float)(4.6f))), /*hidden argument*/NULL);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_23);
		bool L_24 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_21, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00f7;
		}
	}
	{
	}

IL_00f7:
	{
		V_0 = 0;
		goto IL_01aa;
	}

IL_00fe:
	{
		MGR_Cloud_t726529104 * L_25 = __this->get_myCloud_18();
		NullCheck(L_25);
		Entry_t779635924 * L_26 = L_25->get_address_of_myEntry_2();
		List_1_t1398341365 * L_27 = L_26->get_coaches_5();
		int32_t L_28 = V_0;
		NullCheck(L_27);
		String_t* L_29 = List_1_get_Item_m1112119647(L_27, L_28, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_30 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_31 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_32 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_31/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_32);
		GUILayoutOptionU5BU5D_t2108882777* L_33 = L_30;
		int32_t L_34 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_35 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_34)))/(float)(4.6f))), /*hidden argument*/NULL);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_35);
		bool L_36 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_29, L_33, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01a5;
		}
	}
	{
		__this->set_coachSelected_2((bool)1);
		__this->set_traineeSelected_3((bool)0);
		__this->set_playlistSelected_4((bool)0);
		MGR_Cloud_t726529104 * L_37 = __this->get_myCloud_18();
		NullCheck(L_37);
		Entry_t779635924 * L_38 = L_37->get_address_of_myEntry_2();
		List_1_t1398341365 * L_39 = L_38->get_coaches_5();
		int32_t L_40 = V_0;
		NullCheck(L_39);
		String_t* L_41 = List_1_get_Item_m1112119647(L_39, L_40, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_coach_8(L_41);
		MGR_Cloud_t726529104 * L_42 = __this->get_myCloud_18();
		MGR_Cloud_t726529104 * L_43 = __this->get_myCloud_18();
		NullCheck(L_43);
		Entry_t779635924 * L_44 = L_43->get_address_of_myEntry_2();
		List_1_t1398341365 * L_45 = L_44->get_coaches_5();
		int32_t L_46 = V_0;
		NullCheck(L_45);
		String_t* L_47 = List_1_get_Item_m1112119647(L_45, L_46, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_42);
		Il2CppObject * L_48 = MGR_Cloud_readTraineeDB_m1908492693(L_42, L_47, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_48, /*hidden argument*/NULL);
	}

IL_01a5:
	{
		int32_t L_49 = V_0;
		V_0 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_01aa:
	{
		int32_t L_50 = V_0;
		MGR_Cloud_t726529104 * L_51 = __this->get_myCloud_18();
		NullCheck(L_51);
		Entry_t779635924 * L_52 = L_51->get_address_of_myEntry_2();
		int32_t L_53 = L_52->get_cSize_9();
		if ((((int32_t)L_50) < ((int32_t)L_53)))
		{
			goto IL_00fe;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_54 = __this->get_coachSelected_2();
		if (!L_54)
		{
			goto IL_0348;
		}
	}
	{
		Vector2_t2243707579  L_55 = __this->get_traineeScrollPosition_23();
		GUILayoutOptionU5BU5D_t2108882777* L_56 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_57 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_58 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_57/(int32_t)4))-(int32_t)((int32_t)20)))))), /*hidden argument*/NULL);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_58);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_58);
		GUILayoutOptionU5BU5D_t2108882777* L_59 = L_56;
		int32_t L_60 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_61 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_60)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_59);
		ArrayElementTypeCheck (L_59, L_61);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_61);
		Vector2_t2243707579  L_62 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_55, L_59, /*hidden argument*/NULL);
		__this->set_traineeScrollPosition_23(L_62);
		GUILayoutOptionU5BU5D_t2108882777* L_63 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_64 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_65 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_64/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, L_65);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_65);
		GUILayoutOptionU5BU5D_t2108882777* L_66 = L_63;
		int32_t L_67 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_68 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_67)))/(float)(4.6f))), /*hidden argument*/NULL);
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_68);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_68);
		bool L_69 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_66, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_025b;
		}
	}
	{
	}

IL_025b:
	{
		V_1 = 0;
		goto IL_032c;
	}

IL_0262:
	{
		MGR_Cloud_t726529104 * L_70 = __this->get_myCloud_18();
		NullCheck(L_70);
		Entry_t779635924 * L_71 = L_70->get_address_of_myEntry_2();
		List_1_t1398341365 * L_72 = L_71->get_trainees_6();
		int32_t L_73 = V_1;
		NullCheck(L_72);
		String_t* L_74 = List_1_get_Item_m1112119647(L_72, L_73, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_75 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_76 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_77 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_76/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_75);
		ArrayElementTypeCheck (L_75, L_77);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_77);
		GUILayoutOptionU5BU5D_t2108882777* L_78 = L_75;
		int32_t L_79 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_80 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_79)))/(float)(4.6f))), /*hidden argument*/NULL);
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, L_80);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_80);
		bool L_81 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_74, L_78, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_0327;
		}
	}
	{
		__this->set_traineeSelected_3((bool)1);
		__this->set_playlistSelected_4((bool)0);
		MGR_Cloud_t726529104 * L_82 = __this->get_myCloud_18();
		NullCheck(L_82);
		Entry_t779635924 * L_83 = L_82->get_address_of_myEntry_2();
		List_1_t1398341365 * L_84 = L_83->get_trainees_6();
		int32_t L_85 = V_1;
		NullCheck(L_84);
		String_t* L_86 = List_1_get_Item_m1112119647(L_84, L_85, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_trainee_9(L_86);
		MGR_Cloud_t726529104 * L_87 = __this->get_myCloud_18();
		NullCheck(L_87);
		Entry_t779635924 * L_88 = L_87->get_address_of_myEntry_2();
		List_1_t1398341365 * L_89 = L_88->get_trainees_6();
		int32_t L_90 = V_1;
		NullCheck(L_89);
		String_t* L_91 = List_1_get_Item_m1112119647(L_89, L_90, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_92 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2077193552, L_91, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_92, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_93 = __this->get_myCloud_18();
		MGR_Cloud_t726529104 * L_94 = __this->get_myCloud_18();
		NullCheck(L_94);
		Entry_t779635924 * L_95 = L_94->get_address_of_myEntry_2();
		List_1_t1398341365 * L_96 = L_95->get_trainees_6();
		int32_t L_97 = V_1;
		NullCheck(L_96);
		String_t* L_98 = List_1_get_Item_m1112119647(L_96, L_97, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_93);
		Il2CppObject * L_99 = MGR_Cloud_readPLDB_m994522313(L_93, L_98, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_99, /*hidden argument*/NULL);
	}

IL_0327:
	{
		int32_t L_100 = V_1;
		V_1 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_032c:
	{
		int32_t L_101 = V_1;
		MGR_Cloud_t726529104 * L_102 = __this->get_myCloud_18();
		NullCheck(L_102);
		Entry_t779635924 * L_103 = L_102->get_address_of_myEntry_2();
		int32_t L_104 = L_103->get_tSize_10();
		if ((((int32_t)L_101) < ((int32_t)L_104)))
		{
			goto IL_0262;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0348:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_105 = __this->get_traineeSelected_3();
		if (!L_105)
		{
			goto IL_04c4;
		}
	}
	{
		Vector2_t2243707579  L_106 = __this->get_plScrollPosition_24();
		GUILayoutOptionU5BU5D_t2108882777* L_107 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_108 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_109 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_108/(int32_t)4))-(int32_t)((int32_t)20)))))), /*hidden argument*/NULL);
		NullCheck(L_107);
		ArrayElementTypeCheck (L_107, L_109);
		(L_107)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_109);
		GUILayoutOptionU5BU5D_t2108882777* L_110 = L_107;
		int32_t L_111 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_112 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_111)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_110);
		ArrayElementTypeCheck (L_110, L_112);
		(L_110)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_112);
		Vector2_t2243707579  L_113 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_106, L_110, /*hidden argument*/NULL);
		__this->set_plScrollPosition_24(L_113);
		GUILayoutOptionU5BU5D_t2108882777* L_114 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_115 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_116 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_115/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_114);
		ArrayElementTypeCheck (L_114, L_116);
		(L_114)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_116);
		GUILayoutOptionU5BU5D_t2108882777* L_117 = L_114;
		int32_t L_118 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_119 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_118)))/(float)(4.6f))), /*hidden argument*/NULL);
		NullCheck(L_117);
		ArrayElementTypeCheck (L_117, L_119);
		(L_117)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_119);
		bool L_120 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_117, /*hidden argument*/NULL);
		if (!L_120)
		{
			goto IL_03de;
		}
	}
	{
	}

IL_03de:
	{
		V_2 = 0;
		goto IL_04a8;
	}

IL_03e5:
	{
		MGR_Cloud_t726529104 * L_121 = __this->get_myCloud_18();
		NullCheck(L_121);
		Entry_t779635924 * L_122 = L_121->get_address_of_myEntry_2();
		List_1_t1398341365 * L_123 = L_122->get_currentPlaylist_3();
		int32_t L_124 = V_2;
		NullCheck(L_123);
		String_t* L_125 = List_1_get_Item_m1112119647(L_123, L_124, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		GUILayoutOptionU5BU5D_t2108882777* L_126 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_127 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_128 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_127/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_126);
		ArrayElementTypeCheck (L_126, L_128);
		(L_126)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_128);
		GUILayoutOptionU5BU5D_t2108882777* L_129 = L_126;
		int32_t L_130 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_131 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_130)))/(float)(4.6f))), /*hidden argument*/NULL);
		NullCheck(L_129);
		ArrayElementTypeCheck (L_129, L_131);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_131);
		bool L_132 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_125, L_129, /*hidden argument*/NULL);
		if (!L_132)
		{
			goto IL_04a3;
		}
	}
	{
		__this->set_playlistSelected_4((bool)1);
		MGR_Cloud_t726529104 * L_133 = __this->get_myCloud_18();
		NullCheck(L_133);
		Entry_t779635924 * L_134 = L_133->get_address_of_myEntry_2();
		List_1_t1398341365 * L_135 = L_134->get_currentPlaylist_3();
		int32_t L_136 = V_2;
		NullCheck(L_135);
		String_t* L_137 = List_1_get_Item_m1112119647(L_135, L_136, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		__this->set_playlist_10(L_137);
		MGR_Cloud_t726529104 * L_138 = __this->get_myCloud_18();
		NullCheck(L_138);
		Entry_t779635924 * L_139 = L_138->get_address_of_myEntry_2();
		List_1_t1398341365 * L_140 = L_139->get_currentPlaylist_3();
		int32_t L_141 = V_2;
		NullCheck(L_140);
		String_t* L_142 = List_1_get_Item_m1112119647(L_140, L_141, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_143 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral20327090, L_142, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_143, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_144 = __this->get_myCloud_18();
		MGR_Cloud_t726529104 * L_145 = __this->get_myCloud_18();
		NullCheck(L_145);
		Entry_t779635924 * L_146 = L_145->get_address_of_myEntry_2();
		List_1_t1398341365 * L_147 = L_146->get_currentPlaylist_3();
		int32_t L_148 = V_2;
		NullCheck(L_147);
		String_t* L_149 = List_1_get_Item_m1112119647(L_147, L_148, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_144);
		Il2CppObject * L_150 = MGR_Cloud_readHashDB_m3218359951(L_144, L_149, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_150, /*hidden argument*/NULL);
	}

IL_04a3:
	{
		int32_t L_151 = V_2;
		V_2 = ((int32_t)((int32_t)L_151+(int32_t)1));
	}

IL_04a8:
	{
		int32_t L_152 = V_2;
		MGR_Cloud_t726529104 * L_153 = __this->get_myCloud_18();
		NullCheck(L_153);
		Entry_t779635924 * L_154 = L_153->get_address_of_myEntry_2();
		int32_t L_155 = L_154->get_pSize_8();
		if ((((int32_t)L_152) < ((int32_t)L_155)))
		{
			goto IL_03e5;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_04c4:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_156 = __this->get_playlistSelected_4();
		if (!L_156)
		{
			goto IL_069f;
		}
	}
	{
		Vector2_t2243707579  L_157 = __this->get_hashScrollPosition_25();
		GUILayoutOptionU5BU5D_t2108882777* L_158 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_159 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_160 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_159/(int32_t)4))))), /*hidden argument*/NULL);
		NullCheck(L_158);
		ArrayElementTypeCheck (L_158, L_160);
		(L_158)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_160);
		GUILayoutOptionU5BU5D_t2108882777* L_161 = L_158;
		int32_t L_162 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_163 = GUILayout_Height_m607115982(NULL /*static, unused*/, ((float)((float)(((float)((float)L_162)))*(float)(0.85f))), /*hidden argument*/NULL);
		NullCheck(L_161);
		ArrayElementTypeCheck (L_161, L_163);
		(L_161)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_163);
		Vector2_t2243707579  L_164 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_157, L_161, /*hidden argument*/NULL);
		__this->set_hashScrollPosition_25(L_164);
		GUILayoutOptionU5BU5D_t2108882777* L_165 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_166 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_167 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_166/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_165);
		ArrayElementTypeCheck (L_165, L_167);
		(L_165)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_167);
		GUILayoutOptionU5BU5D_t2108882777* L_168 = L_165;
		int32_t L_169 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_170 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_169)))/(float)(4.6f))), /*hidden argument*/NULL);
		NullCheck(L_168);
		ArrayElementTypeCheck (L_168, L_170);
		(L_168)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_170);
		bool L_171 = GUILayout_Button_m3322709003(NULL /*static, unused*/, _stringLiteral372029319, L_168, /*hidden argument*/NULL);
		if (!L_171)
		{
			goto IL_0557;
		}
	}
	{
	}

IL_0557:
	{
		V_3 = 0;
		goto IL_0683;
	}

IL_055e:
	{
		int32_t L_172 = V_3;
		int32_t L_173 = L_172;
		Il2CppObject * L_174 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_173);
		MGR_Cloud_t726529104 * L_175 = __this->get_myCloud_18();
		NullCheck(L_175);
		Entry_t779635924 * L_176 = L_175->get_address_of_myEntry_2();
		int32_t L_177 = L_176->get_hSize_11();
		int32_t L_178 = L_177;
		Il2CppObject * L_179 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_178);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_180 = String_Concat_m2000667605(NULL /*static, unused*/, L_174, _stringLiteral56436829, L_179, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_180, /*hidden argument*/NULL);
		Dictionary_2_t3018738635 * L_181 = __this->get_AnimsOfDick_17();
		MGR_Cloud_t726529104 * L_182 = __this->get_myCloud_18();
		NullCheck(L_182);
		Entry_t779635924 * L_183 = L_182->get_address_of_myEntry_2();
		List_1_t278199169 * L_184 = L_183->get_hashString_7();
		int32_t L_185 = V_3;
		NullCheck(L_184);
		int64_t L_186 = List_1_get_Item_m1950531896(L_184, L_185, /*hidden argument*/List_1_get_Item_m1950531896_MethodInfo_var);
		NullCheck(L_181);
		Dictionary_2_TryGetValue_m3941258745(L_181, L_186, (&V_4), /*hidden argument*/Dictionary_2_TryGetValue_m3941258745_MethodInfo_var);
		String_t* L_187 = V_4;
		Dictionary_2_t3018738635 * L_188 = __this->get_AnimsOfDick_17();
		MGR_Cloud_t726529104 * L_189 = __this->get_myCloud_18();
		NullCheck(L_189);
		Entry_t779635924 * L_190 = L_189->get_address_of_myEntry_2();
		List_1_t278199169 * L_191 = L_190->get_hashString_7();
		int32_t L_192 = V_3;
		NullCheck(L_191);
		int64_t L_193 = List_1_get_Item_m1950531896(L_191, L_192, /*hidden argument*/List_1_get_Item_m1950531896_MethodInfo_var);
		NullCheck(L_188);
		String_t* L_194 = Dictionary_2_get_Item_m1149623516(L_188, L_193, /*hidden argument*/Dictionary_2_get_Item_m1149623516_MethodInfo_var);
		String_t* L_195 = String_Concat_m612901809(NULL /*static, unused*/, L_187, _stringLiteral1842768, L_194, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_195, /*hidden argument*/NULL);
		String_t* L_196 = V_4;
		GUILayoutOptionU5BU5D_t2108882777* L_197 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_198 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_199 = GUILayout_Height_m607115982(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_198/(int32_t)((int32_t)10)))))), /*hidden argument*/NULL);
		NullCheck(L_197);
		ArrayElementTypeCheck (L_197, L_199);
		(L_197)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_199);
		GUILayoutOptionU5BU5D_t2108882777* L_200 = L_197;
		int32_t L_201 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_202 = GUILayout_Width_m261136689(NULL /*static, unused*/, ((float)((float)(((float)((float)L_201)))/(float)(4.6f))), /*hidden argument*/NULL);
		NullCheck(L_200);
		ArrayElementTypeCheck (L_200, L_202);
		(L_200)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_202);
		bool L_203 = GUILayout_Button_m3322709003(NULL /*static, unused*/, L_196, L_200, /*hidden argument*/NULL);
		if (!L_203)
		{
			goto IL_067e;
		}
	}
	{
		__this->set_playlistSelected_4((bool)1);
		MGR_Cloud_t726529104 * L_204 = __this->get_myCloud_18();
		NullCheck(L_204);
		Entry_t779635924 * L_205 = L_204->get_address_of_myEntry_2();
		List_1_t278199169 * L_206 = L_205->get_hashString_7();
		int32_t L_207 = V_3;
		NullCheck(L_206);
		int64_t L_208 = List_1_get_Item_m1950531896(L_206, L_207, /*hidden argument*/List_1_get_Item_m1950531896_MethodInfo_var);
		V_5 = L_208;
		String_t* L_209 = Int64_ToString_m689375889((&V_5), /*hidden argument*/NULL);
		__this->set_playlist_10(L_209);
		MGR_Cloud_t726529104 * L_210 = __this->get_myCloud_18();
		NullCheck(L_210);
		Entry_t779635924 * L_211 = L_210->get_address_of_myEntry_2();
		List_1_t278199169 * L_212 = L_211->get_hashString_7();
		int32_t L_213 = V_3;
		NullCheck(L_212);
		int64_t L_214 = List_1_get_Item_m1950531896(L_212, L_213, /*hidden argument*/List_1_get_Item_m1950531896_MethodInfo_var);
		V_6 = L_214;
		String_t* L_215 = Int64_ToString_m689375889((&V_6), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_216 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral20327090, L_215, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_216, /*hidden argument*/NULL);
	}

IL_067e:
	{
		int32_t L_217 = V_3;
		V_3 = ((int32_t)((int32_t)L_217+(int32_t)1));
	}

IL_0683:
	{
		int32_t L_218 = V_3;
		MGR_Cloud_t726529104 * L_219 = __this->get_myCloud_18();
		NullCheck(L_219);
		Entry_t779635924 * L_220 = L_219->get_address_of_myEntry_2();
		int32_t L_221 = L_220->get_hSize_11();
		if ((((int32_t)L_218) < ((int32_t)L_221)))
		{
			goto IL_055e;
		}
	}
	{
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_069f:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m2019304577(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1904221074(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::.ctor()
extern "C"  void MGR_HLGUI__ctor_m1143505859 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	{
		__this->set_count_22(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::Awake()
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2063026683_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1568294237;
extern const uint32_t MGR_HLGUI_Awake_m2690111322_MetadataUsageId;
extern "C"  void MGR_HLGUI_Awake_m2690111322 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_Awake_m2690111322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_myGO_15();
		NullCheck(L_0);
		MGR_Anim_t3221826994 * L_1 = GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355(L_0, /*hidden argument*/GameObject_GetComponent_TisMGR_Anim_t3221826994_m162306355_MethodInfo_var);
		__this->set_myAnim_14(L_1);
		GameObject_t1756533147 * L_2 = __this->get_myGO_15();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimator_17(L_3);
		Dictionary_2_t3986656710 * L_4 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2063026683(L_4, /*hidden argument*/Dictionary_2__ctor_m2063026683_MethodInfo_var);
		__this->set_DictOfAnims_12(L_4);
		GameObject_t1756533147 * L_5 = __this->get_myGO_15();
		NullCheck(L_5);
		MGR_Cloud_t726529104 * L_6 = GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577(L_5, /*hidden argument*/GameObject_GetComponent_TisMGR_Cloud_t726529104_m548610577_MethodInfo_var);
		__this->set_myCloud_13(L_6);
		MGR_HLGUI_updatePL_m557645580(__this, /*hidden argument*/NULL);
		Canvas_t209405766 * L_7 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		__this->set_myCanvas_16(L_7);
		MGR_HLGUI_helpME_m62156382(__this, _stringLiteral1568294237, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::updatePL()
extern Il2CppCodeGenString* _stringLiteral4225798434;
extern Il2CppCodeGenString* _stringLiteral3680455850;
extern const uint32_t MGR_HLGUI_updatePL_m557645580_MetadataUsageId;
extern "C"  void MGR_HLGUI_updatePL_m557645580 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_updatePL_m557645580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_13();
		NullCheck(L_0);
		Il2CppObject * L_1 = MGR_Cloud_readHashDB_m3218359951(L_0, _stringLiteral4225798434, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
		MGR_HLGUI_helpME_m62156382(__this, _stringLiteral3680455850, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::helpME(System.String)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1214590000;
extern const uint32_t MGR_HLGUI_helpME_m62156382_MetadataUsageId;
extern "C"  void MGR_HLGUI_helpME_m62156382 (MGR_HLGUI_t3333927834 * __this, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_helpME_m62156382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___s0;
		MGR_Cloud_t726529104 * L_1 = __this->get_myCloud_13();
		NullCheck(L_1);
		Entry_t779635924 * L_2 = L_1->get_address_of_myEntry_2();
		int32_t L_3 = L_2->get_hSize_11();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2000667605(NULL /*static, unused*/, L_0, _stringLiteral1214590000, L_5, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0033;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_8 = V_0;
		MGR_Cloud_t726529104 * L_9 = __this->get_myCloud_13();
		NullCheck(L_9);
		Entry_t779635924 * L_10 = L_9->get_address_of_myEntry_2();
		int32_t L_11 = L_10->get_hSize_11();
		if ((((int32_t)L_8) < ((int32_t)L_11)))
		{
			goto IL_002d;
		}
	}
	{
		return;
	}
}
// System.Void MGR_HLGUI::readStates()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3588976330_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t MGR_HLGUI_readStates_m3798139293_MetadataUsageId;
extern "C"  void MGR_HLGUI_readStates_m3798139293 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_readStates_m3798139293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AnimationClip_t3510324950 * V_1 = NULL;
	AnimationClipU5BU5D_t3936083219* V_2 = NULL;
	int32_t V_3 = 0;
	AnimationClip_t3510324950 * V_4 = NULL;
	AnimationClipU5BU5D_t3936083219* V_5 = NULL;
	int32_t V_6 = 0;
	{
		V_0 = 0;
		Animator_t69676727 * L_0 = __this->get_myAnimator_17();
		NullCheck(L_0);
		RuntimeAnimatorController_t670468573 * L_1 = Animator_get_runtimeAnimatorController_m652575931(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationClipU5BU5D_t3936083219* L_2 = RuntimeAnimatorController_get_animationClips_m3074969690(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		V_3 = 0;
		goto IL_002a;
	}

IL_001c:
	{
		AnimationClipU5BU5D_t3936083219* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		AnimationClip_t3510324950 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_3;
		AnimationClipU5BU5D_t3936083219* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->set_clipNames_11(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		V_0 = 0;
		Animator_t69676727 * L_12 = __this->get_myAnimator_17();
		NullCheck(L_12);
		RuntimeAnimatorController_t670468573 * L_13 = Animator_get_runtimeAnimatorController_m652575931(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationClipU5BU5D_t3936083219* L_14 = RuntimeAnimatorController_get_animationClips_m3074969690(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		V_6 = 0;
		goto IL_00df;
	}

IL_005c:
	{
		AnimationClipU5BU5D_t3936083219* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		AnimationClip_t3510324950 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		StringU5BU5D_t1642385972* L_19 = __this->get_clipNames_11();
		int32_t L_20 = V_0;
		AnimationClip_t3510324950 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_23);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (String_t*)L_23);
		Dictionary_2_t3986656710 * L_24 = __this->get_DictOfAnims_12();
		StringU5BU5D_t1642385972* L_25 = __this->get_clipNames_11();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		bool L_29 = Dictionary_2_ContainsKey_m3588976330(L_24, L_28, /*hidden argument*/Dictionary_2_ContainsKey_m3588976330_MethodInfo_var);
		if (L_29)
		{
			goto IL_00d4;
		}
	}
	{
		AnimationClip_t3510324950 * L_30 = V_4;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_30);
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		StringU5BU5D_t1642385972* L_34 = __this->get_clipNames_11();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2000667605(NULL /*static, unused*/, L_33, _stringLiteral372029310, L_38, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		Dictionary_2_t3986656710 * L_40 = __this->get_DictOfAnims_12();
		StringU5BU5D_t1642385972* L_41 = __this->get_clipNames_11();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		String_t* L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		AnimationClip_t3510324950 * L_45 = V_4;
		NullCheck(L_45);
		int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_45);
		NullCheck(L_40);
		Dictionary_2_Add_m1209957957(L_40, L_44, L_46, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
	}

IL_00d4:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)((int32_t)L_47+(int32_t)1));
		int32_t L_48 = V_6;
		V_6 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_00df:
	{
		int32_t L_49 = V_6;
		AnimationClipU5BU5D_t3936083219* L_50 = V_5;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		return;
	}
}
// System.Void MGR_HLGUI::addDBEntry()
extern "C"  void MGR_HLGUI_addDBEntry_m3452731226 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	{
		InputField_t1631627530 * L_0 = __this->get_Input_Coach_2();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		__this->set_i_coach_8(L_1);
		InputField_t1631627530 * L_2 = __this->get_Input_Trainee_3();
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m409351770(L_2, /*hidden argument*/NULL);
		__this->set_i_trainee_9(L_3);
		InputField_t1631627530 * L_4 = __this->get_Input_Playlist_4();
		NullCheck(L_4);
		String_t* L_5 = InputField_get_text_m409351770(L_4, /*hidden argument*/NULL);
		__this->set_i_playlist_10(L_5);
		return;
	}
}
// System.Void MGR_HLGUI::buttonPressed()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral232122944;
extern Il2CppCodeGenString* _stringLiteral806467172;
extern const uint32_t MGR_HLGUI_buttonPressed_m1867206739_MetadataUsageId;
extern "C"  void MGR_HLGUI_buttonPressed_m1867206739 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HLGUI_buttonPressed_m1867206739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_13();
		NullCheck(L_0);
		Entry_t779635924 * L_1 = L_0->get_address_of_myEntry_2();
		int32_t L_2 = L_1->get_hSize_11();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral232122944, L_4, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		MGR_HLGUI_helpME_m62156382(__this, _stringLiteral806467172, /*hidden argument*/NULL);
		MGR_Anim_t3221826994 * L_6 = __this->get_myAnim_14();
		NullCheck(L_6);
		MGR_Anim_playAnims_m3338700699(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HLGUI::OnGUI()
extern "C"  void MGR_HLGUI_OnGUI_m4057632545 (MGR_HLGUI_t3333927834 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_HoloText::.ctor()
extern "C"  void MGR_HoloText__ctor_m419231409 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::Start()
extern const MethodInfo* Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1275371535;
extern Il2CppCodeGenString* _stringLiteral372029325;
extern Il2CppCodeGenString* _stringLiteral372029328;
extern Il2CppCodeGenString* _stringLiteral372029327;
extern const uint32_t MGR_HoloText_Start_m3634721657_MetadataUsageId;
extern "C"  void MGR_HoloText_Start_m3634721657 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HoloText_Start_m3634721657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t1641806576 * L_0 = Component_GetComponent_TisTextMesh_t1641806576_m4177416886(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t1641806576_m4177416886_MethodInfo_var);
		__this->set_playlistText_2(L_0);
		MGR_Cloud_t726529104 * L_1 = Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677(__this, /*hidden argument*/Component_GetComponent_TisMGR_Cloud_t726529104_m4012958677_MethodInfo_var);
		__this->set_myCloud_3(L_1);
		__this->set_updateText_4(_stringLiteral1275371535);
		MGR_Cloud_t726529104 * L_2 = __this->get_myCloud_3();
		NullCheck(L_2);
		MGR_Cloud_initFB_m761421013(L_2, _stringLiteral372029325, _stringLiteral372029328, _stringLiteral372029327, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m804483696_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m870713862_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4175023932_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t MGR_HoloText_OnGUI_m1016716883_MetadataUsageId;
extern "C"  void MGR_HoloText_OnGUI_m1016716883 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_HoloText_OnGUI_m1016716883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_t933071039  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MGR_Cloud_t726529104 * L_0 = __this->get_myCloud_3();
		NullCheck(L_0);
		Entry_t779635924 * L_1 = L_0->get_address_of_myEntry_2();
		String_t* L_2 = L_1->get_coach_0();
		__this->set_updateText_4(L_2);
		String_t* L_3 = __this->get_updateText_4();
		MGR_Cloud_t726529104 * L_4 = __this->get_myCloud_3();
		NullCheck(L_4);
		Entry_t779635924 * L_5 = L_4->get_address_of_myEntry_2();
		String_t* L_6 = L_5->get_trainee_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_6, /*hidden argument*/NULL);
		MGR_Cloud_t726529104 * L_8 = __this->get_myCloud_3();
		NullCheck(L_8);
		Entry_t779635924 * L_9 = L_8->get_address_of_myEntry_2();
		String_t* L_10 = L_9->get_playlistName_2();
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, L_3, L_7, L_11, /*hidden argument*/NULL);
		__this->set_updateText_4(L_12);
		MGR_Cloud_t726529104 * L_13 = __this->get_myCloud_3();
		NullCheck(L_13);
		Entry_t779635924 * L_14 = L_13->get_address_of_myEntry_2();
		List_1_t1398341365 * L_15 = L_14->get_currentPlaylist_3();
		NullCheck(L_15);
		Enumerator_t933071039  L_16 = List_1_GetEnumerator_m804483696(L_15, /*hidden argument*/List_1_GetEnumerator_m804483696_MethodInfo_var);
		V_1 = L_16;
	}

IL_0073:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009e;
		}

IL_0078:
		{
			String_t* L_17 = Enumerator_get_Current_m870713862((&V_1), /*hidden argument*/Enumerator_get_Current_m870713862_MethodInfo_var);
			V_0 = L_17;
			String_t* L_18 = __this->get_updateText_4();
			String_t* L_19 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_20 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029352, L_19, /*hidden argument*/NULL);
			String_t* L_21 = String_Concat_m2596409543(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
			__this->set_updateText_4(L_21);
		}

IL_009e:
		{
			bool L_22 = Enumerator_MoveNext_m4175023932((&V_1), /*hidden argument*/Enumerator_MoveNext_m4175023932_MethodInfo_var);
			if (L_22)
			{
				goto IL_0078;
			}
		}

IL_00aa:
		{
			IL2CPP_LEAVE(0xBD, FINALLY_00af);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00af;
	}

FINALLY_00af:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_1), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(175)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(175)
	{
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00bd:
	{
		TextMesh_t1641806576 * L_23 = __this->get_playlistText_2();
		String_t* L_24 = __this->get_updateText_4();
		NullCheck(L_23);
		TextMesh_set_text_m3390063817(L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_HoloText::Update()
extern "C"  void MGR_HoloText_Update_m2569076898 (MGR_HoloText_t1166120216 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MGR_Menus::.ctor()
extern "C"  void MGR_Menus__ctor_m4221427840 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Menus::Start()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var;
extern const uint32_t MGR_Menus_Start_m2318748400_MetadataUsageId;
extern "C"  void MGR_Menus_Start_m2318748400 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Menus_Start_m2318748400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_player_7();
		NullCheck(L_0);
		Renderer_t257310565 * L_1 = GameObject_GetComponent_TisRenderer_t257310565_m1312615893(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m1312615893_MethodInfo_var);
		bool L_2 = ((bool)0);
		Il2CppObject * L_3 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_1);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_1, L_3);
		GameObject_t1756533147 * L_4 = __this->get_startMenu_4();
		MGR_Menus_hideMenu_m2102574215(__this, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_createMenu_5();
		MGR_Menus_hideMenu_m2102574215(__this, L_5, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_readMenu_6();
		MGR_Menus_hideMenu_m2102574215(__this, L_6, /*hidden argument*/NULL);
		__this->set_currentstate_2(0);
		return;
	}
}
// System.Void MGR_Menus::hideMenu(UnityEngine.GameObject)
extern "C"  void MGR_Menus_hideMenu_m2102574215 (MGR_Menus_t511463637 * __this, GameObject_t1756533147 * ___G0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___G0;
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Menus::showMenu(UnityEngine.GameObject)
extern "C"  void MGR_Menus_showMenu_m2662510982 (MGR_Menus_t511463637 * __this, GameObject_t1756533147 * ___G0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___G0;
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Menus::Update()
extern "C"  void MGR_Menus_Update_m3885601141 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_currentstate_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0073;
		}
	}
	{
		goto IL_009c;
	}

IL_0021:
	{
		GameObject_t1756533147 * L_4 = __this->get_startMenu_4();
		MGR_Menus_showMenu_m2662510982(__this, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_createMenu_5();
		MGR_Menus_hideMenu_m2102574215(__this, L_5, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_readMenu_6();
		MGR_Menus_hideMenu_m2102574215(__this, L_6, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_004a:
	{
		GameObject_t1756533147 * L_7 = __this->get_createMenu_5();
		MGR_Menus_showMenu_m2662510982(__this, L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_startMenu_4();
		MGR_Menus_hideMenu_m2102574215(__this, L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_readMenu_6();
		MGR_Menus_hideMenu_m2102574215(__this, L_9, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_0073:
	{
		GameObject_t1756533147 * L_10 = __this->get_readMenu_6();
		MGR_Menus_showMenu_m2662510982(__this, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = __this->get_createMenu_5();
		MGR_Menus_hideMenu_m2102574215(__this, L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = __this->get_startMenu_4();
		MGR_Menus_hideMenu_m2102574215(__this, L_12, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_009c:
	{
		return;
	}
}
// System.Void MGR_Menus::StartMenu()
extern "C"  void MGR_Menus_StartMenu_m1676692927 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	{
		__this->set_currentstate_2(0);
		return;
	}
}
// System.Void MGR_Menus::CreateMenu()
extern "C"  void MGR_Menus_CreateMenu_m1027925027 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	{
		__this->set_currentstate_2(1);
		return;
	}
}
// System.Void MGR_Menus::ReadMenu()
extern "C"  void MGR_Menus_ReadMenu_m1846344601 (MGR_Menus_t511463637 * __this, const MethodInfo* method)
{
	{
		__this->set_currentstate_2(2);
		return;
	}
}
// System.Void MGR_Scroll::.ctor()
extern "C"  void MGR_Scroll__ctor_m3534779169 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method)
{
	{
		__this->set_scrollMaxBaba_46((0.0f));
		__this->set_scrollMaxKaroli_47((0.0f));
		__this->set_scrollMaxLorem_48((0.0f));
		__this->set_scrollMaxRoland_49((0.0f));
		__this->set_scrollMaxButtons_50((0.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Scroll::Awake()
extern "C"  void MGR_Scroll_Awake_m2905422574 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method)
{
	{
		__this->set_menuLevel_73(0);
		__this->set_deltaY_42((0.0f));
		__this->set_hazSkrollin_44((bool)0);
		return;
	}
}
// System.Void MGR_Scroll::Start()
extern "C"  void MGR_Scroll_Start_m2381332081 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method)
{
	{
		TextAsset_t3973159845 * L_0 = __this->get_introTextAsset_3();
		NullCheck(L_0);
		String_t* L_1 = TextAsset_get_text_m2589865997(L_0, /*hidden argument*/NULL);
		__this->set_introText_8(L_1);
		TextAsset_t3973159845 * L_2 = __this->get_babaTextAsset_4();
		NullCheck(L_2);
		String_t* L_3 = TextAsset_get_text_m2589865997(L_2, /*hidden argument*/NULL);
		__this->set_babaText_9(L_3);
		TextAsset_t3973159845 * L_4 = __this->get_karoliTextAsset_5();
		NullCheck(L_4);
		String_t* L_5 = TextAsset_get_text_m2589865997(L_4, /*hidden argument*/NULL);
		__this->set_karoliText_10(L_5);
		TextAsset_t3973159845 * L_6 = __this->get_loremTextAsset_6();
		NullCheck(L_6);
		String_t* L_7 = TextAsset_get_text_m2589865997(L_6, /*hidden argument*/NULL);
		__this->set_loremText_11(L_7);
		TextAsset_t3973159845 * L_8 = __this->get_rolandTextAsset_7();
		NullCheck(L_8);
		String_t* L_9 = TextAsset_get_text_m2589865997(L_8, /*hidden argument*/NULL);
		__this->set_rolandText_12(L_9);
		return;
	}
}
// System.Void MGR_Scroll::OnGUI()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* WindowFunction_t3486805455_il2cpp_TypeInfo_var;
extern const MethodInfo* MGR_Scroll_CreateWindow_m4154219716_MethodInfo_var;
extern const MethodInfo* MGR_Scroll_CreateGroupWindow_m394102123_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1057720270;
extern Il2CppCodeGenString* _stringLiteral1006879746;
extern Il2CppCodeGenString* _stringLiteral2435266816;
extern Il2CppCodeGenString* _stringLiteral2671768982;
extern Il2CppCodeGenString* _stringLiteral3138505722;
extern Il2CppCodeGenString* _stringLiteral2233079820;
extern Il2CppCodeGenString* _stringLiteral1757116141;
extern Il2CppCodeGenString* _stringLiteral3829823908;
extern Il2CppCodeGenString* _stringLiteral1281338459;
extern Il2CppCodeGenString* _stringLiteral2103682387;
extern Il2CppCodeGenString* _stringLiteral3161336182;
extern Il2CppCodeGenString* _stringLiteral634842547;
extern Il2CppCodeGenString* _stringLiteral3173230063;
extern Il2CppCodeGenString* _stringLiteral3889149889;
extern Il2CppCodeGenString* _stringLiteral2401532284;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral2963722944;
extern Il2CppCodeGenString* _stringLiteral2778558511;
extern Il2CppCodeGenString* _stringLiteral1383890222;
extern Il2CppCodeGenString* _stringLiteral4235102328;
extern Il2CppCodeGenString* _stringLiteral1982654410;
extern Il2CppCodeGenString* _stringLiteral3852433285;
extern Il2CppCodeGenString* _stringLiteral1371885191;
extern Il2CppCodeGenString* _stringLiteral3414426824;
extern Il2CppCodeGenString* _stringLiteral963635650;
extern Il2CppCodeGenString* _stringLiteral2504383212;
extern Il2CppCodeGenString* _stringLiteral490568597;
extern Il2CppCodeGenString* _stringLiteral2374011071;
extern const uint32_t MGR_Scroll_OnGUI_m2075454235_MetadataUsageId;
extern "C"  void MGR_Scroll_OnGUI_m2075454235 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Scroll_OnGUI_m2075454235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GUIStyle_t1799908754 * V_1 = NULL;
	{
		GUISkin_t1436893342 * L_0 = __this->get_gSkin_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		GUISkin_t1436893342 * L_2 = __this->get_gSkin_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_skin_m3391676555(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1057720270, /*hidden argument*/NULL);
	}

IL_002b:
	{
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469(&L_3, (0.0f), (0.0f), (320.0f), (480.0f), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_4 = __this->get_startBG_15();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_menuLevel_73();
		V_0 = L_5;
		int32_t L_6 = V_0;
		if (L_6 == 0)
		{
			goto IL_0079;
		}
		if (L_6 == 1)
		{
			goto IL_0379;
		}
		if (L_6 == 2)
		{
			goto IL_055f;
		}
		if (L_6 == 3)
		{
			goto IL_0765;
		}
		if (L_6 == 4)
		{
			goto IL_096f;
		}
		if (L_6 == 5)
		{
			goto IL_0b79;
		}
	}
	{
		goto IL_0d71;
	}

IL_0079:
	{
		int32_t L_7 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m1220545469(&L_8, ((float)((float)(((float)((float)L_7)))*(float)(0.05f))), (60.0f), (400.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_9 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2435266816, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_8, _stringLiteral1006879746, L_9, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Rect__ctor_m1220545469(&L_11, ((float)((float)(((float)((float)L_10)))*(float)(0.1f))), (80.0f), (400.0f), (100.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_12 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral3138505722, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_11, _stringLiteral2671768982, L_12, /*hidden argument*/NULL);
		Rect_t3681755626  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Rect__ctor_m1220545469(&L_13, (160.0f), (295.0f), (150.0f), (150.0f), /*hidden argument*/NULL);
		String_t* L_14 = __this->get_introText_8();
		GUI_Label_m2412846501(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Rect_t3681755626  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Rect__ctor_m1220545469(&L_15, (10.0f), (175.0f), (100.0f), (35.0f), /*hidden argument*/NULL);
		bool L_16 = GUI_Button_m3054448581(NULL /*static, unused*/, L_15, _stringLiteral2233079820, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0184;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_17 = __this->get_scrollMaxBaba_46();
		__this->set_scrollMax_39(L_17);
		float L_18 = __this->get_scrollMaxBaba_46();
		__this->set_scrollVector_37(L_18);
		float L_19 = __this->get_scrollMinBaba_51();
		__this->set_scrollMin_38(L_19);
		String_t* L_20 = __this->get_babaText_9();
		__this->set_windowText_13(L_20);
		Rect_t3681755626  L_21 = __this->get_babaWindowRect_57();
		__this->set_windowRect_63(L_21);
		__this->set_windowStyle_64(_stringLiteral1757116141);
		__this->set_menuLevel_73(1);
	}

IL_0184:
	{
		Rect_t3681755626  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Rect__ctor_m1220545469(&L_22, (10.0f), (230.0f), (100.0f), (35.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_23 = GUI_Button_m3054448581(NULL /*static, unused*/, L_22, _stringLiteral3829823908, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0203;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_24 = __this->get_scrollMaxKaroli_47();
		__this->set_scrollMax_39(L_24);
		float L_25 = __this->get_scrollMaxKaroli_47();
		__this->set_scrollVector_37(L_25);
		float L_26 = __this->get_scrollMinKaroli_52();
		__this->set_scrollMin_38(L_26);
		String_t* L_27 = __this->get_karoliText_10();
		__this->set_windowText_13(L_27);
		Rect_t3681755626  L_28 = __this->get_karoliWindowRect_58();
		__this->set_windowRect_63(L_28);
		__this->set_windowStyle_64(_stringLiteral1281338459);
		__this->set_menuLevel_73(2);
	}

IL_0203:
	{
		Rect_t3681755626  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Rect__ctor_m1220545469(&L_29, (10.0f), (285.0f), (100.0f), (35.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_30 = GUI_Button_m3054448581(NULL /*static, unused*/, L_29, _stringLiteral2103682387, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0282;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_31 = __this->get_scrollMaxLorem_48();
		__this->set_scrollMax_39(L_31);
		float L_32 = __this->get_scrollMaxLorem_48();
		__this->set_scrollVector_37(L_32);
		float L_33 = __this->get_scrollMinLorem_53();
		__this->set_scrollMin_38(L_33);
		String_t* L_34 = __this->get_loremText_11();
		__this->set_windowText_13(L_34);
		Rect_t3681755626  L_35 = __this->get_loremWindowRect_59();
		__this->set_windowRect_63(L_35);
		__this->set_windowStyle_64(_stringLiteral3161336182);
		__this->set_menuLevel_73(3);
	}

IL_0282:
	{
		Rect_t3681755626  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Rect__ctor_m1220545469(&L_36, (10.0f), (340.0f), (100.0f), (35.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_37 = GUI_Button_m3054448581(NULL /*static, unused*/, L_36, _stringLiteral634842547, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0301;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_38 = __this->get_scrollMaxRoland_49();
		__this->set_scrollMax_39(L_38);
		float L_39 = __this->get_scrollMaxRoland_49();
		__this->set_scrollVector_37(L_39);
		float L_40 = __this->get_scrollMinRoland_54();
		__this->set_scrollMin_38(L_40);
		String_t* L_41 = __this->get_rolandText_12();
		__this->set_windowText_13(L_41);
		Rect_t3681755626  L_42 = __this->get_rolandWindowRect_60();
		__this->set_windowRect_63(L_42);
		__this->set_windowStyle_64(_stringLiteral3173230063);
		__this->set_menuLevel_73(4);
	}

IL_0301:
	{
		Rect_t3681755626  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Rect__ctor_m1220545469(&L_43, (10.0f), (395.0f), (100.0f), (35.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_44 = GUI_Button_m3054448581(NULL /*static, unused*/, L_43, _stringLiteral3889149889, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0374;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)1);
		float L_45 = __this->get_scrollMaxButtons_50();
		__this->set_scrollMax_39(L_45);
		float L_46 = __this->get_scrollMaxButtons_50();
		__this->set_scrollVector_37(L_46);
		float L_47 = __this->get_scrollMinButtons_55();
		__this->set_scrollMin_38(L_47);
		Rect_t3681755626  L_48 = __this->get_buttonsWindowRect_61();
		__this->set_windowRect_63(L_48);
		__this->set_windowStyle_64(_stringLiteral2401532284);
		__this->set_menuLevel_73(5);
	}

IL_0374:
	{
		goto IL_0d71;
	}

IL_0379:
	{
		GUIStyle_t1799908754 * L_49 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_49, /*hidden argument*/NULL);
		V_1 = L_49;
		Rect_t3681755626  L_50 = __this->get_windowRect_63();
		IntPtr_t L_51;
		L_51.set_m_value_0((void*)(void*)MGR_Scroll_CreateWindow_m4154219716_MethodInfo_var);
		WindowFunction_t3486805455 * L_52 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_52, __this, L_51, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_53 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_50, L_52, _stringLiteral371857150, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_53);
		int32_t L_54 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Rect__ctor_m1220545469(&L_55, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_54/(int32_t)2))-(int32_t)((int32_t)100)))))), (0.0f), (200.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_56 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2963722944, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_55, _stringLiteral2233079820, L_56, /*hidden argument*/NULL);
		int32_t L_57 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Rect__ctor_m1220545469(&L_58, ((float)((float)((float)((float)(((float)((float)L_57)))*(float)(0.5f)))-(float)(70.0f))), (400.0f), (140.0f), (70.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_59 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral1383890222, /*hidden argument*/NULL);
		bool L_60 = GUI_Button_m2147724592(NULL /*static, unused*/, L_58, _stringLiteral2778558511, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0424;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_menuLevel_73(0);
	}

IL_0424:
	{
		bool L_61 = __this->get_izSkrollin_45();
		if (!L_61)
		{
			goto IL_055a;
		}
	}
	{
		GUIStyle_t1799908754 * L_62 = V_1;
		NullCheck(L_62);
		GUIStyleState_t3801000545 * L_63 = GUIStyle_get_normal_m2789468942(L_62, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_64 = __this->get_iconVector_14();
		NullCheck(L_63);
		GUIStyleState_set_background_m3931679679(L_63, L_64, /*hidden argument*/NULL);
		Rect_t3681755626 * L_65 = __this->get_address_of_windowRect_63();
		float L_66 = Rect_get_x_m1393582490(L_65, /*hidden argument*/NULL);
		Rect_t3681755626 * L_67 = __this->get_address_of_windowRect_63();
		float L_68 = Rect_get_width_m1138015702(L_67, /*hidden argument*/NULL);
		Rect_t3681755626 * L_69 = __this->get_address_of_windowRect_63();
		float L_70 = Rect_get_y_m1393582395(L_69, /*hidden argument*/NULL);
		float L_71 = __this->get_scrollVector_37();
		float L_72 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_73 = __this->get_address_of_windowRect_63();
		float L_74 = Rect_get_height_m3128694305(L_73, /*hidden argument*/NULL);
		Vector2_t2243707579  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Vector2__ctor_m3067419446(&L_75, ((float)((float)((float)((float)L_66+(float)L_68))-(float)(13.0f))), ((float)((float)L_70-(float)((float)((float)L_71/(float)((float)((float)L_72/(float)((float)((float)L_74-(float)(25.0f))))))))), /*hidden argument*/NULL);
		__this->set_newVectorPosition_72(L_75);
		Vector2_t2243707579 * L_76 = __this->get_address_of_newVectorPosition_72();
		float L_77 = L_76->get_y_1();
		float L_78 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_79 = __this->get_address_of_windowRect_63();
		float L_80 = Rect_get_y_m1393582395(L_79, /*hidden argument*/NULL);
		if ((!(((float)L_77) < ((float)((float)((float)L_78+(float)L_80))))))
		{
			goto IL_04d3;
		}
	}
	{
		Vector2_t2243707579 * L_81 = __this->get_address_of_newVectorPosition_72();
		float L_82 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_83 = __this->get_address_of_windowRect_63();
		float L_84 = Rect_get_y_m1393582395(L_83, /*hidden argument*/NULL);
		L_81->set_y_1(((float)((float)L_82+(float)L_84)));
	}

IL_04d3:
	{
		Vector2_t2243707579 * L_85 = __this->get_address_of_newVectorPosition_72();
		float L_86 = L_85->get_y_1();
		Rect_t3681755626 * L_87 = __this->get_address_of_windowRect_63();
		float L_88 = Rect_get_y_m1393582395(L_87, /*hidden argument*/NULL);
		Rect_t3681755626 * L_89 = __this->get_address_of_windowRect_63();
		float L_90 = Rect_get_height_m3128694305(L_89, /*hidden argument*/NULL);
		if ((!(((float)L_86) > ((float)((float)((float)((float)((float)L_88+(float)L_90))-(float)(25.0f)))))))
		{
			goto IL_0528;
		}
	}
	{
		Vector2_t2243707579 * L_91 = __this->get_address_of_newVectorPosition_72();
		Rect_t3681755626 * L_92 = __this->get_address_of_windowRect_63();
		float L_93 = Rect_get_y_m1393582395(L_92, /*hidden argument*/NULL);
		Rect_t3681755626 * L_94 = __this->get_address_of_windowRect_63();
		float L_95 = Rect_get_height_m3128694305(L_94, /*hidden argument*/NULL);
		L_91->set_y_1(((float)((float)((float)((float)L_93+(float)L_95))-(float)(25.0f))));
	}

IL_0528:
	{
		Vector2_t2243707579 * L_96 = __this->get_address_of_newVectorPosition_72();
		float L_97 = L_96->get_x_0();
		Vector2_t2243707579 * L_98 = __this->get_address_of_newVectorPosition_72();
		float L_99 = L_98->get_y_1();
		Rect_t3681755626  L_100;
		memset(&L_100, 0, sizeof(L_100));
		Rect__ctor_m1220545469(&L_100, L_97, L_99, (12.0f), (25.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_101 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_100, _stringLiteral371857150, L_101, /*hidden argument*/NULL);
	}

IL_055a:
	{
		goto IL_0d71;
	}

IL_055f:
	{
		GUIStyle_t1799908754 * L_102 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_102, /*hidden argument*/NULL);
		V_1 = L_102;
		Rect_t3681755626  L_103 = __this->get_windowRect_63();
		IntPtr_t L_104;
		L_104.set_m_value_0((void*)(void*)MGR_Scroll_CreateWindow_m4154219716_MethodInfo_var);
		WindowFunction_t3486805455 * L_105 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_105, __this, L_104, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_106 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_103, L_105, _stringLiteral371857150, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_106);
		int32_t L_107 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Rect__ctor_m1220545469(&L_108, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_107/(int32_t)2))-(int32_t)((int32_t)150)))))), (0.0f), (300.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_109 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral4235102328, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_108, _stringLiteral3829823908, L_109, /*hidden argument*/NULL);
		Rect_t3681755626  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Rect__ctor_m1220545469(&L_110, (175.0f), (110.0f), (200.0f), (275.0f), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_111 = __this->get_charlemagne_16();
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_110, L_111, /*hidden argument*/NULL);
		int32_t L_112 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_113;
		memset(&L_113, 0, sizeof(L_113));
		Rect__ctor_m1220545469(&L_113, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_112/(int32_t)2))-(int32_t)((int32_t)70)))))), (400.0f), (140.0f), (70.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_114 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral1982654410, /*hidden argument*/NULL);
		bool L_115 = GUI_Button_m2147724592(NULL /*static, unused*/, L_113, _stringLiteral2778558511, L_114, /*hidden argument*/NULL);
		if (!L_115)
		{
			goto IL_062a;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_menuLevel_73(0);
	}

IL_062a:
	{
		bool L_116 = __this->get_izSkrollin_45();
		if (!L_116)
		{
			goto IL_0760;
		}
	}
	{
		GUIStyle_t1799908754 * L_117 = V_1;
		NullCheck(L_117);
		GUIStyleState_t3801000545 * L_118 = GUIStyle_get_normal_m2789468942(L_117, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_119 = __this->get_iconVector_14();
		NullCheck(L_118);
		GUIStyleState_set_background_m3931679679(L_118, L_119, /*hidden argument*/NULL);
		Rect_t3681755626 * L_120 = __this->get_address_of_windowRect_63();
		float L_121 = Rect_get_x_m1393582490(L_120, /*hidden argument*/NULL);
		Rect_t3681755626 * L_122 = __this->get_address_of_windowRect_63();
		float L_123 = Rect_get_width_m1138015702(L_122, /*hidden argument*/NULL);
		Rect_t3681755626 * L_124 = __this->get_address_of_windowRect_63();
		float L_125 = Rect_get_y_m1393582395(L_124, /*hidden argument*/NULL);
		float L_126 = __this->get_scrollVector_37();
		float L_127 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_128 = __this->get_address_of_windowRect_63();
		float L_129 = Rect_get_height_m3128694305(L_128, /*hidden argument*/NULL);
		Vector2_t2243707579  L_130;
		memset(&L_130, 0, sizeof(L_130));
		Vector2__ctor_m3067419446(&L_130, ((float)((float)((float)((float)L_121+(float)L_123))-(float)(13.0f))), ((float)((float)L_125-(float)((float)((float)L_126/(float)((float)((float)L_127/(float)((float)((float)L_129-(float)(25.0f))))))))), /*hidden argument*/NULL);
		__this->set_newVectorPosition_72(L_130);
		Vector2_t2243707579 * L_131 = __this->get_address_of_newVectorPosition_72();
		float L_132 = L_131->get_y_1();
		float L_133 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_134 = __this->get_address_of_windowRect_63();
		float L_135 = Rect_get_y_m1393582395(L_134, /*hidden argument*/NULL);
		if ((!(((float)L_132) < ((float)((float)((float)L_133+(float)L_135))))))
		{
			goto IL_06d9;
		}
	}
	{
		Vector2_t2243707579 * L_136 = __this->get_address_of_newVectorPosition_72();
		float L_137 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_138 = __this->get_address_of_windowRect_63();
		float L_139 = Rect_get_y_m1393582395(L_138, /*hidden argument*/NULL);
		L_136->set_y_1(((float)((float)L_137+(float)L_139)));
	}

IL_06d9:
	{
		Vector2_t2243707579 * L_140 = __this->get_address_of_newVectorPosition_72();
		float L_141 = L_140->get_y_1();
		Rect_t3681755626 * L_142 = __this->get_address_of_windowRect_63();
		float L_143 = Rect_get_y_m1393582395(L_142, /*hidden argument*/NULL);
		Rect_t3681755626 * L_144 = __this->get_address_of_windowRect_63();
		float L_145 = Rect_get_height_m3128694305(L_144, /*hidden argument*/NULL);
		if ((!(((float)L_141) > ((float)((float)((float)((float)((float)L_143+(float)L_145))-(float)(25.0f)))))))
		{
			goto IL_072e;
		}
	}
	{
		Vector2_t2243707579 * L_146 = __this->get_address_of_newVectorPosition_72();
		Rect_t3681755626 * L_147 = __this->get_address_of_windowRect_63();
		float L_148 = Rect_get_y_m1393582395(L_147, /*hidden argument*/NULL);
		Rect_t3681755626 * L_149 = __this->get_address_of_windowRect_63();
		float L_150 = Rect_get_height_m3128694305(L_149, /*hidden argument*/NULL);
		L_146->set_y_1(((float)((float)((float)((float)L_148+(float)L_150))-(float)(25.0f))));
	}

IL_072e:
	{
		Vector2_t2243707579 * L_151 = __this->get_address_of_newVectorPosition_72();
		float L_152 = L_151->get_x_0();
		Vector2_t2243707579 * L_153 = __this->get_address_of_newVectorPosition_72();
		float L_154 = L_153->get_y_1();
		Rect_t3681755626  L_155;
		memset(&L_155, 0, sizeof(L_155));
		Rect__ctor_m1220545469(&L_155, L_152, L_154, (12.0f), (25.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_156 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_155, _stringLiteral371857150, L_156, /*hidden argument*/NULL);
	}

IL_0760:
	{
		goto IL_0d71;
	}

IL_0765:
	{
		GUIStyle_t1799908754 * L_157 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_157, /*hidden argument*/NULL);
		V_1 = L_157;
		Rect_t3681755626  L_158 = __this->get_windowRect_63();
		IntPtr_t L_159;
		L_159.set_m_value_0((void*)(void*)MGR_Scroll_CreateWindow_m4154219716_MethodInfo_var);
		WindowFunction_t3486805455 * L_160 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_160, __this, L_159, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_161 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_158, L_160, _stringLiteral371857150, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_161);
		int32_t L_162 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_163;
		memset(&L_163, 0, sizeof(L_163));
		Rect__ctor_m1220545469(&L_163, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_162/(int32_t)2))-(int32_t)((int32_t)100)))))), (0.0f), (200.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_164 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral3852433285, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_163, _stringLiteral2103682387, L_164, /*hidden argument*/NULL);
		Rect_t3681755626  L_165;
		memset(&L_165, 0, sizeof(L_165));
		Rect__ctor_m1220545469(&L_165, (10.0f), (315.0f), (300.0f), (75.0f), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_166 = __this->get_map_17();
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_165, L_166, /*hidden argument*/NULL);
		int32_t L_167 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_168;
		memset(&L_168, 0, sizeof(L_168));
		Rect__ctor_m1220545469(&L_168, ((float)((float)((float)((float)(((float)((float)L_167)))*(float)(0.5f)))-(float)(70.0f))), (400.0f), (140.0f), (70.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_169 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral1371885191, /*hidden argument*/NULL);
		bool L_170 = GUI_Button_m2147724592(NULL /*static, unused*/, L_168, _stringLiteral2778558511, L_169, /*hidden argument*/NULL);
		if (!L_170)
		{
			goto IL_0834;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_menuLevel_73(0);
	}

IL_0834:
	{
		bool L_171 = __this->get_izSkrollin_45();
		if (!L_171)
		{
			goto IL_096a;
		}
	}
	{
		GUIStyle_t1799908754 * L_172 = V_1;
		NullCheck(L_172);
		GUIStyleState_t3801000545 * L_173 = GUIStyle_get_normal_m2789468942(L_172, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_174 = __this->get_iconVector_14();
		NullCheck(L_173);
		GUIStyleState_set_background_m3931679679(L_173, L_174, /*hidden argument*/NULL);
		Rect_t3681755626 * L_175 = __this->get_address_of_windowRect_63();
		float L_176 = Rect_get_x_m1393582490(L_175, /*hidden argument*/NULL);
		Rect_t3681755626 * L_177 = __this->get_address_of_windowRect_63();
		float L_178 = Rect_get_width_m1138015702(L_177, /*hidden argument*/NULL);
		Rect_t3681755626 * L_179 = __this->get_address_of_windowRect_63();
		float L_180 = Rect_get_y_m1393582395(L_179, /*hidden argument*/NULL);
		float L_181 = __this->get_scrollVector_37();
		float L_182 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_183 = __this->get_address_of_windowRect_63();
		float L_184 = Rect_get_height_m3128694305(L_183, /*hidden argument*/NULL);
		Vector2_t2243707579  L_185;
		memset(&L_185, 0, sizeof(L_185));
		Vector2__ctor_m3067419446(&L_185, ((float)((float)((float)((float)L_176+(float)L_178))-(float)(13.0f))), ((float)((float)L_180-(float)((float)((float)L_181/(float)((float)((float)L_182/(float)((float)((float)L_184-(float)(25.0f))))))))), /*hidden argument*/NULL);
		__this->set_newVectorPosition_72(L_185);
		Vector2_t2243707579 * L_186 = __this->get_address_of_newVectorPosition_72();
		float L_187 = L_186->get_y_1();
		float L_188 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_189 = __this->get_address_of_windowRect_63();
		float L_190 = Rect_get_y_m1393582395(L_189, /*hidden argument*/NULL);
		if ((!(((float)L_187) < ((float)((float)((float)L_188+(float)L_190))))))
		{
			goto IL_08e3;
		}
	}
	{
		Vector2_t2243707579 * L_191 = __this->get_address_of_newVectorPosition_72();
		float L_192 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_193 = __this->get_address_of_windowRect_63();
		float L_194 = Rect_get_y_m1393582395(L_193, /*hidden argument*/NULL);
		L_191->set_y_1(((float)((float)L_192+(float)L_194)));
	}

IL_08e3:
	{
		Vector2_t2243707579 * L_195 = __this->get_address_of_newVectorPosition_72();
		float L_196 = L_195->get_y_1();
		Rect_t3681755626 * L_197 = __this->get_address_of_windowRect_63();
		float L_198 = Rect_get_y_m1393582395(L_197, /*hidden argument*/NULL);
		Rect_t3681755626 * L_199 = __this->get_address_of_windowRect_63();
		float L_200 = Rect_get_height_m3128694305(L_199, /*hidden argument*/NULL);
		if ((!(((float)L_196) > ((float)((float)((float)((float)((float)L_198+(float)L_200))-(float)(25.0f)))))))
		{
			goto IL_0938;
		}
	}
	{
		Vector2_t2243707579 * L_201 = __this->get_address_of_newVectorPosition_72();
		Rect_t3681755626 * L_202 = __this->get_address_of_windowRect_63();
		float L_203 = Rect_get_y_m1393582395(L_202, /*hidden argument*/NULL);
		Rect_t3681755626 * L_204 = __this->get_address_of_windowRect_63();
		float L_205 = Rect_get_height_m3128694305(L_204, /*hidden argument*/NULL);
		L_201->set_y_1(((float)((float)((float)((float)L_203+(float)L_205))-(float)(25.0f))));
	}

IL_0938:
	{
		Vector2_t2243707579 * L_206 = __this->get_address_of_newVectorPosition_72();
		float L_207 = L_206->get_x_0();
		Vector2_t2243707579 * L_208 = __this->get_address_of_newVectorPosition_72();
		float L_209 = L_208->get_y_1();
		Rect_t3681755626  L_210;
		memset(&L_210, 0, sizeof(L_210));
		Rect__ctor_m1220545469(&L_210, L_207, L_209, (12.0f), (25.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_211 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_210, _stringLiteral371857150, L_211, /*hidden argument*/NULL);
	}

IL_096a:
	{
		goto IL_0d71;
	}

IL_096f:
	{
		GUIStyle_t1799908754 * L_212 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_212, /*hidden argument*/NULL);
		V_1 = L_212;
		Rect_t3681755626  L_213 = __this->get_windowRect_63();
		IntPtr_t L_214;
		L_214.set_m_value_0((void*)(void*)MGR_Scroll_CreateWindow_m4154219716_MethodInfo_var);
		WindowFunction_t3486805455 * L_215 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_215, __this, L_214, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_216 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_213, L_215, _stringLiteral371857150, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_216);
		int32_t L_217 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_218;
		memset(&L_218, 0, sizeof(L_218));
		Rect__ctor_m1220545469(&L_218, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_217/(int32_t)2))-(int32_t)((int32_t)100)))))), (0.0f), (200.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_219 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral963635650, /*hidden argument*/NULL);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_218, _stringLiteral3414426824, L_219, /*hidden argument*/NULL);
		Rect_t3681755626  L_220;
		memset(&L_220, 0, sizeof(L_220));
		Rect__ctor_m1220545469(&L_220, (10.0f), (95.0f), (300.0f), (90.0f), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_221 = __this->get_page_18();
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_220, L_221, /*hidden argument*/NULL);
		int32_t L_222 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_223;
		memset(&L_223, 0, sizeof(L_223));
		Rect__ctor_m1220545469(&L_223, ((float)((float)((float)((float)(((float)((float)L_222)))*(float)(0.5f)))-(float)(70.0f))), (400.0f), (140.0f), (70.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_224 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2504383212, /*hidden argument*/NULL);
		bool L_225 = GUI_Button_m2147724592(NULL /*static, unused*/, L_223, _stringLiteral2778558511, L_224, /*hidden argument*/NULL);
		if (!L_225)
		{
			goto IL_0a3e;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_menuLevel_73(0);
	}

IL_0a3e:
	{
		bool L_226 = __this->get_izSkrollin_45();
		if (!L_226)
		{
			goto IL_0b74;
		}
	}
	{
		GUIStyle_t1799908754 * L_227 = V_1;
		NullCheck(L_227);
		GUIStyleState_t3801000545 * L_228 = GUIStyle_get_normal_m2789468942(L_227, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_229 = __this->get_iconVector_14();
		NullCheck(L_228);
		GUIStyleState_set_background_m3931679679(L_228, L_229, /*hidden argument*/NULL);
		Rect_t3681755626 * L_230 = __this->get_address_of_windowRect_63();
		float L_231 = Rect_get_x_m1393582490(L_230, /*hidden argument*/NULL);
		Rect_t3681755626 * L_232 = __this->get_address_of_windowRect_63();
		float L_233 = Rect_get_width_m1138015702(L_232, /*hidden argument*/NULL);
		Rect_t3681755626 * L_234 = __this->get_address_of_windowRect_63();
		float L_235 = Rect_get_y_m1393582395(L_234, /*hidden argument*/NULL);
		float L_236 = __this->get_scrollVector_37();
		float L_237 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_238 = __this->get_address_of_windowRect_63();
		float L_239 = Rect_get_height_m3128694305(L_238, /*hidden argument*/NULL);
		Vector2_t2243707579  L_240;
		memset(&L_240, 0, sizeof(L_240));
		Vector2__ctor_m3067419446(&L_240, ((float)((float)((float)((float)L_231+(float)L_233))-(float)(13.0f))), ((float)((float)L_235-(float)((float)((float)L_236/(float)((float)((float)L_237/(float)((float)((float)L_239-(float)(25.0f))))))))), /*hidden argument*/NULL);
		__this->set_newVectorPosition_72(L_240);
		Vector2_t2243707579 * L_241 = __this->get_address_of_newVectorPosition_72();
		float L_242 = L_241->get_y_1();
		float L_243 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_244 = __this->get_address_of_windowRect_63();
		float L_245 = Rect_get_y_m1393582395(L_244, /*hidden argument*/NULL);
		if ((!(((float)L_242) < ((float)((float)((float)L_243+(float)L_245))))))
		{
			goto IL_0aed;
		}
	}
	{
		Vector2_t2243707579 * L_246 = __this->get_address_of_newVectorPosition_72();
		float L_247 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_248 = __this->get_address_of_windowRect_63();
		float L_249 = Rect_get_y_m1393582395(L_248, /*hidden argument*/NULL);
		L_246->set_y_1(((float)((float)L_247+(float)L_249)));
	}

IL_0aed:
	{
		Vector2_t2243707579 * L_250 = __this->get_address_of_newVectorPosition_72();
		float L_251 = L_250->get_y_1();
		Rect_t3681755626 * L_252 = __this->get_address_of_windowRect_63();
		float L_253 = Rect_get_y_m1393582395(L_252, /*hidden argument*/NULL);
		Rect_t3681755626 * L_254 = __this->get_address_of_windowRect_63();
		float L_255 = Rect_get_height_m3128694305(L_254, /*hidden argument*/NULL);
		if ((!(((float)L_251) > ((float)((float)((float)((float)((float)L_253+(float)L_255))-(float)(25.0f)))))))
		{
			goto IL_0b42;
		}
	}
	{
		Vector2_t2243707579 * L_256 = __this->get_address_of_newVectorPosition_72();
		Rect_t3681755626 * L_257 = __this->get_address_of_windowRect_63();
		float L_258 = Rect_get_y_m1393582395(L_257, /*hidden argument*/NULL);
		Rect_t3681755626 * L_259 = __this->get_address_of_windowRect_63();
		float L_260 = Rect_get_height_m3128694305(L_259, /*hidden argument*/NULL);
		L_256->set_y_1(((float)((float)((float)((float)L_258+(float)L_260))-(float)(25.0f))));
	}

IL_0b42:
	{
		Vector2_t2243707579 * L_261 = __this->get_address_of_newVectorPosition_72();
		float L_262 = L_261->get_x_0();
		Vector2_t2243707579 * L_263 = __this->get_address_of_newVectorPosition_72();
		float L_264 = L_263->get_y_1();
		Rect_t3681755626  L_265;
		memset(&L_265, 0, sizeof(L_265));
		Rect__ctor_m1220545469(&L_265, L_262, L_264, (12.0f), (25.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_266 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_265, _stringLiteral371857150, L_266, /*hidden argument*/NULL);
	}

IL_0b74:
	{
		goto IL_0d71;
	}

IL_0b79:
	{
		GUIStyle_t1799908754 * L_267 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_267, /*hidden argument*/NULL);
		V_1 = L_267;
		int32_t L_268 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_269;
		memset(&L_269, 0, sizeof(L_269));
		Rect__ctor_m1220545469(&L_269, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_268/(int32_t)2))-(int32_t)((int32_t)100)))))), (25.0f), (200.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_270 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral490568597, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_269, _stringLiteral3889149889, L_270, /*hidden argument*/NULL);
		Rect_t3681755626  L_271 = __this->get_windowRect_63();
		IntPtr_t L_272;
		L_272.set_m_value_0((void*)(void*)MGR_Scroll_CreateGroupWindow_m394102123_MethodInfo_var);
		WindowFunction_t3486805455 * L_273 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_273, __this, L_272, /*hidden argument*/NULL);
		Rect_t3681755626  L_274 = GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_271, L_273, _stringLiteral371857150, /*hidden argument*/NULL);
		__this->set_createdWindow_62(L_274);
		int32_t L_275 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_276 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_277;
		memset(&L_277, 0, sizeof(L_277));
		Rect__ctor_m1220545469(&L_277, ((float)((float)((float)((float)(((float)((float)L_275)))*(float)(0.5f)))-(float)(70.0f))), (((float)((float)((int32_t)((int32_t)L_276-(int32_t)((int32_t)80)))))), (140.0f), (70.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_278 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2374011071, /*hidden argument*/NULL);
		bool L_279 = GUI_Button_m2147724592(NULL /*static, unused*/, L_277, _stringLiteral2778558511, L_278, /*hidden argument*/NULL);
		if (!L_279)
		{
			goto IL_0c36;
		}
	}
	{
		__this->set_hazSkrollin_44((bool)0);
		__this->set_hasResetHS_70((bool)0);
		__this->set_resetHSCheck_69((bool)0);
		__this->set_menuLevel_73(0);
	}

IL_0c36:
	{
		bool L_280 = __this->get_izSkrollin_45();
		if (!L_280)
		{
			goto IL_0d6c;
		}
	}
	{
		GUIStyle_t1799908754 * L_281 = V_1;
		NullCheck(L_281);
		GUIStyleState_t3801000545 * L_282 = GUIStyle_get_normal_m2789468942(L_281, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_283 = __this->get_iconVector_14();
		NullCheck(L_282);
		GUIStyleState_set_background_m3931679679(L_282, L_283, /*hidden argument*/NULL);
		Rect_t3681755626 * L_284 = __this->get_address_of_windowRect_63();
		float L_285 = Rect_get_x_m1393582490(L_284, /*hidden argument*/NULL);
		Rect_t3681755626 * L_286 = __this->get_address_of_windowRect_63();
		float L_287 = Rect_get_width_m1138015702(L_286, /*hidden argument*/NULL);
		Rect_t3681755626 * L_288 = __this->get_address_of_windowRect_63();
		float L_289 = Rect_get_y_m1393582395(L_288, /*hidden argument*/NULL);
		float L_290 = __this->get_scrollVector_37();
		float L_291 = __this->get_scrollMin_38();
		Rect_t3681755626 * L_292 = __this->get_address_of_windowRect_63();
		float L_293 = Rect_get_height_m3128694305(L_292, /*hidden argument*/NULL);
		Vector2_t2243707579  L_294;
		memset(&L_294, 0, sizeof(L_294));
		Vector2__ctor_m3067419446(&L_294, ((float)((float)((float)((float)L_285+(float)L_287))-(float)(13.0f))), ((float)((float)L_289-(float)((float)((float)L_290/(float)((float)((float)L_291/(float)((float)((float)L_293-(float)(25.0f))))))))), /*hidden argument*/NULL);
		__this->set_newVectorPosition_72(L_294);
		Vector2_t2243707579 * L_295 = __this->get_address_of_newVectorPosition_72();
		float L_296 = L_295->get_y_1();
		float L_297 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_298 = __this->get_address_of_windowRect_63();
		float L_299 = Rect_get_y_m1393582395(L_298, /*hidden argument*/NULL);
		if ((!(((float)L_296) < ((float)((float)((float)L_297+(float)L_299))))))
		{
			goto IL_0ce5;
		}
	}
	{
		Vector2_t2243707579 * L_300 = __this->get_address_of_newVectorPosition_72();
		float L_301 = __this->get_scrollMax_39();
		Rect_t3681755626 * L_302 = __this->get_address_of_windowRect_63();
		float L_303 = Rect_get_y_m1393582395(L_302, /*hidden argument*/NULL);
		L_300->set_y_1(((float)((float)L_301+(float)L_303)));
	}

IL_0ce5:
	{
		Vector2_t2243707579 * L_304 = __this->get_address_of_newVectorPosition_72();
		float L_305 = L_304->get_y_1();
		Rect_t3681755626 * L_306 = __this->get_address_of_windowRect_63();
		float L_307 = Rect_get_y_m1393582395(L_306, /*hidden argument*/NULL);
		Rect_t3681755626 * L_308 = __this->get_address_of_windowRect_63();
		float L_309 = Rect_get_height_m3128694305(L_308, /*hidden argument*/NULL);
		if ((!(((float)L_305) > ((float)((float)((float)((float)((float)L_307+(float)L_309))-(float)(25.0f)))))))
		{
			goto IL_0d3a;
		}
	}
	{
		Vector2_t2243707579 * L_310 = __this->get_address_of_newVectorPosition_72();
		Rect_t3681755626 * L_311 = __this->get_address_of_windowRect_63();
		float L_312 = Rect_get_y_m1393582395(L_311, /*hidden argument*/NULL);
		Rect_t3681755626 * L_313 = __this->get_address_of_windowRect_63();
		float L_314 = Rect_get_height_m3128694305(L_313, /*hidden argument*/NULL);
		L_310->set_y_1(((float)((float)((float)((float)L_312+(float)L_314))-(float)(25.0f))));
	}

IL_0d3a:
	{
		Vector2_t2243707579 * L_315 = __this->get_address_of_newVectorPosition_72();
		float L_316 = L_315->get_x_0();
		Vector2_t2243707579 * L_317 = __this->get_address_of_newVectorPosition_72();
		float L_318 = L_317->get_y_1();
		Rect_t3681755626  L_319;
		memset(&L_319, 0, sizeof(L_319));
		Rect__ctor_m1220545469(&L_319, L_316, L_318, (12.0f), (25.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_320 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Button_m2147724592(NULL /*static, unused*/, L_319, _stringLiteral371857150, L_320, /*hidden argument*/NULL);
	}

IL_0d6c:
	{
		goto IL_0d71;
	}

IL_0d71:
	{
		return;
	}
}
// System.Void MGR_Scroll::CreateWindow(System.Int32)
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Scroll_CreateWindow_m4154219716_MetadataUsageId;
extern "C"  void MGR_Scroll_CreateWindow_m4154219716 (MGR_Scroll_t3416609842 * __this, int32_t ___windowID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Scroll_CreateWindow_m4154219716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_windowPad_56();
		float L_1 = __this->get_scrollVector_37();
		Rect_t3681755626 * L_2 = __this->get_address_of_windowRect_63();
		float L_3 = Rect_get_width_m1138015702(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_windowPad_56();
		float L_5 = __this->get_scrollMin_38();
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, L_0, L_1, ((float)((float)L_3-(float)((float)((float)L_4*(float)(2.0f))))), L_5, /*hidden argument*/NULL);
		String_t* L_7 = __this->get_windowText_13();
		String_t* L_8 = __this->get_windowStyle_64();
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_9 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_6, L_7, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Scroll::CreateGroupWindow(System.Int32)
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1412472851;
extern Il2CppCodeGenString* _stringLiteral2401532284;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral3387269145;
extern Il2CppCodeGenString* _stringLiteral3021629903;
extern Il2CppCodeGenString* _stringLiteral3759753276;
extern Il2CppCodeGenString* _stringLiteral1496915101;
extern Il2CppCodeGenString* _stringLiteral2939335172;
extern Il2CppCodeGenString* _stringLiteral3387431150;
extern Il2CppCodeGenString* _stringLiteral991689925;
extern Il2CppCodeGenString* _stringLiteral669848589;
extern Il2CppCodeGenString* _stringLiteral3929823022;
extern Il2CppCodeGenString* _stringLiteral2742042351;
extern Il2CppCodeGenString* _stringLiteral2629128627;
extern Il2CppCodeGenString* _stringLiteral3177285002;
extern Il2CppCodeGenString* _stringLiteral1649436034;
extern Il2CppCodeGenString* _stringLiteral4139802140;
extern Il2CppCodeGenString* _stringLiteral4203522109;
extern Il2CppCodeGenString* _stringLiteral868933909;
extern Il2CppCodeGenString* _stringLiteral1410940430;
extern Il2CppCodeGenString* _stringLiteral997113313;
extern Il2CppCodeGenString* _stringLiteral3778989995;
extern Il2CppCodeGenString* _stringLiteral505509294;
extern const uint32_t MGR_Scroll_CreateGroupWindow_m394102123_MetadataUsageId;
extern "C"  void MGR_Scroll_CreateGroupWindow_m394102123 (MGR_Scroll_t3416609842 * __this, int32_t ___windowID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Scroll_CreateGroupWindow_m394102123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUIStyle_t1799908754 * V_0 = NULL;
	{
		float L_0 = __this->get_windowPad_56();
		float L_1 = __this->get_scrollVector_37();
		Rect_t3681755626 * L_2 = __this->get_address_of_windowRect_63();
		float L_3 = Rect_get_width_m1138015702(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_windowPad_56();
		Rect_t3681755626 * L_5 = __this->get_address_of_windowRect_63();
		float L_6 = Rect_get_height_m3128694305(L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_scrollMin_38();
		Rect_t3681755626  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m1220545469(&L_8, L_0, L_1, ((float)((float)L_3-(float)((float)((float)L_4*(float)(2.0f))))), ((float)((float)L_6+(float)L_7)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_9 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_9, /*hidden argument*/NULL);
		V_0 = L_9;
		bool L_10 = __this->get_audioOff_65();
		if (L_10)
		{
			goto IL_00d3;
		}
	}
	{
		GUIStyle_t1799908754 * L_11 = V_0;
		NullCheck(L_11);
		GUIStyleState_t3801000545 * L_12 = GUIStyle_get_normal_m2789468942(L_11, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_13 = __this->get_iconAudio_19();
		NullCheck(L_12);
		GUIStyleState_set_background_m3931679679(L_12, L_13, /*hidden argument*/NULL);
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, (55.0f), (15.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_15 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_14, _stringLiteral1412472851, L_15, /*hidden argument*/NULL);
		Rect_t3681755626  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m1220545469(&L_16, (0.0f), (20.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_17 = V_0;
		bool L_18 = GUI_Button_m2147724592(NULL /*static, unused*/, L_16, _stringLiteral371857150, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00d2;
		}
	}
	{
		__this->set_audioOff_65((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3387269145, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_00d2:
	{
	}

IL_00d3:
	{
		bool L_19 = __this->get_audioOff_65();
		if (!L_19)
		{
			goto IL_015f;
		}
	}
	{
		GUIStyle_t1799908754 * L_20 = V_0;
		NullCheck(L_20);
		GUIStyleState_t3801000545 * L_21 = GUIStyle_get_normal_m2789468942(L_20, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_22 = __this->get_iconAudioNo_20();
		NullCheck(L_21);
		GUIStyleState_set_background_m3931679679(L_21, L_22, /*hidden argument*/NULL);
		Rect_t3681755626  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Rect__ctor_m1220545469(&L_23, (55.0f), (15.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_24 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_23, _stringLiteral3759753276, L_24, /*hidden argument*/NULL);
		Rect_t3681755626  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Rect__ctor_m1220545469(&L_25, (0.0f), (20.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_26 = V_0;
		bool L_27 = GUI_Button_m2147724592(NULL /*static, unused*/, L_25, _stringLiteral371857150, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_015e;
		}
	}
	{
		__this->set_audioOff_65((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3387269145, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_015e:
	{
	}

IL_015f:
	{
		bool L_28 = __this->get_musicOff_66();
		if (L_28)
		{
			goto IL_01eb;
		}
	}
	{
		GUIStyle_t1799908754 * L_29 = V_0;
		NullCheck(L_29);
		GUIStyleState_t3801000545 * L_30 = GUIStyle_get_normal_m2789468942(L_29, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_31 = __this->get_iconMusic_21();
		NullCheck(L_30);
		GUIStyleState_set_background_m3931679679(L_30, L_31, /*hidden argument*/NULL);
		Rect_t3681755626  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Rect__ctor_m1220545469(&L_32, (55.0f), (70.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_33 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_32, _stringLiteral2939335172, L_33, /*hidden argument*/NULL);
		Rect_t3681755626  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Rect__ctor_m1220545469(&L_34, (0.0f), (75.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_35 = V_0;
		bool L_36 = GUI_Button_m2147724592(NULL /*static, unused*/, L_34, _stringLiteral371857150, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01ea;
		}
	}
	{
		__this->set_musicOff_66((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3387431150, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_01ea:
	{
	}

IL_01eb:
	{
		bool L_37 = __this->get_musicOff_66();
		if (!L_37)
		{
			goto IL_0277;
		}
	}
	{
		GUIStyle_t1799908754 * L_38 = V_0;
		NullCheck(L_38);
		GUIStyleState_t3801000545 * L_39 = GUIStyle_get_normal_m2789468942(L_38, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_40 = __this->get_iconMusicNo_22();
		NullCheck(L_39);
		GUIStyleState_set_background_m3931679679(L_39, L_40, /*hidden argument*/NULL);
		Rect_t3681755626  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Rect__ctor_m1220545469(&L_41, (55.0f), (70.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_42 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_41, _stringLiteral991689925, L_42, /*hidden argument*/NULL);
		Rect_t3681755626  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Rect__ctor_m1220545469(&L_43, (0.0f), (75.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_44 = V_0;
		bool L_45 = GUI_Button_m2147724592(NULL /*static, unused*/, L_43, _stringLiteral371857150, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0276;
		}
	}
	{
		__this->set_musicOff_66((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3387431150, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_0276:
	{
	}

IL_0277:
	{
		bool L_46 = __this->get_hideInfo_71();
		if (L_46)
		{
			goto IL_0303;
		}
	}
	{
		GUIStyle_t1799908754 * L_47 = V_0;
		NullCheck(L_47);
		GUIStyleState_t3801000545 * L_48 = GUIStyle_get_normal_m2789468942(L_47, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_49 = __this->get_iconInfo_31();
		NullCheck(L_48);
		GUIStyleState_set_background_m3931679679(L_48, L_49, /*hidden argument*/NULL);
		Rect_t3681755626  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Rect__ctor_m1220545469(&L_50, (55.0f), (130.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_51 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_50, _stringLiteral669848589, L_51, /*hidden argument*/NULL);
		Rect_t3681755626  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Rect__ctor_m1220545469(&L_52, (0.0f), (135.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_53 = V_0;
		bool L_54 = GUI_Button_m2147724592(NULL /*static, unused*/, L_52, _stringLiteral371857150, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_0302;
		}
	}
	{
		__this->set_hideInfo_71((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3929823022, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_0302:
	{
	}

IL_0303:
	{
		bool L_55 = __this->get_hideInfo_71();
		if (!L_55)
		{
			goto IL_038f;
		}
	}
	{
		GUIStyle_t1799908754 * L_56 = V_0;
		NullCheck(L_56);
		GUIStyleState_t3801000545 * L_57 = GUIStyle_get_normal_m2789468942(L_56, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_58 = __this->get_iconInfoNo_32();
		NullCheck(L_57);
		GUIStyleState_set_background_m3931679679(L_57, L_58, /*hidden argument*/NULL);
		Rect_t3681755626  L_59;
		memset(&L_59, 0, sizeof(L_59));
		Rect__ctor_m1220545469(&L_59, (55.0f), (130.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_60 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_59, _stringLiteral2742042351, L_60, /*hidden argument*/NULL);
		Rect_t3681755626  L_61;
		memset(&L_61, 0, sizeof(L_61));
		Rect__ctor_m1220545469(&L_61, (0.0f), (135.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_62 = V_0;
		bool L_63 = GUI_Button_m2147724592(NULL /*static, unused*/, L_61, _stringLiteral371857150, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_038e;
		}
	}
	{
		__this->set_hideInfo_71((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3929823022, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_038e:
	{
	}

IL_038f:
	{
		bool L_64 = __this->get_thrustReversed_67();
		if (L_64)
		{
			goto IL_041b;
		}
	}
	{
		GUIStyle_t1799908754 * L_65 = V_0;
		NullCheck(L_65);
		GUIStyleState_t3801000545 * L_66 = GUIStyle_get_normal_m2789468942(L_65, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_67 = __this->get_iconThrustNormal_27();
		NullCheck(L_66);
		GUIStyleState_set_background_m3931679679(L_66, L_67, /*hidden argument*/NULL);
		Rect_t3681755626  L_68;
		memset(&L_68, 0, sizeof(L_68));
		Rect__ctor_m1220545469(&L_68, (55.0f), (185.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_69 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_68, _stringLiteral2629128627, L_69, /*hidden argument*/NULL);
		Rect_t3681755626  L_70;
		memset(&L_70, 0, sizeof(L_70));
		Rect__ctor_m1220545469(&L_70, (0.0f), (190.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_71 = V_0;
		bool L_72 = GUI_Button_m2147724592(NULL /*static, unused*/, L_70, _stringLiteral371857150, L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_041a;
		}
	}
	{
		__this->set_thrustReversed_67((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3177285002, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_041a:
	{
	}

IL_041b:
	{
		bool L_73 = __this->get_thrustReversed_67();
		if (!L_73)
		{
			goto IL_04a7;
		}
	}
	{
		GUIStyle_t1799908754 * L_74 = V_0;
		NullCheck(L_74);
		GUIStyleState_t3801000545 * L_75 = GUIStyle_get_normal_m2789468942(L_74, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_76 = __this->get_iconThrustReversed_28();
		NullCheck(L_75);
		GUIStyleState_set_background_m3931679679(L_75, L_76, /*hidden argument*/NULL);
		Rect_t3681755626  L_77;
		memset(&L_77, 0, sizeof(L_77));
		Rect__ctor_m1220545469(&L_77, (55.0f), (185.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_78 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_77, _stringLiteral1649436034, L_78, /*hidden argument*/NULL);
		Rect_t3681755626  L_79;
		memset(&L_79, 0, sizeof(L_79));
		Rect__ctor_m1220545469(&L_79, (0.0f), (190.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_80 = V_0;
		bool L_81 = GUI_Button_m2147724592(NULL /*static, unused*/, L_79, _stringLiteral371857150, L_80, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_04a6;
		}
	}
	{
		__this->set_thrustReversed_67((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3177285002, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_04a6:
	{
	}

IL_04a7:
	{
		bool L_82 = __this->get_turnReversed_68();
		if (L_82)
		{
			goto IL_0533;
		}
	}
	{
		GUIStyle_t1799908754 * L_83 = V_0;
		NullCheck(L_83);
		GUIStyleState_t3801000545 * L_84 = GUIStyle_get_normal_m2789468942(L_83, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_85 = __this->get_iconTurnNormal_29();
		NullCheck(L_84);
		GUIStyleState_set_background_m3931679679(L_84, L_85, /*hidden argument*/NULL);
		Rect_t3681755626  L_86;
		memset(&L_86, 0, sizeof(L_86));
		Rect__ctor_m1220545469(&L_86, (55.0f), (240.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_87 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_86, _stringLiteral4139802140, L_87, /*hidden argument*/NULL);
		Rect_t3681755626  L_88;
		memset(&L_88, 0, sizeof(L_88));
		Rect__ctor_m1220545469(&L_88, (0.0f), (245.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_89 = V_0;
		bool L_90 = GUI_Button_m2147724592(NULL /*static, unused*/, L_88, _stringLiteral371857150, L_89, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_0532;
		}
	}
	{
		__this->set_turnReversed_68((bool)1);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral4203522109, _stringLiteral3021629903, /*hidden argument*/NULL);
	}

IL_0532:
	{
	}

IL_0533:
	{
		bool L_91 = __this->get_turnReversed_68();
		if (!L_91)
		{
			goto IL_05bf;
		}
	}
	{
		GUIStyle_t1799908754 * L_92 = V_0;
		NullCheck(L_92);
		GUIStyleState_t3801000545 * L_93 = GUIStyle_get_normal_m2789468942(L_92, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_94 = __this->get_iconTurnReversed_30();
		NullCheck(L_93);
		GUIStyleState_set_background_m3931679679(L_93, L_94, /*hidden argument*/NULL);
		Rect_t3681755626  L_95;
		memset(&L_95, 0, sizeof(L_95));
		Rect__ctor_m1220545469(&L_95, (55.0f), (240.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_96 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_95, _stringLiteral868933909, L_96, /*hidden argument*/NULL);
		Rect_t3681755626  L_97;
		memset(&L_97, 0, sizeof(L_97));
		Rect__ctor_m1220545469(&L_97, (0.0f), (245.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_98 = V_0;
		bool L_99 = GUI_Button_m2147724592(NULL /*static, unused*/, L_97, _stringLiteral371857150, L_98, /*hidden argument*/NULL);
		if (!L_99)
		{
			goto IL_05be;
		}
	}
	{
		__this->set_turnReversed_68((bool)0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral4203522109, _stringLiteral1496915101, /*hidden argument*/NULL);
	}

IL_05be:
	{
	}

IL_05bf:
	{
		bool L_100 = __this->get_hasResetHS_70();
		if (L_100)
		{
			goto IL_0710;
		}
	}
	{
		bool L_101 = __this->get_resetHSCheck_69();
		if (L_101)
		{
			goto IL_0648;
		}
	}
	{
		GUIStyle_t1799908754 * L_102 = V_0;
		NullCheck(L_102);
		GUIStyleState_t3801000545 * L_103 = GUIStyle_get_normal_m2789468942(L_102, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_104 = __this->get_iconResetHS_23();
		NullCheck(L_103);
		GUIStyleState_set_background_m3931679679(L_103, L_104, /*hidden argument*/NULL);
		Rect_t3681755626  L_105;
		memset(&L_105, 0, sizeof(L_105));
		Rect__ctor_m1220545469(&L_105, (55.0f), (295.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_106 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_105, _stringLiteral1410940430, L_106, /*hidden argument*/NULL);
		Rect_t3681755626  L_107;
		memset(&L_107, 0, sizeof(L_107));
		Rect__ctor_m1220545469(&L_107, (0.0f), (300.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_108 = V_0;
		bool L_109 = GUI_Button_m2147724592(NULL /*static, unused*/, L_107, _stringLiteral371857150, L_108, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_0647;
		}
	}
	{
		__this->set_resetHSCheck_69((bool)1);
	}

IL_0647:
	{
	}

IL_0648:
	{
		bool L_110 = __this->get_resetHSCheck_69();
		if (!L_110)
		{
			goto IL_070f;
		}
	}
	{
		GUIStyle_t1799908754 * L_111 = V_0;
		NullCheck(L_111);
		GUIStyleState_t3801000545 * L_112 = GUIStyle_get_normal_m2789468942(L_111, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_113 = __this->get_iconResetHSWarn_25();
		NullCheck(L_112);
		GUIStyleState_set_background_m3931679679(L_112, L_113, /*hidden argument*/NULL);
		Rect_t3681755626  L_114;
		memset(&L_114, 0, sizeof(L_114));
		Rect__ctor_m1220545469(&L_114, (0.0f), (296.0f), (265.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_115 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral3778989995, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_114, _stringLiteral997113313, L_115, /*hidden argument*/NULL);
		Rect_t3681755626  L_116;
		memset(&L_116, 0, sizeof(L_116));
		Rect__ctor_m1220545469(&L_116, (0.0f), (300.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_117 = V_0;
		bool L_118 = GUI_Button_m2147724592(NULL /*static, unused*/, L_116, _stringLiteral371857150, L_117, /*hidden argument*/NULL);
		if (!L_118)
		{
			goto IL_06c4;
		}
	}
	{
		__this->set_resetHSCheck_69((bool)0);
	}

IL_06c4:
	{
		GUIStyle_t1799908754 * L_119 = V_0;
		NullCheck(L_119);
		GUIStyleState_t3801000545 * L_120 = GUIStyle_get_normal_m2789468942(L_119, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_121 = __this->get_iconResetHSDoneWarn_26();
		NullCheck(L_120);
		GUIStyleState_set_background_m3931679679(L_120, L_121, /*hidden argument*/NULL);
		Rect_t3681755626  L_122;
		memset(&L_122, 0, sizeof(L_122));
		Rect__ctor_m1220545469(&L_122, (220.0f), (300.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_123 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_124 = GUI_Button_m2147724592(NULL /*static, unused*/, L_122, _stringLiteral371857150, L_123, /*hidden argument*/NULL);
		if (!L_124)
		{
			goto IL_070e;
		}
	}
	{
		__this->set_resetHSCheck_69((bool)0);
		__this->set_hasResetHS_70((bool)1);
	}

IL_070e:
	{
	}

IL_070f:
	{
	}

IL_0710:
	{
		bool L_125 = __this->get_hasResetHS_70();
		if (!L_125)
		{
			goto IL_078d;
		}
	}
	{
		GUIStyle_t1799908754 * L_126 = V_0;
		NullCheck(L_126);
		GUIStyleState_t3801000545 * L_127 = GUIStyle_get_normal_m2789468942(L_126, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_128 = __this->get_iconResetHSDone_24();
		NullCheck(L_127);
		GUIStyleState_set_background_m3931679679(L_127, L_128, /*hidden argument*/NULL);
		Rect_t3681755626  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Rect__ctor_m1220545469(&L_129, (55.0f), (295.0f), (250.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_130 = GUIStyle_op_Implicit_m781448948(NULL /*static, unused*/, _stringLiteral2401532284, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_129, _stringLiteral505509294, L_130, /*hidden argument*/NULL);
		Rect_t3681755626  L_131;
		memset(&L_131, 0, sizeof(L_131));
		Rect__ctor_m1220545469(&L_131, (0.0f), (300.0f), (50.0f), (50.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_132 = V_0;
		bool L_133 = GUI_Button_m2147724592(NULL /*static, unused*/, L_131, _stringLiteral371857150, L_132, /*hidden argument*/NULL);
		if (!L_133)
		{
			goto IL_078c;
		}
	}
	{
		__this->set_resetHSCheck_69((bool)1);
	}

IL_078c:
	{
	}

IL_078d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MGR_Scroll::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t MGR_Scroll_Update_m851729424_MetadataUsageId;
extern "C"  void MGR_Scroll_Update_m851729424 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MGR_Scroll_Update_m851729424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Touch_t407273883  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_hazSkrollin_44();
		if (!L_0)
		{
			goto IL_0229;
		}
	}
	{
		float L_1 = __this->get_scrollVector_37();
		V_0 = L_1;
		__this->set_resistance_40((1.0f));
		__this->set_marginPressure_41((0.0f));
		float L_2 = __this->get_scrollVector_37();
		float L_3 = __this->get_scrollMax_39();
		if ((!(((float)L_2) >= ((float)L_3))))
		{
			goto IL_0090;
		}
	}
	{
		float L_4 = __this->get_scrollVector_37();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = fabsf(L_4);
		float L_6 = __this->get_scrollMax_39();
		float L_7 = __this->get_scrollMargin_36();
		float L_8 = fabsf(((float)((float)((float)((float)L_5-(float)L_6))/(float)L_7)));
		float L_9 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (1.0f), (0.0f), L_8, /*hidden argument*/NULL);
		__this->set_resistance_40(L_9);
		float L_10 = __this->get_resistance_40();
		float L_11 = __this->get_marginSpringiness_33();
		float L_12 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_marginPressure_41(((-((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_10))*(float)L_11))*(float)L_12)))));
	}

IL_0090:
	{
		float L_13 = __this->get_scrollVector_37();
		float L_14 = __this->get_scrollMin_38();
		if ((!(((float)L_13) <= ((float)((-L_14))))))
		{
			goto IL_00f6;
		}
	}
	{
		float L_15 = __this->get_scrollVector_37();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_16 = fabsf(L_15);
		float L_17 = __this->get_scrollMin_38();
		float L_18 = __this->get_scrollMargin_36();
		float L_19 = fabsf(((float)((float)((float)((float)L_16-(float)L_17))/(float)L_18)));
		float L_20 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (1.0f), (0.0f), L_19, /*hidden argument*/NULL);
		__this->set_resistance_40(L_20);
		float L_21 = __this->get_resistance_40();
		float L_22 = __this->get_marginSpringiness_33();
		float L_23 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_marginPressure_41(((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_21))*(float)L_22))*(float)L_23)));
	}

IL_00f6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_24 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_24) <= ((int32_t)0)))
		{
			goto IL_017c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_25 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_25;
		int32_t L_26 = Touch_get_phase_m196706494((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_017b;
		}
	}
	{
		Vector2_t2243707579  L_27 = Touch_get_deltaPosition_m97688791((&V_1), /*hidden argument*/NULL);
		V_3 = L_27;
		float L_28 = (&V_3)->get_y_1();
		V_2 = L_28;
		float L_29 = V_2;
		float L_30 = __this->get_touchSpeed_34();
		float L_31 = __this->get_resistance_40();
		__this->set_deltaY_42(((float)((float)((float)((float)L_29*(float)L_30))*(float)L_31)));
		float L_32 = __this->get_scrollVector_37();
		float L_33 = __this->get_deltaY_42();
		float L_34 = __this->get_scrollMin_38();
		float L_35 = __this->get_scrollMargin_36();
		float L_36 = __this->get_scrollMax_39();
		float L_37 = __this->get_scrollMargin_36();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_38 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_32+(float)L_33)), ((-((float)((float)L_34+(float)L_35)))), ((float)((float)L_36+(float)L_37)), /*hidden argument*/NULL);
		__this->set_scrollVector_37(L_38);
		float L_39 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_43(L_39);
	}

IL_017b:
	{
	}

IL_017c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_40 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_40) >= ((int32_t)1)))
		{
			goto IL_01f3;
		}
	}
	{
		float L_41 = __this->get_deltaY_42();
		float L_42 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_43 = __this->get_startTime_43();
		float L_44 = __this->get_coastSpeed_35();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_45 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_41, (0.0f), ((float)((float)((float)((float)L_42-(float)L_43))*(float)L_44)), /*hidden argument*/NULL);
		float L_46 = __this->get_resistance_40();
		__this->set_deltaY_42(((float)((float)L_45*(float)L_46)));
		float L_47 = __this->get_scrollVector_37();
		float L_48 = __this->get_deltaY_42();
		float L_49 = __this->get_scrollMin_38();
		float L_50 = __this->get_scrollMargin_36();
		float L_51 = __this->get_scrollMax_39();
		float L_52 = __this->get_scrollMargin_36();
		float L_53 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_47+(float)L_48)), ((-((float)((float)L_49+(float)L_50)))), ((float)((float)L_51+(float)L_52)), /*hidden argument*/NULL);
		float L_54 = __this->get_marginPressure_41();
		__this->set_scrollVector_37(((float)((float)L_53+(float)L_54)));
	}

IL_01f3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_55 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_55) > ((int32_t)0)))
		{
			goto IL_0215;
		}
	}
	{
		float L_56 = V_0;
		float L_57 = __this->get_scrollVector_37();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_58 = fabsf(((float)((float)L_56-(float)L_57)));
		if ((!(((float)L_58) > ((float)(0.01f)))))
		{
			goto IL_0221;
		}
	}

IL_0215:
	{
		__this->set_izSkrollin_45((bool)1);
		goto IL_0228;
	}

IL_0221:
	{
		__this->set_izSkrollin_45((bool)0);
	}

IL_0228:
	{
	}

IL_0229:
	{
		return;
	}
}
// System.Void SetAnimationInAnimator::.ctor()
extern "C"  void SetAnimationInAnimator__ctor_m1105520923 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetAnimationInAnimator::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const uint32_t SetAnimationInAnimator_Start_m747789719_MetadataUsageId;
extern "C"  void SetAnimationInAnimator_Start_m747789719 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetAnimationInAnimator_Start_m747789719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set_animator_2(L_0);
		__this->set_currentAnimNum_3(0);
		return;
	}
}
// System.Void SetAnimationInAnimator::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1144830560;
extern Il2CppCodeGenString* _stringLiteral2626903706;
extern const uint32_t SetAnimationInAnimator_Update_m504862234_MetadataUsageId;
extern "C"  void SetAnimationInAnimator_Update_m504862234 (SetAnimationInAnimator_t2420164504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetAnimationInAnimator_Update_m504862234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1749539436(NULL /*static, unused*/, _stringLiteral1144830560, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_004b;
		}
	}
	{
		Animator_t69676727 * L_1 = __this->get_animator_2();
		int32_t L_2 = __this->get_currentAnimNum_3();
		NullCheck(L_1);
		Animator_SetInteger_m528582597(L_1, _stringLiteral2626903706, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_currentAnimNum_3();
		__this->set_currentAnimNum_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = __this->get_currentAnimNum_3();
		if ((((int32_t)L_4) < ((int32_t)5)))
		{
			goto IL_004a;
		}
	}
	{
		__this->set_currentAnimNum_3(0);
	}

IL_004a:
	{
	}

IL_004b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
