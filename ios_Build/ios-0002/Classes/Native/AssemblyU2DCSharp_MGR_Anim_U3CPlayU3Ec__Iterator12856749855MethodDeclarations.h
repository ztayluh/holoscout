﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Anim/<Play>c__Iterator1
struct U3CPlayU3Ec__Iterator1_t2856749855;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Anim/<Play>c__Iterator1::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator1__ctor_m1254084210 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Anim/<Play>c__Iterator1::MoveNext()
extern "C"  bool U3CPlayU3Ec__Iterator1_MoveNext_m1086451014 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Anim/<Play>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499146672 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Anim/<Play>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3559029192 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Anim/<Play>c__Iterator1::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator1_Dispose_m2715196273 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Anim/<Play>c__Iterator1::Reset()
extern "C"  void U3CPlayU3Ec__Iterator1_Reset_m1936120671 (U3CPlayU3Ec__Iterator1_t2856749855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
