﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"


extern Il2CppGenericClass* const s_Il2CppGenericTypes[];
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[];
extern const Il2CppGenericMethodFunctionsDefinitions s_Il2CppGenericMethodFunctions[];
extern const Il2CppType* const  g_Il2CppTypeTable[];
extern const Il2CppMethodSpec g_Il2CppMethodSpecTable[];
extern const int32_t* g_FieldOffsetTable[];
extern const Il2CppTypeDefinitionSizes* g_Il2CppTypeDefinitionSizesTable[];
extern void** const g_MetadataUsages[];
extern const Il2CppMetadataRegistration g_MetadataRegistration = 
{
	1958,
	s_Il2CppGenericTypes,
	555,
	g_Il2CppGenericInstTable,
	4959,
	s_Il2CppGenericMethodFunctions,
	7435,
	g_Il2CppTypeTable,
	5260,
	g_Il2CppMethodSpecTable,
	1712,
	g_FieldOffsetTable,
	1712,
	g_Il2CppTypeDefinitionSizesTable,
	6777,
	g_MetadataUsages,
};
