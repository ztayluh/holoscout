﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Scroll
struct MGR_Scroll_t3416609842;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Scroll::.ctor()
extern "C"  void MGR_Scroll__ctor_m3534779169 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Scroll::Awake()
extern "C"  void MGR_Scroll_Awake_m2905422574 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Scroll::Start()
extern "C"  void MGR_Scroll_Start_m2381332081 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Scroll::OnGUI()
extern "C"  void MGR_Scroll_OnGUI_m2075454235 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Scroll::CreateWindow(System.Int32)
extern "C"  void MGR_Scroll_CreateWindow_m4154219716 (MGR_Scroll_t3416609842 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Scroll::CreateGroupWindow(System.Int32)
extern "C"  void MGR_Scroll_CreateGroupWindow_m394102123 (MGR_Scroll_t3416609842 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Scroll::Update()
extern "C"  void MGR_Scroll_Update_m851729424 (MGR_Scroll_t3416609842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
