﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_Cloud/<readPLDB>c__Iterator2
struct  U3CreadPLDBU3Ec__Iterator2_t2294453720  : public Il2CppObject
{
public:
	// System.String MGR_Cloud/<readPLDB>c__Iterator2::t
	String_t* ___t_0;
	// System.String MGR_Cloud/<readPLDB>c__Iterator2::<url>__0
	String_t* ___U3CurlU3E__0_1;
	// UnityEngine.WWW MGR_Cloud/<readPLDB>c__Iterator2::<cu_get>__1
	WWW_t2919945039 * ___U3Ccu_getU3E__1_2;
	// System.String MGR_Cloud/<readPLDB>c__Iterator2::<data>__2
	String_t* ___U3CdataU3E__2_3;
	// MGR_Cloud MGR_Cloud/<readPLDB>c__Iterator2::$this
	MGR_Cloud_t726529104 * ___U24this_4;
	// System.Object MGR_Cloud/<readPLDB>c__Iterator2::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean MGR_Cloud/<readPLDB>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 MGR_Cloud/<readPLDB>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CreadPLDBU3Ec__Iterator2_t2294453720, ___t_0)); }
	inline String_t* get_t_0() const { return ___t_0; }
	inline String_t** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(String_t* value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier(&___t_0, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CreadPLDBU3Ec__Iterator2_t2294453720, ___U3CurlU3E__0_1)); }
	inline String_t* get_U3CurlU3E__0_1() const { return ___U3CurlU3E__0_1; }
	inline String_t** get_address_of_U3CurlU3E__0_1() { return &___U3CurlU3E__0_1; }
	inline void set_U3CurlU3E__0_1(String_t* value)
	{
		___U3CurlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3Ccu_getU3E__1_2() { return static_cast<int32_t>(offsetof(U3CreadPLDBU3Ec__Iterator2_t2294453720, ___U3Ccu_getU3E__1_2)); }
	inline WWW_t2919945039 * get_U3Ccu_getU3E__1_2() const { return ___U3Ccu_getU3E__1_2; }
	inline WWW_t2919945039 ** get_address_of_U3Ccu_getU3E__1_2() { return &___U3Ccu_getU3E__1_2; }
	inline void set_U3Ccu_getU3E__1_2(WWW_t2919945039 * value)
	{
		___U3Ccu_getU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Ccu_getU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CdataU3E__2_3() { return static_cast<int32_t>(offsetof(U3CreadPLDBU3Ec__Iterator2_t2294453720, ___U3CdataU3E__2_3)); }
	inline String_t* get_U3CdataU3E__2_3() const { return ___U3CdataU3E__2_3; }
	inline String_t** get_address_of_U3CdataU3E__2_3() { return &___U3CdataU3E__2_3; }
	inline void set_U3CdataU3E__2_3(String_t* value)
	{
		___U3CdataU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdataU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CreadPLDBU3Ec__Iterator2_t2294453720, ___U24this_4)); }
	inline MGR_Cloud_t726529104 * get_U24this_4() const { return ___U24this_4; }
	inline MGR_Cloud_t726529104 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MGR_Cloud_t726529104 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CreadPLDBU3Ec__Iterator2_t2294453720, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CreadPLDBU3Ec__Iterator2_t2294453720, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CreadPLDBU3Ec__Iterator2_t2294453720, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
