﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadPLDBU3Ec__Itera2294453720.h"
#include "AssemblyU2DCSharp_MGR_Cloud_U3CreadHashDBU3Ec__Iter962384949.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu817873244.h"
#include "AssemblyU2DCSharp_MGR_CreateMenu_U3CPlayU3Ec__Iter2013211190.h"
#include "AssemblyU2DCSharp_MGR_GUI2229806790.h"
#include "AssemblyU2DCSharp_MGR_HLGUI3333927834.h"
#include "AssemblyU2DCSharp_MGR_HoloText1166120216.h"
#include "AssemblyU2DCSharp_MGR_Menus511463637.h"
#include "AssemblyU2DCSharp_MGR_Menus_MenuStates1946331629.h"
#include "AssemblyU2DCSharp_MGR_Scroll3416609842.h"
#include "AssemblyU2DCSharp_MGR_Scroll_StartMenuLevel3121722732.h"
#include "AssemblyU2DCSharp_SetAnimationInAnimator2420164504.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (U3CreadPLDBU3Ec__Iterator2_t2294453720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[8] = 
{
	U3CreadPLDBU3Ec__Iterator2_t2294453720::get_offset_of_t_0(),
	U3CreadPLDBU3Ec__Iterator2_t2294453720::get_offset_of_U3CurlU3E__0_1(),
	U3CreadPLDBU3Ec__Iterator2_t2294453720::get_offset_of_U3Ccu_getU3E__1_2(),
	U3CreadPLDBU3Ec__Iterator2_t2294453720::get_offset_of_U3CdataU3E__2_3(),
	U3CreadPLDBU3Ec__Iterator2_t2294453720::get_offset_of_U24this_4(),
	U3CreadPLDBU3Ec__Iterator2_t2294453720::get_offset_of_U24current_5(),
	U3CreadPLDBU3Ec__Iterator2_t2294453720::get_offset_of_U24disposing_6(),
	U3CreadPLDBU3Ec__Iterator2_t2294453720::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (U3CreadHashDBU3Ec__Iterator3_t962384949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[8] = 
{
	U3CreadHashDBU3Ec__Iterator3_t962384949::get_offset_of_p_0(),
	U3CreadHashDBU3Ec__Iterator3_t962384949::get_offset_of_U3CurlU3E__0_1(),
	U3CreadHashDBU3Ec__Iterator3_t962384949::get_offset_of_U3Ccu_getU3E__1_2(),
	U3CreadHashDBU3Ec__Iterator3_t962384949::get_offset_of_U3CdataU3E__2_3(),
	U3CreadHashDBU3Ec__Iterator3_t962384949::get_offset_of_U24this_4(),
	U3CreadHashDBU3Ec__Iterator3_t962384949::get_offset_of_U24current_5(),
	U3CreadHashDBU3Ec__Iterator3_t962384949::get_offset_of_U24disposing_6(),
	U3CreadHashDBU3Ec__Iterator3_t962384949::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (MGR_CreateMenu_t817873244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[16] = 
{
	MGR_CreateMenu_t817873244::get_offset_of_clipNames_2(),
	MGR_CreateMenu_t817873244::get_offset_of_myGO_3(),
	MGR_CreateMenu_t817873244::get_offset_of_myAnimator_4(),
	MGR_CreateMenu_t817873244::get_offset_of_playlistItems_5(),
	MGR_CreateMenu_t817873244::get_offset_of_myCloud_6(),
	MGR_CreateMenu_t817873244::get_offset_of_myGUI_7(),
	MGR_CreateMenu_t817873244::get_offset_of_coach_8(),
	MGR_CreateMenu_t817873244::get_offset_of_trainee_9(),
	MGR_CreateMenu_t817873244::get_offset_of_pl_10(),
	MGR_CreateMenu_t817873244::get_offset_of_hs_11(),
	MGR_CreateMenu_t817873244::get_offset_of_coachIF_12(),
	MGR_CreateMenu_t817873244::get_offset_of_traineeIF_13(),
	MGR_CreateMenu_t817873244::get_offset_of_plIF_14(),
	MGR_CreateMenu_t817873244::get_offset_of_currPLItem_15(),
	MGR_CreateMenu_t817873244::get_offset_of_coachScrollPosition_16(),
	MGR_CreateMenu_t817873244::get_offset_of_coachScrollPosition2_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (U3CPlayU3Ec__Iterator0_t2013211190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[7] = 
{
	U3CPlayU3Ec__Iterator0_t2013211190::get_offset_of_U3CcurrInfoU3E__0_0(),
	U3CPlayU3Ec__Iterator0_t2013211190::get_offset_of_U3CiU3E__1_1(),
	U3CPlayU3Ec__Iterator0_t2013211190::get_offset_of_U3CtimeU3E__2_2(),
	U3CPlayU3Ec__Iterator0_t2013211190::get_offset_of_U24this_3(),
	U3CPlayU3Ec__Iterator0_t2013211190::get_offset_of_U24current_4(),
	U3CPlayU3Ec__Iterator0_t2013211190::get_offset_of_U24disposing_5(),
	U3CPlayU3Ec__Iterator0_t2013211190::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (MGR_GUI_t2229806790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[25] = 
{
	MGR_GUI_t2229806790::get_offset_of_coachSelected_2(),
	MGR_GUI_t2229806790::get_offset_of_traineeSelected_3(),
	MGR_GUI_t2229806790::get_offset_of_playlistSelected_4(),
	MGR_GUI_t2229806790::get_offset_of_Input_Coach_5(),
	MGR_GUI_t2229806790::get_offset_of_Input_Trainee_6(),
	MGR_GUI_t2229806790::get_offset_of_Input_Playlist_7(),
	MGR_GUI_t2229806790::get_offset_of_coach_8(),
	MGR_GUI_t2229806790::get_offset_of_trainee_9(),
	MGR_GUI_t2229806790::get_offset_of_playlist_10(),
	MGR_GUI_t2229806790::get_offset_of_i_coach_11(),
	MGR_GUI_t2229806790::get_offset_of_i_trainee_12(),
	MGR_GUI_t2229806790::get_offset_of_i_playlist_13(),
	MGR_GUI_t2229806790::get_offset_of_clipNames_14(),
	MGR_GUI_t2229806790::get_offset_of_hlist_15(),
	MGR_GUI_t2229806790::get_offset_of_DictOfAnims_16(),
	MGR_GUI_t2229806790::get_offset_of_AnimsOfDick_17(),
	MGR_GUI_t2229806790::get_offset_of_myCloud_18(),
	MGR_GUI_t2229806790::get_offset_of_myGO_19(),
	MGR_GUI_t2229806790::get_offset_of_myCanvas_20(),
	MGR_GUI_t2229806790::get_offset_of_myAnimator_21(),
	MGR_GUI_t2229806790::get_offset_of_coachScrollPosition_22(),
	MGR_GUI_t2229806790::get_offset_of_traineeScrollPosition_23(),
	MGR_GUI_t2229806790::get_offset_of_plScrollPosition_24(),
	MGR_GUI_t2229806790::get_offset_of_hashScrollPosition_25(),
	MGR_GUI_t2229806790::get_offset_of_count_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (MGR_HLGUI_t3333927834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[21] = 
{
	MGR_HLGUI_t3333927834::get_offset_of_Input_Coach_2(),
	MGR_HLGUI_t3333927834::get_offset_of_Input_Trainee_3(),
	MGR_HLGUI_t3333927834::get_offset_of_Input_Playlist_4(),
	MGR_HLGUI_t3333927834::get_offset_of_coach_5(),
	MGR_HLGUI_t3333927834::get_offset_of_trainee_6(),
	MGR_HLGUI_t3333927834::get_offset_of_playlist_7(),
	MGR_HLGUI_t3333927834::get_offset_of_i_coach_8(),
	MGR_HLGUI_t3333927834::get_offset_of_i_trainee_9(),
	MGR_HLGUI_t3333927834::get_offset_of_i_playlist_10(),
	MGR_HLGUI_t3333927834::get_offset_of_clipNames_11(),
	MGR_HLGUI_t3333927834::get_offset_of_DictOfAnims_12(),
	MGR_HLGUI_t3333927834::get_offset_of_myCloud_13(),
	MGR_HLGUI_t3333927834::get_offset_of_myAnim_14(),
	MGR_HLGUI_t3333927834::get_offset_of_myGO_15(),
	MGR_HLGUI_t3333927834::get_offset_of_myCanvas_16(),
	MGR_HLGUI_t3333927834::get_offset_of_myAnimator_17(),
	MGR_HLGUI_t3333927834::get_offset_of_coachScrollPosition_18(),
	MGR_HLGUI_t3333927834::get_offset_of_traineeScrollPosition_19(),
	MGR_HLGUI_t3333927834::get_offset_of_plScrollPosition_20(),
	MGR_HLGUI_t3333927834::get_offset_of_hashScrollPosition_21(),
	MGR_HLGUI_t3333927834::get_offset_of_count_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (MGR_HoloText_t1166120216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[3] = 
{
	MGR_HoloText_t1166120216::get_offset_of_playlistText_2(),
	MGR_HoloText_t1166120216::get_offset_of_myCloud_3(),
	MGR_HoloText_t1166120216::get_offset_of_updateText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (MGR_Menus_t511463637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[6] = 
{
	MGR_Menus_t511463637::get_offset_of_currentstate_2(),
	MGR_Menus_t511463637::get_offset_of_myCanvas_3(),
	MGR_Menus_t511463637::get_offset_of_startMenu_4(),
	MGR_Menus_t511463637::get_offset_of_createMenu_5(),
	MGR_Menus_t511463637::get_offset_of_readMenu_6(),
	MGR_Menus_t511463637::get_offset_of_player_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (MenuStates_t1946331629)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1708[4] = 
{
	MenuStates_t1946331629::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (MGR_Scroll_t3416609842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[72] = 
{
	MGR_Scroll_t3416609842::get_offset_of_gSkin_2(),
	MGR_Scroll_t3416609842::get_offset_of_introTextAsset_3(),
	MGR_Scroll_t3416609842::get_offset_of_babaTextAsset_4(),
	MGR_Scroll_t3416609842::get_offset_of_karoliTextAsset_5(),
	MGR_Scroll_t3416609842::get_offset_of_loremTextAsset_6(),
	MGR_Scroll_t3416609842::get_offset_of_rolandTextAsset_7(),
	MGR_Scroll_t3416609842::get_offset_of_introText_8(),
	MGR_Scroll_t3416609842::get_offset_of_babaText_9(),
	MGR_Scroll_t3416609842::get_offset_of_karoliText_10(),
	MGR_Scroll_t3416609842::get_offset_of_loremText_11(),
	MGR_Scroll_t3416609842::get_offset_of_rolandText_12(),
	MGR_Scroll_t3416609842::get_offset_of_windowText_13(),
	MGR_Scroll_t3416609842::get_offset_of_iconVector_14(),
	MGR_Scroll_t3416609842::get_offset_of_startBG_15(),
	MGR_Scroll_t3416609842::get_offset_of_charlemagne_16(),
	MGR_Scroll_t3416609842::get_offset_of_map_17(),
	MGR_Scroll_t3416609842::get_offset_of_page_18(),
	MGR_Scroll_t3416609842::get_offset_of_iconAudio_19(),
	MGR_Scroll_t3416609842::get_offset_of_iconAudioNo_20(),
	MGR_Scroll_t3416609842::get_offset_of_iconMusic_21(),
	MGR_Scroll_t3416609842::get_offset_of_iconMusicNo_22(),
	MGR_Scroll_t3416609842::get_offset_of_iconResetHS_23(),
	MGR_Scroll_t3416609842::get_offset_of_iconResetHSDone_24(),
	MGR_Scroll_t3416609842::get_offset_of_iconResetHSWarn_25(),
	MGR_Scroll_t3416609842::get_offset_of_iconResetHSDoneWarn_26(),
	MGR_Scroll_t3416609842::get_offset_of_iconThrustNormal_27(),
	MGR_Scroll_t3416609842::get_offset_of_iconThrustReversed_28(),
	MGR_Scroll_t3416609842::get_offset_of_iconTurnNormal_29(),
	MGR_Scroll_t3416609842::get_offset_of_iconTurnReversed_30(),
	MGR_Scroll_t3416609842::get_offset_of_iconInfo_31(),
	MGR_Scroll_t3416609842::get_offset_of_iconInfoNo_32(),
	MGR_Scroll_t3416609842::get_offset_of_marginSpringiness_33(),
	MGR_Scroll_t3416609842::get_offset_of_touchSpeed_34(),
	MGR_Scroll_t3416609842::get_offset_of_coastSpeed_35(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMargin_36(),
	MGR_Scroll_t3416609842::get_offset_of_scrollVector_37(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMin_38(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMax_39(),
	MGR_Scroll_t3416609842::get_offset_of_resistance_40(),
	MGR_Scroll_t3416609842::get_offset_of_marginPressure_41(),
	MGR_Scroll_t3416609842::get_offset_of_deltaY_42(),
	MGR_Scroll_t3416609842::get_offset_of_startTime_43(),
	MGR_Scroll_t3416609842::get_offset_of_hazSkrollin_44(),
	MGR_Scroll_t3416609842::get_offset_of_izSkrollin_45(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMaxBaba_46(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMaxKaroli_47(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMaxLorem_48(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMaxRoland_49(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMaxButtons_50(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMinBaba_51(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMinKaroli_52(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMinLorem_53(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMinRoland_54(),
	MGR_Scroll_t3416609842::get_offset_of_scrollMinButtons_55(),
	MGR_Scroll_t3416609842::get_offset_of_windowPad_56(),
	MGR_Scroll_t3416609842::get_offset_of_babaWindowRect_57(),
	MGR_Scroll_t3416609842::get_offset_of_karoliWindowRect_58(),
	MGR_Scroll_t3416609842::get_offset_of_loremWindowRect_59(),
	MGR_Scroll_t3416609842::get_offset_of_rolandWindowRect_60(),
	MGR_Scroll_t3416609842::get_offset_of_buttonsWindowRect_61(),
	MGR_Scroll_t3416609842::get_offset_of_createdWindow_62(),
	MGR_Scroll_t3416609842::get_offset_of_windowRect_63(),
	MGR_Scroll_t3416609842::get_offset_of_windowStyle_64(),
	MGR_Scroll_t3416609842::get_offset_of_audioOff_65(),
	MGR_Scroll_t3416609842::get_offset_of_musicOff_66(),
	MGR_Scroll_t3416609842::get_offset_of_thrustReversed_67(),
	MGR_Scroll_t3416609842::get_offset_of_turnReversed_68(),
	MGR_Scroll_t3416609842::get_offset_of_resetHSCheck_69(),
	MGR_Scroll_t3416609842::get_offset_of_hasResetHS_70(),
	MGR_Scroll_t3416609842::get_offset_of_hideInfo_71(),
	MGR_Scroll_t3416609842::get_offset_of_newVectorPosition_72(),
	MGR_Scroll_t3416609842::get_offset_of_menuLevel_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (StartMenuLevel_t3121722732)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1710[7] = 
{
	StartMenuLevel_t3121722732::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (SetAnimationInAnimator_t2420164504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[2] = 
{
	SetAnimationInAnimator_t2420164504::get_offset_of_animator_2(),
	SetAnimationInAnimator_t2420164504::get_offset_of_currentAnimNum_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
