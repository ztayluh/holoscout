﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MGR_Cloud/<readTraineeDB>c__Iterator1
struct U3CreadTraineeDBU3Ec__Iterator1_t81797705;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MGR_Cloud/<readTraineeDB>c__Iterator1::.ctor()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator1__ctor_m1422939798 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MGR_Cloud/<readTraineeDB>c__Iterator1::MoveNext()
extern "C"  bool U3CreadTraineeDBU3Ec__Iterator1_MoveNext_m838431130 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3733750148 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MGR_Cloud/<readTraineeDB>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadTraineeDBU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1190909852 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator1::Dispose()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator1_Dispose_m1703927019 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MGR_Cloud/<readTraineeDB>c__Iterator1::Reset()
extern "C"  void U3CreadTraineeDBU3Ec__Iterator1_Reset_m3852545481 (U3CreadTraineeDBU3Ec__Iterator1_t81797705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
