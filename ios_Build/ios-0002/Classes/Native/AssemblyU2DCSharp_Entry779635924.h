﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t278199169;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entry
struct  Entry_t779635924 
{
public:
	// System.String Entry::coach
	String_t* ___coach_0;
	// System.String Entry::trainee
	String_t* ___trainee_1;
	// System.String Entry::playlistName
	String_t* ___playlistName_2;
	// System.Collections.Generic.List`1<System.String> Entry::currentPlaylist
	List_1_t1398341365 * ___currentPlaylist_3;
	// System.Collections.Generic.List`1<System.String> Entry::playlists
	List_1_t1398341365 * ___playlists_4;
	// System.Collections.Generic.List`1<System.String> Entry::coaches
	List_1_t1398341365 * ___coaches_5;
	// System.Collections.Generic.List`1<System.String> Entry::trainees
	List_1_t1398341365 * ___trainees_6;
	// System.Collections.Generic.List`1<System.Int64> Entry::hashString
	List_1_t278199169 * ___hashString_7;
	// System.Int32 Entry::pSize
	int32_t ___pSize_8;
	// System.Int32 Entry::cSize
	int32_t ___cSize_9;
	// System.Int32 Entry::tSize
	int32_t ___tSize_10;
	// System.Int32 Entry::hSize
	int32_t ___hSize_11;
	// System.Int32 Entry::numOfPlaylists
	int32_t ___numOfPlaylists_12;

public:
	inline static int32_t get_offset_of_coach_0() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___coach_0)); }
	inline String_t* get_coach_0() const { return ___coach_0; }
	inline String_t** get_address_of_coach_0() { return &___coach_0; }
	inline void set_coach_0(String_t* value)
	{
		___coach_0 = value;
		Il2CppCodeGenWriteBarrier(&___coach_0, value);
	}

	inline static int32_t get_offset_of_trainee_1() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___trainee_1)); }
	inline String_t* get_trainee_1() const { return ___trainee_1; }
	inline String_t** get_address_of_trainee_1() { return &___trainee_1; }
	inline void set_trainee_1(String_t* value)
	{
		___trainee_1 = value;
		Il2CppCodeGenWriteBarrier(&___trainee_1, value);
	}

	inline static int32_t get_offset_of_playlistName_2() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___playlistName_2)); }
	inline String_t* get_playlistName_2() const { return ___playlistName_2; }
	inline String_t** get_address_of_playlistName_2() { return &___playlistName_2; }
	inline void set_playlistName_2(String_t* value)
	{
		___playlistName_2 = value;
		Il2CppCodeGenWriteBarrier(&___playlistName_2, value);
	}

	inline static int32_t get_offset_of_currentPlaylist_3() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___currentPlaylist_3)); }
	inline List_1_t1398341365 * get_currentPlaylist_3() const { return ___currentPlaylist_3; }
	inline List_1_t1398341365 ** get_address_of_currentPlaylist_3() { return &___currentPlaylist_3; }
	inline void set_currentPlaylist_3(List_1_t1398341365 * value)
	{
		___currentPlaylist_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentPlaylist_3, value);
	}

	inline static int32_t get_offset_of_playlists_4() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___playlists_4)); }
	inline List_1_t1398341365 * get_playlists_4() const { return ___playlists_4; }
	inline List_1_t1398341365 ** get_address_of_playlists_4() { return &___playlists_4; }
	inline void set_playlists_4(List_1_t1398341365 * value)
	{
		___playlists_4 = value;
		Il2CppCodeGenWriteBarrier(&___playlists_4, value);
	}

	inline static int32_t get_offset_of_coaches_5() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___coaches_5)); }
	inline List_1_t1398341365 * get_coaches_5() const { return ___coaches_5; }
	inline List_1_t1398341365 ** get_address_of_coaches_5() { return &___coaches_5; }
	inline void set_coaches_5(List_1_t1398341365 * value)
	{
		___coaches_5 = value;
		Il2CppCodeGenWriteBarrier(&___coaches_5, value);
	}

	inline static int32_t get_offset_of_trainees_6() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___trainees_6)); }
	inline List_1_t1398341365 * get_trainees_6() const { return ___trainees_6; }
	inline List_1_t1398341365 ** get_address_of_trainees_6() { return &___trainees_6; }
	inline void set_trainees_6(List_1_t1398341365 * value)
	{
		___trainees_6 = value;
		Il2CppCodeGenWriteBarrier(&___trainees_6, value);
	}

	inline static int32_t get_offset_of_hashString_7() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___hashString_7)); }
	inline List_1_t278199169 * get_hashString_7() const { return ___hashString_7; }
	inline List_1_t278199169 ** get_address_of_hashString_7() { return &___hashString_7; }
	inline void set_hashString_7(List_1_t278199169 * value)
	{
		___hashString_7 = value;
		Il2CppCodeGenWriteBarrier(&___hashString_7, value);
	}

	inline static int32_t get_offset_of_pSize_8() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___pSize_8)); }
	inline int32_t get_pSize_8() const { return ___pSize_8; }
	inline int32_t* get_address_of_pSize_8() { return &___pSize_8; }
	inline void set_pSize_8(int32_t value)
	{
		___pSize_8 = value;
	}

	inline static int32_t get_offset_of_cSize_9() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___cSize_9)); }
	inline int32_t get_cSize_9() const { return ___cSize_9; }
	inline int32_t* get_address_of_cSize_9() { return &___cSize_9; }
	inline void set_cSize_9(int32_t value)
	{
		___cSize_9 = value;
	}

	inline static int32_t get_offset_of_tSize_10() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___tSize_10)); }
	inline int32_t get_tSize_10() const { return ___tSize_10; }
	inline int32_t* get_address_of_tSize_10() { return &___tSize_10; }
	inline void set_tSize_10(int32_t value)
	{
		___tSize_10 = value;
	}

	inline static int32_t get_offset_of_hSize_11() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___hSize_11)); }
	inline int32_t get_hSize_11() const { return ___hSize_11; }
	inline int32_t* get_address_of_hSize_11() { return &___hSize_11; }
	inline void set_hSize_11(int32_t value)
	{
		___hSize_11 = value;
	}

	inline static int32_t get_offset_of_numOfPlaylists_12() { return static_cast<int32_t>(offsetof(Entry_t779635924, ___numOfPlaylists_12)); }
	inline int32_t get_numOfPlaylists_12() const { return ___numOfPlaylists_12; }
	inline int32_t* get_address_of_numOfPlaylists_12() { return &___numOfPlaylists_12; }
	inline void set_numOfPlaylists_12(int32_t value)
	{
		___numOfPlaylists_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Entry
struct Entry_t779635924_marshaled_pinvoke
{
	char* ___coach_0;
	char* ___trainee_1;
	char* ___playlistName_2;
	List_1_t1398341365 * ___currentPlaylist_3;
	List_1_t1398341365 * ___playlists_4;
	List_1_t1398341365 * ___coaches_5;
	List_1_t1398341365 * ___trainees_6;
	List_1_t278199169 * ___hashString_7;
	int32_t ___pSize_8;
	int32_t ___cSize_9;
	int32_t ___tSize_10;
	int32_t ___hSize_11;
	int32_t ___numOfPlaylists_12;
};
// Native definition for COM marshalling of Entry
struct Entry_t779635924_marshaled_com
{
	Il2CppChar* ___coach_0;
	Il2CppChar* ___trainee_1;
	Il2CppChar* ___playlistName_2;
	List_1_t1398341365 * ___currentPlaylist_3;
	List_1_t1398341365 * ___playlists_4;
	List_1_t1398341365 * ___coaches_5;
	List_1_t1398341365 * ___trainees_6;
	List_1_t278199169 * ___hashString_7;
	int32_t ___pSize_8;
	int32_t ___cSize_9;
	int32_t ___tSize_10;
	int32_t ___hSize_11;
	int32_t ___numOfPlaylists_12;
};
