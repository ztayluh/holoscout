﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MGR_Anim
struct MGR_Anim_t3221826994;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "AssemblyU2DCSharp_Entry779635924.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_Anim/<Play>c__Iterator0
struct  U3CPlayU3Ec__Iterator0_t2856749856  : public Il2CppObject
{
public:
	// UnityEngine.AnimatorStateInfo MGR_Anim/<Play>c__Iterator0::<currInfo>__0
	AnimatorStateInfo_t2577870592  ___U3CcurrInfoU3E__0_0;
	// System.Int32 MGR_Anim/<Play>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_1;
	// Entry MGR_Anim/<Play>c__Iterator0::e
	Entry_t779635924  ___e_2;
	// System.Single MGR_Anim/<Play>c__Iterator0::<time>__2
	float ___U3CtimeU3E__2_3;
	// MGR_Anim MGR_Anim/<Play>c__Iterator0::$this
	MGR_Anim_t3221826994 * ___U24this_4;
	// System.Object MGR_Anim/<Play>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean MGR_Anim/<Play>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 MGR_Anim/<Play>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CcurrInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t2856749856, ___U3CcurrInfoU3E__0_0)); }
	inline AnimatorStateInfo_t2577870592  get_U3CcurrInfoU3E__0_0() const { return ___U3CcurrInfoU3E__0_0; }
	inline AnimatorStateInfo_t2577870592 * get_address_of_U3CcurrInfoU3E__0_0() { return &___U3CcurrInfoU3E__0_0; }
	inline void set_U3CcurrInfoU3E__0_0(AnimatorStateInfo_t2577870592  value)
	{
		___U3CcurrInfoU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t2856749856, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t2856749856, ___e_2)); }
	inline Entry_t779635924  get_e_2() const { return ___e_2; }
	inline Entry_t779635924 * get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(Entry_t779635924  value)
	{
		___e_2 = value;
	}

	inline static int32_t get_offset_of_U3CtimeU3E__2_3() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t2856749856, ___U3CtimeU3E__2_3)); }
	inline float get_U3CtimeU3E__2_3() const { return ___U3CtimeU3E__2_3; }
	inline float* get_address_of_U3CtimeU3E__2_3() { return &___U3CtimeU3E__2_3; }
	inline void set_U3CtimeU3E__2_3(float value)
	{
		___U3CtimeU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t2856749856, ___U24this_4)); }
	inline MGR_Anim_t3221826994 * get_U24this_4() const { return ___U24this_4; }
	inline MGR_Anim_t3221826994 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MGR_Anim_t3221826994 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t2856749856, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t2856749856, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t2856749856, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
