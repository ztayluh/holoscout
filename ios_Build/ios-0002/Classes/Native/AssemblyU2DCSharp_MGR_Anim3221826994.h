﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;
// MGR_Cloud
struct MGR_Cloud_t726529104;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t278199169;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Entry779635924.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MGR_Anim
struct  MGR_Anim_t3221826994  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MGR_Anim::myGO
	GameObject_t1756533147 * ___myGO_2;
	// UnityEngine.Animator MGR_Anim::myAnimator
	Animator_t69676727 * ___myAnimator_3;
	// Entry MGR_Anim::entry
	Entry_t779635924  ___entry_4;
	// MGR_Cloud MGR_Anim::myCloud
	MGR_Cloud_t726529104 * ___myCloud_5;
	// System.Int32 MGR_Anim::currPLItem
	int32_t ___currPLItem_6;
	// System.Int64 MGR_Anim::currHashID
	int64_t ___currHashID_7;
	// System.Collections.Generic.List`1<System.Int64> MGR_Anim::myhashPL
	List_1_t278199169 * ___myhashPL_8;

public:
	inline static int32_t get_offset_of_myGO_2() { return static_cast<int32_t>(offsetof(MGR_Anim_t3221826994, ___myGO_2)); }
	inline GameObject_t1756533147 * get_myGO_2() const { return ___myGO_2; }
	inline GameObject_t1756533147 ** get_address_of_myGO_2() { return &___myGO_2; }
	inline void set_myGO_2(GameObject_t1756533147 * value)
	{
		___myGO_2 = value;
		Il2CppCodeGenWriteBarrier(&___myGO_2, value);
	}

	inline static int32_t get_offset_of_myAnimator_3() { return static_cast<int32_t>(offsetof(MGR_Anim_t3221826994, ___myAnimator_3)); }
	inline Animator_t69676727 * get_myAnimator_3() const { return ___myAnimator_3; }
	inline Animator_t69676727 ** get_address_of_myAnimator_3() { return &___myAnimator_3; }
	inline void set_myAnimator_3(Animator_t69676727 * value)
	{
		___myAnimator_3 = value;
		Il2CppCodeGenWriteBarrier(&___myAnimator_3, value);
	}

	inline static int32_t get_offset_of_entry_4() { return static_cast<int32_t>(offsetof(MGR_Anim_t3221826994, ___entry_4)); }
	inline Entry_t779635924  get_entry_4() const { return ___entry_4; }
	inline Entry_t779635924 * get_address_of_entry_4() { return &___entry_4; }
	inline void set_entry_4(Entry_t779635924  value)
	{
		___entry_4 = value;
	}

	inline static int32_t get_offset_of_myCloud_5() { return static_cast<int32_t>(offsetof(MGR_Anim_t3221826994, ___myCloud_5)); }
	inline MGR_Cloud_t726529104 * get_myCloud_5() const { return ___myCloud_5; }
	inline MGR_Cloud_t726529104 ** get_address_of_myCloud_5() { return &___myCloud_5; }
	inline void set_myCloud_5(MGR_Cloud_t726529104 * value)
	{
		___myCloud_5 = value;
		Il2CppCodeGenWriteBarrier(&___myCloud_5, value);
	}

	inline static int32_t get_offset_of_currPLItem_6() { return static_cast<int32_t>(offsetof(MGR_Anim_t3221826994, ___currPLItem_6)); }
	inline int32_t get_currPLItem_6() const { return ___currPLItem_6; }
	inline int32_t* get_address_of_currPLItem_6() { return &___currPLItem_6; }
	inline void set_currPLItem_6(int32_t value)
	{
		___currPLItem_6 = value;
	}

	inline static int32_t get_offset_of_currHashID_7() { return static_cast<int32_t>(offsetof(MGR_Anim_t3221826994, ___currHashID_7)); }
	inline int64_t get_currHashID_7() const { return ___currHashID_7; }
	inline int64_t* get_address_of_currHashID_7() { return &___currHashID_7; }
	inline void set_currHashID_7(int64_t value)
	{
		___currHashID_7 = value;
	}

	inline static int32_t get_offset_of_myhashPL_8() { return static_cast<int32_t>(offsetof(MGR_Anim_t3221826994, ___myhashPL_8)); }
	inline List_1_t278199169 * get_myhashPL_8() const { return ___myhashPL_8; }
	inline List_1_t278199169 ** get_address_of_myhashPL_8() { return &___myhashPL_8; }
	inline void set_myhashPL_8(List_1_t278199169 * value)
	{
		___myhashPL_8 = value;
		Il2CppCodeGenWriteBarrier(&___myhashPL_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
