Davnym-davno zhil kupets i yego zheny, u nih odin rebenok, devochka po imeni Vasilisa. Odnazhdy matʹ polozhila kukla v rukah rebenka, ona skazala: "Moĭ malʹchik, ya umirayu. Vozʹmite etu kuklu v kachestve moego blagosloveniya. Derzhite yee s soboĭ i ne pokazatʹ yego komu-libo. Yesli chto-to plohoe sluchaet·sya s vami, datʹ kukle prodovolʹstvie i poprositʹ yee rukovodstva ". Vskore umerla matʹ.

Torgovyĭ vskore stalo odinoko i reshila snova vyĭti zamuzh. On zhenilsya na vdove on dumal by horoshyeĭ materʹyu, no i ona, i dve yee docheri byli zaviduyut krasote Vasilisy. Oni dali yeĭ tyazhelye naruzhnye raboty po delu, tak chto ona budet rasti tonkaya, i yee litso v svoyu ocheredʹ, urodlivye vetra i solntsa.

Nesmotrya na eto, Vasilisa stala bolyee krasivoĭ kazhdyĭ denʹ. Za kazhdyĭ denʹ, ona dala kukle prodovolʹstvie i obratilsya za sovetom. Zakonchiv yestʹ, kukla mozhet pomochʹ s zadachami i dazhe privesti Vasilisa trav dlya predotvrashcheniya solnechnyh ozhogov.


Shli gody, Vasilisa rosla yeshche bolyee krasivymi, kak nenavistʹ machehi o yee aktivizatsii.