﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Entry
{
	public string coach;
	public string trainee;
	public string playlistName;
	public List<string> currentPlaylist;
	public List<string> playlists;
	public List<string> coaches;
	public List<string> trainees;
	public List<long> hashString;
	public int pSize;
	public int cSize;
	public int tSize;
	public int hSize;
	public int numOfPlaylists;
}

public class MGR_Cloud : MonoBehaviour
{
	public Entry myEntry;
	public MGR_Anim anims;

	public int numOfClips;
	public int currClip;
	public string currClipName = "";

	public Dictionary<string, string> traineeRecords;


	//----------------------
	//----Awake-------------
	//----------------------
	void Awake ()
	{
		//populate the entry
		anims = GetComponent<MGR_Anim> ();
        
		//Initialize entry
		traineeRecords = new Dictionary<string, string> ();
		myEntry.playlists = new List<string> ();
		myEntry.currentPlaylist = new List<string> ();
		myEntry.trainees = new List<string> ();
		myEntry.coaches = new List<string> ();
		myEntry.hashString = new List<long> ();
	}

	//----------------------
	//----Start-------------
	//----------------------
	void Start ()
	{
		myEntry.coach = "";
		myEntry.trainee = "";
		myEntry.playlistName = "";
		myEntry.cSize = 0;
		myEntry.pSize = 0;
		myEntry.tSize = 0;
		myEntry.hSize = 0;
		myEntry.numOfPlaylists = 0;
		startReadDB ();
	}

	//----------------------
	//----Update------------
	//----------------------
	void Update ()
	{

	}
	//----------------------
	//----setEntry----------
	//----------------------
	public void setEntry (string cName, string tName, string pName)
	{
		myEntry.playlistName = pName;
		myEntry.coach = cName;
		myEntry.trainee = tName;

		if (!traineeRecords.ContainsKey (tName)) {
			traineeRecords.Add (tName, cName);
			myEntry.cSize++;
		}
	}
	//----------------------
	//----initFB------------
	//----------------------
	public void initFB (string cName, string tName, string pName)
	{
		myEntry.playlistName = pName;
		myEntry.coach = cName;
		myEntry.trainee = tName;
		/*
		if (!traineeRecords.ContainsKey (tName)){
			traineeRecords.Add (tName, cName);
			myEntry.cSize++;
			reference.Child (cName).Child ("Trainee-" + myEntry.cSize).SetValueAsync (tName);
		}*/
	}
	//----------------------
	//----Play--------------
	//----------------------

	public void playPlay ()
	{
		anims.playAnims ();
	}
		

	//----------------------
	//----readCoachDB-------
	//----------------------
	public IEnumerator readCoachDB ()
	{
		string data;
		string url = "http://karagundersen.com/holoScout-readDB.php?&funct=" + 1;
		var cu_get = new WWW (url);
		yield return cu_get;

		if (cu_get.error != null) {
			print ("There was an error checking admin: " + cu_get.error);
		} else {
			data = cu_get.text;
			String[] values = data.Split ('\n');

			myEntry.coaches.Clear ();
			myEntry.cSize = values.Length - 1;
			for (int i = 0; i < values.Length - 1; i++) {
				myEntry.coaches.Add (values [i]);
//				print (values [i]);
			}
		}
	}

	//----------------------
	//----readTraineeDB-----
	//----------------------
	public IEnumerator readTraineeDB (string c)
	{
		string data;
		string url = "http://karagundersen.com/holoScout-readDB.php?coach=" + c + "&funct=" + 2;
		var cu_get = new WWW (url);
		yield return cu_get;
		if (cu_get.error != null) {
			print ("There was an error checking admin: " + cu_get.error);
		} else {
			data = cu_get.text;
			String[] values = data.Split ('\n');

			myEntry.trainees.Clear ();
			myEntry.tSize = values.Length - 1;
			for (int i = 0; i < values.Length - 1; i++) {
				myEntry.trainees.Add (values [i]);
//				print (values [i]);
			}
		}
	}
	//----------------------
	//----readPLDB-------
	//----------------------
	public IEnumerator readPLDB (string t)
	{
		string data;
		string url = "http://karagundersen.com/holoScout-readDB.php?trainee=" + t + "&funct=" + 3;
		print (url); 
		var cu_get = new WWW (url);
		yield return cu_get;
		if (cu_get.error != null) {
			print ("There was an error checking admin: " + cu_get.error);
		} else {
			data = cu_get.text;
			String[] values = data.Split ('\n');
			print ("acquired.\n");
			myEntry.currentPlaylist.Clear ();
			myEntry.pSize = values.Length - 1;
			for (int i = 0; i < values.Length - 1; i++) {
				myEntry.currentPlaylist.Add (values [i]);
				print (values [i]); 
			}
		}
	}
	//----------------------
	//----readHashDB-------
	//----------------------
	public IEnumerator readHashDB (string p)
	{
		string data;
		string url = "http://karagundersen.com/holoScout-readDB.php?playlist=" + p + "&funct=" + 4;
		print (url);
		var cu_get = new WWW (url);
		yield return cu_get;
		if (cu_get.error != null) {
			print ("There was an error checking admin: " + cu_get.error + "\n" + url);
		} else {
//			print ("1,2,3");
			data = cu_get.text;
			String[] values = data.Split (',');
			myEntry.hashString.Clear ();
			myEntry.hSize = values.Length;
			for (int i = 0; i < values.Length; i++) {
				print (values [i]);
				myEntry.hashString.Add (Convert.ToInt64 (values [i]));
			}
		}
	}

	public void startReadDB ()
	{
		StartCoroutine (readCoachDB ());
	}

	public void addPL (string c, string t, string pl, string hs)
	{
//		string hashstring = "-601574123,-1422423230,-591676520,1792621188,1349952704";
		string url = "http://karagundersen.com/holoScout-addDB.php?coach=" + c + "&trainee=" + t + "&playlist=" + pl + "&hashstring=" + hs;

		WWW serverMessenger = new WWW (url);
	}

	public void addPL ()
	{
		string hashstring = "-601574123,-1422423230,-591676520,1792621188,1349952704";
		string url = "http://karagundersen.com/holoScout-addDB.php?coach=" + myEntry.coach + "&trainee=" + myEntry.trainee + "&playlist=" + myEntry.playlistName + "&hashstring=" + hashstring;

		WWW serverMessenger = new WWW (url);
	}
}