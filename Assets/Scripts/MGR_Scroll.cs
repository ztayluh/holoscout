﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class MGR_Scroll : MonoBehaviour
{
	//	StartSkrollin.js
	//	An example of skrolling text on the iPhone with bounce and glide
	//	Attach to the main camera

	/*
This example script is driven by two major sections: OnGUI() and Update().

The OnGUI() is also made up of two major divisions: Normal OnGUI() and CreateWindow.
	 (and in one case CreateGroupWindow, for a scrolling field of buttons.)
*/

	//	@script ExecuteInEditMode ()

	//	Enums for Switch/Cases
	enum StartMenuLevel
	{
		Start = 0,
		Baba = 1,
		Karoli = 2,
		Lorem = 3,
		Roland = 4,
		Buttons = 5

	}

	//	Asset Variables
	public GUISkin gSkin;
	//	Contains the GUI Styles for the text layouts

	public TextAsset introTextAsset;
	//	These are the text assets for the text to scroll
	public TextAsset babaTextAsset;
	public TextAsset karoliTextAsset;
	public TextAsset loremTextAsset;
	public TextAsset rolandTextAsset;
	private string introText;
	//	These are the private Strings that are actually scrolled
	private string babaText;
	private string karoliText;
	private string loremText;
	private string rolandText;
	private string windowText;

	public Texture2D iconVector;
	//	This is the tiny graphic that indicates the window is scrolling

	public Texture2D startBG;
	//	Irrelevant Art
	public Texture2D charlemagne;
	public Texture2D map;
	public Texture2D page;

	public Texture2D iconAudio;
	//	Irrelevant Button Art
	public Texture2D iconAudioNo;
	public Texture2D iconMusic;
	public Texture2D iconMusicNo;
	public Texture2D iconResetHS;
	public Texture2D iconResetHSDone;
	public Texture2D iconResetHSWarn;
	public Texture2D iconResetHSDoneWarn;
	public Texture2D iconThrustNormal;
	public Texture2D iconThrustReversed;
	public Texture2D iconTurnNormal;
	public Texture2D iconTurnReversed;
	public Texture2D iconInfo;
	public Texture2D iconInfoNo;


	//	Basic Variables
	public float marginSpringiness;
	//	These are the variables for setting the glide and spring
	public float touchSpeed;
	//	Look at these and look at function Update () where they are used
	public float coastSpeed;
	public float scrollMargin;

	public float scrollVector;
	//	This could be private, but need to be exposed when setting the scrollMinX values

	private float scrollMin;
	//	A container used by function Update() for the current value
	private float scrollMax;
	//	A container used by function Update() for the current value
	private float resistance;
	//	A container used by function Update() for the current value
	private float marginPressure;
	//	A container used by function Update() for the current value
	private float deltaY;
	//	A container used by function Update() for the current value
	private float startTime;
	//	A container used by function Update() for the current value
	private bool hazSkrollin;
	//	Tells function Update () if the current case uses a scrolling window
	private bool izSkrollin;
	//	A bool  indicating if the current layout is currently scrolling

	private float scrollMaxBaba = 0.0f;
	//	The start point. Aways zero
	private float scrollMaxKaroli = 0.0f;
	private float scrollMaxLorem = 0.0f;
	private float scrollMaxRoland = 0.0f;
	private float scrollMaxButtons = 0.0f;

	public float scrollMinBaba;
	//	These variables define the length (or height) along the Y axis of the scrolling window
	public float scrollMinKaroli;
	//	Max and Min are arguably named backwards, but are technically correct
	public float scrollMinLorem;
	//	This is because the window starts from 0.0f and scrolls down to a negative number
	public float scrollMinRoland;
	//	These need to be set by hand AFTER the text and style are set
	public float scrollMinButtons;
	//	Set these to a large number (2k+) then scroll to the end of the text and use var scrollVector as a guage

	public float windowPad;
	//	Used to set the amount of clean space between the text and the window border
	public Rect babaWindowRect;
	//	The size and placement of the window for the scrolling text
	public Rect karoliWindowRect;
	public Rect loremWindowRect;
	public Rect rolandWindowRect;
	public Rect buttonsWindowRect;

	private Rect createdWindow;
	//	The variable that contains the window that will be created
	private Rect windowRect;
	//	A variable container for the current window size
	private string windowStyle;
	//	A variable container for the current window style

	private bool audioOff;
	//	Irrelevant Booleans for the button test
	private bool musicOff;
	private bool thrustReversed;
	private bool turnReversed;
	private bool resetHSCheck;
	private bool hasResetHS;
	private bool hideInfo;
	private new Vector2 newVectorPosition;

	private StartMenuLevel menuLevel;
	//	For the Switch/Case used to define the layouts

	//	STANDARD FUNCTIONS
	void  Awake ()
	{
		//	iPhoneSettings.screenCanDarken = true;
		//	TouchScreenKeyboard.autorotateToPortrait = false;
		//	TouchScreenKeyboard.autorotateToPortraitUpsideDown = false;
		//	TouchScreenKeyboard.autorotateToLandscapeRight = false;
		//	TouchScreenKeyboard.autorotateToLandscapeLeft = false;

		menuLevel = StartMenuLevel.Start;

		deltaY = 0.0f;
		hazSkrollin = false;
	}


	void  Start ()
	{						//	This is where the text assets are converted into strings to scroll
		introText = introTextAsset.text;
		babaText = babaTextAsset.text;
		karoliText = karoliTextAsset.text;
		loremText = loremTextAsset.text;
		rolandText = rolandTextAsset.text;
	}


	void  OnGUI ()
	{
		//	OnGUI ()...
		//	I have separated OnGUI with a Switch/Case for each possible state.
		//	Follow the thread of the first display state "Baba" for more information

		if (gSkin)
			GUI.skin = gSkin;
		else
			Debug.Log ("StartMenuGUI: GUI Skin object is missing!");

		GUI.DrawTexture (new Rect (0, 0, 320, 480), startBG);
		//	GUI.Label ( new Rect(0, -3, 330, 490), startBG);							//	The background image

		switch (menuLevel) {												//	The Switch/Case architecture
		case StartMenuLevel.Start:

			GUI.Label (new Rect ((Screen.width * 0.05f), 60, 400, 100), "IsSkrollin??", "Title");
			GUI.Label (new Rect ((Screen.width * 0.10f), 80, 400, 100), "An example of iPhone Scrolling", "SubTitle");
			GUI.Label (new Rect (160, 295, 150, 150), introText);

			if (GUI.Button (new Rect (10, 175, 100, 35), "Baba Yaga")) {	//	Each button assigns the basic information needed for the next case
				hazSkrollin = true;
				scrollMax = scrollMaxBaba;
				scrollVector = scrollMaxBaba;
				scrollMin = scrollMinBaba;
				windowText = babaText;
				windowRect = babaWindowRect;
				windowStyle = "babatext";
				menuLevel = StartMenuLevel.Baba;
			}

			if (GUI.Button (new Rect (10, 230, 100, 35), "Karoli Magni")) {
				hazSkrollin = true;
				scrollMax = scrollMaxKaroli;
				scrollVector = scrollMaxKaroli;
				scrollMin = scrollMinKaroli;
				windowText = karoliText;
				windowRect = karoliWindowRect;
				windowStyle = "karolitext";
				menuLevel = StartMenuLevel.Karoli;
			}

			if (GUI.Button (new Rect (10, 285, 100, 35), "Lorem Ipsum")) {
				hazSkrollin = true;
				scrollMax = scrollMaxLorem;
				scrollVector = scrollMaxLorem;
				scrollMin = scrollMinLorem;
				windowText = loremText;
				windowRect = loremWindowRect;
				windowStyle = "loremtext";
				menuLevel = StartMenuLevel.Lorem;
			}

			if (GUI.Button (new Rect (10, 340, 100, 35), "Chanson\nde Roland")) {
				hazSkrollin = true;
				scrollMax = scrollMaxRoland;
				scrollVector = scrollMaxRoland;
				scrollMin = scrollMinRoland;
				windowText = rolandText;
				windowRect = rolandWindowRect;
				windowStyle = "rolandtext";
				menuLevel = StartMenuLevel.Roland;
			}

			if (GUI.Button (new Rect (10, 395, 100, 35), "Buttons")) {
				hazSkrollin = true;
				scrollMax = scrollMaxButtons;
				scrollVector = scrollMaxButtons;
				scrollMin = scrollMinButtons;
				//				windowText = someText;				//	Text value is not needed in this layout/view.
				windowRect = buttonsWindowRect;
				windowStyle = "buttonstext";
				menuLevel = StartMenuLevel.Buttons;
			}

			break;

		case StartMenuLevel.Baba:

			GUIStyle buttonStyle = new GUIStyle ();						//	This sets up the containers for scroll vector icon

			createdWindow = GUI.Window (0, windowRect, CreateWindow, "");		//	This creates a new window based on the parameters set when entering this case


			GUI.Label (new Rect ((Screen.width / 2 - 100), 0, 200, 100), "Baba Yaga", "babatitle");

			if (GUI.Button (new Rect (((Screen.width * 0.5f) - 70), 400, 140, 70), "Back", "bababutton")) {
				hazSkrollin = false;
				menuLevel = StartMenuLevel.Start;
			}

			if (izSkrollin) {													//	If view is actively scrolling, display the scroll vector icon
				buttonStyle.normal.background = iconVector;
				//	Establish where iconVector should be based on the scrolling of the window
				newVectorPosition = new Vector2 ((windowRect.x + windowRect.width) - 13, windowRect.y - (scrollVector / (scrollMin / (windowRect.height - 25))));
				//	Because the code will allow this code will allow the text to go beyond the margin and then bounce in an Appe™ way, iconVector can leave the window
				//		if it is not clamped into the size of the window.
				//	Test and clamp iconVector is it tries to go outside of the window during the margin bounce.
				if (newVectorPosition.y < scrollMax + windowRect.y)
					newVectorPosition.y = scrollMax + windowRect.y;
				if (newVectorPosition.y > windowRect.y + windowRect.height - 25)
					newVectorPosition.y = windowRect.y + windowRect.height - 25;
				//	Set the location of iconVector.
				GUI.Button (new Rect (newVectorPosition.x, newVectorPosition.y, 12, 25), "", buttonStyle);

				/*
					The only point to note here are the hard values of 12, 25. These are appropriate for this particular graphic and a different craphic element will
						probably need different values. I have not genericized these values. Please note the " - 13" value in the x placement. This is bringing the
						graphic back into the window (set by windowRect.x + windowRect.width) by it's own x value PLUS one pixel. Also note the " - 25" value in the
						placement. This is to take account for the height of the graphic element and makes sure that the end location of iconVector is within the window.
						all of this code is drawing a rect from (x,y). Without the " - 25" value, (x,y) would be at the end position and the additional area of the rect
						(x.width and y.height) would extend the graphic beyond the window.
				*/

				//	This code is the clean line of code without the clamping.
				//				GUI.Button ( new Rect((windowRect.x + windowRect.width) - 13, windowRect.y - (scrollVector/(scrollMin/(windowRect.height - 25))), 12, 25), "", buttonStyle);
			}

			break;

		case StartMenuLevel.Karoli:

			buttonStyle = new GUIStyle ();

			createdWindow = GUI.Window (0, windowRect, CreateWindow, "");


			GUI.Label (new Rect ((Screen.width / 2 - 150), 0, 300, 100), "Karoli Magni", "karolititle");
			GUI.DrawTexture (new Rect (175, 110, 200, 275), charlemagne);

			if (GUI.Button (new Rect (((Screen.width / 2) - 70), 400, 140, 70), "Back", "karolibutton")) {
				hazSkrollin = false;
				menuLevel = StartMenuLevel.Start;
			}

			if (izSkrollin) {	
				buttonStyle.normal.background = iconVector;
				newVectorPosition = new  Vector2 ((windowRect.x + windowRect.width) - 13, windowRect.y - (scrollVector / (scrollMin / (windowRect.height - 25))));
				if (newVectorPosition.y < scrollMax + windowRect.y)
					newVectorPosition.y = scrollMax + windowRect.y;
				if (newVectorPosition.y > windowRect.y + windowRect.height - 25)
					newVectorPosition.y = windowRect.y + windowRect.height - 25;
				GUI.Button (new Rect (newVectorPosition.x, newVectorPosition.y, 12, 25), "", buttonStyle);
			}

			break;

		case StartMenuLevel.Lorem:

			buttonStyle = new GUIStyle ();

			createdWindow = GUI.Window (0, windowRect, CreateWindow, "");


			GUI.Label (new Rect ((Screen.width / 2 - 100), 0, 200, 100), "Lorem Ipsum", "loremtitle");
			GUI.DrawTexture (new Rect (10, 315, 300, 75), map);

			if (GUI.Button (new Rect (((Screen.width * 0.5f) - 70), 400, 140, 70), "Back", "lorembutton")) {
				hazSkrollin = false;
				menuLevel = StartMenuLevel.Start;
			}

			if (izSkrollin) {	
				buttonStyle.normal.background = iconVector;
				newVectorPosition = new  Vector2 ((windowRect.x + windowRect.width) - 13, windowRect.y - (scrollVector / (scrollMin / (windowRect.height - 25))));
				if (newVectorPosition.y < scrollMax + windowRect.y)
					newVectorPosition.y = scrollMax + windowRect.y;
				if (newVectorPosition.y > windowRect.y + windowRect.height - 25)
					newVectorPosition.y = windowRect.y + windowRect.height - 25;
				GUI.Button (new Rect (newVectorPosition.x, newVectorPosition.y, 12, 25), "", buttonStyle);
			}

			break;

		case StartMenuLevel.Roland:

			buttonStyle = new GUIStyle ();

			createdWindow = GUI.Window (0, windowRect, CreateWindow, "");


			GUI.Label (new Rect ((Screen.width / 2 - 100), 0, 200, 100), "La Chanson\nde Roland", "rolandtitle");
			GUI.DrawTexture (new Rect (10, 95, 300, 90), page);

			if (GUI.Button (new Rect (((Screen.width * 0.5f) - 70), 400, 140, 70), "Back", "rolandbutton")) {
				hazSkrollin = false;
				menuLevel = StartMenuLevel.Start;
			}

			if (izSkrollin) {	
				buttonStyle.normal.background = iconVector;
				newVectorPosition = new Vector2 ((windowRect.x + windowRect.width) - 13, windowRect.y - (scrollVector / (scrollMin / (windowRect.height - 25))));
				if (newVectorPosition.y < scrollMax + windowRect.y)
					newVectorPosition.y = scrollMax + windowRect.y;
				if (newVectorPosition.y > windowRect.y + windowRect.height - 25)
					newVectorPosition.y = windowRect.y + windowRect.height - 25;
				GUI.Button (new Rect (newVectorPosition.x, newVectorPosition.y, 12, 25), "", buttonStyle);
			}

			break;

		case StartMenuLevel.Buttons:

			buttonStyle = new GUIStyle ();

			GUI.Label (new Rect ((Screen.width / 2 - 100), 25, 200, 100), "Buttons", "buttonstitle");

			createdWindow = GUI.Window (0, windowRect, CreateGroupWindow, "");

			if (GUI.Button (new Rect (((Screen.width * 0.5f) - 70), (Screen.height - 80), 140, 70), "Back", "buttonsbutton")) {
				hazSkrollin = false;
				hasResetHS = false;
				resetHSCheck = false;
				menuLevel = StartMenuLevel.Start;
			}

			if (izSkrollin) {	
				buttonStyle.normal.background = iconVector;
				newVectorPosition = new Vector2 ((windowRect.x + windowRect.width) - 13, windowRect.y - (scrollVector / (scrollMin / (windowRect.height - 25))));
				if (newVectorPosition.y < scrollMax + windowRect.y)
					newVectorPosition.y = scrollMax + windowRect.y;
				if (newVectorPosition.y > windowRect.y + windowRect.height - 25)
					newVectorPosition.y = windowRect.y + windowRect.height - 25;
				GUI.Button (new Rect (newVectorPosition.x, newVectorPosition.y, 12, 25), "", buttonStyle);
			}

			break;
		}
	}


	void  CreateWindow (int windowID)
	{
		//	A generic function called by OnGUI() to make the current window
		//	Note that the label with the text is slighly smaller than the window by using var windowPad
		//	The scrolling text is defined by pad, the current scrollVector, width and pad, and the length.
		//	As this is built every frame, the y value needs to be the current scrollVector value
		GUI.Label (new Rect (windowPad, scrollVector, (windowRect.width - (windowPad * 2)), scrollMin), windowText, windowStyle);
	}

	//	A generic function called by OnGUI to make the current window if a group of buttons is needed
	void  CreateGroupWindow (int windowID)
	{
		GUI.BeginGroup (new Rect (windowPad, scrollVector, (windowRect.width - (windowPad * 2)), (windowRect.height + scrollMin))); //, windowStyle);

		GUIStyle buttonStyle = new GUIStyle ();

		if (!audioOff) {
			buttonStyle.normal.background = iconAudio;
			GUI.Label (new Rect (55, 15, 250, 50), "Audio Enabled", "buttonstext");
			if (GUI.Button (new Rect (0, 20, 50, 50), "", buttonStyle)) {
				audioOff = true;
				PlayerPrefs.SetString ("AudioOff", "Yes");
			}
		}

		if (audioOff) {
			buttonStyle.normal.background = iconAudioNo;
			GUI.Label (new Rect (55, 15, 250, 50), "Audio Disabled", "buttonstext");
			if (GUI.Button (new Rect (0, 20, 50, 50), "", buttonStyle)) {
				audioOff = false;
				PlayerPrefs.SetString ("AudioOff", "No");
			}
		}

		if (!musicOff) {
			buttonStyle.normal.background = iconMusic;
			GUI.Label (new Rect (55, 70, 250, 50), "Music Enabled", "buttonstext");
			if (GUI.Button (new Rect (0, 75, 50, 50), "", buttonStyle)) {
				musicOff = true;
				PlayerPrefs.SetString ("MusicOff", "Yes");
			}
		}

		if (musicOff) {
			buttonStyle.normal.background = iconMusicNo;
			GUI.Label (new Rect (55, 70, 250, 50), "Music Disabled", "buttonstext");
			if (GUI.Button (new Rect (0, 75, 50, 50), "", buttonStyle)) {
				musicOff = false;
				PlayerPrefs.SetString ("MusicOff", "No");
			}
		}

		if (!hideInfo) {
			buttonStyle.normal.background = iconInfo;
			GUI.Label (new Rect (55, 130, 250, 50), "Hints On", "buttonstext");
			if (GUI.Button (new Rect (0, 135, 50, 50), "", buttonStyle)) {
				hideInfo = true;
				PlayerPrefs.SetString ("HideInfo", "Yes");
			}
		}

		if (hideInfo) {
			buttonStyle.normal.background = iconInfoNo;
			GUI.Label (new Rect (55, 130, 250, 50), "Hints Off", "buttonstext");
			if (GUI.Button (new Rect (0, 135, 50, 50), "", buttonStyle)) {
				hideInfo = false;
				PlayerPrefs.SetString ("HideInfo", "No");
			}
		}

		if (!thrustReversed) {
			buttonStyle.normal.background = iconThrustNormal;
			GUI.Label (new Rect (55, 185, 250, 50), "Thrust Normal", "buttonstext");
			if (GUI.Button (new Rect (0, 190, 50, 50), "", buttonStyle)) {
				thrustReversed = true;
				PlayerPrefs.SetString ("ThrustReversed", "Yes");
			}
		}

		if (thrustReversed) {
			buttonStyle.normal.background = iconThrustReversed;
			GUI.Label (new Rect (55, 185, 250, 50), "Thrust Reversed", "buttonstext");
			if (GUI.Button (new Rect (0, 190, 50, 50), "", buttonStyle)) {
				thrustReversed = false;
				PlayerPrefs.SetString ("ThrustReversed", "No");
			}
		}

		if (!turnReversed) {
			buttonStyle.normal.background = iconTurnNormal;
			GUI.Label (new Rect (55, 240, 250, 50), "Turn Normal", "buttonstext");
			if (GUI.Button (new Rect (0, 245, 50, 50), "", buttonStyle)) {
				turnReversed = true;
				PlayerPrefs.SetString ("TurnReversed", "Yes");
			}
		}

		if (turnReversed) {
			buttonStyle.normal.background = iconTurnReversed;
			GUI.Label (new Rect (55, 240, 250, 50), "Turn Reversed", "buttonstext");
			if (GUI.Button (new Rect (0, 245, 50, 50), "", buttonStyle)) {
				turnReversed = false;
				PlayerPrefs.SetString ("TurnReversed", "No");
			}
		}


		if (!hasResetHS) {
			if (!resetHSCheck) {
				buttonStyle.normal.background = iconResetHS;
				GUI.Label (new Rect (55, 295, 250, 50), "Reset High Scores", "buttonstext");
				if (GUI.Button (new Rect (0, 300, 50, 50), "", buttonStyle)) {
					resetHSCheck = true;
				}
			}

			if (resetHSCheck) {
				buttonStyle.normal.background = iconResetHSWarn;
				GUI.Label (new Rect (0, 296, 265, 50), "Reset\n High Scores?", "buttonstextRED");
				if (GUI.Button (new Rect (0, 300, 50, 50), "", buttonStyle)) {
					resetHSCheck = false;
				}

				buttonStyle.normal.background = iconResetHSDoneWarn;
				if (GUI.Button (new Rect (220, 300, 50, 50), "", buttonStyle)) {
					resetHSCheck = false;
					hasResetHS = true;
				}
			}
		}

		if (hasResetHS) {
			buttonStyle.normal.background = iconResetHSDone;
			GUI.Label (new Rect (55, 295, 250, 50), "High Scores Reset", "buttonstext");
			if (GUI.Button (new Rect (0, 300, 50, 50), "", buttonStyle)) {
				resetHSCheck = true;
			}
		}

		GUI.EndGroup ();
	}


	void  Update ()
	{
		//	function Update only does somethign if the view has the need to scroll (defined by the bool  "hazSkrollin")
		if (hazSkrollin) {
			//	Save the last scroll vector value before we start to apply new calculations to it.
			float savedScrollVector = scrollVector;

			//	Calculate Resistance & Margin Pressure
			//	Start by setting them to a neutral value
			resistance = 1.0f;
			marginPressure = 0.0f;

			//	Then check to see if the current scroll vector is out of bounds
			//	This is defined by looking at scrollMax and scrollMin
			//	If we are trying to scroll out of the boundaries, then calculate the resistance and margin pressure
			if (scrollVector >= scrollMax) {
				resistance = Mathf.Lerp (1.0f, 0.0f, Mathf.Abs ((Mathf.Abs (scrollVector) - scrollMax) / scrollMargin));
				marginPressure = -((1.0f - resistance) * marginSpringiness * Time.deltaTime);
			}

			if (scrollVector <= -scrollMin) {
				resistance = Mathf.Lerp (1.0f, 0.0f, Mathf.Abs ((Mathf.Abs (scrollVector) - scrollMin) / scrollMargin));
				marginPressure = (1.0f - resistance) * marginSpringiness * Time.deltaTime;
			}

			//	Now check for touches...
			if (Input.touchCount > 0) {							//	If there are touches...
				Touch touch = Input.GetTouch (0);		//	Cache Touch (0)
				if (touch.phase == TouchPhase.Moved) { 			//	If Touch (0) has moved...

					//	Get movement of the finger since last frame
					float touchPositionDeltaY = touch.deltaPosition.y;

					//	Modify the movement with a variable to adjust behaviour
					deltaY = touchPositionDeltaY * touchSpeed * resistance;

					//	Move object across Y axis, but regardless of resistance and pressure, Clamp it anyway within the margins
					scrollVector = Mathf.Clamp ((scrollVector + deltaY), -(scrollMin + scrollMargin), scrollMax + scrollMargin);

					//	Save a start time incase the next frame has no touch and we need to start coasting
					startTime = Time.time;
				}
			}

			if (Input.touchCount < 1) {							//	If there are NO touches...

				//	Take the existing scroll speed (deltaY) from the last frame and Lerp it with the time since the last touch
				deltaY = Mathf.Lerp (deltaY, 0.0f, (Time.time - startTime) * coastSpeed) * resistance;

				//	Move object across Y axis, but regardless of resistance and pressure, Clamp it anyway within the margins
				scrollVector = Mathf.Clamp ((scrollVector + deltaY), -(scrollMin + scrollMargin), scrollMax + scrollMargin) + marginPressure;
			}

			//	Test and set whether we are skrollin' or not. If there is a touch or we are moving more than 0.01f, we are skrolling; or else we're not.
			if ((Input.touchCount > 0) || (Mathf.Abs (savedScrollVector - scrollVector) > 0.01f))
				izSkrollin = true;
			else
				izSkrollin = false;
		}
	}

	// © 2010 Adam Buckner ( Little aka   Angel) and theantranch.com (mailto: adam@theantranch.com)
	// Not for reuse or resale without permission

}