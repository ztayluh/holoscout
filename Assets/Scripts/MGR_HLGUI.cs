﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// Required when Using UI elements.
//using UnityEditor.Animations;
//using UnityEditor;

public class MGR_HLGUI : MonoBehaviour
{


	public InputField Input_Coach;
	public InputField Input_Trainee;
	public InputField Input_Playlist;

	public string coach, trainee, playlist;
	public string i_coach, i_trainee, i_playlist;
	public string[] clipNames;
	public Dictionary< string, int> DictOfAnims;
	public MGR_Cloud myCloud;
	public MGR_Anim myAnim;
	public GameObject myGO;
	public Canvas myCanvas;

	public Animator myAnimator;

	public Vector2 coachScrollPosition;
	public Vector2 traineeScrollPosition;
	public Vector2 plScrollPosition;
	public Vector2 hashScrollPosition;

	public int count = 0;

	//----------------------
	//----Awake ------------
	//----------------------
	public void Awake ()
	{
		myAnim = myGO.GetComponent<MGR_Anim> ();
		myAnimator = myGO.GetComponent<Animator> ();
		DictOfAnims = new Dictionary<string, int> ();
//		readStates ();

		myCloud = myGO.GetComponent<MGR_Cloud> ();
		updatePL ();
		myCanvas = GetComponent<Canvas> ();

		helpME ("awake");


	}

	public void updatePL ()
	{
		
		StartCoroutine (myCloud.readHashDB ("pl"));
		helpME ("After readHashDB");
	}

	public void helpME (string s)
	{
		print (s + "--" + myCloud.myEntry.hSize); 
		for (int i = 0; i < myCloud.myEntry.hSize; i++) {
//			print (myCloud.myEntry.hashString [i].ToString ());
		}
	}

	//----------------------
	//----readStates--------
	//----------------------
	public void readStates ()
	{
		int i = 0;
		foreach (AnimationClip clip in myAnimator.runtimeAnimatorController.animationClips) {
			i++;
		}
		clipNames = new string[i];
		i = 0;
		foreach (AnimationClip clip in myAnimator.runtimeAnimatorController.animationClips) {
			clipNames [i] = clip.name.ToString ();
			if (!DictOfAnims.ContainsKey (clipNames [i])) {
				print (clip.GetHashCode () + " " + clipNames [i].ToString ());
				DictOfAnims.Add (clipNames [i], clip.GetHashCode ());
			}
			i++;
		}
	}

	//----------------------
	//----addDBEntry--------
	//----------------------
	public void addDBEntry ()
	{

		i_coach = Input_Coach.text;
		i_trainee = Input_Trainee.text;
		i_playlist = Input_Playlist.text;

//		myCloud.addPL (i_coach, i_trainee, i_playlist);
		//var input = myCanvas.GetComponent<InputField>();
		//var se= new InputField.SubmitEvent();
		//se.AddListener(SubmitName);
		//input.onEndEdit = se;
	}

	public void buttonPressed ()
	{
		print ("Button Pressed" + myCloud.myEntry.hSize);
		helpME ("buttonPressed");
		myAnim.playAnims ();
	
	}

	//----------------------
	//----onGUI ------------
	//----------------------
	public void OnGUI ()
	{

	}
	
	
}




















//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine.UI;
//
//// Required when Using UI elements.
////using UnityEditor.Animations;
//
//public class MGR_HLGUI : MonoBehaviour
//{
//	public string coach, trainee, playlist;
//	public string[] clipNames;
//	public Dictionary< string, int> DictOfAnims;
//	public MGR_Cloud myCloud;
//	public MGR_Anim myAnim;
//	public GameObject myGO;
//	public Canvas myCanvas;
//
//	//public Animator myAnimator;
//
//	public Vector2 coachScrollPosition;
//	public Vector2 traineeScrollPosition;
//	public Vector2 plScrollPosition;
//	public Vector2 hashScrollPosition;
//
//	public int count = 0;
//
//	public Entry myEntry;
//
//
//
//	//----------------------
//	//----Awake ------------
//	//----------------------
//	public void Awake ()
//	{
//		myCloud = myGO.GetComponent<MGR_Cloud> ();
//		myAnim = myGO.GetComponent<MGR_Anim> ();
//
//
//	}
//
//	public void setEntry ()
//	{
//		myEntry.coach = "YOKARA";
//		myEntry.trainee = "THISWORKS";
//		myEntry.playlistName = "FUUUUUHHH";
//		myEntry.hSize = myCloud.myEntry.hSize;
//
//		myAnim.setEntry (myEntry, myAnim.myAnimator, myEntry.hSize);
//
//	}
//
//	public void buttonPressed ()
//	{
//		print ("Button Pressed" + myCloud.myEntry.hSize);
//		myAnim.playAnims ();
//
//	}
//
//	//----------------------
//	//----onGUI ------------
//	//----------------------
//	public void OnGUI ()
//	{
//
//		if (count < 1) {
//			myCloud.StartCoroutine (myCloud.readHashDB (playlist));
//			print (myCloud.myEntry.hSize);
//			count++;
//		}
//		count++;
//	}
//}
