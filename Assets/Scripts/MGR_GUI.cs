﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// Required when Using UI elements.
//using UnityEditor.Animations;

public class MGR_GUI : MonoBehaviour
{

	//	bool showmenu = true;
	bool coachSelected = false;
	bool traineeSelected = false;
	bool playlistSelected = false;

	public InputField Input_Coach;
	public InputField Input_Trainee;
	public InputField Input_Playlist;

	public string coach, trainee, playlist;
	public string i_coach, i_trainee, i_playlist;
	public string[] clipNames;

	public string hlist;

	public Dictionary< string, long> DictOfAnims;
	public Dictionary<long, string> AnimsOfDick;
	public MGR_Cloud myCloud;
	public GameObject myGO;
	public Canvas myCanvas;
   
	public Animator myAnimator;

	public Vector2 coachScrollPosition;
	public Vector2 traineeScrollPosition;
	public Vector2 plScrollPosition;
	public Vector2 hashScrollPosition;





	public int count = 0;

	//----------------------
	//----Awake ------------
	//----------------------
	public void Awake ()
	{
		myAnimator = myGO.GetComponent<Animator> ();
		AnimsOfDick = new Dictionary<long, string> ();
		DictOfAnims = new Dictionary<string, long> ();
		readStates ();
		myCloud = myGO.GetComponent<MGR_Cloud> ();
		myCanvas = GetComponent<Canvas> ();

	}




	//----------------------
	//----readStates--------
	//----------------------
	public void readStates ()
	{
		int i = 0;
		foreach (AnimationClip clip in myAnimator.runtimeAnimatorController.animationClips) {
			i++;
		}
		clipNames = new string[i];
		i = 0;
		foreach (AnimationClip clip in myAnimator.runtimeAnimatorController.animationClips) {
			clipNames [i] = clip.name.ToString ();
			if (!DictOfAnims.ContainsKey (clipNames [i])) {
				print (clip.GetHashCode () + " " + clipNames [i].ToString ());
				AnimsOfDick.Add (i, clipNames [i]);
				DictOfAnims.Add (clipNames [i], i);
			}
			i++;
		}
	}

	//----------------------
	//----addDBEntry--------
	//----------------------
	public void addDBEntry ()
	{

		i_coach = Input_Coach.text;
		i_trainee = Input_Trainee.text;
		i_playlist = Input_Playlist.text;

        if(i_coach != null && i_trainee != null && i_playlist != null)
        {
            myCloud.addPL(i_coach, i_trainee, i_playlist, hlist);
        }
        else
        {
            print("One of the fields has not been filled, please try again.");
        }

		//var input = myCanvas.GetComponent<InputField>();
		//var se= new InputField.SubmitEvent();
		//se.AddListener(SubmitName);
		//input.onEndEdit = se;
	}

	public void PlayAnims ()
	{
		
	}

	//----------------------
	//----onGUI ------------
	//----------------------
	public void OnGUI ()
	{
		GUI.skin.button.fontSize = 24;
		//if (showmenu) { 
		if (count < 1) {
			StartCoroutine (myCloud.readCoachDB ());
			count++;
		}
		GUILayout.BeginArea (new Rect (10, (Screen.width / 15) + 10, Screen.width, Screen.height));
		GUILayout.BeginHorizontal ();
		GUILayout.BeginVertical ();
		//Add coach.
		coachScrollPosition = GUILayout.BeginScrollView (coachScrollPosition, GUILayout.Width ((Screen.width / 4) - 20), GUILayout.Height (Screen.height * .85f));
		if (GUILayout.Button ("+", GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 4.6f))) {

		}
		for (int i = 0; i < myCloud.myEntry.cSize; i++) {
			if (GUILayout.Button (myCloud.myEntry.coaches [i], GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 4.6f))) {
				//Select Trainee.. then
				coachSelected = true;
				traineeSelected = false;
				playlistSelected = false;
				coach = myCloud.myEntry.coaches [i];
				StartCoroutine (myCloud.readTraineeDB (myCloud.myEntry.coaches [i]));
			}
		}
		GUILayout.EndScrollView ();
		//Start new column in GUI
		GUILayout.EndVertical ();
		GUILayout.BeginVertical ();
		if (coachSelected) {


			traineeScrollPosition = GUILayout.BeginScrollView (traineeScrollPosition, GUILayout.Width ((Screen.width / 4) - 20), GUILayout.Height (Screen.height * .85f));
			//Populate a list of the coaches' trainees
			//Add trainee.
			if (GUILayout.Button ("+", GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 4.6f))) {

			}
			for (int i = 0; i < myCloud.myEntry.tSize; i++) {
				if (GUILayout.Button (myCloud.myEntry.trainees [i], GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 4.6f))) {
					//Select Playlist item
					traineeSelected = true;
					playlistSelected = false;
					trainee = myCloud.myEntry.trainees [i];
					Debug.Log ("Hello, im " + myCloud.myEntry.trainees [i]);
					StartCoroutine (myCloud.readPLDB (myCloud.myEntry.trainees [i]));
				}
			}
			GUILayout.EndScrollView ();
		}
		//Start new column in GUI
		GUILayout.EndVertical ();
		GUILayout.BeginVertical ();
		if (traineeSelected) {

			plScrollPosition = GUILayout.BeginScrollView (plScrollPosition, GUILayout.Width ((Screen.width / 4) - 20), GUILayout.Height (Screen.height * .85f));
			//Add playlist.
			if (GUILayout.Button ("+", GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 4.6f))) {

			}
			for (int i = 0; i < myCloud.myEntry.pSize; i++) {
				if (GUILayout.Button (myCloud.myEntry.currentPlaylist [i], GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 4.6f))) {
					//Select Playlist item
					playlistSelected = true;
					playlist = myCloud.myEntry.currentPlaylist [i];
					Debug.Log ("Playlist: " + myCloud.myEntry.currentPlaylist [i]);
					StartCoroutine (myCloud.readHashDB (myCloud.myEntry.currentPlaylist [i]));

				}
			}
			GUILayout.EndScrollView ();
		}
		//Start new column in GUI
		GUILayout.EndVertical ();
		GUILayout.BeginVertical ();
		if (playlistSelected) {

			hashScrollPosition = GUILayout.BeginScrollView (hashScrollPosition, GUILayout.Width (Screen.width / 4), GUILayout.Height (Screen.height * .85f));
            //Add hashString.
			if (GUILayout.Button ("+", GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 4.6f))) {
                
			}
			for (int i = 0; i < myCloud.myEntry.hSize; i++) {
				print (i + "hSize" + myCloud.myEntry.hSize);

				string AOD;
				AnimsOfDick.TryGetValue (myCloud.myEntry.hashString [i], out AOD);
				print (AOD + " -- " + AnimsOfDick [myCloud.myEntry.hashString [i]]);

				if (GUILayout.Button (AOD, GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 4.6f))) {
					//Select Playlist item
					playlistSelected = true;
					playlist = myCloud.myEntry.hashString [i].ToString ();
					Debug.Log ("Playlist: " + myCloud.myEntry.hashString [i].ToString ());
				}
			}
			GUILayout.EndScrollView ();

		}
		GUILayout.EndVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.EndArea ();
		//}
	}
}
