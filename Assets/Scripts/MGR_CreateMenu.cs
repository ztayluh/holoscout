﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Diagnostics;
using UnityEngine.UI;

public class MGR_CreateMenu : MonoBehaviour
{
	public string[] clipNames;
	public GameObject myGO;

	public Animator myAnimator;
	public List<string> playlistItems;
	public MGR_Cloud myCloud;
	public MGR_GUI myGUI;

	public string coach;
	public string trainee;
	public string pl;
	public string hs = "";

	public InputField coachIF;
	public InputField traineeIF;
	public InputField plIF;
	public int currPLItem = 0;


	public Vector2 coachScrollPosition;
	public Vector2 coachScrollPosition2;


	// Use this for initialization
	void Start ()
	{
		playlistItems = new List<string> ();
		print ("test1");
		myAnimator = myGO.GetComponent<Animator> ();
		print ("test2");
		myCloud = myGO.GetComponent<MGR_Cloud> ();
		readStates ();

//		coachIF = gameObject.GetComponent<InputField> ();
//		var se = new InputField.SubmitEvent ();
//
//		coachIF.onEndEdit.AddListener (coach);
//		traineeIF.onEndEdit.AddListener (trainee);
//		plIF.onEndEdit.AddListener (pl);

	}

	void  Update ()
	{

	}

	public void displayWorkingPL ()
	{

		
	}

	public void readStates ()
	{
		clipNames = new string[myAnimator.runtimeAnimatorController.animationClips.Length];
		int i = 0;
		foreach (AnimationClip clip in myAnimator.runtimeAnimatorController.animationClips) {
			clipNames [i] = clip.name.ToString ();
			print (clip.name); 
			i++;
		}
	}

	public void OnGUI ()
	{ 		
		GUI.skin.button.fontSize = 24;
		GUILayout.BeginArea (new Rect (10, (Screen.width / 15) + 10, Screen.width / 3, (Screen.height - Screen.width / 14)));
		coachScrollPosition = GUILayout.BeginScrollView (coachScrollPosition, GUILayout.Width (Screen.width / 3), GUILayout.Height (Screen.height * .85f));

		for (int i = 0; i < clipNames.Length; i++) {
			if (GUILayout.Button (clipNames [i], GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 3.4f))) {
				
				//--Add to playlist
				playlistItems.Add (clipNames [i]);
				string.Concat (hs, clipNames [i] + "\n");
			}
		}
		GUILayout.EndScrollView ();
		GUILayout.EndArea ();

		GUILayout.BeginArea (new Rect ((Screen.width / 15) + (Screen.width / 3.4f) + 100, (Screen.width / 15) + 10, Screen.width / 3, (Screen.height - Screen.width / 14)));
		coachScrollPosition2 = GUILayout.BeginScrollView (coachScrollPosition2, GUILayout.Width (Screen.width / 3), GUILayout.Height (Screen.height * .85f));

		for (int i = 0; i < playlistItems.Count; i++) {
			if (GUILayout.Button (playlistItems [i], GUILayout.Height (Screen.height / 10), GUILayout.Width (Screen.width / 3.4f))) {

				//--Remove from playlist
				playlistItems.Remove (playlistItems [i]);
			}
		}
		GUILayout.EndScrollView ();
		GUILayout.EndArea ();
	}


	//----------------------
	//----Update------------
	//----------------------
	public void playAnims ()
	{
//		print ("PlayAnims ---- " + myCloud.myEntry.hSize); 

		StartCoroutine (Play ());
	}

	//----------------------
	//----playAnim----------
	//----------------------
	public IEnumerator Play ()
	{
		AnimatorStateInfo currInfo = myAnimator.GetCurrentAnimatorStateInfo (0);
//		print (currInfo);
//		print ("Play ---- " + myCloud.myEntry.hSize); 

		for (int i = 1; i < playlistItems.Count; i++) {
//			print (currPLItem);

			if (currPLItem >= playlistItems.Count || currPLItem < 0) {
				currPLItem = 0;
			}
				
			float time = currInfo.length;

			myAnimator.CrossFade (playlistItems [i], 1f, 0);

			yield return new WaitForSeconds (time - 1f);
			currPLItem++;
		}
	}

	public void resetPL ()
	{
		playlistItems.Clear ();
	}

	public void submitPL ()
	{
		hs = "";

		for (int i = 0; i < playlistItems.Count; i++) {
			long animHS;
			myGUI.DictOfAnims.TryGetValue (playlistItems [i], out animHS);

			if (i < playlistItems.Count - 1) {
				hs = string.Concat (hs, animHS, ",");
			} else {
				hs = string.Concat (hs, animHS);
			}
		}

		coach = coachIF.text;
		trainee = traineeIF.text;
		pl = plIF.text;

        //Add to anims. 

		myCloud.addPL (coach, trainee, pl, hs);
	}
}
