using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MGR_HoloText : MonoBehaviour
{

	public TextMesh playlistText;
	public MGR_Cloud myCloud;
	public string updateText;
	//----------------------
	//----Start-------------
	//----------------------
	public void Start ()
	{
		playlistText = GetComponent<TextMesh> ();
		myCloud = GetComponent<MGR_Cloud> ();
		updateText = "First Text";

		myCloud.initFB ("1", "2", "3");
//		myCloud.initFB("coachNotZach", "traineeKarrahhaha", "getFunkyWidIt");
		//myCloud.ReadPlaylistDB();
	}

	//----------------------
	//----On_GUI------------
	//----------------------
	public void OnGUI ()
	{

		updateText = myCloud.myEntry.coach;
		updateText = string.Concat (updateText, "\n" + myCloud.myEntry.trainee, "\n" + myCloud.myEntry.playlistName);
		foreach (var child in myCloud.myEntry.currentPlaylist) {
			updateText = string.Concat (updateText, "\n" + child);
		}
		playlistText.text = updateText;
	}

	public void Update ()
	{

	}



}
