﻿using UnityEngine;
using System.Collections;

public class SetAnimationInAnimator : MonoBehaviour {

    //public enum motion { idle=0, crouchReady, crouchSet, crouchRun, run };
    public Animator animator;
    public int currentAnimNum;


    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        currentAnimNum = 0;

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("space")) 
        {
            animator.SetInteger("currentAnim", currentAnimNum);
            currentAnimNum++;

            if (currentAnimNum >= 5)
            {
                currentAnimNum = 0;
            }
        }
    }
}
