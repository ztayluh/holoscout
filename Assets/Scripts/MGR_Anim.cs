﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MGR_Anim : MonoBehaviour
{
	public GameObject myGO;
	public Animator myAnimator;
	public Entry entry;
	public MGR_Cloud myCloud;
	public int currPLItem = 0;
	public long currHashID = 0;
	public List<long> myhashPL;


	//----------------------
	//----Start-------------
	//----------------------
	void Start ()
	{
		myCloud = myGO.GetComponent<MGR_Cloud> ();
		myAnimator = myGO.GetComponent<Animator> ();
		entry.playlists = new List<string> ();
		entry.currentPlaylist = new List<string> ();
		entry.trainees = new List<string> ();
		entry.coaches = new List<string> ();
		entry.hashString = new List<long> ();
		myAnimator = myGO.GetComponent<Animator> ();
		//Debug.Log(myAnimator);
	}

	//----------------------
	//----Set_Entry---------
	//----------------------
	public void setEntry (Entry e, Animator a, int numOfClips)
	{
		myAnimator = a;
		print ("NoC: " + numOfClips);
		entry.coach = e.coach;
		entry.trainee = e.trainee;
		myCloud.myEntry.hSize = numOfClips;

		myhashPL = new List<long> (numOfClips);
		for (int i = 0; i < numOfClips; i++) {
			entry.hashString.Add (e.hashString [i]);
			myhashPL.Add (e.hashString [i]);
		}
	}

	//----------------------
	//----Update------------
	//----------------------
	public void playAnims ()
	{
		print ("PlayAnims ---- " + myCloud.myEntry.hSize); 

		StartCoroutine (Play ());
	}

	public void previewAnims (Entry e)
	{
		StartCoroutine (Play (e));
	}

	//----------------------
	//----playAnim----------
	//----------------------
	public IEnumerator Play (Entry e)
	{
		AnimatorStateInfo currInfo = myAnimator.GetCurrentAnimatorStateInfo (0);

		for (int i = 1; i < e.hSize; i++) {
			if (currPLItem >= e.hSize || currPLItem < 0) {
				currPLItem = 0;
			}
			currHashID = e.hashString [currPLItem];

			float time = currInfo.length;

			print (currHashID);

			yield return new WaitForSeconds (time - 1f);
			currPLItem++;

		}
	}

	//----------------------
	//----playAnim----------
	//----------------------
	public IEnumerator Play ()
	{
		AnimatorStateInfo currInfo = myAnimator.GetCurrentAnimatorStateInfo (0);
		print (currInfo);
		print ("Play ---- " + myCloud.myEntry.hSize); 

		for (int i = 1; i < myCloud.myEntry.hSize; i++) {

			if (currPLItem >= myCloud.myEntry.hSize || currPLItem < 0) {
				currPLItem = 0;
			}

			currHashID = myCloud.myEntry.hashString [currPLItem];

			float time = currInfo.length;

			print (currHashID);

			yield return new WaitForSeconds (time - 1f);
			currPLItem++;

		}
	}

	//----------------------
	//----Update_Anim-------
	//----------------------
	public void UpdateAnim (string current, string next)
	{

	}


}
