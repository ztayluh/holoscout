﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//#if UNITY_EDITOR
//	using UnityEditor;
//#endif
using UnityEngine.SceneManagement;

public class MGR_Menus : MonoBehaviour
{

	public MenuStates currentstate;
	public Transform myCanvas;


	public GameObject startMenu;
	public GameObject createMenu;
	public GameObject readMenu;

	public GameObject player;

	public enum MenuStates
	{
		StartMenu,
		CreateMenu,
		ReadMenu}

	;

	// Use this for initialization
	void Start ()
	{
		player.GetComponent<Renderer> ().Equals (false);

		hideMenu (startMenu);
		hideMenu (createMenu);
		hideMenu (readMenu);

		currentstate = MenuStates.StartMenu;
	}

	public void hideMenu (GameObject G)
	{
		G.SetActive (false);

	}

	public void showMenu (GameObject G)
	{
		G.SetActive (true);

	}

	// Update is called once per frame
	void Update ()
	{
		switch (currentstate) {
		case MenuStates.StartMenu:
			showMenu (startMenu);
			hideMenu (createMenu);
			hideMenu (readMenu);
			break;
		case MenuStates.CreateMenu:
			showMenu (createMenu);
			hideMenu (startMenu);
			hideMenu (readMenu);
			break;
		case MenuStates.ReadMenu:
			showMenu (readMenu);
			hideMenu (createMenu);
			hideMenu (startMenu);
			break;
		}
	}

	public void StartMenu ()
	{
		currentstate = MenuStates.StartMenu;
	}

	public void CreateMenu ()
	{
		currentstate = MenuStates.CreateMenu;

	}

	public void ReadMenu ()
	{
		currentstate = MenuStates.ReadMenu;

	}


}
